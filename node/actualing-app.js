Stopwatch = function(listener, resolution) {
	this.startTime = 0;
	this.stopTime = 0;
	this.totalElapsed = 0; // * elapsed number of ms in total
	this.started = false;
	this.listener = (listener != undefined ? listener : null); // * function to receive onTick events
	this.tickResolution = (resolution != undefined ? resolution : 500); // * how long between each tick in milliseconds
	this.tickInterval = null;
	
	// * pretty static vars
	this.oneday = 1000 * 24 * 60 * 60;
	this.onehour = 1000 * 60 * 60;
	this.onemin  = 1000 * 60;
	this.onesec  = 1000;
}
Stopwatch.prototype.start = function() {
	var delegate = function(that, method) { return function() { return method.call(that) } };
	if(!this.started) {
		this.startTime = new Date().getTime();
		this.stopTime = 0;
		this.started = true;
		this.tickInterval = setInterval(delegate(this, this.onTick), this.tickResolution);
	}
}
Stopwatch.prototype.stop = function() {
	if(this.started) {
		this.stopTime = new Date().getTime();
		this.started = false;
		var elapsed = this.stopTime - this.startTime;
		this.totalElapsed += elapsed;
		if(this.tickInterval != null)
			clearInterval(this.tickInterval);
	}
	return this.getElapsed();
}
Stopwatch.prototype.reset = function() {
	this.totalElapsed = 0;
	// * if watch is running, reset it to current time
	this.startTime = new Date().getTime();
	this.stopTime = this.startTime;
}
Stopwatch.prototype.restart = function() {
	this.stop();
	this.reset();
	this.start();
}
Stopwatch.prototype.getElapsed = function() {
	// * if watch is stopped, use that date, else use now
	var elapsed = 0;
	if(this.started)
		elapsed = new Date().getTime() - this.startTime;
	elapsed += this.totalElapsed;
	
	var days = parseInt(elapsed / this.oneday);
	elapsed %= this.oneday;
	var hours = parseInt(elapsed / this.onehour);
	elapsed %= this.onehour;
	var mins = parseInt(elapsed / this.onemin);
	elapsed %= this.onemin;
	var secs = parseInt(elapsed / this.onesec);
	var ms = elapsed % this.onesec;
	
	return {
		days: days,
		hours: hours,
		minutes: mins,
		seconds: secs,
		milliseconds: ms
	};
}
Stopwatch.prototype.setElapsed = function(days, hours, mins, secs) {
	this.reset();
	this.totalElapsed = 0;
	this.totalElapsed += days * this.oneday;
	this.totalElapsed += hours * this.onehour;
	this.totalElapsed += mins  * this.onemin;
	this.totalElapsed += secs  * this.onesec;
	this.totalElapsed = Math.max(this.totalElapsed, 0); // * No negative numbers
}
Stopwatch.prototype.toString = function() {
	var zpad = function(no, digits) {
		no = no.toString();
		while(no.length < digits)
			no = '0' + no;
		return no;
	}
	var e = this.getElapsed();
	return zpad(e.days,2) + ":" + zpad(e.hours,2) + ":" + zpad(e.minutes,2) + ":" + zpad(e.seconds,2);
}
Stopwatch.prototype.setListener = function(listener) {
	this.listener = listener;
}
// * triggered every <resolution> ms
Stopwatch.prototype.onTick = function() {
	if(this.listener != null) {
		this.listener(this);
	}
}
// }}}

function toDDHHMMSS(seconds) {
    var days   = Math.floor(seconds / (24 * 3600));
    var hours   = Math.floor((seconds - (days * (24 * 3600))) / 3600);
    var minutes = Math.floor((seconds - (hours * 3600)) / 60);
    var seconds = seconds - (hours * 3600) - (minutes * 60);
	
    if (days    < 10) {days    = "0" + days}
    if (hours   < 10) {hours   = "0" + hours}
    if (minutes < 10) {minutes = "0" + minutes}
    if (seconds < 10) {seconds = "0" + seconds}
	
    return {days:days,hours:hours,minutes:minutes,seconds:seconds};
}

// ================================================================================================ //

var app = require('express')()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server)
  , port = 1421;

server.listen(port);

io.sockets.on('connection', function (socket) {
	var theTimer = new Stopwatch();
	var theListener = function (eventName) {
		theTimer.setListener(function(watch){
			io.emit(eventName, watch.toString());		
			console.log(watch.toString());
		});
	};
	
	socket.on('reset-timer', function(param){
		theTimer.stop();
		theTimer.reset();
		theTimer.onTick();
		theListener('reset-timer');
		console.log('reset-timer');
	});
	socket.on('start-timer', function(param){
		theTimer.start();
		
		if(!isNaN(param)) {
			var timeArray = toDDHHMMSS(param);
			theTimer.setElapsed(timeArray.days, timeArray.hours, timeArray.minutes, timeArray.seconds);
		}

		theListener('start-timer');
		console.log('start-timer');
	});
	socket.on('pause-timer', function(param){
		theTimer.stop();
		theListener('pause-timer');
		console.log('pause-timer');
	});
});

