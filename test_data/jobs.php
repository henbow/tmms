<?php
$jobs_data = array(
    1 => array(
        "id" => 1,
        "title" => "Brand Supervisor",
        "company" => "PT Glico Indonesia",
        "salary" => "9.000.000 IDR",
        "imageurl" => "http://www.jobnext.net/assets/img/service/emp_cli13.png",
        "tagline" => "Come and Join Us at Glico",
        "background" => "Be inspired by tastefulness! Provide delight through wholesomeness! Glory in the resplendence of life! At Glico, we are inspired to contribute to a tastefully wholesome delight in the resplendence of human life.",
        "job_description" => array( 
            "content" => "As Coordinator of the Sales Team in the area\nResponsible for the sales distribution and sales targets\nResponsible for the quality of the display merchandisers·\nNegotiate with customers<span\nConduct indoctrination, training and supervision / supervision to the sales team",
            "tags" => array("Supervisor", "Fulltime")
        ),
        "job_requirements" => array(
            "content" => "Female\nMarketing or merchandising Leader or Supervisor background\nMaximum age 30 years old\nPhysically and mentally healthy\nActive English language in written and spoken\nAttractive appearance character\nWill be placed at South Jakarta",
            "tags" => array("English", "Bahasa", "1Y Experience")
        )
    ),
    2 => array(
        "id" => 2,
        "title" => "Business Development Manager",
        "company" => "PT Fajar Mitra Indah",
        "salary" => "15.000.000 IDR",
        "imageurl" => "http://www.jobnext.net/uploads/user/2392/logo_qCVXhx.220x140.jpg",
        "tagline" => "Join us to grow our Retail store-chain business from Japan",
        "background" => "FamilyMart started the business in Indonesia, and area franchising contract has been made between FamilyMart Co.,Ltd. and PT. Fajar Mitra Indah, as FamilyMart Indonesia.FamilyMart Indonesia now has successfully opened 9 stores, in Cibubur, Bulungan, Pejaten, Kelapa Gading, Thamrin, Tanjung Duren, Panglima Polim, Jelambar & Ciledug and more stores to open.",
        "job_description" => array( 
            "content" => "- Ensure the available site or location store comply with the procedure and it is beneficial to the company\n- Conduct research-based site or location survey for new store opening\n- Ensure agreement of new store opening process comply with the regulation, procedure, and schedule\n- Coordinate building for new store opening with related parties\n- To keep in view and update competitors expansion\n- Leading, directing and controlling all business development activities to meet the organization’s short and long term business objectives\n",
            "tags" => array("Manager / Senior Manager", "Fulltime")
        ),
        "job_requirements" => array(
            "content" => "- Male, maximum 40 years old\n- Minimum Barchelor Degree from any field\n- Having minimum 5 years experience in same position\n- Able to work in team, good coordination to work and tight deadline\n- Willing to work with high mobility\n- Excellent skill in negotiation and communication\n- Having knowledge of Area Jakarta\n",
            "tags" => array("English", "Bahasa", "5Y Experience")
        )
    ),
    3 => array(
        "id" => 3,
        "title" => "MD E-Commerce Manager",
        "company" => "LINE Indonesia",
        "salary" => "10.000.000 - 20.000.000 IDR",
        "imageurl" => "http://www.jobnext.net/uploads/user/3624/job_XVq2Df.220x140.png",
        "tagline" => "MD E-Commerce Manager",
        "background" => "About LINE\nLINE, the world's hottest mobile messaging platform, offers free text and voice messaging, voice calling and group chat with up to 100 people, integrated with various interactive elements such as Stickers, Home &amp; Timeline and Official Accounts. Since its release in June 2011, the cross-platform mobile messenger has been releasing a series of add-on apps that range from games to an anti-virus app. Today, LINE has become a hot cultural icon with 350 million users in over 230 countries. LINE has also held the coveted No. 1 spot in the 'free apps' category of app stores in 60 countries. \n\nAbout LINE Indonesia \nEstablished in 2013, LINE Indonesia handles operational work and supports for valuable 20million LINE users in Indonesia. Its members are fully equipped with digital market insight, global standard of professionalism, and endless passion towards mobile industry.",
        "job_description" => array( 
            "content" => "The person in the position above will review the LINE products on E-Commerce and provide the feedback before the product launch in Indonesia market. Also identify, develop and execute new initiatives that leverage LINE and its users in a way of C2C &amp; B2C. Optimize LINE e-commerce business model by managing strategic businesses relationships with third party partners handling back-end functions and overall management of product pipeline.\n\n- Overview and manage current LINE eCommerce businesses- Manage relationship with strategic business partners involved in the LINE e-commerce businesses- Manage product pipelines to optimize sales- Develop new opportunities to monetize LINE e-commerce business model- Conduct detailed analysis to evaluate risks and balance against profit potential, size of the opportunity, and resource requirements ",
            "tags" => array("Manager / Senior Manager")
        ),
        "job_requirements" => array(
            "content" => "- Insight for Indonesia on/offline commerce and consumer trends- Required Language Ability: Bahasa Indonesia, English (Fluent)- Understanding of marketing and data analysis- Financial analysis skills a plus, but not required- 3+ years of related experience in business development, e-commerce and/or Brand Management- Strong communication and presentation skills- Able to work with Power Point, Excel, Microsoft Word programs- Result oriented team player",
            "tags" => array("English", "Bahasa", "3Y Experience")
        )
    ),
    4 => array(
        "id" => 4,
        "title" => "Head of Sales Mobile Phone Business",
        "company" => "LG Electronics",
        "salary" => "20.000.000 - 25.000.000 IDR",
        "imageurl" => "http://www.jobnext.net/uploads/user//f19513385a7f7c96825f00fd11367e11.220x140.png",
        "tagline" => "We offer the \"Right People\" great opportunities for growth and sure rewards for exceptional performance",
        "background" => "LG is always seeking out committed and enthusiastic team players. Exceptional individuals who possess the necessary skills to perform at the highest-level. Keen thinkers who are armed with both professionalism and a desire to take an active role on the global stage. And achievers well -versed in global market issues, with cultivated knowledge, expertise, and an engaging work style.",
        "job_description" => array( 
            "content" => "- Responsible to Develops, communicates &amp; implements the strategic direction of whole business operation Mobile Phone Division.\n- Responsible to Develop strategy and execute the plan included Sales, Marketing &amp; Retail.\n- Responsible to Manage the whole business operation included Distributor/Channel Sales and OperateRelationship Management.       \n- Responsible to Handle all key account sell-in, sell-through, &amp; sell-out.\n- Regularly leads important regional and headquarter initiatives.    \n- Has ultimate accountability for the sub-function.\n- Sets short, medium, and long term strategy for the operation.",
            "tags" => array("GM / Director", "Fulltime")
        ),
        "job_requirements" => array(
            "content" => "- Candidate must possess at least a Bachelor's Degree or Master's Degree / Post Graduate Degree in any field.\n- Required language(s): English, Bahasa Indonesia\n- At least 7 year(s) of working experience in the related field is required for this position.\n- Preferably General Manager or Senior Managers specializing in Sales - Corporate or equivalent.\n- Applicants should be Indonesian citizens or hold relevant residence status.",
            "tags" => array("English", "7Y Experience")
        )
    ),
);

header("Access-Control-Allow-Origin: http://localhost:8100");
header("Access-Control-Allow-Methods: GET, POST, PUT");
header("Access-Control-Allow-Headers: Content-Type");
header("Content-Type: application/json");

echo json_encode(isset($_GET['id']) ? ( $_GET['id'] ? $jobs_data[$_GET['id']] : array_values($jobs_data) ) : array_values($jobs_data));