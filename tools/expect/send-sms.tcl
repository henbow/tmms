#!/usr/bin/expect -f

set timeout 20
set mobilenumber [lindex $argv 0]
set message [lindex $argv 1]
set username administrator
set password tas_tekno

spawn sudo su - $username

expect "\[sudo\] password for administrator:"
send "$password\r"

expect "\\\\$"
send "echo \"$message\" | sudo gammu --sendsms TEXT $mobilenumber\r"

expect "\\\\$"
send "exit\r"

expect eof