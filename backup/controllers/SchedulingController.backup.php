<?php

use App\Models\Process;
use App\Models\ProcessGroup;
use App\Models\ProcessResourceGroup;
use App\Models\PlanningMFG;
use App\Models\PlanningNonMFG;
use App\Models\ProjectMasterPlan;
use App\Models\SchedulingMFG;
use App\Models\SchedulingNonMFG;
use App\Models\SchedulingPBBOM;
use App\Models\Resource;
use App\Models\ResourceGroup;
use App\Models\ResourceActualUsed;
use App\Models\ResourcePlanner;
use App\Models\GeneralOptions;

class SchedulingController extends BaseController {

    private $total_delivery_unix = 0;
    private $total_priority = 0;
    private $resource_group_id = 0;

    public function __construct() {
        View::composer(Config::get('app.theme') . '._layouts.master', function ($view) {
            $view->with('parent_active', 'planning');
            $view->with('child_active', 'scheduling');
        });
    }

    public function index() {
        $view = View::make(Config::get('app.theme') . '.scheduling.index');

        $plan = Input::get('plan');
        $process_group_id = Input::get('step');
        $process_id = Input::get('process');
        $resource_group = Input::get('resource_group');
        $start_date_filter = Input::get('start');

        if (Input::get('_token')) {
            $view->with('plan', $plan);
            $view->with('step', $process_group_id); 
            $view->with('selected_resource_group', $resource_group);   

            // debug($_POST);         
        }

        if($process_id) {
            $view->with('process_id', $process_id);
        }

        if (empty($start_date_filter)) {
            $start_date = date('Y-m-d H:i:s');

            if (!empty($resource_group)) {
                $resource_group_last_used = ResourceActualUsed::where('resource_group_id', '=', $resource_group)
                                ->orderBy('finish_used', 'DESC')->take(1);
                if ($resource_group_last_used->count() > 0) {
                    $resource_group_last_used = $resource_group_last_used->get();
                    $start_date = isset($resource_group_last_used[0]->finish_used) ? $resource_group_last_used[0]->finish_used : date('Y-m-d H:i:s');
                }
            }
        } else {
            list($month, $date, $year) = explode('/', $start_date_filter);
            $start_date = "{$year}-{$month}-{$date} 00:00:00";
        }

        $process_group_id = $process_group_id ? $process_group_id : '1';
        $process_group_id_data = ProcessGroup::find($process_group_id);

        if (intval($process_group_id_data->parent_id) > 0) {
            $process_group_id = $process_group_id_data->parent_id;
        }

        switch ($process_group_id_data->type) {
            case 'material':
                $planning_data = DB::table('project_planning_detail_mfg AS p')
                ->join('project_bom AS m', 'm.id', '=', 'p.material_id')
                ->join('project_master_plan AS mp', 'mp.id', '=', 'p.master_planning_id')
                ->join('projects AS pr', 'pr.id', '=', 'p.project_id')
                ->join('master_process AS mpr', 'mpr.id', '=', 'p.process_id')
                ->where(function($query) {
                    $resource_group = Input::get('resource_group');
                    $process_group_id = Input::get('step');
                    $process_id = Input::get('process');
                    $resource_group_ids = array();

                    if ($resource_group) {
                        $query->where('p.resource_group_id', '=', $resource_group);
                    }

                    if ($process_id) {
                        $query->where('p.process_id', '=', $process_id);
                    }

                    if ($process_group_id) {
                        $query_group = ProcessGroup::where('parent_id', '=', $process_group_id);
                        $has_child = $query_group->count() > 0 ? true : false;
                        if ($has_child) {
                            $params = array();
                            foreach ($query_group->get() as $pg) {
                                $params[] = $pg->id;
                            }

                            $query->whereIn('p.process_group_id', $params);
                        } else {
                            $query->where('p.process_group_id', '=', $process_group_id);
                        }
                    }

                })
                ->orderBy('p.priority')
                ->select(
                    'p.id', 'p.project_id', 'pr.project_name AS project', 'pr.nop',
                    'm.barcode', 'p.master_planning_id', 'p.material_id', 'm.material_name', 'mpr.process',
                    'p.process_group_id', 'p.process_id', 'p.resource_group_id', 'p.estimation_hour',
                    'mp.start_date', 'mp.finish_date', 'p.priority', 'm.length', 'm.width', 'm.height','m.qty'
                )->get();

               // debug(DB::getQueryLog());

                $new_planning_data = array();
                $priorities = array();
                if(count($planning_data) > 0)
                {
                    foreach ($planning_data as $pdata) {
                        $estimation_hour = $pdata->estimation_hour > 0 ? $pdata->estimation_hour : 0.1;
                        $priorities[$pdata->id] = $pdata->priority ? $pdata->priority : strtotime($pdata->finish_date) / $estimation_hour;
                    }

                    asort($priorities);

                    $priority_data = array();
                    $priority_num = 1;
                    foreach ($priorities as $id => $weight) {
                        $priority_data[$id] = $priority_num;
                        $priority_num++;
                    }

                    foreach ($planning_data as $pdata) {
                        $new_planning_data[$priority_data[$pdata->id]] = $pdata;
                    }

                    ksort($new_planning_data);
                }

                $view->with('priority_data', $priorities);
                $view->with('planning_data', $new_planning_data);
                $view->with('template_step', 'mfg');
                break;

            default:
                $planning_data = DB::table('project_planning_detail_non_mfg AS p')
                ->join('master_process AS m', 'm.id', '=', 'p.process_id')
                ->join('project_master_plan AS mp', 'mp.id', '=', 'p.master_planning_id')
                ->join('projects AS pr', 'pr.id', '=', 'p.project_id')
                ->join('master_process AS mpr', 'mpr.id', '=', 'p.process_id')
                ->where('p.process_group_id', '=', $process_group_id)
                ->where(function($query) {
                    $resource_group = Input::get('resource_group');
                    $process_id = Input::get('process');
                    $resource_group_ids = array();

                    if ($resource_group) {
                        $query->where('p.resource_group_id', '=', $resource_group);
                    }

                    if ($process_id) {
                        $query->where('p.process_id', '=', $process_id);
                    }
                })
                ->where(function($query) {
                    $process_id = Input::get('process');

                    if ($process_id) {
                        $query->orWhere('p.process_id', '=', $process_id);
                    }
                })
                ->orderBy('priority')
                ->select(
                    'p.id', 'p.project_id', 'pr.project_name AS project', 'pr.nop',
                    'p.master_planning_id', 'mpr.process',
                    'p.process_id', 'p.resource_group_id', 'p.estimation_hour',
                    'mp.start_date', 'mp.finish_date', 'p.priority'
                )->get();

                // debug(DB::getQueryLog());

                $new_planning_data = array();
                $priorities = array();
                if(count($planning_data) > 0)
                {
                    foreach ($planning_data as $pdata) {
                        $estimation_hour = $pdata->estimation_hour > 0 ? $pdata->estimation_hour : 0.1;
                        $priorities[$pdata->id] = $pdata->priority ? $pdata->priority : strtotime($pdata->finish_date) / $estimation_hour;
                    }

                    asort($priorities);

                    $priority_data = array();
                    $priority_num = 1;
                    foreach ($priorities as $id => $weight) {
                        $priority_data[$id] = $priority_num;
                        $priority_num++;
                    }

                    foreach ($planning_data as $pdata) {
                        $new_planning_data[$priority_data[$pdata->id]] = $pdata;
                    }

                    ksort($new_planning_data);
                }

                $view->with('priority_data', $priorities);
                $view->with('planning_data', $new_planning_data);
                $view->with('template_step', 'non-mfg');
                break;
        }

        $processes = Process::where(function($query) {
                        $group_id = Input::get('step');
                        $query_process = ProcessGroup::where('parent_id', '=', $group_id);

                        if ($query_process->count() > 0) {
                            $params = array();
                            foreach ($query_process->get() as $pg) {
                                $params[] = $pg->id;
                            }
                            $query->whereIn('group_id', $params);
                        } else {
                            $query->where('group_id', '=', $group_id ? $group_id : '1');
                        }
                    })->get();
        $resource_groups = ResourceGroup::orderBy('name')->get();
        $view->with('process_groups', ProcessGroup::where('parent_id', '=', '0')->where('id' , '<>' , '2')->get());
        $view->with('process_group_id', $process_group_id);
        $view->with('start_date', $start_date);
        $view->with('page_title', 'Project Scheduling');
        $view->with('theme', Config::get('app.theme'));
        $view->with('active_resource_groups', $resource_groups);
        $view->with('processes', $processes);

        return $view;
    }

    public function process() {
        $process_group_id = Input::get('step');
        $resource_group_id = Input::get('resource_group_id');
        $plan_ids = Input::get('plan_id');

        if (count($plan_ids) == 0) {
            Notification::error('No plans selected for scheduling process.');
            return Redirect::route('scheduling.index');
        }

        self::_clear_previous_schedule($process_group_id, $resource_group_id);
        self::_save_schedule($process_group_id, $plan_ids);

        $view_data = (Object) array(
            'plan' => 'IN PLAN',
            'step' => $process_group_id,
            'resource_group' => $resource_group_id,
            'start' => date('d/m/Y H:i')
        );

        $view = View::make(Config::get('app.theme') . '.scheduling.redirect');
        $view->with('data', $view_data);
        return $view;
    }

    private static function _clear_previous_schedule($process_group_id, $resource_group_id) {
        $process_group_data = ProcessGroup::find($process_group_id);
        switch ($process_group_data->type) {
            case 'material':
                $table = "scheduling_mfg";
                break;

            default:
                $table = "scheduling_non_mfg";
                break;
        }
        return DB::table($table)->where('resource_group_id', '=', $resource_group_id)->delete();
    }

    private static function _save_schedule($process_group_id, $plan_ids) {
        $is_manual = Input::get('is_manual');
        $start_time = Input::get('start_time');
        $predecessor = 0;
        $group_data = ProcessGroup::find($process_group_id);
        $next_process_delay = GeneralOptions::getSingleOption('next_process_delay')['value'];

        if(count($plan_ids) > 0) {
            krsort($plan_ids);

            $resource_orders = array();
            $resource_plannings = array();
            $selected_resource_id = array();
            $prefix = 1;
            foreach($plan_ids as $plan_id) {
                $planning = $group_data->type == 'material' ? PlanningMFG::find($plan_id) : PlanningNonMFG::find($plan_id);
                $master_planning = ProjectMasterPlan::find($planning->master_planning_id);
                $resource_id = select_resource_randomly($planning->resource_group_id,
                                            $group_data->type,
                                            $selected_resource_id); 
                $total_resource = total_resource_per_group($planning->resource_group_id);
                $key = str_pad($prefix++, 5, '0').$resource_id;
                $resource_orders[$key] = strtotime($master_planning->start_date);
                $resource_plannings['data'][$key] = (Object) array(
                    'resource_id' => $resource_id,
                    'plan_id' => $plan_id, 
                    'start_date' => date('Y-m-d H:i', strtotime($master_planning->start_date)),
                    'estimation' => $planning->estimation_hour
                );
                $resource_plannings['total_resources'] = $total_resource;
                
                if(count($selected_resource_id) < $total_resource)
                { 
                    $selected_resource_id[] = $resource_id;
                } 
                else 
                {
                    $selected_resource_id = array();   
                    $selected_resource_id[] = $resource_id;
                }
            }
            
            asort($resource_orders);
            $priority = 1;
            $resource_with_priority = array();
            foreach ($resource_orders as $key => $start_time_unix) {
                $resource_with_priority[$key] = $priority++;
            }

            $schedule_data = array();
            $total_resources = $resource_plannings['total_resources'];
            $finish_dates = array();
            $prev_start_date = null;

            debug($total_resources, false);
            $next_start_date = null;
            foreach ($resource_with_priority as $key => $priority_num) {
                $resource_id = substr($key, 5);
                $resource_planning = $resource_plannings['data'][$key];
                $start_date = $next_start_date != null ? $next_start_date : $resource_planning->start_date;
                $finish_date = date('Y-m-d H:i', strtotime($start_date) + ($resource_planning->estimation * 3600));
                $finish_dates[] = $finish_date;

                $schedule_data[$start_date][$priority_num] = $resource_planning;
                // $schedule_data[$start_date][$priority_num]->start_date = $start_date;
                $schedule_data[$start_date][$priority_num]->finish_date = $finish_date;
                // $next_start_date = min($finish_dates);
            }

            debug($schedule_data);

            $previous_finish_date = '';
            foreach ($schedule_data as $start_date => $data) {
                foreach ($data as $priority_num => $resource_planning) {
                    $resource_id = $resource_planning->resource_id;
                    switch ($group_data->type) {
                        case 'material':
                            $planning = PlanningMFG::find($resource_planning->plan_id);
                            $the_resource_group = Resource::find($planning->resource_group_id);
                            // $estimation_data = self::_process_estimation($planning, $data->finish_date, $the_resource_group->is_human);
                            $existing_schedule = SchedulingMFG::where('project_id','=',$planning->project_id)
                                        ->where('planning_id','=',$planning->master_planning_id)
                                        ->where('process_id','=',$planning->process_id)
                                        ->where('material_id','=',$planning->material_id)
                                        ->where('resource_group_id','=',$planning->resource_group_id)
                                        ->count() > 0 ? true : false;                            
                            if($existing_schedule === false) {
                                $scheduling = new SchedulingMFG;
                                $scheduling->project_id = $planning->project_id;
                                $scheduling->material_id = $planning->material_id;
                                $scheduling->process_id = $planning->process_id;
                                $scheduling->planning_id = $planning->id;
                                $scheduling->resource_id = $resource_id;
                                $scheduling->resource_group_id = $planning->resource_group_id;
                                $scheduling->estimation_hour = $resource_planning->estimation;
                                $scheduling->estimation_day = round($resource_planning->estimation / 24, 1);
                                $scheduling->priority = $priority_num;
                                $scheduling->start_date = $resource_planning->start_date;
                                $scheduling->finish_date = $resource_planning->finish_date;
                                $scheduling->predecessor = $predecessor;
                                $scheduling->save();
                            }
                            break;

                        default:
                            $planning = PlanningNonMFG::find($resource_planning->plan_id);
                            $the_resource_group = Resource::find($planning->resource_group_id);
                            // $estimation_data = self::_process_estimation($planning, $data->finish_date, $the_resource_group->is_human);
                            $existing_schedule = SchedulingNonMFG::where('project_id','=',$planning->project_id)
                                        ->where('planning_id','=',$planning->master_planning_id)
                                        ->where('process_id','=',$planning->process_id)
                                        ->where('resource_group_id','=',$planning->resource_group_id)
                                        ->count() > 0 ? true : false;
                            if($existing_schedule === false) {
                                $scheduling = new SchedulingNonMFG;
                                $scheduling->process_group_id = $process_group_id;
                                $scheduling->project_id = $planning->project_id;
                                $scheduling->process_id = $planning->process_id;
                                $scheduling->planning_id = $planning->id;
                                $scheduling->resource_id = $resource_id;
                                $scheduling->resource_group_id = $planning->resource_group_id;
                                $scheduling->estimation_hour = $resource_planning->estimation;
                                $scheduling->estimation_day = round($resource_planning->estimation / 24, 1);
                                $scheduling->priority = $priority_num;
                                $scheduling->start_date = $resource_planning->start_date;
                                $scheduling->finish_date = $resource_planning->finish_date;
                                $scheduling->predecessor = $predecessor;
                                $scheduling->save();
                            }
                            break;
                    }

                    $predecessor = $scheduling->id;
                }                
            }
        }
    }

    private static function _process_estimation($planning_data, $finish_date, $is_human = '1') {
        list($fdate, $ftime) = explode(' ', $finish_date);
        $orig_finish_date = $finish_date;
        $finish_date_tupple = explode('-', $fdate);
        $finish_time_tupple = explode(':', $ftime);

        if($is_human == '1') {
            if(intval($finish_time_tupple[0]) >= 16 && intval($finish_time_tupple[1] >= 15)) {
                $fhour = 16;
                $fday = intval($finish_date_tupple[2]);
                $finish_date = "{$finish_date_tupple[0]}-{$finish_date_tupple[1]}-{$fday} {$fhour}:15:00";
            }
        }

        $finish_date_unix = strtotime($finish_date);

        $work_plan = ResourcePlanner::where('resource_group_id', '=', $planning_data->resource_group_id)
                        ->where('year', '<=', $finish_date_tupple[0])
                        ->where('month', '<=', $finish_date_tupple[1]);

        $estimation = intval($planning_data->estimation_hour);
        $work_hour_left = $estimation;
        $used_work_time = 0;
        $total_workdays = 0;
        $total_holidays = 0;
        $i = 1;
        if ($work_plan->count() > 0) {
            $the_work_plans = $work_plan->get();
            foreach ($the_work_plans as $wp) {
                if ($wp->work_time > 0) {
                    if ($work_hour_left < $wp->work_time && $work_hour_left > 0) {
                        $used_work_time =round($work_hour_left, 2);
                        break;
                    }

                    $work_hour_left -= $wp->work_time;

                    $total_workdays++;
                } else {
                    $total_holidays++;
                }
            }
        }

//        debug("Work Time Left: ".$work_hour_left, false);
//        debug("Total work day: ".$total_workdays, false);
//        debug("Total holiday: ".$total_holidays, false);
//        debug('============================', false);

        $total_work_hour = ($total_workdays * 24) + $used_work_time;
        $start_time_unix = $finish_date_unix - ($total_work_hour * 3600);
        $start_time = date('Y-m-d H:i:s', $start_time_unix);
        $new_finish_date = date('Y-m-d H:i:s', $finish_date_unix);
        $total_estimation_hour = round(($finish_date_unix - $start_time_unix) / 3600, 2);
        $total_estimation_day = round($total_estimation_hour / 24, 2);

        return array(
            'total_work_hour' => $estimation,
            'total_estimation_hour' => $total_estimation_hour,
            'total_estimation_day' => $total_estimation_day,
            'start_time' => $start_time,
            'finish_time' => $new_finish_date,
            'orig_finish_time' => $orig_finish_date
        );
    }

    public function open_gantt($resource_group_id, $process_group_id) {
        $view = View::make(Config::get('app.theme') . '.scheduling.result.gantt');
        $view->with('theme', Config::get('app.theme'));
        $view->with('resource_group_id', $resource_group_id);
        $view->with('step', $process_group_id);

        return $view;
    }

    public function results($resource_group_id, $process_group_id) {
        $view = View::make(Config::get('app.theme') . '.scheduling.results');
        $view->with('page_title', 'Scheduling');
        $view->with('theme', Config::get('app.theme'));

        $group_data = ProcessGroup::find($process_group_id);
        switch ($group_data->type) {
            case 'material':
                $schedule_data = DB::table('scheduling_mfg AS p')
                        ->join('master_process AS m', 'm.id', '=', 'p.process_id')
                        ->join('projects AS pr', 'pr.id', '=', 'p.project_id')
                        ->select('p.*', 'pr.nop', 'pr.project_name AS project', 'm.process')
                        ->where('p.resource_group_id', '=', $resource_group_id)
                        ->orderBy('p.priority')
                        ->get();
                $view->with('schedule_data', $schedule_data);
                $view->with('module', 'mfg');
                break;

            default:
                $schedule_data = DB::table('scheduling_non_mfg AS p')
                        ->join('master_process AS m', 'm.id', '=', 'p.process_id')
                        ->join('projects AS pr', 'pr.id', '=', 'p.project_id')
                        ->select('p.*', 'pr.nop', 'pr.project_name AS project', 'm.process')
                        ->where('p.process_group_id', '=', $process_group_id)
                        ->where('p.resource_group_id', '=', $resource_group_id)
                        ->orderBy('p.finish_date')
                        ->get();
                $view->with('schedule_data', $schedule_data);
                $view->with('module', 'non-mfg');
                break;
        }

        $view->with('step', $process_group_id);
        $view->with('resource_group', Resource::find($resource_group_id));

        return $view;
    }

    public function results_json($resource_group_id, $process_group_id) {
        $schedules = array();
        $group_data = ProcessGroup::find($process_group_id);
        switch ($group_data->type) {
            case 'material':
                $schedule_data = DB::table('scheduling_mfg AS p')
                        ->join('master_process AS m', 'm.id', '=', 'p.process_id')
                        // ->join('project_planning_detail_mfg AS mp', 'mp.id', '=', 'p.planning_id')
                        ->join('projects AS pr', 'pr.id', '=', 'p.project_id')
                        ->join('project_bom AS pb', 'pb.id', '=', 'p.material_id')
                        ->where('p.resource_group_id', '=', $resource_group_id)
                        ->orderBy('p.priority');
                // ->orderBy('p.finish_date');
                $schedule_result = $schedule_data->get(array('p.*', 'pr.nop', 'pr.project_name AS project', 'm.process', 'pb.material_name'));
                break;

            default:
                $schedule_data = DB::table('scheduling_non_mfg AS p')
                        ->join('master_process AS m', 'm.id', '=', 'p.process_id')
                        ->join('project_planning_detail_non_mfg AS mp', 'mp.id', '=', 'p.planning_id')
                        ->join('projects AS pr', 'pr.id', '=', 'p.project_id')
                        ->where('p.process_group_id', '=', $process_group_id)
                        ->where('p.resource_group_id', '=', $resource_group_id)
                        ->orderBy('p.score', 'DESC');
                $schedule_result = $schedule_data->get(array('p.*', 'pr.nop', 'pr.project_name AS project', 'm.process'));
                break;
        }

        if ($schedule_data->count() > 0) {
            foreach ($schedule_result as $schedule) {
                switch ($group_data->type) {
                    case 'material':
                        $text = $schedule->process . '::' . $schedule->material_name;
                        break;

                    default:
                        $text = $schedule->project;
                        break;
                }
                $schedules['data'][] = array(
                    'id' => $schedule->id,
                    'start_date' => $schedule->start_date,
                    'duration' => $schedule->estimation_hour,
                    'text' => $text
                );
                $schedules['collections']['links'][] = array(
                    'id' => $schedule->id,
                    'source' => $schedule->id,
                    'target' => $schedule->predecessor,
                    'type' => 0
                );
            }
        }
        $response = Response::make($schedules, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function edit_priority() {
        $process_type = Input::get('name');

        if ($process_type) {
            $process_id = Input::get('pk');
            $new_priority = Input::get('value');
            $old_priority = explode(',', Input::get('old_priority_order'));

            $reformat_priority_number = reformat_priority_number($old_priority);
            $is_incremental = is_incremental($reformat_priority_number, $process_id, $new_priority);
            $reorder_priority = sort_priority_number($reformat_priority_number, $new_priority, $is_incremental);
            $reorder_priority[$process_id] = $new_priority;

//            debug($reorder_priority);

            asort($reorder_priority);
            $final_priority_order = fix_priority_numbers($reorder_priority);

            foreach ($final_priority_order as $pid => $priority_num) {
                switch ($process_type) {
                    case 'material':
                        $planning_data = PlanningMFG::find($pid);
                        break;

                    default:
                        $planning_data = PlanningNonMFG::find($pid);
                        break;
                }
//                debug($planning_data);

                if (!is_null($planning_data)) {
                    $planning_data->priority = $priority_num;
                    $planning_data->save();
                }
            }
        }
    }

    public function get_process() {
        $processes = Process::where(function($query) {
                    $group_id = Input::get('group_id');

                    $has_child = ProcessGroup::where('parent_id', '=', $group_id);
                    if ($has_child->count() > 0) {
                        $group_ids = array();
                        foreach ($has_child->get() as $group) {
                            $group_ids[] = $group->id;
                        }
                        $query->whereIn('group_id', $group_ids);
                    } else {
                        $query->where('group_id', '=', $group_id);
                    }
                })->get();
        $response = Response::make($processes, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_resource_group() {
        $resource_groups = Resource::where(function($query) {
                    $process_id = Input::get('process_id');
                    $plan_type = Input::get('plan_type');

                    $process_resource_group = ProcessResourceGroup::where('process_id', '=', $process_id)->get();
                    $resource_group_ids = array();
                    foreach ($process_resource_group as $pr) {
                        $resource_group_ids[] = $pr->resource_group_id;
                    }

                    $query->whereIn('id', $resource_group_ids);
                    $query->where('is_offline', '=', '0');
                    $query->where('plan_type', '=', strtolower($plan_type));
                })->get();
        $response = Response::make($resource_groups, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }



}
