<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResourceWorkPlanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('resource_work_plan', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('resource_id')->nullable();
			$table->integer('year')->nullable();
			$table->integer('month')->nullable();
			$table->integer('week_num')->nullable();
			$table->integer('day_num')->nullable();
			$table->float('work_time', 10)->nullable()->default(0.00);
			$table->text('description', 65535)->nullable();
			$table->timestamps();
			$table->index(['resource_id','year','month','day_num'], 'indexes');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('resource_work_plan');
	}

}
