<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProcessResourceGroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('process_resource_group', function(Blueprint $table)
		{
			$table->integer('process_id')->unsigned();
			$table->integer('resource_group_id')->unsigned();
			$table->primary(['process_id','resource_group_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('process_resource_group');
	}

}
