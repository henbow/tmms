<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMenuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menu', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('order')->default(0);
			$table->integer('parent_id')->default(0);
			$table->string('menu_name', 45)->default('');
			$table->string('alias', 45)->default('');
			$table->string('route', 100)->default('');
			$table->text('param');
			$table->string('class_attr', 40)->default('');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menu');
	}

}
