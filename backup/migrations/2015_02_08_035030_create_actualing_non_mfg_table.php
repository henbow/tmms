<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActualingNonMfgTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('actualing_non_mfg', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('planning_id')->nullable();
			$table->integer('project_id')->nullable();
			$table->integer('process_id')->nullable();
			$table->integer('resource_id')->nullable();
			$table->integer('pic_id')->nullable();
			$table->integer('plan_duration')->nullable();
			$table->integer('actual_duration')->nullable();
			$table->integer('total_start')->nullable();
			$table->integer('total_finish')->nullable();
			$table->integer('total_rework')->nullable();
			$table->integer('total_guarantee')->nullable();
			$table->dateTime('start_date')->nullable();
			$table->dateTime('finish_date')->nullable();
			$table->text('remark')->nullable();
			$table->integer('position')->nullable();
			$table->enum('finish_type', array('PLAN','REWORK','GUARANTEE'))->nullable();
			$table->enum('subfinish_type', array('ONPROCESS','COMPLETE'))->nullable();
			$table->enum('status', array('WAITING','ON PROCESS','COMPLETE'))->nullable()->default('WAITING');
			$table->integer('user_id')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('actualing_non_mfg');
	}

}
