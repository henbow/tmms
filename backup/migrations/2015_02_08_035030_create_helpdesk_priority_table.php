<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHelpdeskPriorityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('helpdesk_priority', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('priority', 45)->default('');
			$table->string('class', 45)->default('');
			$table->string('sms_to', 45)->default('');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('helpdesk_priority');
	}

}
