<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMasterResourceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('master_resource', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('code', 10)->default('');
			$table->string('name', 250)->default('');
			$table->integer('pic_id')->nullable();
			$table->integer('group_id')->nullable();
			$table->enum('is_human', array('1','0'))->default('0');
			$table->enum('is_offline', array('1','0'))->default('0');
			$table->text('offline_reason', 65535)->nullable();
			$table->enum('plan_type', array('in plan','out plan'))->default('in plan');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('master_resource');
	}

}
