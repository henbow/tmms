<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMasterProcessTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('master_process', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('code', 10)->default('');
			$table->integer('group_id')->default(0);
			$table->string('process', 40)->default('');
			$table->enum('process_type', array('nop','material'))->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('master_process');
	}

}
