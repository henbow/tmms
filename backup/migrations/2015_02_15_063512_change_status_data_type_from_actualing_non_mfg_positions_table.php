<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStatusDataTypeFromActualingNonMfgPositionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('actualing_non_mfg_positions', function(Blueprint $table)
		{
			$table->enum('status', array('WAITING', 'ONPROCESS', 'COMPLETE'))->default('WAITING')->after('actualing_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('actualing_non_mfg_positions', function(Blueprint $table)
		{
			$table->dropColumn('status');
		});
	}

}
