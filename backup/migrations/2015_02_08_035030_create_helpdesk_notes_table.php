<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHelpdeskNotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('helpdesk_notes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('helpdesk_id')->default(0);
			$table->integer('reply_to')->default(0);
			$table->integer('reply_by')->default(0);
			$table->enum('reply_type', array('user','pm'))->default('user');
			$table->text('message', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('helpdesk_notes');
	}

}
