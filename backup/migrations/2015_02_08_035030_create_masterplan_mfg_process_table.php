<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMasterplanMfgProcessTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('masterplan_mfg_process', function(Blueprint $table)
		{
			$table->integer('material_process_id')->unsigned();
			$table->integer('master_plan_id')->unsigned();
			$table->primary(['material_process_id','master_plan_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('masterplan_mfg_process');
	}

}
