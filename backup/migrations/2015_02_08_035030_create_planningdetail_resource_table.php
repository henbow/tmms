<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanningdetailResourceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('planningdetail_resource', function(Blueprint $table)
		{
			$table->integer('planning_id')->unsigned();
			$table->integer('resource_id')->unsigned();
			$table->primary(['planning_id','resource_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('planningdetail_resource');
	}

}
