<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToProjectPlanningDetailNonMfgTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('project_planning_detail_non_mfg', function(Blueprint $table)
		{
			$table->foreign('master_planning_id', 'f_master_plan_id')->references('id')->on('project_master_plan')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('project_planning_detail_non_mfg', function(Blueprint $table)
		{
			$table->dropForeign('f_master_plan_id');
		});
	}


}
