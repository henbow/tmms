<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHelpdeskTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('helpdesk', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('ticket_id', 45)->nullable();
			$table->integer('category_id')->nullable();
			$table->string('category_type', 10)->nullable()->default('');
			$table->integer('request_user_id')->nullable();
			$table->integer('incharge_group_id')->nullable();
			$table->integer('incharge_id')->nullable();
			$table->integer('status_id')->nullable();
			$table->integer('priority_id')->nullable();
			$table->text('subject', 65535)->nullable();
			$table->text('summary', 65535)->nullable();
			$table->dateTime('start_request_attendance')->nullable()->default('0000-00-00 00:00:00');
			$table->dateTime('finish_request_attendance')->nullable()->default('0000-00-00 00:00:00');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('helpdesk');
	}

}
