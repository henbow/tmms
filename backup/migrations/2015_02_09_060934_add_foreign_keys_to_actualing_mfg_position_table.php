<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToActualingMfgPositionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('actualing_mfg_positions', function(Blueprint $table)
		{
			$table->foreign('position_id', 'f_position_id')->references('id')->on('bom_positions')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('actualing_mfg_positions', function(Blueprint $table)
		{
			$table->dropForeign('f_position_id');
		});
	}

}
