<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMasterResourceWorkTimeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('master_resource_work_time', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('resource_id')->index('resource');
			$table->float('mon', 6)->nullable()->default(0.00);
			$table->float('tue', 6)->nullable()->default(0.00);
			$table->float('wed', 6)->nullable()->default(0.00);
			$table->float('thu', 6)->nullable()->default(0.00);
			$table->float('fri', 6)->nullable()->default(0.00);
			$table->float('sat', 6)->nullable()->default(0.00);
			$table->float('sun', 6)->nullable()->default(0.00);
			$table->float('per_week', 6)->nullable()->default(0.00);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('master_resource_work_time');
	}

}
