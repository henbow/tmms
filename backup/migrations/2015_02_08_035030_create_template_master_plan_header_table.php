<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTemplateMasterPlanHeaderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('template_master_plan_header', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 45)->default('');
			$table->string('pull_date_from', 45)->default('TRIAL');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('template_master_plan_header');
	}

}
