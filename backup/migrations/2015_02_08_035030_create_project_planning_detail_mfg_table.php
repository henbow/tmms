<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectPlanningDetailMfgTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_planning_detail_mfg', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('project_id')->nullable();
			$table->integer('master_planning_id')->nullable()->index('masterplan');
			$table->integer('material_id')->nullable()->index('material');
			$table->integer('process_group_id')->nullable()->index('process_group');
			$table->integer('process_id')->nullable()->index('process');
			$table->integer('resource_group_id')->nullable()->index('resource_group');
			$table->integer('position')->nullable();
			$table->text('specification', 65535)->nullable();
			$table->integer('estimation')->nullable();
			$table->integer('estimation_hour')->nullable();
			$table->integer('priority')->default(0);
			$table->integer('is_actualing')->nullable()->default(0);
			$table->timestamps();
			$table->index(['project_id','material_id','master_planning_id','process_group_id','process_id','resource_group_id','is_actualing'], 'indexes');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_planning_detail_mfg');
	}

}
