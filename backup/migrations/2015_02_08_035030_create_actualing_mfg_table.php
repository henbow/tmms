<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActualingMfgTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('actualing_mfg', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('planning_id')->nullable();
			$table->integer('material_id')->nullable();
			$table->integer('process_id')->nullable();
			$table->integer('resource_id')->nullable();
			$table->integer('pic_id')->nullable();
			$table->integer('plan_duration')->nullable();
			$table->integer('plan_qty')->nullable();
			$table->integer('actual_duration')->nullable();
			$table->integer('qty_id')->nullable();
			$table->integer('total_start')->nullable()->default(0);
			$table->integer('total_finish')->nullable()->default(0);
			$table->integer('total_rework')->nullable()->default(0);
			$table->integer('total_guarantee')->nullable()->default(0);
			$table->dateTime('start_date')->nullable();
			$table->dateTime('finish_date')->nullable();
			$table->text('remark')->nullable();
			$table->integer('position')->nullable();
			$table->enum('finish_type', array('PLAN','REWORK','GUARANTEE'))->nullable();
			$table->enum('subfinish_type', array('ONPROCESS','COMPLETE'))->nullable();
			$table->enum('status', array('WAITING','ON PROCESS','COMPLETE'))->nullable()->default('WAITING');
			$table->integer('user_id')->nullable();
			$table->timestamps();
			$table->index(['material_id','process_id','resource_id','planning_id','pic_id'], 'indexes');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('actualing_mfg');
	}

}
