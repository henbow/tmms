<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nop', 45)->nullable();
			$table->integer('order_num')->nullable();
			$table->integer('priority')->nullable();
			$table->string('project_name', 250)->nullable();
			$table->integer('qty')->nullable();
			$table->integer('price_per_unit')->nullable();
			$table->integer('price_total')->nullable();
			$table->string('customer_id', 45)->nullable();
			$table->string('customer_code', 15)->nullable();
			$table->string('customer_name', 250)->nullable();
			$table->enum('is_planning', array('1','0'))->nullable()->default('1');
			$table->enum('is_complete', array('1','0'))->nullable()->default('0');
			$table->date('start_date')->nullable()->default('0000-00-00');
			$table->date('trial_date')->nullable()->default('0000-00-00');
			$table->date('delivery_date')->nullable()->default('0000-00-00');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}

}
