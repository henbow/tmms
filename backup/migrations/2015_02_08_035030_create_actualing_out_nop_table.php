<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActualingOutNopTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('actualing_out_nop', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nop', 100)->nullable();
			$table->integer('process_id')->nullable();
			$table->string('process_name', 250)->nullable();
			$table->integer('resource_id')->nullable();
			$table->integer('resource_name')->nullable();
			$table->string('tsr_no', 45)->nullable();
			$table->dateTime('tsr_date')->nullable();
			$table->string('ttb_no', 45)->nullable();
			$table->dateTime('ttb_date')->nullable();
			$table->integer('total_time')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('actualing_out_nop');
	}

}
