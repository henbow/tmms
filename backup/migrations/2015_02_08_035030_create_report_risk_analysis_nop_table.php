<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReportRiskAnalysisNopTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('report_risk_analysis_nop', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('report_id')->nullable();
			$table->integer('resource_id')->nullable();
			$table->integer('master_plan_id')->nullable();
			$table->integer('scheduling_id')->nullable();
			$table->integer('process_id')->nullable();
			$table->integer('process_group_id')->nullable();
			$table->dateTime('planning_finish_date')->nullable();
			$table->dateTime('scheduling_start_date')->nullable();
			$table->string('scheduling_finish_date', 45)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('report_risk_analysis_nop');
	}

}
