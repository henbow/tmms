<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReportRiskAnalysisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('report_risk_analysis', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('report_number', 45)->nullable();
			$table->integer('project_id')->nullable();
			$table->integer('master_plan_id')->nullable();
			$table->enum('type', array('material','nop'))->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('report_risk_analysis');
	}

}
