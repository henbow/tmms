<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActualingDeliveryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('actualing_delivery', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('type', 45)->nullable();
			$table->integer('project_id')->nullable();
			$table->integer('process_id')->nullable();
			$table->integer('planning_id')->nullable();
			$table->integer('scheduling_id')->nullable();
			$table->integer('resource_id')->nullable();
			$table->integer('plan_duration')->nullable();
			$table->integer('actual_duration')->nullable();
			$table->integer('total_start')->nullable();
			$table->integer('total_finish')->nullable();
			$table->integer('total_rework')->nullable();
			$table->integer('total_guarantee')->nullable();
			$table->date('start_date')->nullable();
			$table->date('finish_date')->nullable();
			$table->integer('is_finish')->default(0);
			$table->text('remark')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('actualing_delivery');
	}

}
