<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectMasterPlanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_master_plan', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('parent_id')->default(0);
			$table->integer('order')->default(0);
			$table->integer('project_id')->default(0);
			$table->integer('process_group_id')->default(0);
			$table->integer('percent')->default(0);
			$table->float('estimation', 3, 1)->default(0.0);
			$table->float('estimation_hour', 6, 1)->default(0.0);
			$table->float('real_percent', 14, 12)->default(0.000000000000);
			$table->float('real_estimation', 16, 12)->default(0.000000000000);
			$table->float('real_estimation_hour', 16, 12)->default(0.000000000000);
			$table->enum('process_type', array('first','secondary'))->default('first');
			$table->integer('lag')->default(0);
			$table->dateTime('start_date')->default('0000-00-00 00:00:00');
			$table->dateTime('finish_date')->default('0000-00-00 00:00:00');
			$table->integer('predecessor')->default(0);
			$table->enum('pull_from', array('trial','delivery'))->default('trial');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_master_plan');
	}

}
