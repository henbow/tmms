<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMasterResourceSpecTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('master_resource_spec', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('resource_id')->nullable();
			$table->float('min_table_load', 10)->nullable();
			$table->float('max_table_load', 10)->nullable();
			$table->float('table_size', 10)->nullable();
			$table->float('travel_range', 10)->nullable();
			$table->float('accuracy', 10)->nullable();
			$table->float('min_mould_size', 10)->nullable();
			$table->float('max_mould_size', 10)->nullable();
			$table->integer('std_rate_per_hour')->nullable();
			$table->integer('ovt_rate_per_hour')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('master_resource_spec');
	}

}
