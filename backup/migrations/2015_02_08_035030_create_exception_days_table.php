<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExceptionDaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('exception_days', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('year')->nullable()->default(0);
			$table->integer('month')->nullable()->default(0);
			$table->integer('day')->nullable()->default(0);
			$table->text('description');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('exception_days');
	}

}
