<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTemplateMasterPlanDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('template_master_plan_detail', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('header_id')->default(0);
			$table->integer('parent_id')->default(0);
			$table->float('order', 4, 1)->default(0.0);
			$table->integer('process_group_id')->default(0);
			$table->integer('percent')->default(0);
			$table->enum('process_type', array('first','secondary'))->default('first');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('template_master_plan_detail');
	}

}
