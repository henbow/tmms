<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProcessResourceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('process_resource', function(Blueprint $table)
		{
			$table->integer('process_id')->unsigned();
			$table->integer('resource_id')->unsigned();
			$table->primary(['process_id','resource_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('process_resource');
	}

}
