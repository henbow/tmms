<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActualingMfgLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('actualing_mfg_log', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('actualing_id')->nullable();
			$table->integer('resource_id')->nullable();
			$table->integer('pic_id')->nullable();
			$table->enum('state', array('START','PAUSE','FINISH'))->nullable();
			$table->enum('finish_type', array('PLAN','REWORK','GUARANTEE'))->nullable();
			$table->integer('position')->nullable();
			$table->text('remark')->nullable();
			$table->integer('last_duration')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('actualing_mfg_log');
	}

}
