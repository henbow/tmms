<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectBomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_bom', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('project_id')->nullable()->index('projectID');
			$table->string('nop', 45)->nullable();
			$table->integer('order_num')->nullable();
			$table->integer('part_code')->nullable();
			$table->string('barcode', 45)->nullable();
			$table->text('material_name')->nullable();
			$table->enum('type', array('M','S','H'))->nullable()->default('M');
			$table->string('standard', 45)->nullable();
			$table->integer('length')->nullable()->default(0);
			$table->integer('width')->nullable()->default(0);
			$table->integer('height')->nullable()->default(0);
			$table->integer('diameter_start')->nullable();
			$table->integer('diameter_finish')->nullable();
			$table->string('dimension_desc', 45)->nullable();
			$table->integer('qty')->nullable()->default(0);
			$table->string('unit', 45)->nullable();
			$table->integer('priority')->nullable()->default(0);
			$table->enum('will_processed', array('1','0'))->nullable()->default('1');
			$table->text('description')->nullable();
			$table->date('insert_date')->nullable()->default('0000-00-00');
			$table->string('po_number', 100)->nullable();
			$table->date('po_date')->nullable();
			$table->integer('po_qty')->nullable();
			$table->string('ttb_number', 100)->nullable();
			$table->date('ttb_date')->nullable();
			$table->integer('ttb_qty')->nullable();
			$table->integer('po_ttb_time')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_bom');
	}

}
