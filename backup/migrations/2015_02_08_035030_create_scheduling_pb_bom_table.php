<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchedulingPbBomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('scheduling_pb_bom', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('project_id')->nullable();
			$table->integer('process_id')->nullable();
			$table->integer('material_id')->nullable();
			$table->integer('planning_id')->nullable();
			$table->integer('resource_id')->nullable();
			$table->integer('estimation_hour')->nullable();
			$table->integer('estimation_day')->nullable();
			$table->integer('priority')->nullable();
			$table->date('start_date')->nullable();
			$table->date('finish_date')->nullable();
			$table->integer('predecessor')->nullable();
			$table->integer('is_actualing')->default(0);
			$table->float('score')->default(0.00);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('scheduling_pb_bom');
	}

}
