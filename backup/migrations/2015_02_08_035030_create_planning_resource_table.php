<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanningResourceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('planning_resource', function(Blueprint $table)
		{
			$table->integer('planning_id')->unsigned();
			$table->integer('resource_id')->unsigned();
			$table->primary(['planning_id','resource_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('planning_resource');
	}

}
