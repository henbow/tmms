<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResourcePicTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('resource_pic', function(Blueprint $table)
		{
			$table->integer('resource_id')->unsigned();
			$table->integer('pic_id')->unsigned();
			$table->primary(['resource_id','pic_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('resource_pic');
	}

}
