<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMasterProcessGroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('master_process_group', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('order')->default(0);
			$table->integer('parent_id')->default(0);
			$table->string('group', 45)->default('');
			$table->enum('type', array('material','nop','pb-bom'))->default('nop');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('master_process_group');
	}

}
