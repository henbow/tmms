<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSmsQueueTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sms_queue', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('recipient_id')->nullable();
			$table->string('send_to', 45)->nullable();
			$table->text('message', 65535)->nullable();
			$table->integer('is_sent')->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sms_queue');
	}

}
