<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToBomQuantityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bom_quantities', function(Blueprint $table)
		{
			$table->foreign('material_id', 'f_material_id')->references('id')->on('project_bom')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bom_quantities', function(Blueprint $table)
		{
			$table->dropForeign('f_material_id');
		});
	}

}
