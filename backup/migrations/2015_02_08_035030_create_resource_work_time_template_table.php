<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResourceWorkTimeTemplateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('resource_work_time_template', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 45)->nullable();
			$table->float('mon', 10)->nullable()->default(0.00);
			$table->float('tue', 10)->nullable();
			$table->float('wed', 10)->nullable();
			$table->float('thu', 10)->nullable();
			$table->float('fri', 10)->nullable();
			$table->float('sat', 10)->nullable();
			$table->float('sun', 10)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('resource_work_time_template');
	}

}
