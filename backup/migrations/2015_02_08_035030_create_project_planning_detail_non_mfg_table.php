<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectPlanningDetailNonMfgTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_planning_detail_non_mfg', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('project_id')->nullable();
			$table->integer('process_group_id')->nullable();
			$table->integer('process_id')->nullable();
			$table->integer('master_planning_id')->nullable();
			$table->integer('estimation')->nullable();
			$table->integer('estimation_hour')->nullable();
			$table->integer('resource_group_id')->nullable();
			$table->text('specification', 65535)->nullable();
			$table->integer('priority')->nullable();
			$table->integer('is_actualing')->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_planning_detail_non_mfg');
	}

}
