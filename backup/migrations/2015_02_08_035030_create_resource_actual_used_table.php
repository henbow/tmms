<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResourceActualUsedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('resource_actual_used', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('resource_id')->default(0);
			$table->dateTime('start_used')->default('0000-00-00 00:00:00');
			$table->dateTime('finish_used')->default('0000-00-00 00:00:00');
			$table->integer('estimation')->default(0);
			$table->integer('is_used')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('resource_actual_used');
	}

}
