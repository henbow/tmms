<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMasterPicTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('master_pic', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->nullable();
			$table->integer('dept_id')->nullable();
			$table->integer('unit_id')->nullable();
			$table->string('code', 45)->nullable();
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->enum('sex', array('m','f'))->nullable()->default('m');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('master_pic');
	}

}
