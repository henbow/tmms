<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTemplateMfgProcessDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('template_mfg_process_detail', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('header_id')->default(0);
			$table->integer('process_group_id')->default(0);
			$table->integer('process_id')->default(0);
			$table->integer('estimation')->default(0);
			$table->integer('estimation_hour')->default(0);
			$table->integer('position')->default(0);
			$table->text('specification');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('template_mfg_process_detail');
	}

}
