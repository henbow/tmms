<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectPlanningDetailPbBomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_planning_detail_pb_bom', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('master_planning_id')->nullable();
			$table->integer('process_id')->nullable();
			$table->integer('material_id')->nullable();
			$table->integer('resource_id')->nullable();
			$table->integer('priority')->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_planning_detail_pb_bom');
	}

}
