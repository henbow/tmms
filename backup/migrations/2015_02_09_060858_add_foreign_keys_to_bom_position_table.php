<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToBomPositionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bom_positions', function(Blueprint $table)
		{
			$table->foreign('qty_id', 'f_qty_id')->references('id')->on('bom_quantities')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bom_positions', function(Blueprint $table)
		{
			$table->dropForeign('f_qty_id');
		});
	}

}
