<?php

class ResourcePlannerJob
{
	public function fire($job, $data)
	{
		DB::table('resource_work_plan')->where('year', '=', $data['year'])->delete();

        $resources = Resource::where('plan_type', '=', 'in plan')->get();
        foreach ($resources as $resource)
        {
            if(isset($resource->id))
            {
                // DB::table('resource_work_plan')->where('resource_id', '=', $resource->id)->delete();
                update_work_plan($resource->id, $data['year']);
            }
        }

        $job->delete();
	}

}
