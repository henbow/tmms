<?php
class  ERPSync
{

    public function fire($job, $data)
    {
        $process_start = time();

        if(count($data['ids']) == 0)
        {
        	Log::info('No data. Job is stopped');
        	$job->delete();
        	return false;
        }

        foreach($data['ids'] as $project_id)
        {
        	$project_tmms = Project::find($project_id);
			$projects = NOPPImporter::getProjectByNOP($project_tmms->nop);

			Log::info("Preparing to process ".count($projects)." data...");

	        $new_projects = 0;
	        $update_projects = 0;
	        foreach ($projects as $project)
	        {
				Log::info("");
				Log::info("Get project data from: ".$project->project."[".$project->nop."]");

				$query = Project::where('nop', '=', $project->nop)->where('project_name','=',$project->project);
				$existing_project = $query->count() > 0 ? true : false;
				if($existing_project == false)
				{
					Log::info("Project is not exists. Insert new project data to DB.");
					$new_project = new Project;
					$new_project++;
				}
				else
				{
					Log::info("Project is exists. Update project data.");
					$new_project = Project::find($query->get()[0]->id);
					$update_projects++;
				}

				$erp_customer = NOPPImporter::getCustomerByKey($project->customer_id);
				if(count($erp_customer))
				{
					$customer = $erp_customer[0];
					$existing_customer = Customer::where('erp_code','=',$project->customer_id)->first();
					if(count($existing_customer))
					{
						$new_customer = Customer::find($existing_customer->id);
					}
					else
					{
						$new_customer = new Customer;
					}

					$new_customer->erp_code = $project->customer_id;
					$new_customer->code = $customer->Initial;
					$new_customer->name = $customer->Nama;
					$new_customer->address = $customer->Alamat;
					$new_customer->city = $customer->Kota;
					$new_customer->mailing_address = $customer->AlamatKirim;
					$new_customer->tax_address = $customer->AlamatFakPjk;
					$new_customer->contact_person = $customer->Person;
					$new_customer->phone = $customer->Telp;
					$new_customer->fax = $customer->Fax;
					$new_customer->mobile = $customer->Hp;
					$new_customer->email = $customer->Email;
					$new_customer->save();
				}

				$new_project->nop = $project->nop;
				$new_project->order_num = $project->order_num;
				$new_project->priority = $project->priority;
				$new_project->project_name = $project->project;
				$new_project->qty = $project->qty;
				$new_project->price_per_unit = $project->price_per_unit;
				$new_project->price_total = $project->price_total;
				$new_project->customer_id = isset($new_customer->id) ? $new_customer->id : 0;
				$new_project->po_num = $project->po_num;
				$new_project->is_planning = '1';

				if($new_project->save())
				{
					Log::info("{$project->nop} => success update project data");
				}
				else
				{
					Log::info("{$project->nop} => failed update project data.");
				}

				$tmms_material_ids = array();
				$tmms_materials = ProjectBOM::where('project_id', '=', $project_id)->get();
				if(count($tmms_materials))
				{
					$material_ids = array();
					foreach ($tmms_materials as $tmms_material)
					{
						$tmms_material_ids[trim($tmms_material->barcode)] = (Object) array(
	                      'id' => $tmms_material->id,
	                      'will_processed' => $tmms_material->will_processed
		                );
		                $material_ids[] = $tmms_material->id;
					}

					ProjectBOM::whereIn('id', $material_ids)->delete();
				}

				$erp_materials = NOPPImporter::getBomProject($project->nop, $project->order_num);
				if(count($erp_materials) > 0)
				{
					foreach ($erp_materials as $erp_material)
					{
						$bom = new ProjectBOM;

						if(isset($tmms_material_ids[trim($erp_material->barcode)]))
						{
					        $bom->id = $tmms_material_ids[trim($erp_material->barcode)]->id;
					        $bom->will_processed = $tmms_material_ids[trim($erp_material->barcode)]->will_processed;

					        unset($tmms_material_ids[trim($erp_material->barcode)]);
						}

				        $bom->project_id = $new_project->id;
				        $bom->nop = $erp_material->nop;
				        $bom->order_num = $erp_material->order_num;
				        $bom->priority = $erp_material->priority;
				        $bom->part_code = $erp_material->part_code;
				        $bom->barcode = trim($erp_material->barcode);
				        $bom->material_name = $erp_material->material_name;
				        $bom->type = $erp_material->type;
				        $bom->standard = $erp_material->standard;
				        $bom->length = $erp_material->length;
				        $bom->width = $erp_material->width;
				        $bom->height = $erp_material->height;
				        $bom->diameter_start = $erp_material->diameter_start;
				        $bom->diameter_finish = $erp_material->diameter_finish;
				        $bom->dimension_desc = $erp_material->dimension_desc;
				        $bom->qty = $erp_material->qty;
				        $bom->unit = $erp_material->unit;
				        $bom->description = $erp_material->description;
				        $bom->po_number = $erp_material->po_number;
				        $bom->insert_date = $erp_material->insert_date;

						if($bom->save())
						{
							Log::info("{$erp_material->barcode} => success insert new material data");
						}
						else
						{
							Log::info("{$erp_material->barcode} => failed insert new material data.");
						}
					}
				}

				// $unused_material_ids = array_values($tmms_material_ids);
		  //       if(count($unused_material_ids))
		  //       {
		  //       	foreach ($unused_material_ids as $unused_material_id)
		  //       	{
		  //       		$unused_material = ProjectBOM::find($unused_material_id);
		  //       		$unused_material->delete();
		  //       	}
		  //       }

				$planning_mfg = PlanningMFG::where('project_id','=',$new_project->id)->get();
				if(count($planning_mfg) > 0)
				{
					$no = 1;
					Log::debug("Start generating quantities and positions for planning with material type...");
					foreach ($planning_mfg as $mfg)
					{
						if(isset($mfg->id))
						{
							Log::debug($no . " - Generate quantities...");
							save_mfg_qty($mfg->id);
							Log::debug($no . " - Generate positions...");
			            	save_mfg_position($mfg->id);
			            	$no++;
						}
					}
					Log::debug("Finish generating quantities and positions for planning with material type...");
				}

				$planning_non_mfg = PlanningNonMFG::where('project_id','=',$new_project->id)->get();
				if(count($planning_non_mfg) > 0)
				{
					$no = 1;
					Log::debug("Start generating positions for planning with NOP type...");
					foreach ($planning_non_mfg as $non_mfg)
					{
						if(isset($non_mfg))
						{
							Log::debug($no . " - Generate quantities...");
							save_non_mfg_qty($non_mfg->id);
							Log::debug($no . " - Generate positions...");
			            	save_non_mfg_position($non_mfg->id);
			            	$no++;
						}
					}
					Log::debug("Finish generating positions for planning with NOP type...");
				}
	        }
	    }

		$process_duration = time() - $process_start;
        Log::info("Process time: ".round($process_duration / 60, 2)." minutes");

        $job->delete();
    }

}
