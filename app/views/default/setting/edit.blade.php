@extends($theme . '._layouts.master')
 
@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="wrapper">
    
    <!-- Note -->
    {{ Notification::showAll() }}
    
    <!-- Validation form -->
    {{ Form::model($settings, array('method' => 'put', 'class' => 'form', 'id' => 'validate', 'route' => array('setting.update', 1))) }}
    	<fieldset>
            <div class="widget">
                <div class="title">
                    <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
                    <h6>Settings</h6>
                </div>
                @foreach($settings as $setting)
                <div class="formRow">
                    <label>{{{ $setting->alias }}}:<span class="req">*</span></label>
                    <div class="formRight">
                        @if($setting->value_type == 'string')
                        <input type="text" class="validate[required]" name="{{{ $setting->name }}}" id="{{{ $setting->name }}}" value="{{{ $setting->value }}}"/>
                        @elseif($setting->value_type == 'split')
                        <?php $values = explode(',', $setting->value);?>
                        <a href="{{{ route($values[1]) }}}" title="{{{ $values[0] }}}" class="button greenB"><span>{{{ $values[0] }}}</span></a>
                        @endif
                    </div><div class="clear"></div>
                </div>
                @endforeach
            </div>
            <div class="formSubmit"><input type="submit" value="Change Settings" class="redB" /></div>
            <div class="clear"></div>
        </fieldset>
    {{ Form::close() }}        
    
</div>
@stop