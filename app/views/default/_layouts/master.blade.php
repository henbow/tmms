<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<title>{{{ $app_name }}}</title>

@include($theme.'._partials.assets')

</head>

<body>

<!-- Left side content -->
@include($theme.'._partials.left_sidebar')

<!-- Right side -->
<div id="rightSide">

    <!-- Top fixed navigation -->
    @include($theme.'._partials.topnav')
    @include($theme.'._partials.responsive_menu')
    
    @yield('main')
    
    <!-- Footer line -->
    <div id="footer">
        <div class="wrapper">&copy; {{{ date('Y') }}}. {{{ $company_name }}}</div>
    </div>

</div>

<div class="clear"></div>

</body>
</html>