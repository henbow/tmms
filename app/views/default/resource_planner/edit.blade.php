@extends($theme . '._layouts.master')

@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>

        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="wrapper">

    {{ Notification::showAll() }}

    <!-- Validation form -->
    {{ Form::open(array('route' => array('resource_work_time.update', $resource_id, $specification->id), 'class' => 'form', 'id' => 'validate')) }}
        <fieldset>
            <div class="widget">
                <div class="title">
                    <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
                    <h6>Work Time For {{{ Resource::getName($resource_id) }}}</h6>
                </div>

                <div class="formRow machine">
                    <label>Monday:</label>
                    <div class="formRight"><input type="text" class="validate[required] day" name="mon" id="mon" value="{{{ $specification->mon }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow machine">
                    <label>Tuesday:</label>
                    <div class="formRight"><input type="text" class="validate[required] day" name="tue" id="tue" value="{{{ $specification->tue }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow machine">
                    <label>Wednesday:</label>
                    <div class="formRight"><input type="text" class="validate[required] day" name="wed" id="wed" value="{{{ $specification->wed }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow machine">
                    <label>Thursday:</label>
                    <div class="formRight"><input type="text" class="validate[required] day" name="thu" id="thu" value="{{{ $specification->thu }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow machine">
                    <label>Friday:</label>
                    <div class="formRight"><input type="text" class="validate[required] day" name="fri" id="fri" value="{{{ $specification->fri }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Saturday:</label>
                    <div class="formRight"><input type="text" class="validate[required] day" name="sat" id="sat" value="{{{ $specification->sat }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Sunday:</label>
                    <div class="formRight"><input type="text" class="day" name="sun" id="sun" value="{{{ $specification->sun }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Capacity Per Week:</label>
                    <div class="formRight"><input type="text" class="validate[required] week" name="per_week" id="per_week" value="{{{ $specification->per_week }}}"/></div><div class="clear"></div>
                </div>
                <script type="text/javascript">
                $('input.day').keyup(function(){
                    var sum = 0;
                    $('input.day').each(function() {
                        sum += Number($(this).val());
                    });
                    $('input.week').val(sum);
                });
                </script>
            </div>
            <div class="formSubmit"><input type="submit" value="Set Work Time" class="redB" /></div>
            <div class="clear"></div>
        </fieldset>

    {{ Form::close() }}

</div>
@stop