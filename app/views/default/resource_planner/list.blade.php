@extends($theme . '._layouts.master')

@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>

<div class="statsRow">
    <div class="wrapper">
        <div class="controlB">
            <ul>
                <li><a href="{{{ route('resource.index') }}}" title="Manage resource planification data"><img src="<?php echo asset('assets/'.$theme.'/images/icons/control/32/administrative-docs.png') ?>" alt=""><span>Resource Specification</span></a></li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="line"></div>

<!-- Main content wrapper -->
<div class="wrapper">
    {{ Notification::showAll() }}

    <!-- Static table -->
    <div class="widget">
        <div class="title">
            <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/frames.png') }}}" alt="" class="titleIcon" />
            <h6>Resource Work Plans</h6>
            <a href="{{{ route('resource_planner.create') }}}" title="Update Resource Work Plan" class="button greenB" style="margin: 4px; float: right;">
                <img src="{{{ asset('assets/'.$theme.'/images/icons/color/plus.png') }}}" alt="Update Resource Work Plan" class="icon" style="margin-top: 5px; margin-left: 5px;">
                <span>Update Resource Work Plan</span>
            </a>
        </div>

        <table cellpadding="0" cellspacing="0" class="sTable" width="100%">
            <thead>
                <tr>
                    <td rowspan="3" style="vertical-align: middle; width: 100px;">Code</td>
                    <td rowspan="3" style="vertical-align: middle; min-width: 200px;">Name</td>
                    <td colspan="12">Work Time in {{{ date('Y') }}} (hours)</td>
                </tr>
                <tr>
                    @foreach($months as $month)
                    <td width="5%">{{{ substr($month, 0, 3) }}}</td>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach($resources as $resource)
                <tr>
                    <td align="center">{{{ $resource->code }}}</td>
                    <td align="center">{{{ $resource->name }}}</td>
                    @foreach($months as $key => $month)
                    <td align="center" class="month-select tipN" title="Click for detail" data-resource="{{{ $resource->id }}}" data-month="{{{ $key + 1 }}}" data-year="{{{ date('Y') }}}">
                        {{{ ResourcePlanner::getWorkTimePlanPerMonth($resource->id, $key + 1) }}}
                    </td>
                    @endforeach
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
    <div class="widget" id="work-plan-detail" style="display:none">
        <div class="title">
            <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/frames.png') }}}" alt="" class="titleIcon" />
            <h6>Work Plan For <span id="resource-name" style="font-weight: bold;"></span> in <span id="month-year" style="font-weight: bold;">January 2014</span></h6>
        </div>
        <table cellpadding="0" cellspacing="0" class="sTable" id="week-loader" width="100%">
            <tbody>
                <tr>
                    <td align="center"><img src="{{{ asset('assets/'.$theme.'/images/loaders/loader4.gif') }}}" alt="" style="margin: 5px;"></td>
                </tr>
            </tbody>
        </table>
        <table cellpadding="0" cellspacing="0" class="sTable" width="100%" id="week-table" style="display:none">
            <thead>
                <tr>
                    <td id="header1">Week </td>
                    <td id="header2">Week </td>
                    <td id="header3">Week </td>
                    <td id="header4">Week </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td id="body1" align="center"><a href="#" class="edit tipN"></a></td>
                    <td id="body2" align="center"><a href="#" class="edit tipN"></a></td>
                    <td id="body3" align="center"><a href="#" class="edit tipN"></a></td>
                    <td id="body4" align="center"><a href="#" class="edit tipN"></a></td>
                </tr>
            </tbody>
        </table>
        <script type="text/javascript">
        $(function() {
            $('td.month-select').click(function(){
                var resourceId = $(this).attr('data-resource');
                var month = $(this).attr('data-month');
                var year = $(this).attr('data-year');

                $('#work-plan-detail').fadeIn();
                $('#week-loader').show();
                $('#week-table').hide();

                $.get('{{{ URL::to('/resource_planner/') }}}/'+resourceId+'/'+month+'/'+year+'/work_plan', function(resp){
                    var i = 1;
                    var response = $.parseJSON(resp);

                    // console.log(response);

                    $('#week-loader').fadeOut('fast', function(){$('#week-table').show();});
                    $('#resource-name').text(response.resource);
                    $('#month-year').text(response.month_year);

                    $.each(response.work_time, function(weeknum, worktime) {
                        var idworktime = worktime.split('|');

                        $('#header'+i).text('Week '+weeknum);
                        $('#body'+i+' a').text(idworktime[1]);
                        $('#body'+i+' a').attr('id', idworktime[0]);
                        i++;
                    });
                });
            });
            $('.edit').editable("{{{ route('resource_planner.update_work_time_plan') }}}", {
                indicator : 'Saving...',
                tooltip   : 'Click to edit work time plan for this week',
                callback : function(value, settings) {
                    redirect_to("{{{ route('resource_planner.index') }}}");
                }
            });

            function redirect_to(url) {
                window.location.href = url;
            }
});
</script>
</div>
<?php echo $resources->links(); ?>

</div>
@stop