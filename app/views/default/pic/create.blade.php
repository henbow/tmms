@extends($theme . '._layouts.master')
 
@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="wrapper">
    
    <!-- Note -->
    {{ Notification::showAll() }}
    
    <!-- Validation form -->
    {{ Form::open(array('route' => 'pic.store', 'class' => 'form', 'id' => 'validate')) }}
    	<fieldset>
            <div class="widget">
                <div class="title">
                    <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
                    <h6>PIC Details</h6>
                </div>
                <div class="formRow">
                    <label>Username:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="floatL">
                            <select name="userid" id="userid" class="validate[required]">
                                <option value="">Choose Username</option>
                                @foreach (Sentry::findAllUsers() as $user)
                                <option value="{{{ $user->id }}}">{{{ $user->username }}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Code:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="code" id="code" value=""/></div><div class="clear"></div>
                </div>                
                <div class="formRow">
                    <label>First Name:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="firstname" id="firstname" value=""/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Last Name:</label>
                    <div class="formRight"><input type="text" name="lastname" id="lastname" value=""/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Gender:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="floatL">
                            <select name="gender" id="gender" class="validate[required]">
                                <option value="m">Male</option>
                                <option value="f">Female</option>
                            </select>
                        </div>
                    </div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Department:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="floatL">
                            <select name="department" id="department" class="validate[required]">
                                <option value=""></option>
                                @foreach ($departments as $dept)
                                <option value="{{{ $dept->id }}}">{{{ $dept->name }}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Unit:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="floatL">
                            <select name="unit" id="unit" class="validate[required]">
                                <option value=""></option>
                                @foreach ($units as $unit)
                                <option value="{{{ $unit->id }}}">{{{ $unit->name }}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><div class="clear"></div>
                </div>
                
            </div>
            <div class="formSubmit"><input type="submit" value="Add New PIC" class="redB" /></div>
            <div class="clear"></div>
        </fieldset>
        
    	
    {{ Form::close() }}        
    
</div>
@stop