@extends($theme . '._layouts.master')
 
@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>

<!-- Main content wrapper -->
<div class="wrapper">
    {{ Notification::showAll() }}
    
    <!-- Static table -->
    <div class="widget">
      <div class="title">
        <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/frames.png') }}}" alt="" class="titleIcon" />
        <h6>SMS Number Lists</h6>
        <a href="{{{ route('sms_number.create') }}}" title="Add New SMS Number" class="button greenB" style="margin: 4px; float: right;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/plus.png') }}}" alt="Add User" class="icon" style="margin-top: 5px; margin-left: 5px;"><span>Add New SMS Number</span></a>
      </div>
        <table cellpadding="0" cellspacing="0" width="100%" class="sTable">
            <thead>
                <tr>
                    <td>Recipient</td>
                    <td>SMS/Phone Number</td>
                    <td width="11%">Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($sms_numbers as $sms_number)
                <tr>
                    <td align="center">{{{ ucwords($sms_number->name) }}}</td>
                    <td align="center">{{{ $sms_number->number }}}</td>
                    <td align="center">
                        <a href="{{{ route('sms_number.edit', $sms_number->id) }}}" title="Edit" class="smallButton tipN" style="margin: 5px;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/pencil.png') }}}" alt=""></a>
                        {{ Form::open(array('route' => array('sms_number.destroy', $sms_number->id), 'style' => 'display: inline-block;', 'method' => 'delete', 'onclick' => "if(!confirm('Are you sure to delete?'))return false;")) }}
                        <button type="submit" class="smallButton tipN" style="margin: 5px;" title="Delete"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/cross.png') }}}" alt=""></button>
                        {{ Form::close() }}                        
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
    
</div>
@stop