@extends($theme . '._layouts.master')
 
@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="wrapper">
    
    <!-- Note -->
    {{ Notification::showAll() }}
    
    <!-- Validation form -->
    {{ Form::open(array('route' => 'sms_number.store', 'class' => 'form', 'id' => 'validate')) }}
    	<fieldset>
            <div class="widget">
                <div class="title">
                    <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
                    <h6>SMS Number</h6>
                </div>
                <div class="formRow">
                    <label>Recipient Name:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="recipient" id="recipient"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>SMS/Phone Number:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="number" id="number"/></div><div class="clear"></div>
                </div>
            </div>
            <div class="formSubmit"><input type="submit" value="Add New SMS Number" class="redB" /></div>
            <div class="clear"></div>
        </fieldset>
    {{ Form::close() }}        
    
</div>
@stop