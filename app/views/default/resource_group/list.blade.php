@extends($theme . '._layouts.master')
 
@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="statsRow">
    <div class="wrapper">
    	<div class="controlB">
        	<ul>
                <li><a href="{{{ route('resource.index') }}}" title="Manage resources data"><img src="<?php echo asset('assets/'.$theme.'/images/icons/control/32/administrative-docs.png') ?>" alt=""><span>Resource</span></a></li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="line"></div>

<!-- Main content wrapper -->
<div class="wrapper">
    {{ Notification::showAll() }}
    
    <!-- Static table -->
    <div class="widget">
      <div class="title">
        <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/frames.png') }}}" alt="" class="titleIcon" />
        <h6>Resource Group Lists</h6>
        <a href="{{{ route('resource_group.create') }}}" title="Add New Resource Group" class="button greenB" style="margin: 4px; float: right;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/plus.png') }}}" alt="Add User" class="icon" style="margin-top: 5px; margin-left: 5px;"><span>Add New Resource Group</span></a>
      </div>
        <table cellpadding="0" cellspacing="0" width="100%" class="sTable">
            <thead>
                <tr>
                    <td>Code</td>
                    <td>Name</td>
                    <td width="11%">Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($resource_groups as $res_group)
                <tr>
                    <td align="center">{{{ $res_group->code }}}</td>
                    <td align="center">{{{ $res_group->name }}}</td>
                    <td align="center">
                        <a href="{{{ route('resource_group.edit', $res_group->id) }}}" title="Edit" class="smallButton tipN" style="margin: 5px;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/pencil.png') }}}" alt=""></a>
                        {{ Form::open(array('route' => array('resource_group.destroy', $res_group->id), 'style' => 'display: inline-block;', 'method' => 'delete', 'onclick' => "if(!confirm('Are you sure to delete?'))return false;")) }}
                        <button type="submit" class="smallButton tipN" title="Delete" style="margin: 5px;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/cross.png') }}}" alt=""></button>
                        {{ Form::close() }}                        
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
    
</div>
@stop