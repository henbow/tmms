@extends($theme . '._layouts.master')

@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>

<div class="statsRow">
    <div class="wrapper">
        <div class="controlB">
            <ul>
                <li><a href="{{{ route('resource.index') }}}" title="Manage resource specification data"><img src="<?php echo asset('assets/'.$theme.'/images/icons/control/32/administrative-docs.png') ?>" alt=""><span>Resource Specification</span></a></li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="line"></div>

<!-- Main content wrapper -->
<div class="wrapper">
    {{ Notification::showAll() }}

    <!-- Static table -->
    <div class="widget">
      <div class="title">
        <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/frames.png') }}}" alt="" class="titleIcon" />
        <h6>Resources</h6>
        <a href="{{{ route('resource_spec.create', $resource_id) }}}" title="Add Resource Specification" class="button greenB" style="margin: 4px; float: right;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/plus.png') }}}" alt="Add Resource Specification" class="icon" style="margin-top: 5px; margin-left: 5px;"><span>Add Resource Specification</span></a>
      </div>
        <table cellpadding="0" cellspacing="0" width="100%" class="sTable">
            <thead>
                <tr>
                    <td rowspan="3" style="vertical-align: middle;">Name</td>
                    <td colspan="5" width="40%">Machine</td>
                    <td colspan="2" width="15%">Human</td>
                    <td colspan="2" width="20%">Rate/Hour</td>
                    <td width="10%" rowspan="3" style="vertical-align: middle;">Action</td>
                </tr>
                <tr>
                    <td>Min. Table Load</td>
                    <td>Max. Table Load</td>
                    <td>Table Size</td>
                    <td>Travel Range</td>
                    <td>Accuracy</td>
                    <td>Min Mould Size</td>
                    <td>Max Mould Size</td>
                    <td>Standard</td>
                    <td>Overtime</td>
                </tr>
                <tr>
                    <td>KG</td>
                    <td>KG</td>
                    <td>MM</td>
                    <td>MM</td>
                    <td>MM</td>
                    <td>MM</td>
                    <td>MM</td>
                    <td>RP</td>
                    <td>RP</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($resource_spec as $spec)
                <tr style="{{{ Resource::isOffline($spec->resource_id) ? 'background: #BE1414; color: #FFFFFF !important;' : '' }}}">
                    <td align="center">{{{ Resource::getName($spec->resource_id) }}}</td>
                    <td align="center">{{{ $spec->min_table_load }}}</td>
                    <td align="center">{{{ $spec->max_table_load }}}</td>
                    <td align="center">{{{ $spec->table_size }}}</td>
                    <td align="center">{{{ $spec->travel_range }}}</td>
                    <td align="center">{{{ $spec->accuracy }}}</td>
                    <td align="center">{{{ $spec->min_mould_size }}}</td>
                    <td align="center">{{{ $spec->max_mould_size }}}</td>
                    <td align="center">{{{ $spec->std_rate_per_hour }}}</td>
                    <td align="center">{{{ $spec->ovt_rate_per_hour }}}</td>
                    <td align="center">
                        <a href="{{{ route('resource_spec.edit', array($spec->resource_id, $spec->id)) }}}" title="Edit" class="smallButton tipN" style="margin: 5px;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/pencil.png') }}}" alt=""></a>
                        {{ Form::open(array('route' => array('resource_spec.destroy', $spec->resource_id, $spec->id), 'style' => 'display: inline-block;', 'method' => 'delete', 'onclick' => "if(!confirm('Are you sure to delete?'))return false;")) }}
                        <button type="submit" class="smallButton tipN" title="Delete" style="margin: 5px;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/cross.png') }}}" alt=""></button>
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
    <?php echo $resource_spec->links(); ?>

</div>
@stop