@extends($theme . '._layouts.master')

@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>

        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="wrapper">

    {{ Notification::showAll() }}

    <!-- Validation form -->
    {{ Form::open(array('route' => array('resource_spec.update', $resource_id, $spec->id), 'class' => 'form', 'id' => 'validate')) }}
        <fieldset>
            <div class="widget">
                <div class="title">
                    <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
                    <h6>Resource Specification Data For {{{ Resource::getName($resource_id) }}}</h6>
                </div>

                @if(Resource::isHuman($resource_id))
                <!-- Human -->
                <div class="formRow human">
                    <label>Min Mould Size:</label>
                    <div class="formRight"><input placeholder="in MM" type="text" name="minmouldsize" id="minmouldsize" value="{{{ $spec->min_mould_size }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow human">
                    <label>Max Mould Size:</label>
                    <div class="formRight"><input placeholder="in MM" type="text" name="maxmouldsize" id="maxmouldsize" value="{{{ $spec->max_mould_size }}}"/></div><div class="clear"></div>
                </div>

                @else
                <!-- Machine -->
                <div class="formRow machine">
                    <label>Min Table Load:</label>
                    <div class="formRight"><input placeholder="in KG" type="text" name="mintabload" id="mintabload" value="{{{ $spec->min_table_load }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow machine">
                    <label>Max Table Load:</label>
                    <div class="formRight"><input placeholder="in KG" type="text" name="maxtabload" id="maxtabload" value="{{{ $spec->max_table_load }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow machine">
                    <label>Table Size:</label>
                    <div class="formRight"><input placeholder="in MM" type="text" name="tablesize" id="tablesize" value="{{{ $spec->table_size }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow machine">
                    <label>Travel Range:</label>
                    <div class="formRight"><input placeholder="in MM" type="text" name="travelrange" id="travelrange" value="{{{ $spec->travel_range }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow machine">
                    <label>Accuracy:</label>
                    <div class="formRight"><input placeholder="in MM" type="text" name="accuracy" id="accuracy" value="{{{ $spec->accuracy }}}"/></div><div class="clear"></div>
                </div>
                @endif

                <div class="formRow">
                    <label>Standard Rate/Hour:</label>
                    <div class="formRight"><input type="text" name="stdrate" id="stdrate" value="{{{ $spec->std_rate_per_hour }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Overtime Rate/Hour:</label>
                    <div class="formRight"><input type="text" name="ovtrate" id="ovtrate" value="{{{ $spec->ovt_rate_per_hour }}}"/></div><div class="clear"></div>
                </div>
            </div>
            <div class="formSubmit"><input type="submit" value="Edit Specification" class="redB" /></div>
            <div class="clear"></div>
        </fieldset>

    {{ Form::close() }}

</div>
@stop