@extends($theme . '._layouts.master')

@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>

        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="wrapper">

    {{ Notification::showAll() }}

    <!-- Validation form -->
    {{ Form::open(array('route' => array('resource_spec.update', $resource_id, $spec->id), 'class' => 'form', 'id' => 'validate')) }}
        <fieldset>
            <div class="widget">
                <div class="title">
                    <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
                    <h6>Resource Specification Data For {{{ Resource::getName($resource_id) }}}</h6>
                </div>

                @if(Resource::isHuman($resource_id))
                <!-- Human -->
                <div class="formRow human">
                    <label>Min Mould Size:</label>
                    <div class="formRight">{{{ $spec->min_mould_size }}}</div><div class="clear"></div>
                </div>
                <div class="formRow human">
                    <label>Max Mould Size:</label>
                    <div class="formRight">{{{ $spec->max_mould_size }}}</div><div class="clear"></div>
                </div>

                @else
                <!-- Machine -->
                <div class="formRow machine">
                    <label>Min Table Load:</label>
                    <div class="formRight">{{{ $spec->min_table_load }}}</div><div class="clear"></div>
                </div>
                <div class="formRow machine">
                    <label>Max Table Load:</label>
                    <div class="formRight">{{{ $spec->max_table_load }}}</div><div class="clear"></div>
                </div>
                <div class="formRow machine">
                    <label>Table Size:</label>
                    <div class="formRight">{{{ $spec->table_size }}}</div><div class="clear"></div>
                </div>
                <div class="formRow machine">
                    <label>Travel Range:</label>
                    <div class="formRight">{{{ $spec->travel_range }}}</div><div class="clear"></div>
                </div>
                <div class="formRow machine">
                    <label>Accuracy:</label>
                    <div class="formRight">{{{ $spec->accuracy }}}</div><div class="clear"></div>
                </div>
                @endif

                <div class="formRow">
                    <label>Standard Rate/Hour:</label>
                    <div class="formRight">{{{ $spec->std_rate_per_hour }}}</div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Overtime Rate/Hour:</label>
                    <div class="formRight">{{{ $spec->ovt_rate_per_hour }}}</div><div class="clear"></div>
                </div>
            </div>
            <div class="clear"></div>
        </fieldset>

    {{ Form::close() }}

</div>
@stop