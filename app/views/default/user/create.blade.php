@extends($theme . '._layouts.master')

@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>

        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="wrapper">
    {{ Notification::showAll() }}

    <!-- Validation form -->
    {{ Form::open(array('route' => 'user.store', 'class' => 'form', 'id' => 'validate')) }}
    	<fieldset>
            <div class="widget">
                <div class="title">
                    <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
                    <h6>User Details</h6>
                </div>
                <div class="formRow">
                    <label>User Name:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="username" id="username"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Password:<span class="req">*</span></label>
                    <div class="formRight"><input type="password" class="validate[required]" name="password1" id="password1" /></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Repeat password:<span class="req">*</span></label>
                    <div class="formRight"><input type="password" class="validate[required,equals[password1]]" name="password2" id="password2" /></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>First Name:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="firstname" id="firstname"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Last Name:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="lastname" id="lastname"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Email:</label>
                    <div class="formRight"><input type="text" class="validate[email]" name="email" id="email"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Phone:</label>
                    <div class="formRight"><input type="text" class="" name="phone" id="phone"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Level:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="floatL">
                            <select name="userlevel" id="userlevel" class="validate[required]">
                                <option value=""></option>
                                @foreach (Sentry::findAllGroups() as $group)
                                <option value="{{{ $group->id }}}">{{{ $group->name }}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><div class="clear"></div>
                </div>

            </div>
            <div class="formSubmit"><input type="hidden" name="formtype" value="informations" /><input type="submit" value="Create New User" class="redB" /></div>
            <div class="clear"></div>
        </fieldset>

<!--     	<fieldset> -->
<!--             <div class="widget"> -->
<!--                 <div class="title"><img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" /><h6>User (PIC) Informations</h6></div> -->
<!--                 <div class="formRow"> -->
<!--                     <label>Code:</label> -->
<!--                     <div class="formRight"><input type="text" name="code" id="code"/></div><div class="clear"></div> -->
<!--                 </div> -->

<!--                 <div class="formRow"> -->
<!--                     <label>Gender:</label> -->
<!--                     <div class="formRight"> -->
<!--                         <div class="floatL"> -->
<!--                             <select name="sex" id="sex" class="" > -->
<!--                                 <option value="m">Male</option> -->
<!--                                 <option value="f">Female</option> -->
<!--                             </select> -->
<!--                         </div> -->
<!--                     </div><div class="clear"></div> -->
<!--                 </div> -->
<!--                 <div class="formRow"> -->
<!--                     <label>Department:</label> -->
<!--                     <div class="formRight"> -->
<!--                         <div class="floatL"> -->
<!--                             <select name="department" id="department" class="" > -->
<!--                                 <option value=""></option> -->
<!--                                 @foreach (Department::getAll() as $dept) -->
<!--                                 <option value="{{{ $dept->id }}}">{{{ $dept->name.' ['.$dept->code.']' }}}</option> -->
<!--                                 @endforeach -->
<!--                             </select> -->
<!--                         </div> -->
<!--                     </div><div class="clear"></div> -->
<!--                 </div> -->
<!--                 <div class="formRow"> -->
<!--                     <label>Unit:</label> -->
<!--                     <div class="formRight"> -->
<!--                         <div class="floatL"> -->
<!--                             <select name="unit" id="unit" class="" > -->
<!--                                 <option value=""></option> -->
<!--                                 @foreach (Unit::getAll() as $unit) -->
<!--                                 <option value="{{{ $unit->id }}}">{{{ $unit->name.' ['.$unit->code.']' }}}</option> -->
<!--                                 @endforeach -->
<!--                             </select> -->
<!--                         </div> -->
<!--                     </div><div class="clear"></div> -->
<!--                 </div> -->

<!--             </div> -->

<!--         </fieldset> -->
    {{ Form::close() }}

</div>
@stop