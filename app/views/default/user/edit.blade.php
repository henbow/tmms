@extends($theme . '._layouts.master')
 
@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="wrapper">
    {{ Notification::showAll() }}
        
    <!-- Validation form -->
    {{ Form::model($user, array('method' => 'put', 'class' => 'form', 'id' => 'validate', 'route' => array('user.update', $user->id))) }}
    	<fieldset>
            <div class="widget">
                <div class="title">
                    <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
                    <h6>User Details</h6>
                </div>
                <div class="formRow">
                    <label>User Name:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="username" id="username" value="{{{ $user->username }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Old Password:</label>
                    <div class="formRight"><input type="password" name="password1" id="password1" value="" placeholder="Type old password" /></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Password:</label>
                    <div class="formRight"><input type="password" name="password2" id="password2" value="" placeholder="Type new password" /></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Repeat password:</label>
                    <div class="formRight"><input type="password" class="validate[equals[password2]]" name="password3" id="password3" value="" placeholder="Confirm new password" /></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>First Name:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="firstname" id="firstname" value="{{{ $user->first_name }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Last Name:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="lastname" id="lastname" value="{{{ $user->last_name }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Email:</label>
                    <div class="formRight"><input type="text" class="validate[email]" name="email" id="email" value="{{{ $user->email }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Phone:</label>
                    <div class="formRight"><input type="text" class="" name="phone" id="phone" value="{{{ $user->phone_number }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Level:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="floatL">
                            <select name="userlevel" id="userlevel" class="validate[required]">
                                <option value=""></option>
                                @foreach (Sentry::findAllGroups() as $group)
                                <option {{{ $group_id == $group->id ? 'selected' : '' }}} value="{{{ $group->id }}}">{{{ $group->name }}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><div class="clear"></div>
                </div>
                
            </div>
            <div class="formSubmit"><input type="submit" value="Edit User" class="redB" /></div>
            <div class="clear"></div>
        </fieldset>
        
    	
    {{ Form::close() }}        
    
</div>
@stop