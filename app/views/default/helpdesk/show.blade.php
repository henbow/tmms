@extends($theme . '._layouts.master')

@section('main')
@include($theme.'._partials.breadcrumb')

<?php $font_color = @HelpDeskPriority::find($request->priority_id)->background_color; ?>
<?php $user = \Sentry::getUser($request->request_user_id); ?>
<?php $ic_user_id = HelpDeskNotes::where('reply_to', '<>', '0')->where('reply_type', '<>', 'user')->where('helpdesk_id', '=', $request->id)->take(1)->get(); ?>
<?php $ic_user = isset($ic_user_id[0]) ? \Sentry::findUserById($ic_user_id[0]->reply_by) : ''; ?>

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>

        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>

<div class="wrapper">
    {{ Notification::showAll() }}

    <div class="widget">
        <div class="title">
            <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
            <h6>Request details</h6>
        </div>

        <div class="formRow">
            <table width="100%">
                <tr>
                    <td width="50%">
                        <table width="100%">
                            <tr><td width="20%">Ticket ID</td><td width="2%">:</td><td><strong>{{{ $request->ticket_id }}}</strong></td></tr>
                            <tr><td>Priority</td><td>:</td><td><span style="font-weight:bold;{{{ $font_color ? 'color:'.$font_color:'' }}}">{{{ strtoupper(HelpDeskPriority::find($request->priority_id)->priority) }}}</span></td></tr>
                            <tr><td>Category</td><td>:</td><td>{{{ HelpDeskCategory::find($request->category_id)->category }}}</td></tr>
                            <tr><td>Sub Category</td><td>:</td><td>{{{ strtoupper($request->category_type) }}}</td></tr>
                            <tr><td>Request By</td><td>:</td><td>{{{ $user->first_name . ' ' . $user->last_name }}}</td></tr>
                            <tr><td>Department</td><td>:</td><td>{{{ Department::getName(Pic::where('user_id','=',$request->request_user_id)->get()[0]->dept_id) }}}</td></tr>
                            <tr><td>Subject</td><td>:</td><td>{{{ $request->subject }}}</td></tr>
                            <tr><td>Description</td><td>:</td><td><?php echo HelpDeskNotes::where('reply_to', '=', '0')->where('helpdesk_id', '=', $request->id)->get()[0]->message; ?></td></tr>
                        </table>
                    </td>
                    <td width="50%">
                        <table width="100%">
                            <tr><td width="20%">Process Manager</td><td width="2%">:</td><td>{{{ @\Sentry::findGroupByID($request->incharge_group_id)->name }}}</td></tr>
                            <tr><td>Person In Charge</td><td>:</td><td>{{{ $ic_user ? $ic_user->first_name . ' ' . $ic_user->last_name : '' }}}</td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    {{ Form::open(array('route' => array('helpdesk.add_note', $request->id), 'class' => 'form')) }}
    <div class="widget">
        <div class="title">
            <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
            <h6>Insert a note</h6>
        </div>

        <div class="formRow" style="padding: 5px 10px 10px;">
            <div class="formNote">
                <textarea id="editor" name="newnote" rows="" cols=""></textarea>
            </div><div class="clear"></div>
        </div>
        <div class="formSubmit"><input type="submit" value="Submit" class="redB" /></div>
        <div class="clear"></div>
    </div>
    {{ Form::close() }}

    <div class="widget">
        <div class="title">
            <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
            <h6>History of current request</h6>

        </div>
        <table cellpadding="0" cellspacing="0" width="100%" class="sTable">
            <tbody>
                @foreach($request_notes as $note)
                <tr>
                    <td width="5%">
                        <img width='32' src="{{{ asset('assets/'.$theme.'/images/icons/'.($note->reply_type == 'user' ? 'user-64.png' : 'mechanic-64.png')) }}}" />
                    </td>
                    <td>
                        <strong>{{{ date('d M Y H:m:s', strtotime($note->created_at)) }}}</strong>
                        [{{{ \Sentry::findUserByID($request->request_user_id)->first_name.' '.\Sentry::findUserByID($request->request_user_id)->last_name }}}]<br>
                        <?php echo $note->message; ?>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
</div>
@stop