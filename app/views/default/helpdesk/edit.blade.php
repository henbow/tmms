@extends($theme . '._layouts.master')
 
@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="wrapper">
    
    <!-- Note -->
    {{ Notification::showAll() }}
    
    <!-- Validation form -->
    {{ Form::model($unit, array('method' => 'put', 'class' => 'form', 'id' => 'validate', 'route' => array('unit.update', $unit->id))) }}
    	<fieldset>
            <div class="widget">
                <div class="title">
                    <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
                    <h6>Unit</h6>
                </div>
                <div class="formRow">
                    <label>Code:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="code" id="code" value="{{{ $unit->code }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Name:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="unitname" id="unitname" value="{{{ $unit->name }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Description:</label>
                    <div class="formRight">
                        <textarea name="desc">{{{ $unit->desc }}}</textarea>
                    </div><div class="clear"></div>
                </div>
                
            </div>
            <div class="formSubmit"><input type="hidden" name="formtype" value="informations" /><input type="submit" value="Edit Unit Data" class="redB" /></div>
            <div class="clear"></div>
        </fieldset>
    {{ Form::close() }}        
    
</div>
@stop