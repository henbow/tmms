@extends($theme . '._layouts.master')

@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>

<!-- <div class="statsRow">
    <div class="wrapper">
    	<div class="controlB">
        	<ul>
                <li><a href="{{{ route('pic.index') }}}" title="Manage PIC data"><img src="<?php echo asset('assets/'.$theme.'/images/icons/control/32/administrative-docs.png') ?>" alt=""><span>PIC</span></a></li>
                <li><a href="{{{ route('department.index') }}}" title="Manage Department data"><img src="<?php echo asset('assets/'.$theme.'/images/icons/control/32/administrative-docs.png') ?>" alt=""><span>Department</span></a></li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="line"></div> -->

<!-- Main content wrapper -->
<div class="wrapper">
    {{ Notification::showAll() }}

    <!-- Static table -->
    <div class="widget">
      <div class="title">
        <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/frames.png') }}}" alt="" class="titleIcon" />
        <h6>My Requests</h6>
        <a href="{{{ route('helpdesk.create') }}}" title="Add New Request" class="button greenB" style="margin: 4px; float: right;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/plus.png') }}}" alt="Add User" class="icon" style="margin-top: 5px; margin-left: 5px;"><span>Add New Request</span></a>
      </div>
        <table cellpadding="0" cellspacing="0" width="100%" class="sTable">
            <thead>
                <tr>
                    <td>Ticket ID</td>
                    <td>Entry Date</td>
                    <td>From</td>
                    <td>Subject</td>
                    <td>Status</td>
                    <td>In Charge</td>
                    <td width="11%">Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($requests as $request)
                <?php $background_color = HelpDeskPriority::find($request->priority_id)->background_color; ?>
                <?php $font_color = HelpDeskPriority::find($request->priority_id)->font_color; ?>
                <tr class="tipE" title="Priority is {{{ strtoupper(HelpDeskPriority::find($request->priority_id)->priority) }}}" style="{{{ !$background_color ? '' : "background:".$background_color.";" }}}{{{  !$font_color ? '' : "color:".$font_color.";" }}}">
                    <td align="center" width="10%">{{{ $request->ticket_id }}}</td>
                    <td align="center" width="12%">{{{ date('d M Y H:m', strtotime($request->created_at)) }}}</td>
                    <td align="center" width="12%">{{{ \Sentry::findUserByID($request->request_user_id)->first_name.' '.\Sentry::findUserByID($request->request_user_id)->last_name }}}</td>
                    <td align="center">{{{ $request->subject }}}</td>
                    <td align="center" width="10%">{{{ HelpDeskStatus::find($request->status_id)->status }}}</td>
                    @if($request->incharge_group_id)
                    <td align="center">{{{ \Sentry::findGroupByID($request->incharge_group_id)->name }}}</td>
                    @else
                    <td align="center"></td>
                    @endif
                    <td align="center">
                        <a href="{{{ route('helpdesk.show', $request->id) }}}" title="Show Details" class="smallButton tipN" style="margin: 5px;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/projection-screen.png') }}}" alt=""></a>
                        {{ Form::open(array('route' => array('helpdesk.destroy', $request->id), 'style' => 'display: inline-block;', 'method' => 'delete', 'onclick' => "if(!confirm('Are you sure to delete?'))return false;")) }}
                        <button type="submit" class="smallButton tipN" style="margin: 5px;" title="Cancel Request" ><img src="{{{ asset('assets/'.$theme.'/images/icons/color/cross.png') }}}" alt=""></button>
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>


</div>
@stop