@extends($theme . '._layouts.master')
 
@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="wrapper">
    
    <!-- Note -->
    {{ Notification::showAll() }}
    
    <!-- Validation form -->
    {{ Form::open(array('route' => 'helpdesk.store', 'class' => 'form', 'id' => 'validate')) }}
    	<fieldset>
            <div class="widget">
                <div class="title">
                    <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
                    <h6>Create new request</h6>
                </div>
                <div class="formRow">
                    <label>Ticket ID:<span class="req">*</span></label>
                    <div class="formRight">
                        <strong>{{{ generate_helpdesk_ticket() }}}</strong>
                        <input type="hidden" name="ticket_id" value="{{{ generate_helpdesk_ticket() }}}" />
                    </div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Category:<span class="req">*</span></label>
                    <div class="formRight">
                        <select name="category" id="category" class="validate[required]">
                            <option></option>
                            @foreach($categories as $cat)
                            <option value="{{{ $cat->id }}}">{{{ $cat->category }}}</option>
                            @endforeach
                        </select>
                    </div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Sub Category:</label>
                    <div class="formRight">
                        <select name="subcategory" id="subcategory">
                            <option></option>
                            <option value="mechanic">MECHANIC</option>
                            <option value="electric">ELECTRIC</option>
                        </select>
                    </div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Priority:<span class="req">*</span></label>
                    <div class="formRight">
                        <select name="priority" id="priority" class="validate[required]">
                            @foreach($priorities as $priority)
                            <option {{{ $priority->priority == 'Normal' ? 'selected' : '' }}} value="{{{ $priority->id }}}">{{{ $priority->priority }}}</option>
                            @endforeach
                        </select>
                    </div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Process Manager:<span class="req">*</span></label>
                    <div class="formRight">
                        <select name="in_charge" id="in_charge" class="validate[required]">
                            <option></option>
                            @foreach(\Sentry::findAllGroups() as $group)
                            @if($group->id != 1)
                            <option value="{{{ $group->id }}}">{{{ $group->name }}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Subject:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="title" id="title" value=""/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Description:<span class="req">*</span></label>
                    <div class="formRight">
                        <textarea name="desc" id="editor" rows="5" class="validate[required]"></textarea>
                    </div><div class="clear"></div>
                </div>
                
            </div>
            <div class="formSubmit"><input type="submit" value="Submit New Request" class="redB" /></div>
            <div class="clear"></div>
        </fieldset>
    {{ Form::close() }}        
    
</div>
@stop