<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<title>{{{ $app_name }}}</title>

@include($theme.'._partials.assets')

</head>

<body class="nobg loginPage">

<!-- Top fixed navigation -->
<div class="topNav">
    <div class="wrapper">
        <div class="userNav">
           &nbsp;
        </div>
        <div class="clear"></div>
    </div>
</div>

<!-- Main content wrapper -->
<div class="loginWrapper">
    <div class="loginLogo"><img alt="" src="<?php echo asset('assets/'.$theme.'/images/loginLogo.png'); ?>" /></div>
    <div class="widget">
        <div class="title"><img alt="" src="<?php echo asset('assets/'.$theme.'/images/icons/dark/files.png'); ?>" class="titleIcon" /><h6>Login panel</h6></div>
        <!-- <form action="" id="validate" class="form" method="post"> -->
         
        {{ Form::open(array('route' => 'auth.login.post', 'class' => 'form', 'id' => 'validate')) }}
            <fieldset>
                
                <div class="formRow">                    
                    <label for="login">Username:</label>
                    <div class="loginInput"><input type="text" name="login" class="validate[required]" id="login" /></div>
                   
                    <div class="clear"></div>
                </div>
                
                <div class="formRow">
                    <label for="pass">Password:</label>
                    <div class="loginInput"><input type="password" name="password" class="validate[required]" id="pass" /></div>
                    <div class="clear"></div>
                </div>
                
                <div class="loginControl">
                    <div class="rememberMe"><input type="checkbox" id="remMe" name="remMe" /><label for="remMe">Remember me</label></div>
                    <input type="submit" value="Log me in" class="dredB logMeIn" />
                    <div class="clear"></div>
                </div>
            </fieldset>
        {{ Form::close() }}
    </div>
</div>    

<!-- Footer line -->
<div id="footer">
    <div class="wrapper">&copy; {{{ date('Y') }}}. {{{ $company_name }}}</div>
</div>

</body>
</html>