@extends($theme . '._layouts.master')
 
@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="statsRow">
    <div class="wrapper">
    	<div class="controlB">
        	<ul>
                <li><a href="{{{ route('pic.index') }}}" title="Manage PIC data"><img src="<?php echo asset('assets/'.$theme.'/images/icons/control/32/administrative-docs.png') ?>" alt=""><span>PIC</span></a></li>
                <li><a href="{{{ route('department.index') }}}" title="Manage Department data"><img src="<?php echo asset('assets/'.$theme.'/images/icons/control/32/administrative-docs.png') ?>" alt=""><span>Department</span></a></li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="line"></div>

<!-- Main content wrapper -->
<div class="wrapper">
    {{ Notification::showAll() }}
    
    <!-- Static table -->
    <div class="widget">
      <div class="title">
        <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/frames.png') }}}" alt="" class="titleIcon" />
        <h6>Unit Lists</h6>
        <a href="{{{ route('unit.create') }}}" title="Add New Unit" class="button greenB" style="margin: 4px; float: right;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/plus.png') }}}" alt="Add User" class="icon" style="margin-top: 5px; margin-left: 5px;"><span>Add New Unit</span></a>
      </div>
        <table cellpadding="0" cellspacing="0" width="100%" class="sTable">
            <thead>
                <tr>
                    <td>Code</td>
                    <td>Name</td>
                    <td width="11%">Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($units as $unit)
                <tr>
                    <td>{{{ $unit->code }}}</td>
                    <td align="center">{{{ $unit->name }}}</td>
                    <td align="center">
                        <a href="{{{ route('unit.edit', $unit->id) }}}" title="Edit" class="smallButton tipN" style="margin: 5px;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/pencil.png') }}}" alt=""></a>
                        {{ Form::open(array('route' => array('unit.destroy', $unit->id), 'style' => 'display: inline-block;', 'method' => 'delete', 'onclick' => "if(!confirm('Are you sure to delete?'))return false;")) }}
                        <button type="submit" class="smallButton tipN" style="margin: 5px;" title="Delete"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/cross.png') }}}" alt=""></button>
                        {{ Form::close() }}                        
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
    
</div>
@stop