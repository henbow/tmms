<div id="leftSide">
    <div class="logo"><a href="{{{ route('user.index') }}}"><img src="{{{ asset('assets/'.$theme.'/images/logo.png') }}}" alt="" /></a></div>
    
    <div class="sidebarSep mt0"></div>
    
    <!-- Left navigation -->
    <ul id="menu" class="nav">
        <!-- Home -->
        <li class="dash"><a href="{{{ route('user.index') }}}" <?php echo ( $parent_active == 'index' ? 'class="active exp" id="current"' : 'class="exp"' ) ?> title=""><span>Dashboard</span></a></li>

        <!-- Projects -->
        <li class="forms"><a href="#" title="" <?php echo ( $parent_active == 'project' ? 'class="active exp" id="current"' : 'class="exp"' ) ?>><span>Projects</span><strong></strong></a>
            <ul class="sub">
                <li class="{{{ ( $child_active == 'my_requests' ? 'this' : '' ) }}}"><a href="{{{ route('helpdesk.index') }}}" title="">Projects</a></li>
                <li class="{{{ ( $child_active == 'request_lists' ? 'this' : '' ) }}}"><a href="{{{ route('helpdesk_request.index') }}}" title="">User Requests</a></li>
            </ul>
        </li>

        <!-- Helpdesk -->
        <li class="forms"><a href="#" title="" <?php echo ( $parent_active == 'helpdesk' ? 'class="active exp" id="current"' : 'class="exp"' ) ?>><span>Helpdesk</span><strong></strong></a>
            <ul class="sub">
                <li class="{{{ ( $child_active == 'my_requests' ? 'this' : '' ) }}}"><a href="{{{ route('helpdesk.index') }}}" title="">My Requests</a></li>
                <li class="{{{ ( $child_active == 'request_lists' ? 'this' : '' ) }}}"><a href="{{{ route('helpdesk_request.index') }}}" title="">User Requests</a></li>
            </ul>
        </li>

        <!-- Resource Management -->
        <li class="forms"><a href="#" title="" <?php echo ( $parent_active == 'resource_center' ? 'class="active exp" id="current"' : 'class="exp"' ) ?>><span>Resource Center</span><strong></strong></a>
            <ul class="sub">
                <li class="{{{ ( $child_active == 'resource_planner' ? 'this' : '' ) }}}"><a href="{{{ route('resource_planner.index') }}}" title="">Resource Planner</a></li>
                <li class="{{{ ( $child_active == 'resource_management' ? 'this' : '' ) }}}"><a href="{{{ route('resource.index') }}}" title="">Resource Management</a></li>
            </ul>
        </li>

        <!-- User Management -->
        <li class="forms"><a href="#" title="" <?php echo ( $parent_active == 'user_management' ? 'class="active exp" id="current"' : 'class="exp"' ) ?>><span>User Management</span><strong></strong></a>
            <ul class="sub">
                <li class="{{{ ( $child_active == 'user' ? 'this' : '' ) }}}"><a href="{{{ route('user.index') }}}" title="">Users</a></li>
                <li class="{{{ ( $child_active == 'pic' ? 'this ' : '' ) }}}last"><a href="{{{ route('pic.index') }}}" title="">PIC</a></li>
            </ul>
        </li>

        <!-- Settings -->
        <li class="forms"><a href="#" title="" <?php echo ( $parent_active == 'settings' ? 'class="active exp" id="current"' : 'class="exp"' ) ?>><span>Settings</span><strong></strong></a>
            <ul class="sub">
                <li class="{{{ ( $child_active == 'setting' ? 'this' : '' ) }}}"><a href="{{{ route('setting.index') }}}" title="">Setting</a></li>
                <li class="{{{ ( $child_active == 'sms_numbers' ? 'this ' : '' ) }}}last"><a href="{{{ route('sms_number.index') }}}" title="">SMS Alert Recipients</a></li>
            </ul>
        </li>
    </ul>
</div>
