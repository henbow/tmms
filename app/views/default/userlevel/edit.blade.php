@extends($theme . '._layouts.master')
 
@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="wrapper">
    
    <!-- Note 
    <div class="nNote nInformation hideit">
        <p><strong>INFORMATION: </strong>Form elements were divided into 4 different pages. Don't forget to check all of them!</p>
    </div>
    -->
    {{ Notification::showAll() }}
    
    <!-- Validation form -->
    {{ Form::model($level, array('method' => 'put', 'class' => 'form', 'id' => 'validate', 'route' => array('userlevel.update', $level->id))) }}
    	<fieldset>
            <div class="widget">
                <div class="title">
                    <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
                    <h6>User Details</h6>
                </div>
                <div class="formRow">
                    <label>Level Name:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="levelname" id="levelname" value="{{{ $level->name }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Menu Permissions:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="floatL">
                            <select multiple="multiple" class="multiple" name="permissions[]" id="permissions" class="validate[required] tipN" title="Click to select permissions" style="min-width: 200px">
                                @foreach ($permissions as $permission)
                                <option <?php echo in_array("menu-".$permission->id, array_keys($level->permissions)) ? 'selected="selected"' : ''; ?> value="menu-{{{ $permission->id }}}">{{{ $permission->parent_id == '0' ? '' : '--- ' }}}{{{ ucwords($permission->menu_name) }}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><div class="clear"></div>
                </div>
                
            </div>
            <div class="formSubmit"><input type="hidden" name="formtype" value="informations" /><input type="submit" value="Edit Level" class="redB" /></div>
            <div class="clear"></div>
        </fieldset>
        
    {{ Form::close() }}        
    
</div>
@stop