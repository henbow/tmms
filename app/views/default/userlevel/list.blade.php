@extends($theme . '._layouts.master')

@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="statsRow">
    <div class="wrapper">
    	<div class="controlB">
        	<ul>
        	    <li><a href="{{{ route('pic.index') }}}" title="Manage PIC data"><img src="<?php echo asset('assets/'.$theme.'/images/icons/control/32/administrative-docs.png') ?>" alt=""><span>PIC</span></a></li>
                <li><a href="{{{ route('user.index') }}}" title="Manage user level"><img src="<?php echo asset('assets/'.$theme.'/images/icons/control/32/administrative-docs.png') ?>" alt=""><span>User Management</span></a></li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="line"></div>

<!-- Main content wrapper -->
<div class="wrapper">
    {{ Notification::showAll() }}

    <!-- Static table -->
    <div class="widget">
      <div class="title">
        <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/frames.png') }}}" alt="" class="titleIcon" />
        <h6>User Levels</h6>
        <a href="{{{ route('userlevel.create') }}}" title="Add Level" class="button greenB" style="margin: 4px; float: right;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/plus.png') }}}" alt="Add Level" class="icon" style="margin-top: 5px; margin-left: 5px;"><span>Add Level</span></a>
      </div>
        <table cellpadding="0" cellspacing="0" width="100%" class="sTable">
            <thead>
                <tr>
                    <td align="left" style="/*text-align: left; margin-left:20px;*/">Level</td>
                    <td>Permissions</td>
                    <td width="11%">Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($level_lists as $level)
                <tr>
                    <td align="left">{{{ $level->name }}}</td>
                    <td align="left">
                    <ul style="list-style: disc; margin-left: 10px;">
                    @foreach ($level->permissions as $name => $perm)
                        @if($perm == '1')
                        <?php $permission = explode('-', $name); ?>
                        <?php $menu = Menu::find($permission[1]); ?>
                        <li>{{{ $menu->parent_id == '0' ? '' : '--- ' }}}{{{ ucwords($menu->menu_name) }}}</li>
                        @endif
                    @endforeach
                    </ul>
                    </td>
                    <td align="center">
                        <a href="{{{ route('userlevel.edit', $level->id) }}}" title="Edit" class="smallButton tipN" style="margin: 5px;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/pencil.png') }}}" alt=""></a>
                        {{ Form::open(array('route' => array('userlevel.destroy', $level->id), 'style' => 'display: inline-block;', 'method' => 'delete', 'onclick' => "if(!confirm('Are you sure to delete?'))return false;")) }}
                        <button type="submit" class="smallButton tipN" style="margin: 5px;" title="Delete"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/cross.png') }}}" alt=""></button>
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>


</div>
@stop