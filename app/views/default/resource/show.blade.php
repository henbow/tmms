@extends($theme . '._layouts.master')

@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>

        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>

<div class="wrapper">
    {{ Notification::showAll() }}

    <div class="widget">
        <div class="title">
            <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
            <h6>Resource Information</h6>
        </div>

        <div class="formRow">
            <table width="100%">
                <tr><td width="10%">Code</td><td width="2%">:</td><td>{{{ $resource->code }}}</td></tr>
                <tr><td>Name</td><td>:</td><td>{{{ $resource->name }}}</td></tr>
                <tr><td>Group</td><td>:</td><td>{{{ ResourceGroup::getName($resource->group_id) }}}</td></tr>
                <tr><td>Type</td><td>:</td><td>{{{ $resource->is_human == '1' ? 'HUMAN' : 'MACHINE' }}}</td></tr>
                <tr><td>Offline Status</td><td>:</td><td>{{{ $resource->is_offline == '1' ? 'OFFLINE' : 'ONLINE' }}}</td></tr>
                @if($resource->is_offline == '1')
                <tr><td>Offline Reason</td><td>:</td><td>{{{ $resource->offline_reason }}}</td></tr>
                @endif
            </table>
        </div>
    </div>
    <div class="widget">
        <div class="title">
            <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
            <h6>Specification Data</h6>
            @if(isset($specification->id))
            <a href="{{{ route('resource_spec.edit', array($resource->id, $specification->id)) }}}" title="Edit Specification" class="button greenB" style="margin: 4px; float: right;">
            @else
            <a href="{{{ route('resource_spec.create', $resource->id) }}}" title="Edit Specification" class="button greenB" style="margin: 4px; float: right;">
            @endif
                <span>Edit Specification</span>
            </a>
        </div>
        @if($resource->is_human == '1')
        <table cellpadding="0" cellspacing="0" width="100%" class="sTable">
            <thead>
                <tr>
                    <td colspan="2" width="50%">Specifications</td>
                    <td colspan="2" width="20%">Rate/Hour</td>
                </tr>
                <tr>
                    <td>Min Mould Size (mm)</td>
                    <td>Max Mould Size (mm)</td>
                    <td>Standard (Rp)</td>
                    <td>Overtime (Rp)</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center">{{{ @$specification->min_mould_size }}}</td>
                    <td align="center">{{{ @$specification->max_mould_size }}}</td>
                    <td align="center">{{{ @$specification->std_rate_per_hour }}}</td>
                    <td align="center">{{{ @$specification->ovt_rate_per_hour }}}</td>
                </tr>
            </tbody>
        </table>
        @else
        <table cellpadding="0" cellspacing="0" width="100%" class="sTable">
            <thead>
                <tr>
                    <td colspan="5" width="40%">Specifications</td>
                    <td colspan="2" width="20%">Rate/Hour</td>
                </tr>
                <tr>
                    <td>Min. Table Load (kg)</td>
                    <td>Max. Table Load (kg)</td>
                    <td>Table Size (mm)</td>
                    <td>Travel Range (mm)</td>
                    <td>Accuracy (mm)</td>
                    <td>Standard (Rp)</td>
                    <td>Overtime (Rp)</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center">{{{ @$specification->min_table_load }}}</td>
                    <td align="center">{{{ @$specification->max_table_load }}}</td>
                    <td align="center">{{{ @$specification->table_size }}}</td>
                    <td align="center">{{{ @$specification->travel_range }}}</td>
                    <td align="center">{{{ @$specification->accuracy }}}</td>
                    <td align="center">{{{ @$specification->std_rate_per_hour }}}</td>
                    <td align="center">{{{ @$specification->ovt_rate_per_hour }}}</td>
                </tr>
            </tbody>
        </table>
        @endif
    </div>

    <div class="widget">
        <div class="title">
            <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
            <h6>Work Time</h6>
            @if(isset($work_time->id))
            <a href="{{{ route('resource_work_time.edit', array($resource->id, $work_time->id)) }}}" title="Edit Work Time" class="button greenB" style="margin: 4px; float: right;">
            @else
            <a href="{{{ route('resource_work_time.create', $resource->id) }}}" title="Edit Work Time" class="button greenB" style="margin: 4px; float: right;">
            @endif
                <span>Edit Work Time</span>
            </a>
        </div>
        <table cellpadding="0" cellspacing="0" width="100%" class="sTable">
            <thead>
                <tr>
                    <td colspan="7">Work Week</td>
                    <td rowspan="2" style="vertical-align: middle;">Capacity Per Week</td>
                </tr>
                <tr>
                    <td>Mon</td>
                    <td>Tue</td>
                    <td>Wed</td>
                    <td>Thu</td>
                    <td>Fri</td>
                    <td>Sat</td>
                    <td>Sun</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center">{{{ @$work_time->mon }}}</td>
                    <td align="center">{{{ @$work_time->tue }}}</td>
                    <td align="center">{{{ @$work_time->wed }}}</td>
                    <td align="center">{{{ @$work_time->thu }}}</td>
                    <td align="center">{{{ @$work_time->fri }}}</td>
                    <td align="center">{{{ @$work_time->sat }}}</td>
                    <td align="center">{{{ @$work_time->sun }}}</td>
                    <td align="center">{{{ @$work_time->per_week }}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
</div>
@stop