@extends($theme . '._layouts.master')

@section('main')
<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>

<div class="statsRow">
    <div class="wrapper">
    	<div class="controlB">
        	<ul>
                <li><a href="{{{ route('resource_group.index') }}}" title="Manage resource group"><img src="<?php echo asset('assets/'.$theme.'/images/icons/control/32/administrative-docs.png') ?>" alt=""><span>Resource Group</span></a></li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="line"></div>

<!-- Main content wrapper -->
<div class="wrapper">
    {{ Notification::showAll() }}

    <!-- Static table -->
    <div class="widget">
      <div class="title">
        <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/frames.png') }}}" alt="" class="titleIcon" />
        <h6>Resources</h6>
        <!--<a href="{{{ route('resource.create') }}}" title="Export Resource" class="button blueB" style="margin: 4px; float: right;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/blue-document-excel.png') }}}" alt="Add Resource" class="icon" style="margin-top: 5px; margin-left: 5px;"><span>Export</span></a>-->
        <a href="{{{ route('resource.create') }}}" title="Add Resource" class="button greenB" style="margin: 4px; float: right;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/plus.png') }}}" alt="Add Resource" class="icon" style="margin-top: 5px; margin-left: 5px;"><span>Add Resource</span></a>
      </div>
        <table cellpadding="0" cellspacing="0" width="100%" class="sTable">
            <thead>
                <tr>
                    <td width="8%">Code</td>
                    <td width="30%">Name</td>
                    <td width="10%">Group</td>
                    <td width="10%">Type</td>
                    <td width="20%">Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($resources as $resource)
                <tr style="{{{ $resource->is_offline ? 'background: #BE1414; color: #FFFFFF !important;' : '' }}}">
                    <td align="center">{{{ $resource->code }}}</td>
                    <td align="center">{{{ $resource->name }}}</td>
                    <td align="center">{{{ ResourceGroup::getName($resource->group_id) }}}</td>
                    <td align="center">{{{ $resource->is_human ? 'Human' : 'Machine' }}}</td>
                    <td align="center">
                        <a href="{{{ route('resource.show', $resource->id) }}}" title="Show Details" class="smallButton tipN" style="margin: 5px;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/projection-screen.png') }}}" alt=""></a>
                        <a href="{{{ route('resource.set_offline', $resource->id) }}}" title="{{{ $resource->is_offline ? 'Set Online' : 'Set Offline' }}}" class="smallButton tipN" style="margin: 5px;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/switch.png') }}}" alt=""></a>
                        <a href="{{{ route('resource.edit', $resource->id) }}}" title="Edit" class="smallButton tipN" style="margin: 5px;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/pencil.png') }}}" alt=""></a>
                        {{ Form::open(array('route' => array('resource.destroy', $resource->id), 'style' => 'display: inline-block;', 'method' => 'delete', 'onclick' => "if(!confirm('Are you sure to delete?'))return false;")) }}
                        <button type="submit" class="smallButton tipN" title="Delete" style="margin: 5px;"><img src="{{{ asset('assets/'.$theme.'/images/icons/color/cross.png') }}}" alt=""></button>
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
    <?php echo $resources->links(); ?>

</div>
@stop