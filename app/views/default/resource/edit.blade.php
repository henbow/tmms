@extends($theme . '._layouts.master')
 
@section('main')
@include($theme.'._partials.breadcrumb')

<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>{{{ $page_title }}}</h5>
            <span>{{{ $sub_title }}}</span>
        </div>
        
        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="wrapper">
    
    {{ Notification::showAll() }}
    
    <!-- Validation form -->
    {{ Form::model($resource, array('method' => 'put', 'class' => 'form', 'id' => 'validate', 'route' => array('resource.update', $resource->id))) }}
    	<fieldset>
            <div class="widget">
                <div class="title">
                    <img src="{{{ asset('assets/'.$theme.'/images/icons/dark/alert.png') }}}" alt="" class="titleIcon" />
                    <h6>Resource Data</h6>
                </div>
                <div class="formRow">
                    <label>PIC:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="floatL">
                            <select name="picid" id="picid" class="validate[required]">                                
                                <option value="">Select PIC</option>
                                @foreach($pics as $pic)
                                <option {{{ $resource->pic_id == $pic->id ? 'selected' : '' }}} value="{{{ $pic->id }}}">{{{ ucwords($pic->first_name." ".$pic->last_name) }}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><div class="clear"></div>
                </div>  
                <div class="formRow">
                    <label>Group:<span class="req">*</span></label>
                    <div class="formRight">
                        <div class="floatL">
                            <select name="groupid" id="groupid" class="validate[required]">                                
                                <option value="">Select Resource Group</option>
                                @foreach($groups as $group)
                                <option {{{ $resource->group_id == $group->id ? 'selected' : '' }}} value="{{{ $group->id }}}">{{{ $group->name }}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><div class="clear"></div>
                </div>  
                <div class="formRow">
                    <label>Code:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="code" id="code" value="{{{ $resource->code }}}"/></div><div class="clear"></div>
                </div>
                <div class="formRow">
                    <label>Name:<span class="req">*</span></label>
                    <div class="formRight"><input type="text" class="validate[required]" name="resname" id="resname" value="{{{ $resource->name }}}"/></div><div class="clear"></div>
                </div>   
                <div class="formRow">
                    <label>Human[?]:</label>
                    <div class="formRight"><input {{{ $resource->is_human == '1' ? 'checked' : '' }}} type="checkbox" name="human" id="human" value="1"/></div><div class="clear"></div>
                </div>           
                <div class="formRow">
                    <label>Offline[?]:</label>
                    <div class="formRight"><input {{{ $resource->is_offline == '1' ? 'checked' : '' }}} type="checkbox" name="offline" id="offline" value="1"/></div><div class="clear"></div>
                </div>
                <div class="formRow" id="offline_reason" style="display:none;">
                    <label>Offline Reason:</label>
                    <div class="formRight"><textarea name="offlinereason" id="offlinereason">{{{ $resource->offline_reason }}}</textarea></div><div class="clear"></div>
                </div>
                <script type="text/javascript">
                $( "input[type=checkbox]#offline" ).on( "click", function(){
                    if($(this).is(':checked')) $('#offline_reason').slideDown();
                    else $('#offline_reason').slideUp();
                });
                </script>
            </div>
            <div class="formSubmit"><input type="submit" value="Edit Resource" class="redB" /></div>
            <div class="clear"></div>
        </fieldset>
        
    {{ Form::close() }}        
    
</div>
@stop