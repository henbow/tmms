<link href="{{{ asset('assets/'.$theme.'/css/main.css') }}}" rel="stylesheet" type="text/css">

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/jquery-1.8.3.min.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/spinner/ui.spinner.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/spinner/jquery.mousewheel.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/jquery-ui.min.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/charts/excanvas.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/charts/jquery.sparkline.min.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/forms/uniform.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/forms/jquery.cleditor.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/forms/jquery.validationEngine-en.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/forms/jquery.validationEngine.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/forms/jquery.tagsinput.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/forms/autogrowtextarea.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/forms/jquery.maskedinput.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/forms/jquery.dualListBox.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/forms/jquery.inputlimiter.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/forms/chosen.jquery.min.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/wizard/jquery.form.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/wizard/jquery.validate.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/wizard/jquery.form.wizard.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/uploader/plupload.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/uploader/plupload.html5.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/uploader/plupload.html4.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/uploader/jquery.plupload.queue.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/tables/datatable.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/tables/tablesort.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/tables/resizable.min.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/ui/jquery.tipsy.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/ui/jquery.collapsible.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/ui/jquery.prettyPhoto.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/ui/jquery.progress.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/ui/jquery.timeentry.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/ui/jquery.colorpicker.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/ui/jquery.jgrowl.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/ui/jquery.breadcrumbs.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/ui/jquery.sourcerer.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/calendar.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/plugins/elfinder.min.js') }}}"></script>

<script type="text/javascript" src="http://www.appelsiini.net/download/jquery.jeditable.js"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/js/custom.js') }}}"></script>