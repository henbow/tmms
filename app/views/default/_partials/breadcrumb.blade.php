
<div class="bc">
    <ul id="breadcrumbs" class="breadcrumbs">
      @if(isset($breadcrumb))
         @foreach($breadcrumb as $title => $url)
         <li class="{{{ isset($url[1]) ? 'current' : '' }}}">
              <a href="{{{ $url[0] }}}">{{{ $title }}}</a>
         </li>
         @endforeach
      @endif 
    </ul>
    <div class="clear"></div>    
</div>
