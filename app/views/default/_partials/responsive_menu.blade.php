<div class="resp">
        <div class="respHead">
            <a href="{{{ route('user.index') }}}"><img src="{{{ asset('assets/'.$theme.'/images/logo.png') }}}" alt="" /></a>
        </div>

        <div class="cLine"></div>
        <div class="smalldd">
            <span class="goTo inactive" style="text-align: center">Menu</span>
            <ul class="smallDropdown" style="display: none;">
                @foreach(Menu::where('parent_id', '=', '0')->get() as $parent_menu)
                    <!-- {{{ $parent_menu->menu_name }}} -->
                    <?php $user_info = \Sentry::getUser(); ?>
                    @if($user_info->hasAccess("menu-".$parent_menu->id))
                    <li class="{{{ $parent_menu->class_attr }}}">
                        <a href="{{{ $parent_menu->route == '#' ? $parent_menu->route : route($parent_menu->route) }}}" <?php echo ( $parent_active == $parent_menu->alias ? 'class="active exp" id="current"' : 'class="exp"' ) ?> title="{{{ $parent_menu->menu_name }}}">
                            <span>{{{ $parent_menu->menu_name }}}</span>
                        </a>
                        <?php $child_menu = Menu::where('parent_id', '=', $parent_menu->id); ?>
                        @if($child_menu->count() > 0)
                        <ul class="sub">
                        @foreach($child_menu->get() as $child)
                        @if($user_info->hasAccess("menu-".$child->id))
                        <li class="{{{ ( $child_active == $child->alias ? 'this' : '' ) }}}">
                            <a href="{{{ $child->route == '#' ? $child->route : route($child->route) }}}" title="{{{ $child->menu_name }}}">{{{ $child->menu_name }}}</a>
                        </li>
                        @endif
                        @endforeach
                        </ul>
                        @endif
                    </li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div class="cLine"></div>
    </div>