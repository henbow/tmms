<!-- START modal-lg -->
{{ Form::open(array('route' => array('project.planning.store', $project_id), 'id' => 'new-process', 'class' => 'form-horizontal', 'data-parsley-validate')) }}
<?php $random = mt_rand(0,999); ?>
<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">Add New Master Planning</h3>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Planning</label>
                    <div class="col-sm-6">
                        <input type="text" name="process_group_{{{ $random }}}" class="form-control" required />
                        <input type="hidden" name="group_id" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Parent</label>
                    <div class="col-sm-6">
                        <select name="parent_id" class="form-control">
                            <option></option>
                            @foreach($parent_planning as $parent)
                            <option value="{{{ $parent->id }}}">{{{ $parent->group }}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Start Date</label>
                    <div class="col-sm-6">
                        <input type="text" name="start" id="start" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Finish Date</label>
                    <div class="col-sm-6">
                        <input type="text" name="finish" id="finish" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Percent</label>
                    <div class="col-sm-2">
                        <input type="text" name="percent" id="percent" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Order Num</label>
                    <div class="col-sm-2">
                        <input type="number" name="ordernum" id="ordernum" value="{{{ $order_num }}}" class="form-control" required />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">

    <!-- START Hidden field -->
    <input type="hidden" name="project_id" value="{{{ $project_id }}}" />
    <input type="hidden" name="type" value="{{{ $type }}}" />
    <!--/END Hidden field -->

    <button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
    <button type="submit" class="btn btn-primary"> Save </button>
</div>
{{ Form::close() }}
<script type="text/javascript">
(function(){
    var calculateDays = function(start, finish){
        var startArray = start.split('/');
        var finishArray = finish.split('/');
        var oneDay = 24*60*60*1000;
        var firstDate = new Date(startArray[2],startArray[0],startArray[1]);
        var secondDate = new Date(finishArray[2],finishArray[0],finishArray[1]);

        return Math.round(((secondDate.getTime() - firstDate.getTime())/(oneDay)));
    };
    $("[name=\"process_group_{{{ $random }}}\"]").typeahead({
        source:function (query, group) {
            groups = [];
            map = {};

            <?php $group_data = array(); ?>
            <?php foreach($process_group as $group): ?>
            <?php $group_data[] = array('id' => $group->id, 'group' => $group->group); ?>
            <?php endforeach; ?>

            var data = <?php echo json_encode($group_data); ?>;

            $.each(data, function (i, group) {
                map[group.group] = group;
                groups.push(group.group);
            });

            group(groups);
        },
        updater: function (group) {
            $('[name="group_id"]').val(map[group].id);
            return group;
        }
    });
    $("#start, #finish").datepicker({
        changeMonth: true
        ,changeYear: true
        // @ if($min_date)
        // ,minDate: new Date('{{{ $min_date }}}')
        // @ endif
        // @ if($max_date)
        // ,maxDate: new Date('{{{ $max_date }}}')
        // @ endif
    });
    $('#start').change(function(e){
        var start = $(this).val();
        var finish = $('#finish').val();
        var calcDays = calculateDays(start, finish);
        $('#estimation').val(finish === '' ? 0 : calcDays);
    });
    $('#finish').change(function(e){
        var start = $('#start').val();
        var finish = $(this).val();
        var calcDays = calculateDays(start, finish);
        $('#estimation').val(start === '' ? 0 : calcDays);
    });
})();
</script>
<!--/ END modal-lg