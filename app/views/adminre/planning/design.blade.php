@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatables/css/jquery.datatables.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/dhtmlxgantt/codebase/dhtmlxgantt.css') }}}">
<style type="text/css">
.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }
</style>
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}} [{{{ $sub_title }}}]</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="{{{ route('project.index') }}}">Project List</a></li>
                        <li class="active">Planning {{{ $sub_title }}}</li>
                    </ol>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->
        
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Master Plan</h3>
                    </div>
                    <!--/ panel heading/header -->
                    
                    <!-- panel toolbar wrapper -->
                    
                    <!--/ panel toolbar wrapper -->
    
                    <!-- panel body with collapse capabale -->
                    <div class="panel-body">
                        <div id="gantt_planning" style='width:100%; height:600px;'></div>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
        
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->
    
</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/dhtmlxgantt/codebase/dhtmlxgantt.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/tabletools.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/zeroclipboard.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>
<script type="text/javascript">
(function(){
    gantt.config.xml_date = "%Y-%m-%d";
    gantt.config.scale_unit = "month";
    gantt.config.date_scale = "%F, %Y";
    gantt.config.scale_height = 45;
    gantt.config.grid_width = 500;     
    gantt.config.subscales = [{unit:"week", step:1, date:"%W" }];
    gantt.config.columns = [
        {name:"text_with_link", label:"Process", width:"*", align:"left", tree:true },
        {name:"start_date_formatted", label:"Start", align:"center" },
        {name:"end_date_formatted", label:"End", width:80, align:"center" },
        {name:"lags", label:"Lags", align: "center", width:40 },
        {name:"duration", label:"Est.", align: "center", width:40 },
        {name:"add", label:"", width:44 }
    ];
    gantt.config.grid_resize = true;

    gantt.attachEvent("onAfterTaskAdd", function(id, item){
        $.post('{{{ route('project.planning.store', $project_id) }}}', item, function(resp){
            NProgress.start();
            window.location.href = '{{{ route('project.planning', $project_id) }}}';
        });        
    });

    gantt.attachEvent("onAfterTaskUpdate", function(id, item){
        $.post('{{{ route('project.planning.update') }}}', item, function(resp){
            NProgress.start();
            window.location.href = '{{{ route('project.planning', $project_id) }}}';
        });        
    });

    gantt.attachEvent("onAfterTaskDelete", function(id, item){
        $.post('{{{ route('project.planning.destroy') }}}', {task_id:id}, function(resp){
            NProgress.start();
            window.location.href = '{{{ route('project.planning', $project_id) }}}';
        });
    });

    gantt.init("gantt_planning");
    gantt.load("{{{ route('project.gantt_chart_data', $project_id) }}}", "json");

})();
</script>
@stop