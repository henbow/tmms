@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/selectize/css/selectize.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui-timepicker.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}} [{{{ $sub_title }}}]</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="{{{ route('project.index') }}}">Project List</a></li>
                        <li class="active">{{{ $sub_title }}}</li>
                    </ol>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="table-responsive panel-collapse pull out">
                        <div class="panel-body">
                            <dl class="dl-horizontal" style="padding-bottom: 0;margin-bottom: 0;">
                                <dt style="width:200px;">NOP</dt><dd style="margin-left: 210px;">{{{ $project->nop }}}</dd>
                                <dt style="width:200px;">Project Name</dt><dd style="margin-left: 210px;">{{{ $project->project_name }}}</dd>
                                <dt style="width:200px;">Start Date</dt><dd style="margin-left: 210px;"><a href="javascript:void(0)" id="project-start-date" data-type="combodate" data-template="D-MMM-YYYY" data-format="YYYY-MM-DD HH:mm" data-viewformat="D-MMM-YYYY" data-pk="{{{ $project->id }}}" data-title="Edit Project Start Date">{{{ date('d-M-Y', $project->start_date ? strtotime($project->start_date) : time()) }}}</a></dd>
                                <dt style="width:200px;">First Process Date</dt><dd style="margin-left: 210px;"><a href="javascript:void(0)" id="project-first-date" data-type="combodate" data-template="D-MMM-YYYY" data-format="YYYY-MM-DD HH:mm" data-viewformat="D-MMM-YYYY" data-pk="{{{ $project->id }}}" data-title="Edit Project First Process Date">{{{ date('d-M-Y', $project->trial_date ? strtotime($project->trial_date) : time()) }}}</a></dd>
                                <dt style="width:200px;">Secondary Process Date</dt><dd style="margin-left: 210px;"><a href="javascript:void(0)" id="project-secondary-date" data-type="combodate" data-template="D-MMM-YYYY" data-format="YYYY-MM-DD HH:mm" data-viewformat="D-MMM-YYYY" data-pk="{{{ $project->id }}}" data-title="Edit Project Secondary Process Date">{{{ date('d-M-Y', $project->delivery_date ? strtotime($project->delivery_date) : time()) }}}</a></dd>
                                <dt style="width:200px;">Estimation First Process</dt><dd style="margin-left: 210px;" id="first-process-text">{{{ $estimation_trial }}} Days</dd>
                                <dt style="width:200px;">Estimation Secondary Process</dt><dd style="margin-left: 210px;" id="secondary-process-text">{{{ $estimation_delivery }}} Days</dd>
                                <input type="hidden" name="total_estimation_trial" id="total_estimation_trial" value="{{{ $estimation_trial }}}" />
                                <input type="hidden" name="total_estimation_delivery" id="total_estimation_delivery" value="{{{ $estimation_delivery }}}" />
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- First Process -->
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>First Process</h3>
                    </div>
                    <!--/ panel heading/header -->

                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <div class="panel-toolbar pl10">
                        </div>
                        <div class="panel-toolbar text-right">
                            <a href="javascript:void(0)" class="btn btn-danger mb5 select-template"><i class="ico-calendar"></i> Template Master Plan</a>
                            <a href="javascript:void(0)" class="btn btn-success mb5" id="link-task1"><i class="ico-calendar"></i> Calculate Project Date</a>
                            <a href="{{{ route('project.planning.create', array($project->id, 'first')) }}}" class="btn btn-success mb5 add-planning"><i class="ico-stack2"></i> Add New Planning</a>
                        </div>
                    </div>

                    <!-- panel body with collapse capabale -->
                    <table class="table table-striped table-bordered table-hover" id="table-planning">
                        <thead>
                            <tr>
                                <th class="text-center" width="5%">No</th>
                                <th class="text-center">Process</th>
                                <th class="text-center" width="10%">Percentage (%)</th>
                                <th class="text-center" width="10%">Est (Days)</th>
                                <th class="text-center" width="10%">Lag (Days)</th>
                                <th class="text-center" width="12%">Start</th>
                                <th class="text-center" width="12%">Finish</th>
                                <th class="text-center" width="10%"></th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 11px;">
                            <?php $no = 1; ?>
                            <?php $total_percent = 0; ?>
                            <?php $total_estimation = 0; ?>
                            <?php $total_lags = 0; ?>
                            @foreach ($processes as $process)
                            @if($process->process_type == 'first')
                            <?php $has_child = ProjectMasterPlan::where('parent_id', '=', $process->id)->count(); ?>
                            <tr style="{{{ $process->parent_id ? '' : 'font-weight:bold;' }}}">
                                <td align="center">{{{ $no }}}</td>
                                <td style="{{{ $process->parent_id ? 'font-size:9px;' : '' }}}" align="left">
                                    <a href="{{{ route('project.planning.detail', array($process->process_group_id, $process->id)) }}}" class="text-primary">
                                        {{{ $process->parent_id ? '&nbsp;&#8627;&nbsp;' . $process->group : $process->group }}}
                                    </a>
                                </td>
                                <td align="center">
                                    @if($has_child)
                                    {{{ $process->percent ? $process->percent : 0 }}}
                                    @else
                                    <a href="#" data-type="text" data-process-type="first" data-name="percentage" data-pk="{{{ $process->id }}}" data-title="Edit Percentage" class="edit_percent">
                                        {{{ $process->percent ? $process->percent : 0 }}}
                                    </a>
                                    @endif
                                </td>
                                <td align="center">{{{ number_format($process->real_estimation, 2, '.', '') }}}</td>
                                <!-- <td align="center">{{{ $process->real_estimation }}}</td> -->
                                <td align="center">{{{ $process->lag }}}</td>
                                <td align="center">{{{ $process->start_date == '0000-00-00 00:00:00' ? '-' : date('d-M-Y H:i', strtotime($process->start_date)) }}}</td>
                                <td align="center">{{{ $process->finish_date == '0000-00-00 00:00:00' ? '-' : date('d-M-Y H:i', strtotime($process->finish_date)) }}}</td>
                                <td align="center">
                                    <div class="toolbar">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-sm btn-default">Action</button>
                                            <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="{{{ route('project.planning.edit', array($project->id, $process->id, 'first')) }}}" class="edit-planning"><i class="ico-stack2"></i>&nbsp;&nbsp;Edit Planning</a></li>
                                                <li class="divider"></li>
                                                <li><a href="{{{ route('project.planning.detail', array($process->process_group_id, $process->id)) }}}" class="text-primary"><i class="ico-calendar5"></i>&nbsp;&nbsp;Detail Planning</a></li>
                                                <li class="divider"></li>
                                                <li><a href="{{{ route('project.planning.destroy', array($project->id, $process->id)) }}}" class="text-danger delete-confirm"><i class="ico-remove2"></i>&nbsp;&nbsp;Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php $no++; ?>
                            @if(!$has_child)
                            <?php $total_percent += @$process->percent; ?>
                            <?php $total_estimation += $process->real_estimation; ?>
                            @endif
                            @endif
                            <?php $total_lags += $process->lag; ?>
                            @endforeach
                            <tr style="font-weight:bold;font-size:14px;">
                                <td align="left" colspan="2"><strong>Sub Total</strong></td>
                                <td align="center">{{{ $total_percent }}}</td>
                                <td align="center">{{{ round($total_estimation, 2) }}} </td>
                                <td align="center">{{{ $total_lags }}}</td>
                                <td align="center"></td>
                                <td align="center"></td>
                                <td align="center"></td>
                            </tr>
                        </tbody>
                    </table>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
        <!--/ First Process -->

        <!-- Secondary Process -->
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Secondary Process</h3>
                    </div>
                    <!--/ panel heading/header -->

                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <div class="panel-toolbar pl10">
                        </div>
                        <div class="panel-toolbar text-right">
                            <a href="javascript:void(0)" class="btn btn-success mb5" id="link-task2"><i class="ico-calendar"></i> Calculate Project Date</a>
                            <a href="{{{ route('project.planning.create', array($project->id, 'secondary')) }}}" class="btn btn-success mb5 add-planning"><i class="ico-stack2"></i> Add New Planning</a>
                        </div>
                    </div>

                    <!-- panel body with collapse capabale -->
                    <table class="table table-striped table-bordered table-hover" id="table-planning">
                        <thead>
                            <tr>
                                <th class="text-center" width="5%">No</th>
                                <th class="text-center">Process</th>
                                <th class="text-center" width="10%">Percentage (%)</th>
                                <th class="text-center" width="10%">Est (Days)</th>
                                <th class="text-center" width="10%">Lag (Days)</th>
                                <th class="text-center" width="12%">Start</th>
                                <th class="text-center" width="12%">Finish</th>
                                <th class="text-center" width="10%"></th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 11px;">
                            <?php $no = 1; ?>
                            <?php $total_percent = 0; ?>
                            <?php $total_estimation = 0; ?>
                            <?php $total_lags = 0; ?>
                            @foreach ($processes as $process)
                            @if($process->process_type == 'secondary')
                            <?php $has_child = ProjectMasterPlan::where('parent_id', '=', $process->id)->count(); ?>
                            <tr style="{{{ $process->parent_id ? '' : 'font-weight:bold;' }}}">
                                <td align="center">{{{ $no }}}</td>
                                <td style="{{{ $process->parent_id ? 'font-size:9px;' : '' }}}" align="left">
                                    <a href="{{{ route('project.planning.detail', array($process->process_group_id, $process->id)) }}}" class="text-primary">
                                        {{{ $process->parent_id ? '&nbsp;&#8627;&nbsp;' . $process->group : $process->group }}}
                                    </a>
                                </td>
                                <td align="center">
                                    @if($has_child)
                                    {{{ $process->percent ? $process->percent : 0 }}}
                                    @else
                                    <a href="#" data-type="text" data-process-type="secondary" data-name="percentage" data-pk="{{{ $process->id }}}" data-title="Edit Percentage" class="edit_percent">
                                        {{{ $process->percent ? $process->percent : 0 }}}
                                    </a>
                                    @endif
                                </td>
                                <td align="center">{{{ number_format($process->real_estimation, 2, '.', '') }}}</td>
                                <td align="center">{{{ $process->lag }}}</td>
                                <td align="center">{{{ $process->start_date == '0000-00-00 00:00:00' ? '-' : date('d-M-Y H:i', strtotime($process->start_date)) }}}</td>
                                <td align="center">{{{ $process->finish_date == '0000-00-00 00:00:00' ? '-' : date('d-M-Y H:i', strtotime($process->finish_date)) }}}</td>
                                <td align="center">
                                    <div class="toolbar">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-sm btn-default">Action</button>
                                            <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="{{{ route('project.planning.edit', array($project->id, $process->id, 'secondary')) }}}" class="edit-planning"><i class="ico-stack2"></i>&nbsp;&nbsp;Edit Planning</a></li>
                                                <li class="divider"></li>
                                                <li><a href="{{{ route('project.planning.detail', array($process->process_group_id, $process->id)) }}}" class="text-primary"><i class="ico-calendar5"></i>&nbsp;&nbsp;Detail Planning</a></li>
                                                <li class="divider"></li>
                                                <li><a href="{{{ route('project.planning.destroy', array($project->id, $process->id)) }}}" class="text-danger delete-confirm"><i class="ico-remove2"></i>&nbsp;&nbsp;Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php $no++; ?>
                            @if(!$has_child)
                            <?php $total_percent += @$process->percent; ?>
                            <?php $total_estimation += $process->real_estimation; ?>
                            @endif
                            @endif
                            <?php $total_lags += $process->lag; ?>
                            @endforeach
                            <tr style="font-weight:bold;font-size:14px;">
                                <td align="left" colspan="2"><strong>Sub Total</strong></td>
                                <td align="center">{{{ $total_percent }}}</td>
                                <td align="center">{{{ round($total_estimation,1) }}} </td>
                                <td align="center">{{{ $total_lags }}}</td>
                                <td align="center"></td>
                                <td align="center"></td>
                                <td align="center"></td>
                            </tr>
                        </tbody>
                    </table>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
        <!--/ Secondary Process-->

    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

    <!-- START modal-lg -->
    <div id="bs-modal-lg" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content"></div>
        </div>
    </div>
    <!--/ END modal-lg -->
</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-timepicker.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-touch.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/xeditable/js/bootstrap-editable.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/typeahead/bootstrap3-typeahead.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/element.js') }}}"></script>
<script type="text/javascript">
(function () {
    $('.select-template').click(function(e){
        $('#bs-modal-lg .modal-content').load('{{{ route('project.planning.template', $project->id) }}}',function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $('.add-planning').click(function(e){
        e.preventDefault();
        $('#bs-modal-lg .modal-content').load($(this).attr('href'),function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $('.add-sub-planning').click(function(e){
        e.preventDefault();
        $('#bs-modal-lg .modal-content').load($(this).attr('href'),function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $('.edit-planning').click(function(e){
        e.preventDefault();
        $('#bs-modal-lg .modal-content').load($(this).attr('href'), function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $(".delete-confirm").on("click", function (event) {
        var self = $(this);
        bootbox.confirm("Are you sure to delete this planning?", function (result) {
            if(result){
                NProgress.start();

                if(NProgress.done()) window.location.href = self.attr('href');
            }
        });
        event.preventDefault();
    });
    $('.edit_percent').editable({
        url: "{{{ route('project.planning.edit_percentage', $project->id) }}}",
        params: {estimation: $('#total_estimation').val(), process_type: $(this).data('process-type')},
        title: "Edit Percentage"
    });
    $('.edit_percent').on('save', function(e, params){
        location.reload();
    });
    $('#project-start-date').editable({
        url: "{{{ route('project.planning.edit_project_date') }}}",
        params: {type: "start-date"},
        title: "Edit Project Start Date"
    });
    $('#project-start-date').on('save', function(e, params){
        location.reload();
    });
    $('#project-first-date').editable({
        url: "{{{ route('project.planning.edit_project_date') }}}",
        params: {type: "first-date"},
        title: "Edit Project First Process Date"
    });
    $('#project-first-date').on('save', function(e, params){
        location.reload();
    });
    $('#project-secondary-date').editable({
        url: "{{{ route('project.planning.edit_project_date') }}}",
        params: {type: "secondary-date"},
        title: "Edit Project Secondary Process Date"
    });
    $('#project-secondary-date').on('save', function(e, params){
        location.reload();
    });
    $('#link-task1').click(function(e){
        NProgress.start();
        window.location.href = '{{{ route('project.planning.refresh_date', array($project->id, 'first')) }}}';
    });
    $('#link-task2').click(function(e){
        NProgress.start();
        window.location.href = '{{{ route('project.planning.refresh_date', array($project->id, 'secondary')) }}}';
    });
})();
</script>
@stop