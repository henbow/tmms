<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">Select Template Master Plan</h3>
</div>
<div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Template List</h3>
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                    </div>
                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-striped table-bordered table-hover" id="master-plan-header" style="font-size: 10px">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">No</th>
                                    <th class="text-center">Template Name</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1; ?>
                            @foreach($templates as $template)
                                <tr class="tpl-header" id="tpl-{{{ $template->id }}}" style="cursor:pointer;">
                                    <td class="text-center">{{{ $no++ }}}</td>
                                    <td>{{{ $template->name }}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
                
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>First Process</h3>
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                    </div>
                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-striped table-bordered table-hover" id="master-plan-detail-first" style="font-size: 10px">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">No</th>
                                    <th class="text-center">Process Planning</th>
                                    <th class="text-center">Percentage</th>
                                </tr>
                            </thead>
                            <tbody><!-- Template Content --></tbody>
                        </table>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
                
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Secondary Process</h3>
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                    </div>
                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-striped table-bordered table-hover" id="master-plan-detail-secondary" style="font-size: 10px">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">No</th>
                                    <th class="text-center">Process Planning</th>
                                    <th class="text-center">Percentage</th>
                                </tr>
                            </thead>
                            <tbody><!-- Template Content --></tbody>
                        </table>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
            
            <div class="col-md-12 text-right">
            {{ Form::open(array('route' => array('project.planning.from_template', $project_id), 'id' => 'from-template', 'class' => 'form-horizontal', 'data-parsley-validate')) }}
                <input type="hidden" name="template-id" value="" />
                <button type="submit" class="btn btn-success mb5"> Select Template </button>
            {{ Form::close() }}
            </div>
        </div>
    </div>
    
    <!--/ END Template Container -->
</div>
<script>
(function(){
    $('#master-plan-header tr.tpl-header').click(function(e){
        var idAttr = $(this).attr('id').split('-');
        var tplId = idAttr[1];
        $('[name="template-id"]').val(tplId);
        $('#master-plan-detail-first tbody').empty();
        $('#master-plan-detail-secondary tbody').empty();
        $.post('{{{ route('project.planning.template.detail') }}}', {tid:tplId}, function(resp){
            var tpldata = $.parseJSON(resp);
            $.each(tpldata, function(i, data){
                if (data.process_type == 'first') {
                    var item = '<tr>'+
                        '<td class="text-center">'+(i+1)+'</td>'+
                        '<td class="text-left">'+(data.parent_id != '0' ? '&#8627; ' : '')+data.group+'</td>'+
                        '<td class="text-center">'+data.percent+'</td>'+
                        '</tr>';
                    $('#master-plan-detail-first tbody').append(item);
                } else {
                    var item = '<tr>'+
                        '<td class="text-center">'+(i+1)+'</td>'+
                        '<td class="text-left">'+(data.parent_id != '0' ? '&#8627; ' : '')+data.group+'</td>'+
                        '<td class="text-center">'+data.percent+'</td>'+
                        '</tr>';
                    $('#master-plan-detail-secondary tbody').append(item);
                }                
            });
        });
    });
})();
</script>