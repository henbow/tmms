<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">New Process</h3>
</div>
<div class="modal-body">
    <div class="col-md-12">
        {{ Form::open(array('route' => 'pic.store', 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate')) }}
            <div class="panel-heading">
                <h3 class="panel-title">PIC Details</h3>
            </div>               
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">User Name</label>
                    <div class="col-sm-6">
                        <select name="userid" id="userid" class="form-control" parsley-required="true">
                            <option value="">Choose Username</option>
                            @foreach (Sentry::findAllUsers() as $user)
                            <option value="{{{ $user->id }}}">{{{ $user->username }}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">PIC Code</label>
                    <div class="col-sm-6">
                        <input type="text" name="code" id="code" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">First Name</label>
                    <div class="col-sm-6">
                        <input type="text" name="firstname" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Last Name</label>
                    <div class="col-sm-6">
                        <input type="text" name="lastname" class="form-control" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Gender</label>
                    <div class="col-sm-6">
                        <select name="gender" id="gender" class="form-control">
                            <option value="m">Male</option>
                            <option value="f">Female</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Department</label>
                    <div class="col-sm-6">
                        <select name="department" id="department" class="form-control" parsley-required="true">
                            <option value=""></option>
                            @foreach ($departments as $dept)
                            <option value="{{{ $dept->id }}}">{{{ $dept->name }}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Unit</label>
                    <div class="col-sm-6">
                        <select name="unit" id="unit" class="form-control" parsley-required="true">
                            <option value=""></option>
                            @foreach ($units as $unit)
                            <option value="{{{ $unit->id }}}">{{{ $unit->name }}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Create New PIC</button>
            </div>
        {{ Form::close() }}
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>