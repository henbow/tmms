@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatables/css/jquery.datatables.min.css') }}}">
<style type="text/css">
.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }
</style>
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="{{{ route('project.filter') }}}">Filter Projects</a></li>
                        <li class="active">{{{ $sub_title }}}</li>
                    </ol>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel body with collapse capabale -->
                    <div class="panel-collapse pull out">
                        <div class="panel-body">
                            <dl class="dl-horizontal" style="padding-bottom: 0;margin-bottom: 0;">
                                <dt style="width:100px;">NOP</dt><dd style="margin-left: 120px;">{{{ $project->nop }}}</dd>
                                <dt style="width:100px;">Project Name</dt><dd style="margin-left: 120px;">{{{ $project->project_name }}}</dd>
                                <dt style="width:100px;">Order Num</dt><dd style="margin-left: 120px;">{{{ $project->order_num }}}</dd>
                                <dt style="width:100px;">Customer</dt><dd style="margin-left: 120px;">{{{ (strlen(trim($project->customer_code)) > 0 ? $project->customer_code.' - ' : '') . $project->customer_name }}}</dd>
                            </dl>
                        </div>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
                <!--/ END panel -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Project BOM</h3>
                    </div>
                    <!--/ panel heading/header -->
                    
                    {{ Form::open(array('route' => array('project.select_bom_for_process.post', $project->id), 'class' => 'form-horizontal')) }}
                    <!-- panel toolbar wrapper -->
                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <div class="panel-toolbar text-right">
                            <button type="submit" id="process_schedule" class="btn btn-danger mb5" style="font-weight: bold;"><i class="ico-calculate"></i> SAVE </button>
                        </div>
                    </div>
                    <!--/ panel toolbar wrapper -->

                    <!-- panel body with collapse capabale -->
                    <table class="table table-striped table-bordered table-hover" id="ajax-source">
                        <thead>
                            <tr>
                                <th class="text-center">Barcode</th>
                                <th class="text-center">Part Name</th>
                                <th class="text-center">Type</th>
                                <th class="text-center">Standard</th>
                                <th class="text-center">Dimension</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">UOM</th>
                                <th class="text-center">Process [?]</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 11px"></tbody>
                    </table>
                    <!--/ panel body with collapse capabale -->
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->
    
</section>

<!-- START modal-lg -->
<div id="bs-modal-lg" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<!--/ END modal-lg -->
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/tabletools.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/zeroclipboard.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>
<script type="text/javascript">
(function(){
    // $('#bs-modal-lg').on('hidden.bs.modal', function (e) {
    //     $('.modal-content').empty();
    // })

    $("#ajax-source").dataTable({
        // "sDom": "<'row'<'toolbar_button'T><'toolbar_paging'l><'toolbar_search'f>><'table-responsive'rt><'row'<'col-sm-6'p><'col-sm-6'i>>",
        "sDom": "<'row'<'col-sm-6'Tl><'col-sm-6'f>><'table-responsive'rt><'row'<'col-sm-6'p><'col-sm-6'i>>",
        "oTableTools": {
            "sSwfPath": "{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/swf/copy_csv_xls_pdf.swf') }}}",
            "aButtons": [
                "print",
                "pdf",
                "csv"
            ]
        },
        "aoColumns": [
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false }
        ],
        "iDisplayLength": 250,
        "bProcessing": true,
        "sAjaxSource": "{{{ route('project.bom_data') }}}",
        "sServerMethod": "POST",
        "fnServerParams": function ( aoData ) {
            aoData.push({ "name": "project_id", "value": "{{{$project->id}}}" });
            aoData.push({ "name": "mode", "value": "{{{ isset($mode) ? $mode : 'view' }}}" });
        }
    });

    $('.select_all').click(function(){
        if ($(this).attr('checked')) {
            $('.material_id_value').attr('checked', 'checked');
        } else {
            $('.material_id_value').removeAttr('checked');
        }
    });
})();
</script>
@stop