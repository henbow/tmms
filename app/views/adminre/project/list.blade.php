@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatables/css/jquery.datatables.min.css') }}}">
<style type="text/css">
.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }
</style>
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
            <!-- Toolbar -->

            <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Notification::showAll() }}
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>{{{ $sub_title }}}</h3>
                    </div>
                    @if(isset($filtered))
                        @if($filtered == 0)
                            {{ Form::open(array('route' => array('project.filter.do'), 'class' => 'form-horizontal filter-update-form')) }}
                            <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                                <div class="panel-toolbar text-right">
                                    <button type="submit" id="filter-btn" class="btn btn-primary mb5 action-btn" style="font-weight: bold;">Filter Project</button>
                                    <button type="submit" id="update-btn" class="btn btn-primary mb5 action-btn" style="font-weight: bold;">Update Project</button>
                                    <input type="hidden" name="action-mode" value="filter" />
                                </div>
                            </div>
                        @endif
                    @endif
                    <!--/ panel heading/header -->

                    <!-- panel body with collapse capabale -->
                    <table class="table table-striped table-bordered table-hover" id="ajax-source">
                        <thead>
                            <tr>
                                <th class="text-center" width="10%">NOP</th>
                                <th class="text-center" width="5%">Order</th>
                                <th class="text-center">Project</th>
                                <th class="text-center">Customer</th>
                                @if(isset($filtered))
                                    @if($filtered == 1)
                                    <th class="text-center">Start</th>
                                    <th class="text-center">First Process</th>
                                    <th class="text-center">Secondary Process</th>
                                    <th class="text-center">Planned[?]</th>
                                    @endif
                                    @if($filtered == 0)
                                    <th class="text-center" width="5%">Filter[?]</th>
                                    <th class="text-center" width="5%">Update[?]</th>
                                    @endif
                                @endif
                                <th class="text-center" width="5%">Action</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 10px"></tbody>
                    </table>
                    @if(isset($filtered))
                        @if($filtered == 0)
                            {{ Form::close() }}
                        @endif
                    @endif
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>

<!-- START modal-lg -->
<div id="bs-modal-lg" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<!--/ END modal-lg -->
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/tabletools.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/zeroclipboard.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/gritter/js/jquery.gritter.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript">
(function(){
    $('button.action-btn').click(function(e){
        e.preventDefault();
        if($(this).attr('id') == 'update-btn') {
            $('[name="action-mode"]').val('update');
        } else {
            $('[name="action-mode"]').val('filter');
        }
        $(this).parents('form').submit();
    });
    var table = $("#ajax-source").dataTable({
        "sDom": "<'row'<'col-sm-6'Tl><'col-sm-6'f>><'table-responsive'rt><'row'<'col-sm-6'p><'col-sm-6'i>>",
        "oTableTools": {
            "sSwfPath": "{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/swf/copy_csv_xls_pdf.swf') }}}",
            "aButtons": [
                "print",
                "pdf",
                "csv"
            ]
        },
        "aoColumns": [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            @if(isset($filtered))
            @if($filtered == 1)
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            @endif
            @if($filtered == 0)
            { "sClass": "text-center", "bSearchable": false, "bSortable": false },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false },
            @endif
            @endif
            { "sClass": "text-center" }
        ],
        "iDisplayLength": 25,
        "bProcessing": true,
        "sAjaxSource": "{{{ route('project.get_data') }}}",
        "sServerMethod": "POST",
        "fnServerParams": function ( aoData ) {
            aoData.push({ "name": "filtered", "value": {{{ isset($filtered) ? $filtered ? 1 : 0 : 0 }}} });
            aoData.push({ "name": "mode", "value": "{{{ isset($mode) ? $mode : '' }}}" });
        },
        // "fnServerData": fnDataTablesPipeline
    });
    table.fnSort( [ [0,'desc'],[1,'asc'] ] );
})();
function do_redirect(url)
{
    window.location.href = url;
}
function do_planning(elem)
{
    var self = $(elem);
    bootbox.dialog({
        message: "Link Start/Finish date from?",
        title: "Start/Finish Date Linking",
        buttons: {
            trial: {
                label: "Trial",
                className: "btn-success",
                callback: function () {
                    NProgress.start();
                    window.location.href= self.attr('data-trial-url');
                }
            },
            delivery: {
                label: "Delivery",
                className: "btn-primary",
                callback: function () {
                    NProgress.start();
                    window.location.href= self.attr('data-delivery-url');
                }
            }
        }
    });
}
</script>
@stop
