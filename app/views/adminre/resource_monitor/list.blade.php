@extends($theme.'._layouts.master')

@section('additional_css')
<meta http-equiv="refresh" content="1800" />
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatables/css/jquery.datatables.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/treegrid/css/jquery.treegrid.css') }}}">
<style type="text/css">
.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }
</style>
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="{{{ route('dashboard.index') }}}">Dashboard</a></li>
                        <li class="active">Resource Monitor</li>
                    </ol>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Notification::showAll() }}

                <ul class="nav nav-tabs">
                    <li class="active"><a href="#inplan" data-toggle="tab">In Plan</a></li>
                    <li><a href="#outplan" data-toggle="tab">Out Plan</a></li>
                </ul>

                <div class="tab-content panel">
                    <div class="tab-pane active" id="inplan">
                        <!-- START panel -->
                        <div class="panel panel-primary">
                            <!-- panel heading/header -->
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>In Plan Resources</h3>
                                <!-- panel toolbar -->
                                <div class="panel-toolbar text-right">
                                    <!-- option -->
                                    <div class="option">
                                        <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                        <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                                    </div>
                                    <!--/ option -->
                                </div>
                                <!--/ panel toolbar -->
                            </div>
                            <!--/ panel heading/header -->

                            <!-- panel body with collapse capabale -->
                            <div class="table-responsive panel-collapse pull out">
                                <table class="table table-striped table-hover tree" id="in-resources-table">
                                    <thead>
                                        <tr>
                                            <th class="text-left"></th>
                                            <th class="text-left">Code</th>
                                            <th class="text-center">Name</th>
                                            <th class="text-center">Group</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size: 10px;">
                                        @foreach ($in_resources as $resource)
                                            <?php $actualing = $resource->actualing()->count(); ?>
                                            @if($resource->id != '43')
                                                <tr title="{{{ $resource->is_offline ? 'OFFLINE' : '' }}}" class="treegrid-{{{ md5($resource->id) }}} mp-{{ $resource->id }} {{{ $resource->is_offline ? 'danger text-danger' : ($actualing ? 'info text-primary' : 'success text-success') }}}" style="font-weight: bold; font-size: 12px;">
                                                    <td class="text-left"></td>
                                                    <td class="text-left">{{{ $resource->code }}}</td>
                                                    <td class="text-center">{{{ strtoupper($resource->name) }}}</td>
                                                    <td class="text-center">{{{ $resource->group->name }}}</td>
                                                    <td class="text-center">{{{ $resource->is_offline ? 'OFFLINE' : ($actualing ? 'RUNNING' : 'IDLE') }}}</td>
                                                </tr>

                                                <?php $actualing_details = Resource::find($resource->id)->actualing()->where('status','<>','COMPLETE')->orderBy('updated_at', 'desc')->limit(1)->get(); ?>
                                                @if(@$actualing_details[0]->planning->process->group->type == 'nop')
                                                {{-- BY NOP --}}
                                                <tr class="treegrid-parent-{{{ md5($resource->id) }}} {{{ $resource->is_offline ? 'danger' : ($actualing ? '' : 'success') }}}" style="font-weight: bold; font-size: 11px;">
                                                    <td class="text-center">NOP</td>
                                                    <td class="text-center">Project</td>
                                                    <td class="text-center">Process</td>
                                                    <td class="text-center">PIC</td>
                                                    <td class="text-center">Start Date</td>
                                                </tr>
                                                @foreach ($actualing_details as $detail)
                                                    @if(@$detail->planning->masterplan->project['nop'])
                                                        <tr class="treegrid-{{{ $detail->id }}} treegrid-parent-{{{ md5($resource->id) }}} {{{ $resource->is_offline ? 'danger' : ($actualing ? '' : 'success') }}}">
                                                            <td class="text-center">{{{ $detail->planning->masterplan->project['nop'] }}}</td>
                                                            <td class="text-left">{{{ $detail->planning->masterplan->project['project_name'] }}}</td>
                                                            <td class="text-center">{{{ $detail->planning->process->process }}}</td>
                                                            <td class="text-center">{{{ @$detail->pic->first_name.' '.@$detail->pic->last_name }}}</td>
                                                            <td class="text-center">{{{ date('d F Y H:i:s', strtotime($detail->start_date)) }}}</td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                {{-- END BY NOP --}}
                                                @endif

                                                @if(@$actualing_details[0]->planning->process->group->type == 'material')
                                                {{-- BY MATERIAL --}}
                                                <tr class="treegrid-parent-{{{ md5($resource->id) }}} {{{ $resource->is_offline ? 'danger' : ($actualing ? '' : 'success') }}}" style="font-weight: bold; font-size: 11px;">
                                                    <td class="text-center">NOP</td>
                                                    <td class="text-center">Material</td>
                                                    <td class="text-center">Process</td>
                                                    <td class="text-center">PIC</td>
                                                    <td class="text-center">Start Date</td>
                                                </tr>
                                                @foreach ($actualing_details as $detail)
                                                    @if(@$detail->planning->masterplan->project['nop'] && isset($detail->planning->bom->barcode))
                                                        <tr class="treegrid-{{{ $detail->id }}} treegrid-parent-{{{ md5($resource->id) }}} {{{ $resource->is_offline ? 'danger' : ($actualing ? '' : 'success') }}}">
                                                            <td class="text-center">{{{ $detail->planning->masterplan->project['nop'] }}}</td>
                                                            <td class="text-left"><strong>{{{ @$detail->planning->bom->barcode }}}</strong> - {{{ @$detail->planning->bom->material_name }}}</td>
                                                            <td class="text-center">{{{ $detail->planning->process->process }}}</td>
                                                            <td class="text-center">{{{ @$detail->pic->first_name.' '.@$detail->pic->last_name }}}</td>
                                                            <td class="text-center">{{{ date('d F Y H:i:s', strtotime($detail->start_date)) }}}</td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                {{-- END BY MATERIAL --}}
                                                @endif

                                                <tr style="border-bottom: 1.5px #000 solid; height:0; padding:0; margin:0"><td style="height:0; padding:0; margin:0" colspan="6"></td></tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="outplan">
                        <!-- START panel -->
                        <div class="panel panel-primary">
                            <!-- panel heading/header -->
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>Out Plan Resources</h3>
                                <!-- panel toolbar -->
                                <div class="panel-toolbar text-right">
                                    <!-- option -->
                                    <div class="option">
                                        <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                        <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                                    </div>
                                    <!--/ option -->
                                </div>
                                <!--/ panel toolbar -->
                            </div>
                            <!--/ panel heading/header -->

                            <!-- panel body with collapse capabale -->
                            <div class="table-responsive panel-collapse pull out">
                                <table class="table table-striped table-hover tree"  id="out-resources-table">
                                    <thead>
                                        <tr>
                                            <th class="text-left"></th>
                                            <th class="text-left">Code</th>
                                            <th class="text-center">Name</th>
                                            <th class="text-center">Group</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size: 10px;">
                                        @foreach ($out_resources as $resource)
                                            <?php $actualing = $resource->actualing()->count(); ?>
                                            @if($resource->id != '43')
                                                <tr title="{{{ $resource->is_offline ? 'OFFLINE' : '' }}}" class="treegrid-{{{ md5($resource->id) }}} mp-{{ $resource->id }} {{{ $resource->is_offline ? 'danger' : ($actualing ? '' : 'success') }}} text-primary" style="font-weight: bold; font-size: 12px;">
                                                    <td class="text-left"></td>
                                                    <td class="text-left">{{{ $resource->code }}}</td>
                                                    <td class="text-center">{{{ strtoupper($resource->name) }}}</td>
                                                    <td class="text-center">{{{ $resource->group->name }}}</td>
                                                    <td class="text-center">{{{ $resource->is_offline ? 'OFFLINE' : ($actualing ? 'RUNNING' : 'IDLE') }}}</td>
                                                </tr>

                                                <?php $actualing_details = Resource::find($resource->id)->actualing; ?>
                                                @if(@$actualing_details[0]->planning->process->group->type == 'nop')
                                                {{-- BY NOP --}}
                                                <tr class="treegrid-parent-{{{ md5($resource->id) }}} {{{ $resource->is_offline ? 'danger' : ($actualing ? '' : 'success') }}}" style="font-weight: bold; font-size: 11px;">
                                                    <td class="text-center">NOP</td>
                                                    <td class="text-center">Project</td>
                                                    <td class="text-center">Process</td>
                                                    <td class="text-center">PIC</td>
                                                    <td class="text-center">Start Date</td>
                                                </tr>
                                                @foreach ($actualing_details as $detail)
                                                    @if(@$detail->planning->masterplan->project['nop'] && @$detail->status != 'COMPLETE')
                                                        <tr class="treegrid-{{{ $detail->id }}} treegrid-parent-{{{ md5($resource->id) }}} {{{ $resource->is_offline ? 'danger' : ($actualing ? '' : 'success') }}}">
                                                            <td class="text-center">{{{ $detail->planning->masterplan->project['nop'] }}}</td>
                                                            <td class="text-left">{{{ $detail->planning->masterplan->project['project_name'] }}}</td>
                                                            <td class="text-center">{{{ $detail->planning->process->process }}}</td>
                                                            <td class="text-center">{{{ @$detail->pic->first_name.' '.@$detail->pic->last_name }}}</td>
                                                            <td class="text-center">{{{ date('d F Y H:i:s', strtotime($detail->start_date)) }}}</td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                {{-- END BY NOP --}}
                                                @endif

                                                @if(@$actualing_details[0]->planning->process->group->type == 'material')
                                                {{-- BY MATERIAL --}}
                                                <tr class="treegrid-parent-{{{ md5($resource->id) }}} {{{ $resource->is_offline ? 'danger' : ($actualing ? '' : 'success') }}}" style="font-weight: bold; font-size: 11px;">
                                                    <td class="text-center">NOP</td>
                                                    <td class="text-center">Material</td>
                                                    <td class="text-center">Process</td>
                                                    <td class="text-center">PIC</td>
                                                    <td class="text-center">Start Date</td>
                                                </tr>
                                                @foreach ($actualing_details as $detail)
                                                    @if(@$detail->planning->masterplan->project['nop'] && @$detail->status != 'COMPLETE')
                                                        <tr class="treegrid-{{{ $detail->id }}} treegrid-parent-{{{ md5($resource->id) }}} {{{ $resource->is_offline ? 'danger' : ($actualing ? '' : 'success') }}}">
                                                            <td class="text-center">{{{ $detail->planning->masterplan->project['nop'] }}}</td>
                                                            <td class="text-left">{{{ @$detail->planning->bom->material_name }}}</td>
                                                            <td class="text-center">{{{ $detail->planning->process->process }}}</td>
                                                            <td class="text-center">{{{ @$detail->pic->first_name.' '.@$detail->pic->last_name }}}</td>
                                                            <td class="text-center">{{{ date('d F Y H:i:s', strtotime($detail->start_date)) }}}</td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                {{-- END BY MATERIAL --}}
                                                @endif

                                                <tr style="border-bottom: 1.5px #000 solid; height:0; padding:0; margin:0"><td style="height:0; padding:0; margin:0" colspan="6"></td></tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>

                            <!--/ panel body with collapse capabale -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/tabletools.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/zeroclipboard.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/gritter/js/jquery.gritter.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/treegrid/js/jquery.treegrid.js') }}}"></script>
<script type="text/javascript">
function deleteItem(elem) {
    var self = $(elem);
    bootbox.confirm("Are you sure to delete this resource?", function (result) {
        if(result){
            var resourceid = self.data('id');
            $('button#del-'+resourceid).click();
        }
    });
}
(function(){
    $('.tree').treegrid();
})();
</script>
@stop

