@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/selectize/css/selectize.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui-timepicker.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <!-- <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="#">Table</a></li>
                        <li class="active">Default</li>
                    </ol>
                </div> -->
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Notification::showAll() }}

                <!-- START panel -->
                <div class="panel panel-primary">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>{{{ $page_title }}}</h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <!--/ panel heading/header -->

                    <!-- panel toolbar wrapper -->
                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <!-- <div class="panel-toolbar pl10">
                            <div class="checkbox custom-checkbox pull-left">
                                <input type="checkbox" id="customcheckbox-one0" value="1" data-toggle="checkall" data-target="#table1">
                                <label for="customcheckbox-one0">&nbsp;&nbsp;Select all</label>
                            </div>
                        </div> -->
                        <div class="panel-toolbar text-right">
                            <button onclick="window.location.href='{{{ route('alert_setting.create') }}}';" type="button" class="btn btn-success mb5"><i class="ico-user22"></i> Add New Alert Setting</button>
                        </div>
                    </div>
                    <!--/ panel toolbar wrapper -->

                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-bordered table-hover" id="table1">
                            <thead>
                                <tr>
                                    <th class="text-center">Setting Name</th>
                                    <th class="text-center">Recipients</th>
                                    <th class="text-center">Email[?]</th>
                                    <th class="text-center">SMS[?]</th>
                                    <th width="10%" class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($alert_settings as $alert_setting)
                                <?php $alert_users = AlertSettingUser::where('setting_id','=',$alert_setting->id)->get()->toArray(); ?>
                                <?php $first = (Object) array_shift($alert_users); ?>
                                <tr>
                                    <td class="text-center" rowspan="{{{ count($alert_users) + 1 }}}">{{{ $alert_setting->setting }}}</td>
                                    <td class="text-center">{{{ UserInformation::getUserName($first->user_id) }}}</td>
                                    <td class="text-center">
                                        <a href="#" data-type="select" data-pk="{{{ $first->id }}}" data-val="{{{ $first->send_email }}}" class="text-primary set_email">
                                            {{{ $first->send_email ? 'Yes' : 'No' }}}
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" data-type="select" data-pk="{{{ $first->id }}}" data-val="{{{ $first->send_email }}}" class="text-primary set_sms">
                                            {{{ $first->send_sms ? 'Yes' : 'No' }}}
                                        </a>
                                    </td>
                                    <td class="text-center" rowspan="{{{ count($alert_users) + 1 }}}" width="10%">
                                        <!-- button toolbar -->
                                        <div class="toolbar">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-default">Action</button>
                                                <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="{{{ route('alert_setting.edit', $alert_setting->id) }}}"><i class="icon ico-pencil"></i>Update</a></li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a href="#" data-id="{{{ $alert_setting->id }}}" class="delete-confirm text-danger"><i class="icon ico-remove3"></i>Delete</a>
                                                        {{ Form::open(array('route' => array('alert_setting.destroy', $alert_setting->id), 'method' => 'delete')) }}
                                                        <button type="submit" id="del-{{{ $alert_setting->id }}}" style="display:none"></button>
                                                        {{ Form::close() }}
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--/ button toolbar -->
                                    </td>
                                </tr>
                                @foreach($alert_users as $alert_user)
                                <?php $alert_user = (Object) $alert_user; ?>
                                <tr>
                                    <td class="text-center">{{{ UserInformation::getUserName($alert_user->user_id) }}}</td>
                                    <td class="text-center">
                                        <a href="#" data-type="select" data-pk="{{{ $alert_user->id }}}" data-val="{{{ $alert_user->send_email }}}" class="text-primary set_email">
                                            {{{ $alert_user->send_email ? 'Yes' : 'No' }}}
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" data-type="select" data-pk="{{{ $alert_user->id }}}" data-val="{{{ $alert_user->send_sms }}}" class="text-primary set_sms">
                                            {{{ $alert_user->send_sms ? 'Yes' : 'No' }}}
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-timepicker.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-touch.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/xeditable/js/bootstrap-editable.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/typeahead/bootstrap3-typeahead.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/element.js') }}}"></script>
<script type="text/javascript">
(function(){
    $('.set_email').editable({
        url: '{{{ route('alert.set_receive_email') }}}',
        params: {estimation: $('#total_estimation').val(), process_type: $(this).data('process-type')},
        source: [
            {value: 1, text: 'Yes'},
            {value: 0, text: 'No'}
        ],
        title: "Set to receive email[?]"
    });
    $('.set_sms').editable({
        url: '{{{ route('alert.set_receive_sms') }}}',
        params: {estimation: $('#total_estimation').val(), process_type: $(this).data('process-type')},
        source: [
            {value: 1, text: 'Yes'},
            {value: 0, text: 'No'}
        ],
        title: "Set to receive SMS[?]"
    });
    $(".delete-confirm").on("click", function (event) {
        var self = $(this);
        bootbox.confirm("Are you sure to delete this sms number?", function (result) {
            if(result){
                var smsid = self.attr('data-id');
                $('button#del-'+smsid).click();
            }
        });
        event.preventDefault();
    });
})();
</script>
<!-- <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/components/notification.js') }}}"></script> -->
@stop

