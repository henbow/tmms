@extends($theme.'._layouts.master')

@section('main')
<section id="main" role="main">
    {{ Notification::showAll() }}

    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Form::open(array('route' => 'alert_setting.store', 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate')) }}
                    <div class="panel-heading">
                        <h3 class="panel-title">Alert Setting</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Setting Name</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="setting_name" id="setting_name" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Recipients</label>
                            <div class="col-sm-6">
                                <select multiple class="form-control" name="recipients[]" id="recipients" required>
                                    <?php $users = UserInformation::getUsers(); ?>
                                    @foreach($users as $user)
                                    <option value="{{{ $user->id }}}">{{{ $user->username }}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Add Alert Setting</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/parsley/js/parsley.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/validation.js') }}}"></script>
@stop
