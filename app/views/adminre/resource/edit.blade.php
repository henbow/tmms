@extends($theme.'._layouts.master')
 
@section('main')
<section id="main" role="main">
    {{ Notification::showAll() }}

    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Form::model($resource, array('method' => 'put', 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate', 'route' => array('resource.update', $resource->id))) }}
                    <div class="panel-heading">
                        <h3 class="panel-title">Settings</h3>
                    </div>               
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">PIC</label>
                            <div class="col-sm-6">
                                <select name="picid[]" id="picid" class="form-control" multiple>   
                                    @foreach($pics as $pic)
                                    <option {{{ in_array($pic->id, $selected_pics) ? "selected" : "" }}} value="{{{ $pic->id }}}">{{{ ucwords($pic->first_name." ".$pic->last_name) }}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Group</label>
                            <div class="col-sm-6">
                                <select name="groupid" id="groupid" class="form-control">                                
                                    <option value="">Select Resource Group</option>
                                    @foreach($groups as $group)
                                    <option {{{ $resource->group_id == $group->id ? 'selected' : '' }}} value="{{{ $group->id }}}">{{{ $group->name }}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Code</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="code" id="code"  value="{{{ $resource->code }}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="resname" id="resname"  value="{{{ $resource->name }}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Plan Type</label>
                            <div class="col-sm-6">
                                <select name="plan_type" class="form-control">
                                    <option {{{ $resource->plan_type == 'in plan' ? 'selected' : '' }}} value="in plan">IN PLAN</option>
                                    <option {{{ $resource->plan_type == 'out plan' ? 'selected' : '' }}} value="out plan">OUT PLAN</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Human [?]</label>
                            <div class="col-sm-6">   
                                <span class="checkbox custom-checkbox custom-checkbox-inverse">                           
                                    <input {{{ $resource->is_human == '1' ? 'checked' : '' }}} type="checkbox" name="human" id="human" value="1" class="form-control">
                                    <label for="human">&nbsp;&nbsp;Human</label>
                                </span>  
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Offline [?]</label>
                            <div class="col-sm-6">
                                <span class="checkbox custom-checkbox custom-checkbox-inverse">
                                    <input {{{ $resource->is_offline == '1' ? 'checked' : '' }}} type="checkbox" name="offline" id="offline" value="1" class="form-control">
                                    <label for="offline">&nbsp;&nbsp;Offline</label>
                                </span>                                
                            </div>
                        </div>
                        <div class="form-group" id="offline-reason" style="display:none">
                            <label class="col-sm-2 control-label">Offline Reason</label>
                            <div class="col-sm-6">
                                <textarea name="offlinereason" id="offlinereason" class="form-control">{{{ $resource->offline_reason }}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Update</button>
                        <button type="reset" class="btn btn-reset">Reset</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/parsley/js/parsley.min.js') }}}"></script>        
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/validation.js') }}}"></script>
<script type="text/javascript">
$( "input[type=checkbox]#offline" ).on( "click", function(){$('#offline-reason').slideToggle()});
</script>
@stop
