@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatables/css/jquery.datatables.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<style type="text/css">
.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }
</style>
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <!-- <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="#">Table</a></li>
                        <li class="active">Default</li>
                    </ol>
                </div> -->
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Notification::showAll() }}

                <!-- START panel -->
                <div class="panel panel-primary">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>In Plan Resources</h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <!--/ panel heading/header -->

                    <!-- panel toolbar wrapper -->
                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <!-- <div class="panel-toolbar pl10">
                            <div class="checkbox custom-checkbox pull-left">
                                <input type="checkbox" id="customcheckbox-one0" value="1" data-toggle="checkall" data-target="#table1">
                                <label for="customcheckbox-one0">&nbsp;&nbsp;Select all</label>
                            </div>
                        </div> -->
                        <div class="panel-toolbar text-right">
                            <button onclick="window.location.href='{{{ route('resource.create') }}}';" type="button" class="btn btn-success mb5"><i class="ico-user22"></i> Add New Resource</button>
                        </div>
                    </div>
                    <!--/ panel toolbar wrapper -->

                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-striped table-bordered table-hover" id="inplan-resources-table">
                            <thead>
                                <tr>
                                    <th class="text-center">Code</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Group</th>
                                    <th class="text-center">Type</th>
                                    <th class="text-center">Standard Rate</th>
                                    <th class="text-center">Overtime Rate</th>
                                    <th class="text-center">Work Time Per Week</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 10px;">
                                @foreach ($in_plan_resources as $resource)
                                <?php $specification = ResourceSpec::where('resource_id', '=', $resource->id)->get(); ?>
                                <?php $work_time = ResourceWorkTime::where('resource_id', '=', $resource->id)->get(array('per_week')); ?>
                                <tr title="{{{ $resource->is_offline ? 'OFFLINE' : '' }}}" class="{{{ $resource->is_offline ? 'danger' : '' }}}">
                                    <td class="text-center">{{{ $resource->code }}}</td>
                                    <td class="text-center">{{{ strtoupper($resource->name) }}} {{{ $resource->is_offline ? '[OFFLINE]' : '' }}}</td>
                                    <td class="text-center">{{{ @ResourceGroup::getName($resource->group_id) }}}</td>
                                    <td class="text-center {{{ $resource->is_human ? 'text-primary' : '' }}}">{{{ $resource->is_human ? 'HUMAN' : 'MACHINE' }}}</td>
                                    <td class="text-center" width="10%">{{{ number_format(@$specification[0]->std_rate_per_hour, 0, '', '.') }}}</td>
                                    <td class="text-center" width="10%">{{{ number_format(@$specification[0]->ovt_rate_per_hour, 0, '', '.') }}}</td>
                                    <td class="text-center" width="10%">{{{ intval(@$work_time[0]->per_week) }}}</td>
                                    <td width="10%" class="text-center">
                                        <!-- button toolbar -->
                                        <div class="toolbar">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-default">Action</button>
                                                <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="{{{ route('resource.show', $resource->id) }}}"><i class="icon ico-profile"></i>Show Details</a></li>
                                                    <li><a href="{{{ route('resource.edit', $resource->id) }}}"><i class="icon ico-pencil"></i>Edit</a></li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a href="javascript:void();" data-id="{{{ $resource->id }}}" onclick="deleteItem(this)" class="delete-confirm text-danger"><i class="icon ico-remove3"></i>Delete</a>
                                                        {{ Form::open(array('route' => array('resource.destroy', $resource->id), 'method' => 'delete')) }}
                                                        <button type="submit" id="del-{{{ $resource->id }}}" style="display:none"></button>
                                                        {{ Form::close() }}
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--/ button toolbar -->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                    <!--/ panel body with collapse capabale -->
                </div>

                <div class="panel panel-primary">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>Out Plan Resources</h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <!--/ panel heading/header -->

                    <!-- panel toolbar wrapper -->
                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <!-- <div class="panel-toolbar pl10">
                            <div class="checkbox custom-checkbox pull-left">
                                <input type="checkbox" id="customcheckbox-one0" value="1" data-toggle="checkall" data-target="#table1">
                                <label for="customcheckbox-one0">&nbsp;&nbsp;Select all</label>
                            </div>
                        </div> -->
                        <div class="panel-toolbar text-right">
                            <button onclick="window.location.href='{{{ route('resource.create') }}}';" type="button" class="btn btn-success mb5"><i class="ico-user22"></i> Add New Resource</button>
                        </div>
                    </div>
                    <!--/ panel toolbar wrapper -->

                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-striped table-bordered table-hover" id="outplan-resources-table">
                            <thead>
                                <tr>
                                    <th class="text-center">Code</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Group</th>
                                    <th class="text-center">Type</th>
                                    <th class="text-center">Standard Rate</th>
                                    <th class="text-center">Overtime Rate</th>
                                    <th class="text-center">Work Time Per Week</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 10px;">
                                @foreach ($out_plan_resources as $resource)
                                <?php $specification = ResourceSpec::where('resource_id', '=', $resource->id)->get(); ?>
                                <?php $work_time = ResourceWorkTime::where('resource_id', '=', $resource->id)->get(array('per_week')); ?>
                                <tr title="{{{ $resource->is_offline ? 'OFFLINE' : '' }}}" class="{{{ $resource->is_offline ? 'danger' : '' }}}">
                                    <td class="text-center">{{{ $resource->code }}}</td>
                                    <td class="text-center">{{{ strtoupper($resource->name) }}} {{{ $resource->is_offline ? '[OFFLINE]' : '' }}}</td>
                                    <td class="text-center">{{{ @ResourceGroup::getName($resource->group_id) }}}</td>
                                    <td class="text-center {{{ $resource->is_human ? 'text-primary' : '' }}}">{{{ $resource->is_human ? 'HUMAN' : 'MACHINE' }}}</td>
                                    <td class="text-center" width="10%">{{{ number_format(@$specification[0]->std_rate_per_hour, 0, '', '.') }}}</td>
                                    <td class="text-center" width="10%">{{{ number_format(@$specification[0]->ovt_rate_per_hour, 0, '', '.') }}}</td>
                                    <td class="text-center" width="10%">{{{ intval(@$work_time[0]->per_week) }}}</td>
                                    <td width="10%" class="text-center">
                                        <!-- button toolbar -->
                                        <div class="toolbar">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-default">Action</button>
                                                <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="{{{ route('resource.show', $resource->id) }}}"><i class="icon ico-profile"></i>Show Details</a></li>
                                                    <li><a href="{{{ route('resource.edit', $resource->id) }}}"><i class="icon ico-pencil"></i>Update</a></li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a href="javascript:void();" data-id="{{{ $resource->id }}}" onclick="deleteItem(this)" class="delete-confirm text-danger"><i class="icon ico-remove3"></i>Delete</a>
                                                        {{ Form::open(array('route' => array('resource.destroy', $resource->id), 'method' => 'delete')) }}
                                                        <button type="submit" id="del-{{{ $resource->id }}}" style="display:none"></button>
                                                        {{ Form::close() }}
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--/ button toolbar -->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/tabletools.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/zeroclipboard.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/gritter/js/jquery.gritter.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript">
function deleteItem(elem) {
    var self = $(elem);
    bootbox.confirm("Are you sure to delete this resource?", function (result) {
        if(result){
            var resourceid = self.data('id');
            $('button#del-'+resourceid).click();
        }
    });
}
(function(){
    var table = $("#inplan-resources-table").dataTable({
        "sDom": "<'row'<'col-sm-6'Tl><'col-sm-6'f>><'table-responsive'rt><'row'<'col-sm-6'p><'col-sm-6'i>>",
        "oTableTools": {
            "sSwfPath": "{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/swf/copy_csv_xls_pdf.swf') }}}",
            "aButtons": [
                "print",
                "pdf",
                "csv"
            ]
        },
        "aoColumns": [
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left", "bSearchable": false },
            { "sClass": "text-left", "bSearchable": false },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false }
        ],
        "iDisplayLength": 10
    });
    var table = $("#outplan-resources-table").dataTable({
        "sDom": "<'row'<'col-sm-6'Tl><'col-sm-6'f>><'table-responsive'rt><'row'<'col-sm-6'p><'col-sm-6'i>>",
        "oTableTools": {
            "sSwfPath": "{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/swf/copy_csv_xls_pdf.swf') }}}",
            "aButtons": [
                "print",
                "pdf",
                "csv"
            ]
        },
        "aoColumns": [
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left", "bSearchable": false },
            { "sClass": "text-left", "bSearchable": false },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false }
        ],
        "iDisplayLength": 10
    });
})();
</script>
<!-- <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/components/notification.js') }}}"></script> -->
@stop

