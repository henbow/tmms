<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">Select Resource Work Time Template</h3>
</div>
<div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Template List</h3>
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                    </div>
                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-striped table-bordered table-hover" id="tpl-work-time-table" style="font-size: 10px">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">No</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Mon</th>
                                    <th class="text-center">Tue</th>
                                    <th class="text-center">Wed</th>
                                    <th class="text-center">Thu</th>
                                    <th class="text-center">Fri</th>
                                    <th class="text-center">Sat</th>
                                    <th class="text-center">Sun</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1; ?>
                            @foreach($templates as $template)
                                <tr class="tpl-work-time" data-id="{{{ $template->id }}}" style="cursor:pointer;">
                                    <td class="text-center">{{{ $no++ }}}</td>
                                    <td>{{{ $template->name }}}</td>
                                    <td>{{{ $template->mon }}}</td>
                                    <td>{{{ $template->tue }}}</td>
                                    <td>{{{ $template->wed }}}</td>
                                    <td>{{{ $template->tue }}}</td>
                                    <td>{{{ $template->fri }}}</td>
                                    <td>{{{ $template->sat }}}</td>
                                    <td>{{{ $template->sun }}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
            
            <div class="col-md-12 text-right">
            {{ Form::open(array('route' => array('resource_work_time.from_template', $resource_id), 'id' => 'from-template', 'class' => 'form-horizontal', 'data-parsley-validate')) }}
                <div class="col-md-6"><p class="text-left">Selected template: <span id="selected-tpl-text">NONE</span></p></div>
                <input type="hidden" name="template-id" value="" />
                <button type="submit" class="btn btn-success mb5"> Select Template </button>
            {{ Form::close() }}
            </div>
        </div>
    </div>
    
    <!--/ END Template Container -->
</div>
<script>
(function(){
    $('#tpl-work-time-table tr.tpl-work-time').click(function(e){
        var idAttr = $(this).attr('data-id');
        $('[name="template-id"]').val(idAttr);
        $('#selected-tpl-text').text($(this).children('td:eq(1)').text());
    });
})();
</script>