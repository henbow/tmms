<!-- START modal-lg -->
{{ Form::open(array('route' => array('template_master_plan_detail.store', $header_id), 'id' => 'new-detail_plan', 'class' => 'form-horizontal', 'data-parsley-validate')) }}
<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    @if(isset($detail_id))
    <h3 class="semibold modal-title text-primary">Add New Child Planning</h3>
    @else
    <h3 class="semibold modal-title text-primary">Add New Planning</h3>
    @endif
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Parent</label>
                    <div class="col-sm-3">
                        <select name="parent_id" class="form-control">
                        <option></option>
                        @foreach($plannings as $parent_planning)
                            @if($parent_planning->parent_id == '0')
                                @if($parent_planning->process_group_id != '0')
                                    <?php $group_data = ProcessGroup::find($parent_planning->process_group_id); ?>
                                    <option {{{ @$detail_id == $parent_planning->id ? 'selected' : '' }}} value="{{{ $parent_planning->id }}}">{{{ $group_data->group }}}</option>
                                @endif
                            @endif
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Planning</label>
                    <div class="col-sm-6">
                        <input type="text" name="process_group_{{{ $random }}}" class="form-control" required />
                        <input type="hidden" name="group_id" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Percent</label>
                    <div class="col-sm-2">
                        <input type="number" name="percent" class="form-control" value="0" required />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Process Type</label>
                    <div class="col-sm-5">
                        <select class="form-control" name="process_type">
                            <option value="first">First Process</option>
                            <option value="secondary">Secondary Process</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Position</label>
                    <div class="col-sm-2">
                        <select name="position_type" class="form-control">
                            <option>BEFORE</option>
                            <option selected>AFTER</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="planning_id" class="form-control">
                        <?php $i = 0; ?>
                        @foreach($plannings as $planning)
                            <?php $group_data = ProcessGroup::find($planning->process_group_id); ?>
                            <option {{{ ++$i === count($plannings) ? 'selected' : '' }}} value="{{{$planning->id}}}">{{{$planning->parent_id?'&nbsp;&#8627;&nbsp;':''}}}{{{$group_data->group}}}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-success"> Save </button>
    <button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
</div>
{{ Form::close() }}
<script type="text/javascript">
(function(){
    $("[name=\"process_group_{{{ $random }}}\"]").typeahead({
        source:function (query, group) {
            groups = [];
            map = {};

            <?php $group_data = array(); ?>
            <?php foreach($process_group as $group): ?>
            <?php $group_data[] = array('id' => $group->id, 'group' => $group->group); ?>
            <?php endforeach; ?>

            var data = <?php echo json_encode($group_data); ?>;

            $.each(data, function (i, group) {
                map[group.group] = group;
                groups.push(group.group);
            });

            group(groups);
        },
        updater: function (group) {
            $('[name="group_id"]').val(map[group].id);
            return group;
        }
    });
})();
</script>
<!--/ END modal-lg
