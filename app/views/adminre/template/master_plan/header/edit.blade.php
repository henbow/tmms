<!-- START modal-lg -->
{{ Form::open(array('method' => 'put', 'route' => array('template_master_plan.update', $template->id), 'id' => 'edit-template', 'class' => 'form-horizontal', 'data-parsley-validate')) }}
<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">Add New Template Master Plan</h3>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">   
            <div class="panel-body">    
                <div class="form-group">
                    <label class="col-sm-3 control-label">Template Name</label>
                    <div class="col-sm-5">
                        <input type="text" name="header_name" class="form-control" value="{{{ $template->name }}}" required />
                    </div>
                </div>            
                <div class="form-group">
                    <label class="col-sm-3 control-label">Estimation Date From</label>
                    <div class="col-sm-5">
                        <select name="date_from" class="form-control">
                            <option {{{ $template->pull_date_from == 'TRIAL' ? 'selected' : '' }}}>TRIAL</option>
                            <option {{{ $template->pull_date_from == 'DELIVERY' ? 'selected' : '' }}}>DELIVERY</option>
                        </select>
                    </div>
                </div>         
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-success"> Update </button>
    <button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
</div>
{{ Form::close() }}
<!--/ END modal-lg

