@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/selectize/css/selectize.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui-timepicker.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}} [{{{ $sub_title }}}]</h4>
            </div>
        </div>
        <!-- Page Header -->
        
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Template Manufacting Process</h3>
                    </div>
                    <!--/ panel heading/header -->
                    
                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <div class="panel-toolbar pl10">
                        </div>
                        <div class="panel-toolbar text-right">
                            <a href="javascript:void(0)" class="btn btn-success mb5" id="import-excel"><i class="ico-stack2"></i> Import From Excel</a>
                            <a href="javascript:void(0)" class="btn btn-success mb5" id="add-planning"><i class="ico-stack2"></i> Add New Template Header</a>
                        </div>
                    </div>

                    <!-- panel body with collapse capabale -->
                    <table class="table table-striped table-bordered table-hover" id="table-planning">
                        <thead>
                            <tr>
                                <th class="text-center" width="5%">No</th>
                                <th class="text-center">Name</th>
                                <th class="text-center" width="10%"></th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 11px;">
                            <?php $no = 1; ?>
                            @foreach ($headers as $header)
                            <tr>
                                <td align="center">{{{ $no++ }}}</td>
                                <td align="left"><a href="{{{ route('template_mfg_process_detail.index', $header->id) }}}">{{{ $header->name }}}</a></td>
                                <td align="center">
                                    <div class="toolbar">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-sm btn-default">Action</button>
                                            <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="{{{ route('template_mfg_process.edit', $header->id) }}}" class="edit-planning"><i class="ico-stack2"></i>&nbsp;&nbsp;Edit </a></li>
                                                <li><a href="{{{ route('template_mfg_process_detail.index', $header->id) }}}"><i class="ico-stack2"></i>&nbsp;&nbsp;Detail </a></li>
                                                <li class="divider"></li>
                                                <li><a href="#" data-id="{{{ $header->id }}}" class="text-danger delete-confirm"><i class="ico-remove2"></i>&nbsp;&nbsp;Delete </a></li>
                                            </ul>
                                            {{ Form::open(array('route' => array('template_mfg_process.destroy', $header->id), 'method' => 'delete')) }}
                                            <button type="submit" id="del-{{{ $header->id }}}" style="display:none"></button>
                                            {{ Form::close() }} 
                                        </div>
                                    </div>
                                </td>
                            </tr>   
                            @endforeach
                        </tbody>
                    </table>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>        
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->
    
    <!-- START modal-lg -->
    <div id="bs-modal-lg" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content"></div>
        </div>
    </div>
    <!--/ END modal-lg -->
</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-timepicker.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-touch.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/xeditable/js/bootstrap-editable.min.js') }}}"></script>      
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/typeahead/bootstrap3-typeahead.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/element.js') }}}"></script>
<script type="text/javascript">
(function () {    
    $('#import-excel').click(function(e){
        $('#bs-modal-lg .modal-content').load('{{{ route('template_mfg_process.excel.import') }}}',function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $('#add-planning').click(function(e){
        $('#bs-modal-lg .modal-content').load('{{{ route('template_mfg_process.create') }}}',function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $('.edit-planning').click(function(e){
        e.preventDefault();
        $('#bs-modal-lg .modal-content').load($(this).attr('href'),function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $(".delete-confirm").on("click", function (e) {
        e.preventDefault();
        var self = $(this);
        bootbox.confirm("Are you sure to delete this template?", function (result) {
            if(result){
                var id = self.attr('data-id');
                $('button#del-'+id).click();
            }
        });
    });
})();
</script>
@stop