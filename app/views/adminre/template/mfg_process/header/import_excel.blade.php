<!-- START modal-lg -->
{{ Form::open(array('route' => array('template_mfg_process.excel.do_import'), 'files' => true, 'id' => 'new-master_plan', 'class' => 'form-horizontal', 'data-parsley-validate')) }}
<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">Import Template From Excel</h3>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">   
            <div class="panel-body">    
                <div class="form-group">
                    <label class="col-sm-2 control-label">File Excel</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <input type="text" class="form-control" id="file_name" value="" readonly>
                            <span class="input-group-btn">
                                <div class="btn btn-primary btn-file">
                                    <span class="icon iconmoon-file-3"></span> Browse <input name="file_xls" type="file" value="">
                                </div>
                            </span>
                        </div>
                    </div>
                </div>         
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">    
    <button type="submit" class="btn btn-success"> Import </button>
    <button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
</div>
{{ Form::close() }}
<script type="text/javascript">
(function(){
    $('[name="file_xls"]').change(function(){
        var filename = $(this).val().split('\\');
        $('#file_name').val(filename[2]);
    });
})();
</script>
<!--/ END modal-lg