<!-- START modal-lg -->
{{ Form::open(array('route' => array('template_mfg_process_detail.store', $header_id), 'id' => 'new-detail_plan', 'class' => 'form-horizontal', 'data-parsley-validate')) }}
<div class="modal-header text-left"> 
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">Add New Planning</h3>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">   
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Process</label>
                    <div class="col-sm-4">
                        <input type="text" name="process_name_{{{ $random }}}" class="form-control" required />
                        <input type="hidden" name="group_id" value="" />
                        <input type="hidden" name="process_id" value="" /> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Estimation</label>
                    <div class="col-sm-2">
                        <input type="number" step="0.1" name="estimation" class="form-control" required />
                        <label for="estimation">Days</label>
                    </div>
                    <div class="col-sm-2">
                        <input type="number" step="0.1" name="estimation_hour" class="form-control" required />
                        <label for="estimation_hour">Hours</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Position</label>
                    <div class="col-sm-2">
                        <input type="number" name="position" class="form-control" required />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Specification</label>
                    <div class="col-sm-5">
                        <textarea name="specification" class="form-control" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    
    <button type="submit" class="btn btn-success"> Save </button>
    <button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
</div>
{{ Form::close() }}
<script type="text/javascript">
(function(){
    $('[name="estimation"]').change(function(e){
        var current_value = parseFloat($(this).val());
        var floatNum = current_value * 24;
        $('[name="estimation_hour"]').val(parseFloat(floatNum.toFixed(1)));
    });
    $('[name="estimation_hour"]').change(function(e){
        var current_value = parseInt($(this).val());
        var floatNum = current_value / 24;
        $('[name="estimation"]').val(parseFloat(floatNum.toFixed(1)));
    });
    $("[name=\"process_name_{{{ $random }}}\"]").typeahead({
        source:function (query, process) {
            processes = [];
            map = {};
            
            var data = <?php echo json_encode($process_data); ?>;
         
            $.each(data, function (i, process) {
                map[process.process_name] = process;
                processes.push(process.process_name);
            });
         
            process(processes);
        },
        updater: function (process) {
            console.log(map[process]);
            $('[name="process_id"]').val(map[process].id);
            $('[name="group_id"]').val(map[process].group_id);            
            return process;
        }
    });
    $('[name="resource_select"]').on('click', function(e){
        if($(this).val() === 'select'){
            if($('[name="group_id"]').val() && $('[name="start"]').val() && $('[name="finish"]').val()) {
                $.post('{{{ route('project.planning.detail.available_resource') }}}', {
                    group_id: $('[name="group_id"]').val(), 
                    start: $('[name="start"]').val(), 
                    finish: $('[name="finish"]').val()
                }, function(resp){
                    var datares = $.parseJSON(resp);
                    $('[name="resource"]').empty();
                    $.each(datares, function (i, resource) {
                        $('[name="resource"]').append('<option value="'+resource.id+'">'+resource.name+'</option>');
                    });
                });
            }
            
            $('#resource_name').fadeIn();
        }else{
            $('#resource_name').fadeOut();
        }
    });
    var $form = $("form#new-process");
    $form.on("click", "button[type=submit]", function (e) {
        var $this = $(this);

        // NProgress.start();

        // you can do the ajax request here
        // this is for demo purpose only
        $this.submit();
    });
})();
</script>
<!--/ END modal-lg