@extends($theme . '._layouts.master')

@section('main')
<section id="main" role="main">
    {{ Notification::showAll() }}

    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Form::open(array('method' => 'put', 'route' => array('work_time_template.update', $template->id), 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate')) }}
                    <div class="panel-heading">
                        <h3 class="panel-title">Work Time Template</h3>
                    </div>               
                    <div class="panel-body">                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Template Name:</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="name" id="name" value="{{{ $template->name }}}"/></div><div class="clear"></div>
                        </div>   
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Monday:</label>
                            <div class="col-sm-2"><input type="number" min="0" step="0.01" class="form-control day" name="mon" id="mon" value="{{{ $template->mon }}}"/></div><div class="clear"></div>
                        </div>   
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tuesday:</label>
                            <div class="col-sm-2"><input type="number" min="0" step="0.01" class="form-control day" name="tue" id="tue" value="{{{ $template->tue }}}"/></div><div class="clear"></div>
                        </div>   
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Wednesday:</label>
                            <div class="col-sm-2"><input type="number" min="0" step="0.01" class="form-control day" name="wed" id="wed" value="{{{ $template->wed }}}"/></div><div class="clear"></div>
                        </div>  
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Thursday:</label>
                            <div class="col-sm-2"><input type="number" min="0" step="0.01" class="form-control day" name="thu" id="thu" value="{{{ $template->thu }}}"/></div><div class="clear"></div>
                        </div>   
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Friday:</label>
                            <div class="col-sm-2"><input type="number" min="0" step="0.01" class="form-control day" name="fri" id="fri" value="{{{ $template->fri }}}"/></div><div class="clear"></div>
                        </div>  
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Saturday:</label>
                            <div class="col-sm-2"><input type="number" min="0" step="0.01" class="form-control day" name="sat" id="sat" value="{{{ $template->sat }}}"/></div><div class="clear"></div>
                        </div>   
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Sunday:</label>
                            <div class="col-sm-2"><input type="number" min="0" step="0.01" class="form-control day" name="sun" id="sun" value="{{{ $template->sun }}}"/></div><div class="clear"></div>
                        </div>   
                    </div> 
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Update</button>
                        <button type="reset" class="btn btn-reset">Reset</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/parsley/js/parsley.min.js') }}}"></script>        
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/validation.js') }}}"></script>
<script type="text/javascript">
$('input.day').keyup(function(){
    var sum = 0;
    $('input.day').each(function() {
        sum += Number($(this).val());
    });
    $('input.week').val(sum);
});
</script>
@stop

