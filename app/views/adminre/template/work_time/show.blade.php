@extends($theme . '._layouts.master')

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <!-- <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="#">Table</a></li>
                        <li class="active">Default</li>
                    </ol>
                </div> -->
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title ellipsis">Resource Information</h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-6"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <!--/ panel heading/header -->
                    <!-- panel body with collapse capabale -->
                    <div class="panel-collapse pull out">
                        <div class="panel-body">
                            <dl class="dl-horizontal">
                                <dt>Code</dt>
                                <dd>{{{ $resource->code }}}&nbsp;</dd>
                                <dt>Name</dt>
                                <dd>{{{ $resource->name }}}&nbsp;</dd>
                                <dt>Group</dt>
                                <dd>{{{ @ResourceGroup::getName($resource->group_id) }}}&nbsp;</dd>
                                <dt>Type</dt>
                                <dd>{{{ $resource->is_human == '1' ? 'HUMAN' : 'MACHINE' }}}&nbsp;</dd>
                                <dt>Status</dt>
                                <dd>{{{ $resource->is_offline == '1' ? 'OFFLINE' : 'ONLINE' }}}&nbsp;</dd>
                                @if($resource->is_offline == '1')
                                <dt>Type</dt>
                                <dd>{{{ $resource->offline_reason }}}&nbsp;</dd>
                                @endif
                            </dl>
                        </div>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
                <!--/ END panel -->
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-primary">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>Specification Data</h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <!--/ panel heading/header -->

                    <!-- panel toolbar wrapper -->
                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <!-- <div class="panel-toolbar pl10">
                            <div class="checkbox custom-checkbox pull-left">
                                <input type="checkbox" id="customcheckbox-one0" value="1" data-toggle="checkall" data-target="#table1">
                                <label for="customcheckbox-one0">&nbsp;&nbsp;Select all</label>
                            </div>
                        </div> -->
                        <div class="panel-toolbar text-right">
                            @if(isset($specification->id))
                            <a href="{{{ route('resource_spec.edit', array($resource->id, $specification->id)) }}}" title="Edit Specification" class="btn btn-success mb5">
                            @else
                            <a href="{{{ route('resource_spec.create', $resource->id) }}}" title="Edit Specification" class="btn btn-success mb5">
                            @endif
                                <span>Edit Specification</span>
                            </a>
                        </div>
                    </div>
                    <!--/ panel toolbar wrapper -->

                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        @if($resource->is_human == '1')
                        <table class="table table-bordered table-hover" id="table1">
                            <thead>
                                <tr>
                                    <th class="text-center" colspan="2" width="50%">Specifications</th>
                                    <th class="text-center" colspan="2" width="20%">Rate/Hour</th>
                                </tr>
                                <tr>
                                    <th class="text-center">Min Mould Size (mm)</th>
                                    <th class="text-center">Max Mould Size (mm)</th>
                                    <th class="text-center">Standard (Rp)</th>
                                    <th class="text-center">Overtime (Rp)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">{{{ @$specification->min_mould_size }}}</td>
                                    <td class="text-center">{{{ @$specification->max_mould_size }}}</td>
                                    <td class="text-center">{{{ number_format(@$specification->std_rate_per_hour, 0, '', '.') }}}</td>
                                    <td class="text-center">{{{ number_format(@$specification->ovt_rate_per_hour, 0, '', '.') }}}</td>
                                </tr>
                            </tbody>
                        </table>
                        @else
                        <table class="table table-bordered table-hover" id="table1">
                            <thead>
                                <tr>
                                    <th class="text-center" colspan="5" width="40%">Specifications</th>
                                    <th class="text-center" colspan="2" width="20%">Rate/Hour</th>
                                </tr>
                                <tr>
                                    <th class="text-center">Min. Table Load (kg)</th>
                                    <th class="text-center">Max. Table Load (kg)</th>
                                    <th class="text-center">Table Size (mm)</th>
                                    <th class="text-center">Travel Range (mm)</th>
                                    <th class="text-center">Accuracy (mm)</th>
                                    <th class="text-center">Standard (Rp)</th>
                                    <th class="text-center">Overtime (Rp)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">{{{ @$specification->min_table_load }}}</td>
                                    <td class="text-center">{{{ @$specification->max_table_load }}}</td>
                                    <td class="text-center">{{{ @$specification->table_size }}}</td>
                                    <td class="text-center">{{{ @$specification->travel_range }}}</td>
                                    <td class="text-center">{{{ @$specification->accuracy }}}</td>
                                    <td class="text-center">{{{ number_format(@$specification->std_rate_per_hour, 0, '', '.') }}}</td>
                                    <td class="text-center">{{{ number_format(@$specification->ovt_rate_per_hour, 0, '', '.') }}}</td>
                                </tr>
                            </tbody>
                        </table>
                        @endif

                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-primary">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>Work Time Data</h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <!--/ panel heading/header -->

                    <!-- panel toolbar wrapper -->
                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <!-- <div class="panel-toolbar pl10">
                            <div class="checkbox custom-checkbox pull-left">
                                <input type="checkbox" id="customcheckbox-one0" value="1" data-toggle="checkall" data-target="#table1">
                                <label for="customcheckbox-one0">&nbsp;&nbsp;Select all</label>
                            </div>
                        </div> -->
                        <div class="panel-toolbar text-right">
                            @if(isset($work_time->id))
                            <a href="{{{ route('resource_work_time.edit', array($resource->id, $work_time->id)) }}}" title="Edit Work Time" class="btn btn-success mb5">
                            @else
                            <a href="{{{ route('resource_work_time.create', $resource->id) }}}" title="Edit Work Time" class="btn btn-success mb5">
                            @endif
                                <span>Edit Work Time</span>
                            </a>
                        </div>
                    </div>
                    <!--/ panel toolbar wrapper -->

                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-bordered table-hover" id="table1">
                            <thead>
                                <tr>
                                    <th class="text-center" colspan="7">Work Week</th>
                                    <th class="text-center" rowspan="2" style="vertical-align: middle;">Capacity Per Week</th>
                                </tr>
                                <tr>
                                    <th class="text-center">Mon</th>
                                    <th class="text-center">Tue</th>
                                    <th class="text-center">Wed</th>
                                    <th class="text-center">Thu</th>
                                    <th class="text-center">Fri</th>
                                    <th class="text-center">Sat</th>
                                    <th class="text-center">Sun</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">{{{ @$work_time->mon }}}</td>
                                    <td class="text-center">{{{ @$work_time->tue }}}</td>
                                    <td class="text-center">{{{ @$work_time->wed }}}</td>
                                    <td class="text-center">{{{ @$work_time->thu }}}</td>
                                    <td class="text-center">{{{ @$work_time->fri }}}</td>
                                    <td class="text-center">{{{ @$work_time->sat }}}</td>
                                    <td class="text-center">{{{ @$work_time->sun }}}</td>
                                    <td class="text-center">{{{ @$work_time->per_week }}}</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript">
(function(){
    $(".delete-confirm").on("click", function (event) {
        var self = $(this);
        bootbox.confirm("Are you sure to delete this resource?", function (result) {
            if(result){
                var resourceid = self.attr('data-id');
                $('button#del-'+resourceid).click();
            }
        });
        event.preventDefault();
    });
})();
</script>
<!-- <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/components/notification.js') }}}"></script> -->
@stop
