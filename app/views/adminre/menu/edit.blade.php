@extends($theme.'._layouts.master')
 
@section('main')
<section id="main" role="main">
    {{ Notification::showAll() }}

    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Form::model($menu, array('method' => 'put', 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate', 'route' => array('menu.update', $menu->id))) }}
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit User Details</h3>
                    </div>               
                    <div class="panel-body">
                        <div class="panel-heading">
                            <h3 class="panel-title">Menu Details</h3>
                        </div>
                    </div>               
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Parent</label>
                            <div class="col-sm-5">
                                <select name="parent_id" class="form-control">
                                <option></option>
                                @foreach($parents as $parent)
                                <option {{{ $menu->parent_id == $parent->id ? 'selected' : '' }}} value="{{{ $parent->id }}}">{{{ $parent->parent_id ? '&#8627; ' : '' }}}{{{ $parent->menu_name }}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Menu Name</label>
                            <div class="col-sm-5">
                                <input type="text" name="menu" class="form-control" value="{{{ $menu->menu_name }}}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Route</label>
                            <div class="col-sm-5">
                                <input type="text" name="route" class="form-control" value="{{{ $menu->route }}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Route Params</label>
                            <div class="col-sm-5">
                                <input type="text" name="param" class="form-control" value="{{{ $menu->param }}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Attribute Class</label>
                            <div class="col-sm-5">
                                <input type="text" name="class" class="form-control" value="{{{ $menu->class_attr }}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Order</label>
                            <div class="col-sm-2">
                                <input type="number" name="order" class="form-control" value="{{{ $menu->order }}}" required>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Edit Menu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop