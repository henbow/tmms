@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/summernote/css/summernote.min.css') }}}"> 
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/selectize/css/selectize.min.css') }}}"> 
@stop

@section('main')
<section id="main" role="main">
    {{ Notification::showAll() }}

    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Form::open(array('route' => 'helpdesk.store', 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate')) }}
                    <div class="panel-heading">
                        <h3 class="panel-title">New Request</h3>
                    </div>               
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ticket ID:</label>
                            <div class="col-sm-6">
                                <input type="text" name="ticket_id" value="{{{ generate_helpdesk_ticket() }}}" class="form-control" style="font-weight: bold;" required />
                            </div><div class="clear"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Category:</label>
                            <div class="col-sm-6">
                                <select name="category" id="category" class="form-control" required>
                                    <option></option>
                                    @foreach($categories as $cat)
                                    <option value="{{{ $cat->id }}}">{{{ $cat->category }}}</option>
                                    @endforeach
                                </select>
                            </div><div class="clear"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Sub Category:</label>
                            <div class="col-sm-6">
                                <select name="subcategory" id="subcategory" class="form-control">
                                    <option></option>
                                    <option value="mechanic">MECHANIC</option>
                                    <option value="electric">ELECTRIC</option>
                                </select>
                            </div><div class="clear"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Priority:</label>
                            <div class="col-sm-6">
                                <select name="priority" id="priority" class="form-control" required>
                                    @foreach($priorities as $priority)
                                    <option {{{ $priority->priority == 'Normal' ? 'selected' : '' }}} value="{{{ $priority->id }}}">{{{ $priority->priority }}}</option>
                                    @endforeach
                                </select>
                            </div><div class="clear"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Process Manager (Request To):</label>
                            <div class="col-sm-6">
                                <select name="in_charge" id="in_charge" class="form-control" required>
                                    <option></option>
                                    @foreach(\Sentry::findAllGroups() as $group)
                                    @if($group->id != 4)
                                    <option value="{{{ $group->id }}}">{{{ $group->name }}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div><div class="clear"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Subject:</label>
                            <div class="col-sm-6"><input type="text" class="form-control" name="title" id="title" value="" required /></div><div class="clear"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Description:</label>
                        </div>                        
                    </div>
                    <textarea name="desc" rows="5" class="addrequestnote"></textarea>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Create Request</button>
                        <button type="reset" class="btn btn-reset">Reset</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/parsley/js/parsley.min.js') }}}"></script>        
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/summernote/js/summernote.min.js') }}}"></script>   
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/inputmask/js/inputmask.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-timepicker.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-touch.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/typeahead/bootstrap3-typeahead.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/element.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/jquery.runner.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/wysiwyg.js') }}}"></script>


<script type="text/javascript">
$(".addrequestnote").summernote({
    height: 200,
    toolbar: [
        ["style", ["style"]],
        ["style", ["bold", "italic", "underline", "clear"]],
        ["fontsize", ["fontsize"]],
        ["color", ["color"]],
        ["para", ["ul", "ol", "paragraph"]],
        ["height", ["height"]],
        ["table", ["table"]]
    ]
});
</script>
<script type="text/javascript">
$(function () {
    // custom select
    // ================================
    $("select").selectize();
});
</script>
@stop
