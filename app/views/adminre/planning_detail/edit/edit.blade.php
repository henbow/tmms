@extends($theme.'._layouts.master')

@section('main')
<section id="main" role="main">
    {{ Notification::showAll() }}

    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Form::model($process, array('method' => 'put', 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate', 'route' => array('processes.update', $process->id))) }}
                    <div class="panel-heading">
                        <h3 class="panel-title">Material Process</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Code</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="code" id="code" value="{{{ $process->code }}}" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Group</label>
                            <div class="col-sm-6">
                                <select name="group" class="form-control" required>
                                    <option></option>
                                    @foreach(ProcessGroup::all() as $group)
                                    <option {{{ $process->group_id == $group->id ? 'selected' : '' }}} value="{{{ $group->id }}}">{{{ $group->group }}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Process Name</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="process" id="process" value="{{{ $process->process }}}" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Resource</label>
                            <div class="col-sm-6">
                                <select multiple="multiple" class="form-control multiple" name="resource[]" id="resource" title="Click to select resource" required>
                                    @foreach (Resource::all() as $resource)
                                        <?php $selected = ''; ?>
                                        <?php foreach(ProcessResource::where('process_id', '=', $process->id)->get() as $process_resource): ?>
                                        <?php
                                        if($process_resource->resource_id == $resource->id):
                                            $selected = 'selected';
                                        endif;
                                        ?>
                                        <?php endforeach; ?>
                                        <option {{{ $selected }}} value="{{{ $resource->id }}}">{{{ ucwords($resource->name) }}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Type</label>
                            <div class="col-sm-6">
                                <select class="form-control" name="type" id="type" required>
                                    <option {{{ $process->type == 'in plan' ? 'selected' : '' }}} value="in plan">In Plan</option>
                                    <option {{{ $process->type == 'out plan' ? 'selected' : '' }}} value="out plan">Out Plan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Update</button>
                        <button type="reset" class="btn btn-reset">Reset</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/parsley/js/parsley.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/validation.js') }}}"></script>
@stop

