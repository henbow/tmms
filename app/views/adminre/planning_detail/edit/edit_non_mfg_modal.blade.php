<!-- START modal-lg -->
@if($is_planning)
{{ Form::open(array('route' => array('project.planning.detail.update', $planning_data->id), 'id' => 'new-process', 'class' => 'form-horizontal', 'data-parsley-validate')) }}
@else
{{ Form::open(array('route' => array('project.planning.detail.update', $planning_data->id), 'onsubmit' => 'doSubmit(this)', 'id' => 'new-process', 'class' => 'form-horizontal', 'data-parsley-validate')) }}
@endif
<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">Edit {{{ $type }}} Process Planning</h3>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Process</label>
                    <div class="col-sm-6">
                        <input type="text" name="process_name_{{{ $random }}}" value="{{{ $planning_process }}}" class="form-control" required />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Process Specification</label>
                    <div class="col-sm-6">
                        <textarea name="specification" class="form-control" rows="5">{{{ $planning_data->specification }}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Position</label>
                    <div class="col-sm-2">
                        <input type="number" name="position" id="position" class="form-control" value="{{{ $planning_data->position }}}" required >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Estimation</label>
                    <div class="col-sm-4">
                        <input type="number" name="estimation" id="estimation" class="form-control" value="{{{ $planning_data->estimation_hour }}}" required >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Resource Type</label>
                    <div class="col-sm-6">
                        <span class="radio custom-radio custom-radio-primary">
                            <input type="radio" id="resource_select1"name="resource_select" value="auto">
                            <label for="resource_select1">&nbsp;&nbsp;Automatic</label>
                        </span>
                        <span class="radio custom-radio custom-radio-teal">
                            <input type="radio" id="resource_select2" name="resource_select" value="select" checked>
                            <label for="resource_select2">&nbsp;&nbsp;Manual </label>
                        </span>
                    </div>
                </div>
                <div class="form-group" id="resource_name">
                    <label class="col-sm-2 control-label">Resource Group</label>
                    <div class="col-sm-5">
                        <select name="resource_group" class="form-control">
                            <?php $resource_groups = ResourceGroup::all(); ?>
                            @foreach($resource_groups as $resource_group)
                            <option value="{{{$resource_group->id}}}" {{{ $planning_data->resource_group_id == $resource_group->id ? 'selected' : '' }}}>{{{$resource_group->name}}}</option>
                            @endforeach
                        </select>
                        <input type="hidden" name="resource_group_id" value="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">

    <!-- START Hidden field -->
    <input type="hidden" name="process_id" value="{{{ $planning_data->process_id }}}" />
    <input type="hidden" name="group_id" value="{{{ $group_id }}}" />
    <input type="hidden" name="resource_id" value="{{{ $planning_data->resource_id }}}" />
    <input type="hidden" name="start" value="{{{ $min_start_date }}}" />
    <input type="hidden" name="finish" value="{{{ $max_finish_date }}}" />
    <input type="hidden" name="planning_type" value="nop" />
    <input type="hidden" name="project_id" value="{{{ $project->id }}}" />
    <input type="hidden" name="random" value="{{{ $random }}}" />
    <!--/END Hidden field -->

    <button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
    <button type="submit" class="btn btn-primary"> Save </button>
</div>
{{ Form::close() }}
<script type="text/javascript">
(function(){
    $("[name=\"process_name_{{{ $random }}}\"]").typeahead({
        source:function (query, process) {
            processes = [];
            map = {};

            <?php $process_data = array(); ?>
            <?php foreach($processes as $process): ?>
            <?php $process_data[] = array('id' => $process->id, 'process_name' => $process->process); ?>
            <?php endforeach; ?>

            var data = <?php echo json_encode($process_data); ?>;

            $.each(data, function (i, process) {
                map[process.process_name] = process;
                processes.push(process.process_name);
            });

            process(processes);
        },
        updater: function (process) {
            selectedId = map[process].id;
            $('[name="process_id"]').val(selectedId);
            return process;
        }
    });
    $('[name="resource_select"]').on('click', function(e){
        if($(this).val() === 'select'){
            $('#resource_name').fadeIn();
        }else{
            $('#resource_name').fadeOut();
        }
    });
    $("#start-form, #finish-form").datepicker({
        changeMonth: true,
        changeYear: true,
        minDate: new Date('{{{ $min_start_date }}}'),
        maxDate: new Date('{{{ $max_finish_date }}}'),
    });
    var $form = $("form#new-process");
    $form.on("click", "button[type=submit]", function (e) {
        var $this = $(this);

        // NProgress.start();

        // you can do the ajax request here
        // this is for demo purpose only
        $this.submit();
    });
})();
</script>
<!--/ END modal-lg