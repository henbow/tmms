@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatables/css/jquery.datatables.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/selectize/css/selectize.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui-timepicker.min.css') }}}">
<style type="text/css">
.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }
</style>
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="{{{ route('project.index') }}}">Project List</a></li>
                        <li class="active">{{{ $sub_title }}}</li>
                    </ol>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        @include($theme . '.planning_detail.search_filter')

        <div class="row">
            <div class="col-md-12">
                {{ Notification::showAll() }}

                <!-- START panel -->
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Material List</h3>
                    </div>

                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <div class="panel-toolbar text-right">
                            <a href="{{{ route('template.planning.mfg.header', array($master_plan_id)) }}}" id="copy-template" class="btn btn-success mb5"><i class="ico-folder-plus2"></i> Process Template for Materials</a>
                        </div>
                    </div>

                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-striped table-bordered table-hover" id="material-table">
                            <thead>
                                <tr>
                                    <th class="text-center" width="5%">Barcode</th>
                                    <th class="text-center">Part Name</th>
                                    <th class="text-center">Dimension</th>
                                    <th class="text-center">Qty</th>
                                    <th class="text-center">Est (Hours)</th>
                                    <!-- <th class="text-center">Priority</th> -->
                                    <th class="text-center" width="10%"></th>
                                    <th class="text-center" width="1%"><input type="checkbox" name="select_all" value="select_all" class="form-control" style="height: 14px; width: 14px;"></th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 11px">
                            @foreach($materials as $material)
                                <?php $material_is_processed = is_material_planned($master_plan_id, $material->id); ?>
                                <?php $planning_data = PlanningMFG::where('master_planning_id', '=',$master_plan_id)->where('material_id', '=', $material->id); ?>
                                <?php $estimation = $planning_data->sum('estimation'); ?>
                                <?php $estimation_hour = $planning_data->sum('estimation_hour'); ?>
                                <tr class="material_item">
                                    <td class="text-center">{{{ @$material->barcode }}}</td>
                                    <td>
                                        <!-- <a class="popover-material" data-toggle="popover" title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?" href="{{{ route('project.planning.material_mfg_process', array($material->id, $master_plan_id)) }}}"> -->
                                        <a href="{{{ route('project.planning.material_mfg_process', array($material->id, $master_plan_id)) }}}">
                                            {{{ $material->material_name }}}
                                        </a>
                                    </td>
                                    <td class="text-center">{{{ 'L: '.$material->length.', W: '.$material->width.', H: '.$material->height }}}</td>
                                    <td class="text-center">{{{ $material->qty }}}</td>
                                    <td class="text-center">{{{ $estimation_hour }}}</td>
                                    <!-- <td class="text-center">{{{ 0 }}}</td> -->
                                    <td class="text-center"><a href="{{{ route('project.planning.material_mfg_process', array($material->id, $master_plan_id)) }}}" class="btn btn-sm {{{ $material_is_processed ? 'btn-primary' : 'btn-danger' }}}">Process</a></td>
                                    <td class="text-center">
                                        <input type="checkbox" name="material[]" value="{{{ $material->id }}}" class="form-control material-id" style="height: 14px; width: 14px;">
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>

<!-- START modal-lg -->
<div id="bs-modal-lg" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<!--/ END modal-lg -->
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/tabletools.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/zeroclipboard.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/inputmask/js/inputmask.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-timepicker.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-touch.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/typeahead/bootstrap3-typeahead.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/element.js') }}}"></script>
<script type="text/javascript">
(function(){
    // $('.popover-material').popover('toggle');
    var table = $("#material-table").dataTable({
        "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>><'table-responsive'rt><'row'<'col-sm-6'p><'col-sm-6'i>>",
        "aoColumns": [
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false }
        ],
        "iDisplayLength": 50
    });
    var details_in_popup = function(material_id, master_plan_id, div_id){
        // $.post('{{{ route('project.planning.material_process.popover') }}}', function(response) {
        //     $('#'+div_id).html(response);
        // });
        // return '<div id="'+ div_id +'">Loading...</div>';
    };

    $('a.popup-ajax').popover({
        "html": true,
        "content": function(){
            var div_id =  "tmp-id-" + $.now();
            return details_in_popup($(this).attr('href'), div_id);
        }
    });
    $(".delete-confirm").on("click", function (event) {
        var self = $(this);
        bootbox.confirm("Are you sure to delete this process?", function (result) {
            if(result){
                return true;
            }
        });
        event.preventDefault();
    });
    $('#copy-template').click(function(e){
        e.preventDefault();
        $('#bs-modal-lg .modal-content').load($(this).attr('href'),function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $('#add-process').click(function(e){
        $('#bs-modal-lg .modal-content').load('{{{ route('project.planning.detail.create', $master_plan_id) }}}',function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $('.edit-process').click(function(e){
        var editUrl = $(this).attr('data-href');
        $('#bs-modal-lg .modal-content').load(editUrl,function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $("#start, #finish, #delivery, #start-form, #finish-form").datepicker({
        changeMonth: true,
        changeYear: true
    });
    $('[name="select_all"]').click(function(){
        if ($(this).attr('checked')) {
            $('.material-id').attr('checked', 'checked');
        } else {
            $('.material-id').removeAttr('checked');
        }
    });
})();
</script>
@stop
