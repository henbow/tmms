<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">Select Template Material Process</h3>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <!-- START panel -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Template List</h3>
                    <div class="panel-toolbar text-right">
                        <!-- option -->
                        <div class="option">
                            <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                        </div>
                        <!--/ option -->
                    </div>
                </div>
                <!-- panel body with collapse capabale -->
                <div class="table-responsive panel-collapse pull out">
                    <table class="table table-striped table-bordered table-hover" id="master-plan-header" style="font-size: 10px">
                        <thead>
                            <tr>
                                <th width="5%" class="text-center">No</th>
                                <th class="text-center">Template Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach($templates as $template)
                            <tr onclick="getTplDetail(this);" class="tpl-header" id="tpl-{{{ $template->id }}}" style="cursor:pointer;">
                                <td class="text-center">{{{ $no++ }}}</td>
                                <td>{{{ $template->name }}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--/ panel body with collapse capabale -->
            </div>
        </div>

        <div class="col-md-12">
            <!-- START panel -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Material Process</h3>
                    <div class="panel-toolbar text-right">
                        <!-- option -->
                        <div class="option">
                            <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                        </div>
                        <!--/ option -->
                    </div>
                </div>
                <!-- panel body with collapse capabale -->
                <div class="table-responsive panel-collapse pull out">
                    <table class="table table-striped table-bordered table-hover" id="material-mfg-process" style="font-size: 10px">
                        <thead>
                            <tr>
                                <th width="5%" class="text-center">No</th>
                                <th class="text-center" width="10%">Process Group</th>
                                <th class="text-center">Process</th>
                                <th class="text-center">Position</th>
                                <th class="text-center">Specification</th>
                                <th class="text-center" width="20%">Resources</th>
                                <th class="text-center" width="10%">Est. (Hours)</th>
                            </tr>
                        </thead>
                        <tbody><!-- Template Content --></tbody>
                    </table>
                </div>
                <!--/ panel body with collapse capabale -->
            </div>
        </div>

        <div class="col-md-12 text-right">
            {{ Form::open(array('route' => array('template.planning.mfg.do_process.header', $master_plan_id), 'id' => 'from-template', 'class' => 'form-horizontal', 'data-parsley-validate')) }}
            <input type="hidden" name="template-id" value="" />
            <input type="hidden" name="material-ids" value="" />
            <button type="submit" class="btn btn-success mb5"> Select Template </button>
            {{ Form::close() }}
        </div>
    </div>
</div>

<!--/ END Template Container -->
</div>
<script type="text/javascript">
    (function () {
        var table = $("#master-plan-header").dataTable({
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>><'table-responsive'rt><'row'<'col-sm-6'p><'col-sm-6'i>>",
            "aoColumns": [
                { "sClass": "text-center", "bSearchable": false, "bSortable": false },
                { "sClass": "text-left" },
            ],
            "iDisplayLength": 10
        });
        $('#from-template').submit(function(e){
            NProgress.start();
            return true;
        });
    })();
    function getTplDetail (elem) {
        var idAttr = $(elem).attr('id').split('-');
        var tplId = idAttr[1];
        var matIds = [];

        $('.material-id:checked').each(function(i){matIds[i]=$(this).val()});
        $('[name="template-id"]').val(tplId);
        $('[name="material-ids"]').val(matIds.join(','));
        $('#material-mfg-process tbody').empty();

        $.post('{{{ route('template.planning.mfg.detail') }}}', {tpl_id: tplId}, function (resp) {
            var tpldata = $.parseJSON(resp);
            $.each(tpldata, function (i, data) {
                var item = '<tr>' +
                        '<td class="text-center">' + (i + 1) + '</td>' +
                        '<td class="text-left">' + data.group + '</td>' +
                        '<td class="text-center">' + data.process + '</td>' +
                        '<td class="text-center">' + data.position + '</td>' +
                        '<td class="text-center">' + data.specification + '</td>' +
                        '<td class="text-left">' + data.resource + '</td>' +
                        '<td class="text-center">' + data.estimation_hour + '</td>' +
                        '</tr>';
                $('#material-mfg-process tbody').append(item);
            });
        });
    }
</script>