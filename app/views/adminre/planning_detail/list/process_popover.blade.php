<table class="table table-striped table-bordered table-hover" id="ajax-source">
    <thead>
        <tr>
            <th class="text-center" width="5%">Code</th>
            <th class="text-center">Process Name</th>
            <th class="text-center">Est (Hours)</th>
            <th class="text-center">Position</th>
            <th class="text-center">Specification</th>
            <th class="text-center" width="15%">Resource</th>
            <th class="text-center" width="10%"></th>
        </tr>
    </thead>
    <tbody style="font-size: 11px"></tbody>
</table>