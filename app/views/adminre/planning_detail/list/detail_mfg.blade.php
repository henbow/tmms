@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatables/css/jquery.datatables.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/selectize/css/selectize.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui-timepicker.min.css') }}}">
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="{{{ route('project.index') }}}">Project List</a></li>
                        <li><a href="{{{ route('project.planning.detail', array($process_group_id, $master_plan_id)) }}}">Material List</a></li>
                        <li class="active">{{{ $sub_title }}}</li>
                    </ol>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        <!--include($theme . '.planning_detail.search_filter')-->

        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel body with collapse capabale -->
                    <div class="panel-collapse pull out">
                        <div class="panel-body">
                            <dl class="dl-horizontal" style="padding-bottom: 0;margin-bottom: 0;">
                                <dt style="width:100px;">Code</dt><dd style="margin-left: 120px;">{{{ $material->part_code }}}</dd>
                                <dt style="width:100px;">Material Name</dt><dd style="margin-left: 120px;">{{{ $material->material_name }}}</dd>
                                <dt style="width:100px;">PO Number</dt><dd style="margin-left: 120px;">{{{ $material->po_number }}}</dd>
                            </dl>
                        </div>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
                <!--/ END panel -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                {{ Notification::showAll() }}

                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel heading/header -->
                    <!--/ panel heading/header -->

                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <div class="panel-toolbar text-right">
                            <a href="{{{ route('template.planning.mfg', array($material->id, $master_plan_id)) }}}" id="copy-template" class="btn btn-success mb5"><i class="ico-folder-plus2"></i> Copy From Template</a>
                            <!-- <a href="#" id="copy-nop" class="btn btn-success mb5"><i class="ico-folder-plus2"></i> Copy From Other NOP</a>   -->
                            <a href="#" id="add-process" class="btn btn-success mb5"><i class="ico-folder-plus2"></i> Add New Process</a>
                        </div>
                    </div>

                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-striped table-bordered table-hover" id="ajax-source">
                            <thead>
                                <tr>
                                    <th class="text-center" width="5%">Code</th>
                                    <th class="text-center">Process Group</th>
                                    <th class="text-center">Process Name</th>
                                    <th class="text-center">Est (Hours)</th>
                                    <th class="text-center">Position</th>
                                    <th class="text-center">Specification</th>
                                    <th class="text-center" width="15%">Resource Group</th>
                                    <th class="text-center" width="10%"></th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 11px">
                            @foreach($processes as $process)
                                <?php $process_data = Process::find($process->process_id); ?>
                                <?php $resource_group = ResourceGroup::find($process->resource_group_id); ?>
                                @if($process_data)
                                <?php $process_group = ProcessGroup::find($process_data->group_id); ?>
                                <tr>
                                    <td class="text-center" style="vertical-align:top;">{{{ $process_data->code }}}</td>
                                    <td class="text-center" style="vertical-align:top;">{{{ $process_group->group }}}</td>
                                    <td class="text-center" style="vertical-align:top;">{{{ $process_data->process }}}</td>
                                    <td class="text-center" style="vertical-align:top;">{{{ $process->estimation_hour }}}</td>
                                    <td class="text-center" style="vertical-align:top;">{{{ $process->position }}}</td>
                                    <td class="text-center" style="vertical-align:top;">{{{ $process->specification }}}</td>
                                    <td class="text-center">{{{ isset($resource_group->name) ? $resource_group->name : '' }}}</td>
                                    <td class="text-center" style="vertical-align:top;">
                                        <!-- button toolbar -->
                                        <div class="toolbar">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-default">Action</button>
                                                <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="javascript:void(0)" data-href="{{{ route('project.planning.detail.edit_mfg', $process->id) }}}" class="edit-process"><i class="icon ico-newspaper"></i> Edit</a></li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a href="{{{ route('project.planning.detail.destroy', array('material', $process->id)) }}}" data-id="{{{ $process->id }}}" class="delete-confirm text-danger"><i class="icon ico-remove3"></i> Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--/ button toolbar -->
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>

<!-- START modal-lg -->
<div id="bs-modal-lg" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<!--/ END modal-lg -->
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/tabletools.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/zeroclipboard.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/inputmask/js/inputmask.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-timepicker.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-touch.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/typeahead/bootstrap3-typeahead.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/element.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript">
(function(){
    $(".delete-confirm").on("click", function (e) {
        e.preventDefault();
        var self = $(this);
        bootbox.confirm("Are you sure to delete this process?", function (result) {
            if(result){
                window.location.href = self.attr('href');
            }
        });
    });
    $('#copy-template').click(function(e){
        e.preventDefault();
        $('#bs-modal-lg .modal-content').load($(this).attr('href'),function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $('#add-process').click(function(e){
        e.preventDefault();
        $('#bs-modal-lg .modal-content').load('{{{ route('project.planning.detail.create_mfg', array($material->id, $master_plan_id)) }}}',function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $('.edit-process').click(function(e){
        var editUrl = $(this).attr('data-href');
        $('#bs-modal-lg .modal-content').load(editUrl,function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $("#start, #finish, #delivery, #start-form, #finish-form").datepicker({
        changeMonth: true,
        changeYear: true
    });
})();
</script>
@stop
