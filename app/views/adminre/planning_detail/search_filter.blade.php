<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">

            <div class="table-responsive panel-collapse pull out">
                {{ Form::open(array('route' => 'project.planning.filter', 'class' => 'form-horizontal', 'id' => 'filter-planning', 'data-parsley-validate')) }}
                <br>
                <div class="form-group">
                    <label class="col-sm-2 control-label">NOP</label>
                    <div class="col-sm-3">
                        <input type="hidden" name="project_id" id="project_id" value="{{{ $project->id }}}" />
                        <input type="text" name="nop" id="nop" value="{{{ @$project->nop }}}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-3">
                        <input type="text" name="project" id="project" value="{{{ @$project->project_name }}}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Master Planning</label>
                    <div class="col-sm-3">
                        <select name="step" class="form-control" required>
                            @foreach($steps as $_step)
                            <?php $group = ProcessGroup::find($_step->process_group_id); ?>
                            <option value="{{{ $_step->process_group_id }}}" {{{ ( isset($step) ? ( $step == $_step->process_group_id ? 'selected' : '' ) : '' ) }}}>{{{ $_step->parent_id ? '&nbsp;&#8627;&nbsp;' : '' }}}{{{ $group->group }}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Estimation</label>
                    <div class="col-sm-2">
                        <input type="number" name="estimation" id="estimation" value="{{{ $estimation }}}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Start Date</label>
                    <div class="col-sm-3">
                        <input type="text" name="start" id="start" value="{{{ @date('m/d/Y', strtotime($start_date)) }}}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Finish Date</label>
                    <div class="col-sm-3">
                        <input type="text" name="finish" id="finish" value="{{{ @date('m/d/Y', strtotime($finish_date)) }}}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                    <div class="col-sm-3">
                        <button type="submit" id="search" class="btn btn-success">Search</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>

    </div>
</div>