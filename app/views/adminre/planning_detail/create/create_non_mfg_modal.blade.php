<!-- START modal-lg -->
{{ Form::open(array('route' => array('project.planning.detail.store', $master_plan_id), 'id' => 'new-process', 'class' => 'form-horizontal', 'data-parsley-validate')) }}
<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">Add New {{{ $step_name }}} Process Planning</h3>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Process</label>
                    <div class="col-sm-6">
                        <input type="text" name="process_name_{{{ $random }}}" class="form-control" required />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Process Specification</label>
                    <div class="col-sm-6">
                        <textarea name="specification" class="form-control" rows="5"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Position</label>
                    <div class="col-sm-2">
                        <input type="number" name="position" id="position" class="form-control" value="0" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Estimation</label>
                    <div class="col-sm-4">
                        <input type="number" value="0" step="0.01" name="estimation" id="estimation" class="form-control" required >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Resource Type</label>
                    <div class="col-sm-6">
                        <span class="radio custom-radio custom-radio-primary">
                            <input type="radio" id="resource_select1"name="resource_select" value="auto" checked>
                            <label for="resource_select1">&nbsp;&nbsp;Automatic</label>
                        </span>
                        <span class="radio custom-radio custom-radio-teal">
                            <input type="radio" id="resource_select2" name="resource_select" value="select">
                            <label for="resource_select2">&nbsp;&nbsp;Manual </label>
                        </span>
                    </div>
                </div>
                <div class="form-group" id="resource_name" style="display: none">
                    <label class="col-sm-2 control-label">Resource Group</label>
                    <div class="col-sm-5">
                        <select name="resource_group" class="form-control">
                        <?php $resource_groups = ResourceGroup::all(); ?>
                        @foreach($resource_groups as $resource_group)
                        <option value="{{{$resource_group->id}}}">{{{$resource_group->name}}}</option>
                        @endforeach
                        </select>
                        <input type="hidden" name="resource_group_id" value="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">

    <!-- START Hidden field -->
    <input type="hidden" name="process_id" value="" />
    <input type="hidden" name="group_id" value="" />
    <input type="hidden" name="resource_id" value="" />
    <input type="hidden" name="start" value="{{{ $min_start_date }}}" />
    <input type="hidden" name="finish" value="{{{ $max_finish_date }}}" />
    <input type="hidden" name="step" value="{{{ $step }}}" />
    <input type="hidden" name="project_id" value="{{{ $project->id }}}" />
    <input type="hidden" name="random" value="{{{ $random }}}" />
    <!--/END Hidden field -->

    <button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
    <button type="submit" class="btn btn-primary"> Save </button>
</div>
{{ Form::close() }}
<script type="text/javascript">
(function(){
    $("[name=\"process_name_{{{ $random }}}\"]").typeahead({
        source:function (query, process) {
            processes = [];
            map = {};

            <?php $process_data = array(); ?>
            <?php foreach($processes as $process): ?>
            <?php $process_data[] = array(
                'id' => $process->id,
                'process_name' => $process->process,
                'group_id' => $process->group_id
            ); ?>
            <?php endforeach; ?>

            var data = <?php echo json_encode($process_data); ?>;

            $.each(data, function (i, process) {
                map[process.process_name] = process;
                processes.push(process.process_name);
            });

            process(processes);
        },
        updater: function (process) {
            $('[name="process_id"]').val(map[process].id);
            $('[name="group_id"]').val(map[process].group_id);
            return process;
        }
    });
    $('[name="resource_select"]').on('click', function(e){
        if($(this).val() === 'select'){
            $('#resource_name').fadeIn();
        }else{
            $('#resource_name').fadeOut();
        }
    });
    var $form = $("form#new-process");
    $form.on("click", "button[type=submit]", function (e) {
        var $this = $(this);
        NProgress.start();
        $this.submit();
    });
})();
</script>
<!--/ END modal-lg