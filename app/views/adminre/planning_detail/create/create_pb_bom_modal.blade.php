<!-- START modal-lg -->
{{ Form::open(array('route' => array('project.planning.detail.store_pb_bom', $material->id, $plan_id), 'id' => 'new-process', 'class' => 'form-horizontal', 'data-parsley-validate')) }}
<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">Add PB-BOM Process</h3>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">   
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Flag PB</label>
                    <div class="col-sm-6">
                        <input type="text" name="flag_pb" class="form-control" value="" required />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Estimation</label>
                    <div class="col-sm-4">
                        <input type="number" name="estimation" class="form-control" required />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Resource Type</label>
                    <div class="col-sm-6">
                        <span class="radio custom-radio custom-radio-primary">  
                            <input type="radio" id="resource_select1"name="resource_select" value="auto" checked>  
                            <label for="resource_select1">&nbsp;&nbsp;Automatic</label>   
                        </span>
                        <span class="radio custom-radio custom-radio-teal">  
                            <input type="radio" id="resource_select2" name="resource_select" value="select">  
                            <label for="resource_select2">&nbsp;&nbsp;Manual </label>   
                        </span>
                    </div>
                </div>
                <div class="form-group" id="resource_name" style="display: none">
                    <label class="col-sm-2 control-label">Resource Name</label>
                    <div class="col-sm-5">
                        <select name="resource" class="form-control"></select>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <!-- START Hidden field -->
    <input type="hidden" name="start" value="{{{ $min_start_date }}}" />
    <input type="hidden" name="finish" value="{{{ $max_finish_date }}}" />
    <input type="hidden" name="project_id" value="{{{ $project->id }}}" />
    <!--/END Hidden field -->
    
    <button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
    <button type="submit" class="btn btn-primary"> Save </button>
</div>
{{ Form::close() }}
<script type="text/javascript">
(function(){
    $("#start, #finish").datepicker({
        changeMonth: true,
        changeYear: true,
        minDate: new Date('{{{ $min_start_date }}}'),
        maxDate: new Date('{{{ $max_finish_date }}}')
    });
    $('[name="resource_select"]').on('click', function(e){
        if($(this).val() === 'select'){
            if($('[name="group_id"]').val()) {
                $.post('{{{ route('project.planning.detail.available_resource') }}}', {
                    group_id: $('[name="group_id"]').val(), 
                    start: $('[name="start"]').val(), 
                    finish: $('[name="finish"]').val()
                }, function(resp){
                    var datares = $.parseJSON(resp);
                    $.each(datares, function (i, resource) {
                        $('[name="resource"]').append('<option value="'+resource.id+'">'+resource.name+'</option>');
                    });
                });
            }
            
            $('#resource_name').fadeIn();
        }else{
            $('#resource_name').fadeOut();
        }
    });
    var $form = $("form#new-process");
    $form.on("click", "button[type=submit]", function (e) {
        var $this = $(this);

        // NProgress.start();

        // you can do the ajax request here
        // this is for demo purpose only
        $this.submit();
    });
})();
</script>
<!--/ END modal-lg