<!-- START modal-lg -->
{{ Form::open(array('route' => array('project.planning.detail.store_mfg', $material->id, $plan_id), 'id' => 'new-process', 'class' => 'form-horizontal', 'data-parsley-validate')) }}
<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">Add New Manufacturing Planning</h3>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Process</label>
                    <div class="col-sm-5">
                        <input type="text" name="process_name_{{{ $random }}}" class="form-control" required />
                        <input type="hidden" name="process_code" value="" />
                        <input type="hidden" name="group_id" value="" />
                        <input type="hidden" name="process_id" value="" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Position</label>
                    <div class="col-sm-2">
                        <input type="number" name="position" id="position" class="form-control" value="0" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Specification</label>
                    <div class="col-sm-6">
                        <textarea name="specification" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Estimation</label>
                    <div class="col-sm-2">
                        <input type="number" name="estimation" id="estimation" value="0" class="form-control" required />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Resource Type</label>
                    <div class="col-sm-6">
                        <span class="radio custom-radio custom-radio-primary">
                            <input type="radio" id="resource_select1"name="resource_select" value="auto" checked>
                            <label for="resource_select1">&nbsp;&nbsp;Automatic</label>
                        </span>
                        <span class="radio custom-radio custom-radio-teal">
                            <input type="radio" id="resource_select2" name="resource_select" value="select">
                            <label for="resource_select2">&nbsp;&nbsp;Manual </label>
                        </span>
                    </div>
                </div>
                <div class="form-group" id="resource_name" style="display: none">
                    <label class="col-sm-2 control-label">Resource Group</label>
                    <div class="col-sm-5">
                        <select name="resource_group" class="form-control">
                        <?php $resource_groups = ResourceGroup::all(); ?>
                        @foreach($resource_groups as $resource_group)
                        <option value="{{{$resource_group->id}}}">{{{$resource_group->name}}}</option>
                        @endforeach
                        </select>
                        <input type="hidden" name="resource_group_id" value="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">

    <!-- START Hidden field -->
    <input type="hidden" name="material_id" value="{{{ $material->id }}}" />
    <input type="hidden" name="planning_type" value="MANUFACTURING" />
    <input type="hidden" name="project_id" value="{{{ $project->id }}}" />
    <input type="hidden" name="random" value="{{{ $random }}}" />
    <input type="hidden" name="start" value="{{{ $min_start_date }}}" />
    <input type="hidden" name="finish" value="{{{ $max_finish_date }}}" />
    <!--/END Hidden field -->

    <button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
    <button type="submit" class="btn btn-primary"> Save </button>
</div>
{{ Form::close() }}
<script type="text/javascript">
(function(){
    $("[name=\"process_name_{{{ $random }}}\"]").typeahead({
        source:function (query, process) {
            processes = [];
            map = {};

            var data = <?php echo json_encode($process_data); ?>;

            $.each(data, function (i, process) {
                map[process.process_name] = process;
                processes.push(process.process_name);
            });

            process(processes);
        },
        updater: function (process) {
            $('[name="process_id"]').val(map[process].id);
            $('[name="process_code"]').val(map[process].code);
            $('[name="group_id"]').val(map[process].group_id);
            return process;
        }
    });
    $("#start-form, #finish-form").datepicker({
        changeMonth: true,
        changeYear: true,
        minDate: new Date('{{{ $min_start_date }}}'),
        maxDate: new Date('{{{ $max_finish_date }}}')
    });
    $('[name="resource_select"]').on('click', function(e){
        if($(this).val() === 'select'){
            $('#resource_name').fadeIn();
        }else{
            $('#resource_name').fadeOut();
        }
    });
    var $form = $("form#new-process");
    $form.on("click", "button[type=submit]", function (e) {
        var $this = $(this);

        // NProgress.start();

        // you can do the ajax request here
        // this is for demo purpose only
        $this.submit();
    });
})();
</script>
<!--/ END modal-lg
