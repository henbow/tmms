@extends($theme . '._layouts.master')

@section('main')
<section id="main" role="main">
    {{ Notification::showAll() }}

    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Form::open(array('route' => array('resource_spec.update', $resource_id, $spec->id), 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate')) }}
                    <div class="panel-heading">
                        <h3 class="panel-title">Update Resource Specification</h3>
                    </div>
                    <div class="panel-body">
                        @if(Resource::isHuman($resource_id))
                        <!-- Human -->
                        <div class="form-group human">
                            <label class="col-sm-2 control-label">Min Mould Size:</label>
                            <div class="col-sm-6"><input class="form-control" placeholder="in MM" type="text" name="minmouldsize" id="minmouldsize" value="{{{ $spec->min_mould_size }}}"/></div><div class="clear"></div>
                        </div>
                        <div class="form-group human">
                            <label class="col-sm-2 control-label">Max Mould Size:</label>
                            <div class="col-sm-6"><input class="form-control" placeholder="in MM" type="text" name="maxmouldsize" id="maxmouldsize" value="{{{ $spec->max_mould_size }}}"/></div><div class="clear"></div>
                        </div>

                        @else
                        <!-- Machine -->
                        <div class="form-group machine">
                            <label class="col-sm-2 control-label">Min Table Load:</label>
                            <div class="col-sm-6"><input class="form-control" placeholder="in KG" type="text" name="mintabload" id="mintabload" value="{{{ $spec->min_table_load }}}"/></div><div class="clear"></div>
                        </div>
                        <div class="form-group machine">
                            <label class="col-sm-2 control-label">Max Table Load:</label>
                            <div class="col-sm-6"><input class="form-control" placeholder="in KG" type="text" name="maxtabload" id="maxtabload" value="{{{ $spec->max_table_load }}}"/></div><div class="clear"></div>
                        </div>
                        <div class="form-group machine">
                            <label class="col-sm-2 control-label">Table Size:</label>
                            <div class="col-sm-6"><input class="form-control" placeholder="in MM" type="text" name="tablesize" id="tablesize" value="{{{ $spec->table_size }}}"/></div><div class="clear"></div>
                        </div>
                        <div class="form-group machine">
                            <label class="col-sm-2 control-label">Travel Range:</label>
                            <div class="col-sm-6"><input class="form-control" placeholder="in MM" type="text" name="travelrange" id="travelrange" value="{{{ $spec->travel_range }}}"/></div><div class="clear"></div>
                        </div>
                        <div class="form-group machine">
                            <label class="col-sm-2 control-label">Accuracy:</label>
                            <div class="col-sm-6"><input class="form-control" placeholder="in MM" type="text" name="accuracy" id="accuracy" value="{{{ $spec->accuracy }}}"/></div><div class="clear"></div>
                        </div>
                        @endif

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Standard Rate/Hour:</label>
                            <div class="col-sm-6"><input class="form-control" type="text" name="stdrate" id="stdrate" value="{{{ $spec->std_rate_per_hour }}}"/></div><div class="clear"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Overtime Rate/Hour:</label>
                            <div class="col-sm-6"><input class="form-control" type="text" name="ovtrate" id="ovtrate" value="{{{ $spec->ovt_rate_per_hour }}}"/></div><div class="clear"></div>
                        </div>

                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Update</button>
                        <button type="reset" class="btn btn-reset">Reset</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/parsley/js/parsley.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/validation.js') }}}"></script>
@stop

