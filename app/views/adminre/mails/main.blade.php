<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; margin: 0; padding: 0;">
<head>
  <meta name="viewport" content="width=device-width" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>TMMS Notification</title>
</head>
<body bgcolor="#FFFFFF" style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;">
<style type="text/css">
@media only screen and (max-width: 750px) {
  a[class="btn"] {
    display: block !important; margin-bottom: 10px !important; background-image: none !important; margin-right: 0 !important;
  }
  div[class="column"] {
    width: auto !important; float: none !important;
  }
  table.social div[class="column"] {
    width: auto !important;
  }
}
</style>
<table class="head-wrap" bgcolor="#CCCCCC" style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; margin: 0; padding: 0;"></td>
  <td class="header container" style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; display: block !important; max-width: 750px !important; clear: both !important; margin: 0 auto; padding: 0;">
    <h3>Tridaya Manufacturing Management System (TMMS) Notification</h3>
    </td>
    <td style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; margin: 0; padding: 0;"></td>
  </tr></table><table class="body-wrap" style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; margin: 0; padding: 0;"></td>
  <td class="container" bgcolor="#FFFFFF" style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; display: block !important; max-width: 750px !important; clear: both !important; margin: 0 auto; padding: 0;">
    <div class="content" style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; max-width: 750px; display: block; margin: 0 auto; padding: 11px;">
      <table style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; margin: 0; padding: 0;">
        <?php echo $content; ?>
      </td></tr></table>
    </div> 
</td><td style="font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif; margin: 0; padding: 0;"></td>
</tr></table></body>
</html>
