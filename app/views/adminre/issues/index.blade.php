@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatable/jquery.dataTables.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/treegrid/css/jquery.treegrid.css') }}}">
<style type="text/css">
/*.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }*/
</style>
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <!-- <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="#">Table</a></li>
                        <li class="active">Default</li>
                    </ol>
                </div> -->
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Notification::showAll() }}
                <!-- START panel -->
	            <div class="panel panel-primary">
	                <!-- panel heading/header -->
	                <div class="panel-heading">
	                    <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>{{{ $page_title }}}</h3>
	                    <!-- panel toolbar -->
	                    <div class="panel-toolbar text-right">
	                        <!-- option -->
	                        <div class="option">
	                            <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
	                            <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
	                        </div>
	                        <!--/ option -->
	                    </div>
	                    <!--/ panel toolbar -->
	                </div>
	                <!--/ panel heading/header -->

	                <!-- panel toolbar wrapper -->
	                <div class="panel-toolbar-wrapper pl0 pt5 pb5">
	                    <!-- <div class="panel-toolbar pl10">
	                        <div class="checkbox custom-checkbox pull-left">
	                            <input type="checkbox" id="customcheckbox-one0" value="1" data-toggle="checkall" data-target="#table1">
	                            <label for="customcheckbox-one0">&nbsp;&nbsp;Select all</label>
	                        </div>
	                    </div> -->
	                    <div class="panel-toolbar text-right">
	                        <a href="{{{ route('issue_management.create') }}}" class="btn btn-success mb5"><i class="ico-user22"></i> Add New Issue</a>
	                    </div>
	                </div>
	                <!--/ panel toolbar wrapper -->

	                <!-- panel body with collapse capabale -->
	                <div class="table-responsive panel-collapse pull out">
	                    <table class="table table-hover tree" id="table1">
	                        <thead>
	                            <tr>
	                                <th class="text-center">NOP</th>
	                                <th class="text-center">Issue ID</th>
	                                <th class="text-center">Title</th>
	                                <th class="text-center">Assigned To</th>
	                                <th class="text-center">Due Date</th>
	                                <th class="text-center">Owner</th>
	                                <th class="text-center">Status</th>
	                                <th class="text-center">Priority</th>
	                                <th class="text-center">Action</th>
	                            </tr>
	                        </thead>
	                        <tbody style="font-size:10px">
	                        	@if(count($issues))
	                        	@foreach($issues as $project => $issues)
	                        	<tr class="treegrid-{{{ md5($project) }}}">
	                        		<td colspan="9"><strong>{{{ $project }}}</strong></td>
	                        	<tr>
	                        	@if(count($issues))
	                        	@foreach($issues as $issue)
	                        	<?php //$assigned = Pic::find($issue->assigned_to); ?>
								<?php $owner = Pic::find($issue->owner_id); ?>
								<?php $priority = HelpDeskPriority::find($issue->priority); ?>
								<tr class="treegrid-{{{ $issue->id }}} treegrid-parent-{{{ md5($project) }}}">
									<td class="text-left">{{{ $issue->nop }}}</td>
									<td class="text-center">{{{ $issue->issue_code }}}</td>
									<td class="text-center">{{{ $issue->title }}}</td>
									<td class="text-center">{{{ $issue->assigned_to }}}</td>
									<td class="text-center">{{{ $issue->due_date }}}</td>
									<td class="text-center">{{{ $owner->first_name." ".$owner->last_name }}}</td>
									<td class="text-center">{{{ strtoupper($issue->status) }}}</td>
									<td class="text-center">{{{ $priority->priority }}}</td>
									<td class="text-center">
										<!-- button toolbar -->
								        <div class="toolbar">
								            <div class="btn-group">
								                <button type="button" class="btn btn-sm btn-default">Action</button>
								                <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
								                    <span class="caret"></span>
								                </button>
								                <ul class="dropdown-menu dropdown-menu-right">
								                    <li><a href="{{{ route('issue_management.edit', $issue->id) }}}"><i class="icon ico-pencil"></i>Update</a></li>
								                    <li class="divider"></li>
								                    <li>
								                        <a href="#" data-id="{{{ $issue->id }}}" class="delete-confirm text-danger"><i class="icon ico-remove3"></i>Delete</a>
								                        {{ Form::open(array('route' => array('issue_management.destroy', $issue->id), 'method' => 'delete')) }}
								                        <button type="submit" id="del-{{{ $issue->id }}}" style="display:none"></button>
								                        {{ Form::close() }}
								                    </li>
								                </ul>
								            </div>
								        </div>
								        <!--/ button toolbar -->
									</td>
								</tr>
	                        	@endforeach
	                        	@endif
	                        	@endforeach
	                        	@endif
	                        </tbody>
	                    </table>
	                </div>

	                <!--/ panel body with collapse capabale -->
	            </div>

            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatable/jquery.dataTables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatable/dataTables.bootstrap.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/gritter/js/jquery.gritter.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/treegrid/js/jquery.treegrid.js') }}}"></script>
<script>
(function(){
	$('.tree').treegrid();

    $(".delete-confirm").on("click", function (event) {
        event.preventDefault();
        var self = $(this);
        bootbox.confirm("Are you sure to delete this Issue?", function (result) {
            if(result){
            	window.location.href = self.attr('href');
            }
        });
    });
})();
</script>
<!-- <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/components/notification.js') }}}"></script> -->
@stop
