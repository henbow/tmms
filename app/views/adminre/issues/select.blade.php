<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">Select Issue</h3>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <!-- START panel -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Issue List</h3>
                    <div class="panel-toolbar text-right">
                        <!-- option -->
                        <div class="option">
                            <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                        </div>
                        <!--/ option -->
                    </div>
                </div>

                <!-- panel body with collapse capabale -->
                <div class="table-responsive panel-collapse pull out">
                    <table class="table table-striped table-bordered table-hover" id="issue-list" style="font-size: 10px">
                        <thead>
                            <tr>
                                <th class="text-center">NOP</th>
                                <th class="text-center">Issue ID</th>
                                <th class="text-center">Title</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($issues as $issue)
                            <?php $project = Project::find($issue->project_id); ?>
                            <tr style="cursor:pointer;">
                                <td>{{{ $project->nop }}}</td>
                                <td><a href="{{{ route('issue_management.edit', $issue->id) }}}" target="_blank">{{{ $issue->issue_code }}}</a></td>
                                <td>{{{ $issue->title }}}</td>
                                <td><a href="#" onclick="selectIssue(this);return false;" data-id="{{{ $issue->id }}}" class="btn btn-primary">Select</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--/ panel body with collapse capabale -->
            </div>
        </div>
    </div>
</div>

<!--/ END Template Container -->
</div>
<script type="text/javascript">
function selectIssue(elem) {
    var id = $(elem).data('id');
    $('[name="issue_id"]').val(id);
    $.post("{{{ route('data.issue') }}}", {id:id}, function(resp){
        var data = $.parseJSON(resp);
        $('[name="issue"]').val(data.issue_code);
    });
    $('.close').click();
}
(function () {
    var table = $("#issue-list").dataTable({
        "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>><'table-responsive'rt><'row'<'col-sm-6'p><'col-sm-6'i>>",
        "aoColumns": [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false },
        ],
        "iDisplayLength": 10
    });
})();
</script>