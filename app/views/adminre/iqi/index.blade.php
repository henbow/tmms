@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatables/css/jquery.datatables.min.css') }}}">
<style type="text/css">
.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }
</style>
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <!-- <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="#">Table</a></li>
                        <li class="active">Default</li>
                    </ol>
                </div> -->
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Notification::showAll() }}

                <ul class="nav nav-tabs">
			        <li class="{{{ $mode == 'iqi' ? 'active' : '' }}}"><a href="#iqi" data-toggle="tab">IQI</a></li>
			        <li class="{{{ $mode == 'lpk' ? 'active' : '' }}}"><a href="#lpk" data-toggle="tab">ECR / LPK</a></li>
			    </ul>

			    <div class="tab-content panel">
        			<div class="tab-pane {{{ $mode == 'iqi' ? 'active' : '' }}}" id="iqi">
		                <!-- START panel -->
		                <div class="panel panel-primary">
		                    <!-- panel heading/header -->
		                    <div class="panel-heading">
		                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>{{{ $page_title }}}</h3>
		                        <!-- panel toolbar -->
		                        <div class="panel-toolbar text-right">
		                            <!-- option -->
		                            <div class="option">
		                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
		                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
		                            </div>
		                            <!--/ option -->
		                        </div>
		                        <!--/ panel toolbar -->
		                    </div>
		                    <!--/ panel heading/header -->

		                    <!-- panel toolbar wrapper -->
		                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
		                        <!-- <div class="panel-toolbar pl10">
		                            <div class="checkbox custom-checkbox pull-left">
		                                <input type="checkbox" id="customcheckbox-one0" value="1" data-toggle="checkall" data-target="#table1">
		                                <label for="customcheckbox-one0">&nbsp;&nbsp;Select all</label>
		                            </div>
		                        </div> -->
		                        <div class="panel-toolbar text-right">
		                            <a href="{{{ route('iqi.create') }}}" class="btn btn-success mb5"><i class="ico-user22"></i> Add New Issue</a>
		                        </div>
		                    </div>
		                    <!--/ panel toolbar wrapper -->

		                    <!-- panel body with collapse capabale -->
		                    <div class="table-responsive panel-collapse pull out">
		                        <table class="table table-bordered table-hover" id="table1">
		                            <thead>
		                                <tr>
		                                    <th class="text-center" width="5%">No</th>
		                                    <th class="text-center">IQI ID</th>
		                                    <th class="text-center">Date</th>
		                                    <th class="text-center">QC Staff</th>
		                                    <th class="text-center">Part<br>Name</th>
		                                    <th class="text-center">Problem<br>Maker</th>
		                                    <th class="text-center">Dept</th>
		                                    <th class="text-center">Description</th>
		                                    <th class="text-center">Status</th>
		                                    <th class="text-center">Action</th>
		                                </tr>
		                            </thead>
		                            <tbody style="font-size:10px">
		                            	@if(count($iqis) > 0)
		                            	<?php $no = 1; ?>
		                            	@foreach($iqis as $iqi)
		                            	<?php $material = ProjectBOM::find($iqi->material_id); ?>
		                            	<?php $qc_staff = Pic::find($iqi->qc_staff_id); ?>
		                            	<?php $problem_maker = @Pic::find($iqi->problem_maker_id); ?>
		                            	<?php $problem_maker_dept = @Department::find($problem_maker->dept_id); ?>
		                            	<tr class="{{{ $iqi->status == 'close' ? 'success' : '' }}}">
		                                    <td class="text-center">{{{ $no++ }}}</td>
		                                    <td class="text-center">{{{ $iqi->iqi_code }}}</td>
		                                    <td class="text-center">{{{ $iqi->created_at }}}</td>
		                                    <td class="text-center">{{{ $qc_staff->first_name." ".$qc_staff->last_name }}}</td>
		                                    <td class="text-left"><strong>{{{ $material->barcode }}}</strong> - {{{ $material->material_name }}}</td>
		                                    <!-- <td class="text-center">{{{ $material->type }}}</td> -->
		                                    <td class="text-center">{{{ @$problem_maker->first_name." ".@$problem_maker->last_name }}}</td>
		                                    <td class="text-center">{{{ @$problem_maker_dept->name }}}</td>
		                                    <td class="text-center">{{{ $iqi->description }}}</td>
		                                    <td class="text-center">{{{ strtoupper($iqi->status) }}}</td>
		                                    <td class="text-center">
					                            <div class="btn-group">
						                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="ico-paper-plane"></i> Action <span class="caret"></span></button>
						                            <ul class="dropdown-menu" role="menu">
						                                <li>
						                                	@if($iqi->status == 'open')
								                            <a href="{{{ route('iqi.set_status', $iqi->id) }}}?status=close" class="text-danger">Close Issue</a>
								                            @else
								                            <a href="{{{ route('iqi.set_status', $iqi->id) }}}?status=open" class="text-success">Open Issue</a>
								                            @endif
						                                </li>
						                            	<li>
						                                	<a href="{{{ route('iqi.show', $iqi->id) }}}" class="text-primary">Show Issue</a>
						                                </li>
						                                <li>
						                                	<a href="{{{ route('iqi.edit', $iqi->id) }}}" class="text-primary">Edit Issue</a>
						                                </li>
						                                <li class="divider"></li>
						                                <!-- To Detail Planning Page -->
						                                <li><a href="{{{ route('iqi.destroy', $iqi->id) }}}" class="text-danger delete-confirm"><strong>Delete</strong></a>
						                                </li>
						                            </ul>
						                        </div>
				                        	</td>
		                                </tr>
		                                @endforeach
		                                @endif
		                            </tbody>
		                        </table>
		                    </div>
		                    <!--/ panel body with collapse capabale -->
		                </div>
                	</div>

        			<div class="tab-pane {{{ $mode == 'lpk' ? 'active' : '' }}}" id="lpk">
		                <!-- START panel -->
		                <div class="panel panel-primary">
		                    <!-- panel heading/header -->
		                    <div class="panel-heading">
		                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>Employee Compensation Reports (ECR) / Laporan Kompensasi Karyawan (LPK)</h3>
		                        <!-- panel toolbar -->
		                        <div class="panel-toolbar text-right">
		                            <!-- option -->
		                            <div class="option">
		                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
		                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
		                            </div>
		                            <!--/ option -->
		                        </div>
		                        <!--/ panel toolbar -->
		                    </div>
		                    <!--/ panel heading/header -->

		                    <!-- panel toolbar wrapper -->
		                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
		                        <!-- <div class="panel-toolbar pl10">
		                            <div class="checkbox custom-checkbox pull-left">
		                                <input type="checkbox" id="customcheckbox-one0" value="1" data-toggle="checkall" data-target="#table1">
		                                <label for="customcheckbox-one0">&nbsp;&nbsp;Select all</label>
		                            </div>
		                        </div> -->
		                        <div class="panel-toolbar text-right">
		                            <a href="{{{ route('lpk.create') }}}" class="btn btn-success mb5"><i class="ico-user22"></i> Add New LPK</a>
		                        </div>
		                    </div>
		                    <!--/ panel toolbar wrapper -->

		                    <!-- panel body with collapse capabale -->
		                    <div class="table-responsive panel-collapse pull out">
		                        <table class="table table-bordered table-hover" id="table2">
		                            <thead>
		                                <tr>
		                                    <th class="text-center" width="5%">No</th>
		                                    <th class="text-center">LPK ID</th>
		                                    <th class="text-center">IQI ID</th>
		                                    <th class="text-center">Sanctions</th>
		                                    <th class="text-center">Penalty</th>
		                                    <th class="text-center">Lost Hour</th>
		                                    <th class="text-center">Lost Hr Total</th>
		                                    <th class="text-center">Remark</th>
		                                    <th class="text-center">Action</th>
		                                </tr>
		                            </thead>
		                            <tbody style="font-size:10px">
		                            	@if(count($lpks) > 0)
		                            	<?php $no = 1; ?>
		                            	<?php $total_lost_hour = 0; ?>
		                            	@foreach($lpks as $lpk)
		                            	<?php $iqi = Iqi::find($lpk->iqi_id); ?>
		                            	<?php $total_lost_hour += $lpk->hour_losts; ?>
		                            	<tr class="{{{ $lpk->status == 'close' ? 'success' : '' }}}">
		                                    <td class="text-center">{{{ $no++ }}}</td>
		                                    <td class="text-center">{{{ $lpk->lpk_code }}}</td>
		                                    <td class="text-center">
	                                    	@if(isset($iqi->id))
	                                    	<a href="{{{ route('iqi.show', $iqi->id) }}}" target="_blank">{{{ $iqi->iqi_code }}}</a></td>
	                                    	@endif
		                                    <td class="text-center">{{{ $lpk->sanctions }}}</td>
		                                    <td class="text-center">{{{ $lpk->penalty }}}</td>
		                                    <td class="text-center">{{{ $lpk->hour_losts }}}</td>
		                                    <td class="text-center">{{{ $total_lost_hour }}}</td>
		                                    <td class="text-center">{{{ $lpk->remarks }}}</td>
		                                    <td class="text-center">
					                            <div class="btn-group">
						                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="ico-paper-plane"></i> Action <span class="caret"></span></button>
						                            <ul class="dropdown-menu" role="menu">
						                            	<li>
						                                	<a href="{{{ route('lpk.edit', $lpk->id) }}}" class="text-primary">Edit LPK</a>
						                                </li>
						                                <li class="divider"></li>
						                                <!-- To Detail Planning Page -->
						                                <li><a href="{{{ route('lpk.destroy', $lpk->id) }}}" class="text-danger delete-confirm"><strong>Delete</strong></a>
						                                </li>
						                            </ul>
						                        </div>
				                        	</td>
		                                </tr>
		                                @endforeach
		                                @endif
		                            </tbody>
		                        </table>
		                    </div>
		                    <!--/ panel body with collapse capabale -->
		                </div>
                	</div>
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/tabletools.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/zeroclipboard.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/gritter/js/jquery.gritter.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script>
(function(){
    var table1 = $("#table1").dataTable({
        "sDom": "<'row'<'col-sm-6'Tl><'col-sm-6'f>><'table-responsive'rt><'row'<'col-sm-6'p><'col-sm-6'i>>",
        "oTableTools": {
            "sSwfPath": "{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/swf/copy_csv_xls_pdf.swf') }}}",
            "aButtons": [
                "print",
                "pdf",
                "csv"
            ]
        },
        "aoColumns": [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false }
        ],
        "iDisplayLength": 50
    });
    table1.fnSort( [ [1,'desc'] ] );
    var table2 = $("#table2").dataTable({
        "sDom": "<'row'<'col-sm-6'Tl><'col-sm-6'f>><'table-responsive'rt><'row'<'col-sm-6'p><'col-sm-6'i>>",
        "oTableTools": {
            "sSwfPath": "{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/swf/copy_csv_xls_pdf.swf') }}}",
            "aButtons": [
                "print",
                "pdf",
                "csv"
            ]
        },
        "aoColumns": [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false }
        ],
        "iDisplayLength": 50
    });
    table2.fnSort( [ [1,'desc'] ] );
    $(".delete-confirm").on("click", function (event) {
        event.preventDefault();
        var self = $(this);
        bootbox.confirm("Are you sure to delete this Issue?", function (result) {
            if(result){
            	window.location.href = self.attr('href');
            }
        });
    });
})();
</script>
<!-- <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/components/notification.js') }}}"></script> -->
@stop
