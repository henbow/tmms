<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">Select IQI</h3>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <!-- START panel -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>IQI List</h3>
                    <div class="panel-toolbar text-right">
                        <!-- option -->
                        <div class="option">
                            <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                        </div>
                        <!--/ option -->
                    </div>
                </div>

                <!-- panel body with collapse capabale -->
                <div class="table-responsive panel-collapse pull out">
                    <table class="table table-striped table-bordered table-hover" id="iqi-list" style="font-size: 10px">
                        <thead>
                            <tr>
                                <th width="5%" class="text-center">No</th>
                                <th class="text-center">IQI ID</th>
                                <th class="text-center">Date</th>
                                <th class="text-center">Part<br>Name</th>
                                <th class="text-center">Problem<br>Maker</th>
                                <th class="text-center">Description</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach($iqis as $iqi)
                            <?php $has_bom = is_bom_imported($iqi->project_id); ?>
                            @if($has_bom)
                            <?php $part = ProjectBOM::find($iqi->material_id); ?>
                            <?php $pm = Pic::find($iqi->problem_maker_id); ?>
                            <tr style="cursor:pointer;">
                                <td class="text-center">{{{ $no++ }}}</td>
                                <td><a href="{{{ route('iqi.show', $iqi->id) }}}" target="_blank">{{{ $iqi->iqi_code }}}</a></td>
                                <td>{{{ $iqi->created_at }}}</td>
                                <td>{{{ $part->material_name }}}</td>
                                <td>{{{ isset($pm->first_name) ? $pm->first_name." ".$pm->last_name : '' }}}</td>
                                <td>{{{ $iqi->description }}}</td>
                                <td>{{{ strtoupper($iqi->status) }}}</td>
                                <td><a href="#" onclick="selectIQI(this);return false;" data-id="{{{ $iqi->id }}}" class="btn btn-primary">Select</a></td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--/ panel body with collapse capabale -->
            </div>
        </div>
    </div>
</div>

<!--/ END Template Container -->
</div>
<script type="text/javascript">
function selectIQI(elem) {
    var id = $(elem).data('id');
    $('[name="iqi_id"]').val(id);
    $.post("{{{ route('data.iqi') }}}", {id:id}, function(resp){
        var data = $.parseJSON(resp);
        $('[name="iqi"]').val(data.iqi_code);
    });
    $('.close').click();
}
(function () {
    var table = $("#iqi-list").dataTable({
        "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>><'table-responsive'rt><'row'<'col-sm-6'p><'col-sm-6'i>>",
        "aoColumns": [
            { "sClass": "text-center", "bSearchable": false, "bSortable": false },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false },
        ],
        "iDisplayLength": 10
    });
})();
</script>