<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">Select Project</h3>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <!-- START panel -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Project List</h3>
                    <div class="panel-toolbar text-right">
                        <!-- option -->
                        <div class="option">
                            <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                        </div>
                        <!--/ option -->
                    </div>
                </div>
                
                <!-- panel body with collapse capabale -->
                <div class="table-responsive panel-collapse pull out">
                    <table class="table table-striped table-bordered table-hover" id="project-list" style="font-size: 10px">
                        <thead>
                            <tr>
                                <th width="5%" class="text-center">No</th>
                                <th class="text-center">NOP</th>
                                <th class="text-center">Project</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach($projects as $project)
                            <?php $has_bom = is_bom_imported($project->id); ?>
                            @if($has_bom)
                            <tr style="cursor:pointer;">
                                <td class="text-center">{{{ $no++ }}}</td>
                                <td>{{{ $project->nop }}}</td>
                                <td>{{{ $project->project_name }}}</td>
                                <td><a href="#" onclick="selectProject(this);return false;" data-id="{{{ $project->id }}}" class="btn btn-primary">Select</a></td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--/ panel body with collapse capabale -->
            </div>
        </div>
    </div>
</div>

<!--/ END Template Container -->
</div>
<script type="text/javascript">
function selectProject(elem) {
    var id = $(elem).data('id');
    $('[name="project_id"]').val(id);
    $.get("{{{ route('data.project') }}}?id="+id, function(resp){
        var data = $.parseJSON(resp);
        $('[name="project"]').val(data.nop + " -> " + data.project_name);
    });
    $('.close').click();
}
(function () {
    var table = $("#project-list").dataTable({
        "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>><'table-responsive'rt><'row'<'col-sm-6'p><'col-sm-6'i>>",
        "aoColumns": [
            { "sClass": "text-center", "bSearchable": false, "bSortable": false },
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-center" },
        ],
        "iDisplayLength": 10
    });        
})();
</script>