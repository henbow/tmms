@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatables/css/jquery.datatables.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/selectize/css/selectize.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui-timepicker.min.css') }}}">
<style type="text/css">
.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }
</style>
@stop

@section('main')
<section id="main" role="main">
    {{ Notification::showAll() }}

    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}} - <small>Create New Issue</small></h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Form::open(array('route' => 'iqi.store', 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate')) }}
                    <div class="panel-heading">
                        <h3 class="panel-title">Issue Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">IQI ID</label>
                            <div class="col-sm-6">
                                <input type="text" name="iqi_code" id="iqi_code" class="form-control" value="{{{ generate_iqi_code() }}}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Project</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input type="text" name="project" id="project" class="form-control" value="" disabled>
                                    <input type="hidden" name="project_id" id="project_id" value="" required>
                                    <span class="input-group-btn">
                                        <a href="{{{ route('iqi.select_project') }}}" class="btn btn-primary" id="select-project" style="width:108px;">Select Project</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Part</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input type="text" name="part_name" class="form-control" value="" disabled>
                                    <input type="hidden" name="material_id" id="material_id" value="" required>
                                    <span class="input-group-btn">
                                        <a href="{{{ route('iqi.select_material') }}}" class="btn btn-primary" id="select-material" style="width:108px;">Select Part</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">QC Staff</label>
                            <div class="col-sm-4">
                                <input type="text" name="qc_staff_{{{$random}}}" class="form-control">
                                <input type="hidden" name="qc_staff" value="" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Problem Maker</label>
                            <div class="col-sm-4">
                                <input type="text" name="problem_maker_{{{$random}}}" class="form-control">
                                <input type="hidden" name="problem_maker" value="" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Problem Description</label>
                            <div class="col-sm-9">
                                <textarea name="desc" class="form-control" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Possible Causes</label>
                            <div class="col-sm-9">
                                <textarea name="causes" class="form-control" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Corrective Action</label>
                            <div class="col-sm-9">
                                <textarea name="action" class="form-control" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">PIC</label>
                            <div class="col-sm-3">
                                <select name="pic" id="pic" class="form-control" required>
                                    <option value=""></option>
                                    @foreach ($units as $unit)
                                    <option value="{{{ $unit->id }}}">{{{ $unit->name }}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Remark</label>
                            <div class="col-sm-9">
                                <textarea name="remark" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Status</label>
                            <div class="col-sm-3">
                                <select name="status" id="status" class="form-control" required>
                                    <option value="open">OPEN</option>
                                    <option value="close">CLOSE</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary">Create New Issue</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
<!-- START modal-lg -->
<div id="bs-modal-lg" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<!--/ END modal-lg -->
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/parsley/js/parsley.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/tabletools.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/zeroclipboard.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/inputmask/js/inputmask.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-timepicker.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-touch.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/typeahead/bootstrap3-typeahead.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/element.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/jquery.runner.js') }}}"></script>
<script type="text/javascript">
(function(){
	$('[name="qc_staff_{{{ $random }}}"]').typeahead({
        source:function (query, qc) {
            qcs = [];
            map = {};

            var data = <?php echo json_encode($qc_staffs); ?>;

            $.each(data, function (i, item) {
                var key = item.code+' -> '+item.first_name+' '+item.last_name;
                map[key] = item;
                qcs.push(key);
            });

            qc(qcs);
        },
        updater: function (qc) {
            $('[name="qc_staff"]').val(map[qc].id);
            return qc;
        }
    });
    $('[name="problem_maker_{{{ $random }}}"]').typeahead({
        source:function (query, pm) {
            pms = [];
            map = {};

            var data = <?php echo json_encode($pics); ?>;

            $.each(data, function (i, item) {
                var key = item.code+' -> '+item.first_name+' '+item.last_name;
                map[key] = item;
                pms.push(key);
            });

            pm(pms);
        },
        updater: function (pm) {
            $('[name="problem_maker"]').val(map[pm].id);
            return pm;
        }
    });
    $('#select-project').click(function(e){
        e.preventDefault();
        $('#bs-modal-lg .modal-content').load($(this).attr('href'),function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $('#select-material').click(function(e){
        e.preventDefault();
        var projectId = $('[name="project_id"]').val();
        if(!projectId) {
            alert('Project is not selected!');
            return false;
        }
        $('#bs-modal-lg .modal-content').load($(this).attr('href')+"?project_id="+projectId,function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
})();
</script>
@stop
