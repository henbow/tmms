<!DOCTYPE html>
<html class="backend">
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{{ $app_name }}}</title>
        <meta name="author" content="pampersdry.info">
        <meta name="description" content="Adminre is a clean and flat backend and frontend theme build with twitter bootstrap 3.1.1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('/assets/'.$theme.'/image/touch/apple-touch-icon-144x144-precomposed.png') }}}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('/assets/'.$theme.'/image/touch/apple-touch-icon-114x114-precomposed.png') }}}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('/assets/'.$theme.'/image/touch/apple-touch-icon-72x72-precomposed.png') }}}">
        <link rel="apple-touch-icon-precomposed" href="{{{ asset('/assets/'.$theme.'/image/touch/apple-touch-icon-57x57-precomposed.png') }}}">
        <link rel="shortcut icon" href="{{{ asset('/assets/'.$theme.'/image/favicon.ico') }}}">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->
        
        
        <!--/ Plugins stylesheet -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="{{{ asset('/assets/'.$theme.'/library/bootstrap/css/bootstrap.min.css') }}}">
        <link rel="stylesheet" href="{{{ asset('/assets/'.$theme.'/stylesheet/layout.min.css') }}}">
        <link rel="stylesheet" href="{{{ asset('/assets/'.$theme.'/stylesheet/uielement.min.css') }}}">
        <!--/ Application stylesheet -->
        <!-- END STYLESHEETS -->

        <!-- START JAVASCRIPT SECTION - Load only modernizr script here -->
        <script src="{{{ asset('/assets/'.$theme.'/library/modernizr/js/modernizr.min.js') }}}"></script>
        <!--/ END JAVASCRIPT SECTION -->
    </head>
    <!--/ END Head -->

    <!-- START Body -->
    <body>
        <!-- START Template Main -->
        <section id="main" role="main">
            <!-- START Template Container -->
            <section class="container">
                <!-- START row -->
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4">
                        <!-- Brand -->
                        <div class="text-center" style="margin-bottom:40px;">
                            <span class="logo-figure inverse"></span>
                            <span class="logo-text inverse"></span>
                            <h5 class="semibold text-muted mt-5">Login to your account.</h5>
                        </div>
                        <!--/ Brand -->

                        <hr><!-- horizontal line -->

                        <!-- Login form -->
                        {{ Form::open(array('route' => 'auth.login.post', 'class' => 'panel', 'name' => 'form-login')) }}
                            <div class="panel-body">
                                <!-- Alert message
                                <div class="alert alert-warning">
                                    <span class="semibold">Note :</span>&nbsp;&nbsp;Just put anything and hit 'sign-in' button.
                                </div>
                                Alert message -->
                                
                                <div class="form-group">
                                    <div class="form-stack has-icon pull-left">
                                        <input name="login" type="text" class="form-control input-lg" placeholder="Username / email" data-parsley-errors-container="#error-container" data-parsley-error-message="Please fill in your username / email" data-parsley-required>
                                        <i class="ico-user2 form-control-icon"></i>
                                    </div>
                                    <div class="form-stack has-icon pull-left">
                                        <input name="password" type="password" class="form-control input-lg" placeholder="Password" data-parsley-errors-container="#error-container" data-parsley-error-message="Please fill in your password" data-parsley-required>
                                        <i class="ico-lock2 form-control-icon"></i>
                                    </div>
                                </div>

                                <!-- Error container -->
                                <div id="error-container"class="mb15"></div>
                                <!--/ Error container -->

                                
                                <div class="form-group nm">
                                    <button type="submit" class="btn btn-block btn-success"><span class="semibold">Sign In</span></button>
                                </div>
                            </div>
                        {{ Form::close() }}
                        <!-- Login form -->
                        
                        <hr><!-- horizontal line -->
                    </div>
                </div>
                <!--/ END row -->
            </section>
            <!--/ END Template Container -->
        </section>
        <!--/ END Template Main -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Library script : mandatory -->
        <script type="text/javascript" src="{{{ asset('/assets/'.$theme.'/library/jquery/js/jquery.min.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('/assets/'.$theme.'/library/jquery/js/jquery-migrate.min.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('/assets/'.$theme.'/library/bootstrap/js/bootstrap.min.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('/assets/'.$theme.'/library/core/js/core.min.js') }}}"></script>
        <!--/ Library script -->

        <!-- App and page level script -->
        <script type="text/javascript" src="{{{ asset('/assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script><!-- will be use globaly as a summary on sidebar menu -->
        <script type="text/javascript" src="{{{ asset('/assets/'.$theme.'/javascript/app.min.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('/assets/'.$theme.'/plugins/parsley/js/parsley.min.js') }}}"></script>
        <script type="text/javascript">
        $(function () {
            // Login form function
            // ================================
            var $form    = $("form[name=form-login]");
        
            // On button submit click
            $form.on("click", "button[type=submit]", function (e) {
                var $this = $(this);
        
                // Run parsley validation
                if ($form.parsley().validate()) {
                    // Disable submit button
                    $this.prop("disabled", true);
        
                    // start nprogress bar
                    NProgress.start();
        
                    // you can do the ajax request here
                    // this is for demo purpose only
                    $this.submit();
                } else {
                    // toggle animation
                    $form
                        .removeClass("animation animating shake")
                        .addClass("animation animating shake")
                        .one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                            $(this).removeClass("animation animating shake");
                        });
                }
                // prevent default
                e.preventDefault();
            });
        });
        </script>
        
        <!--/ App and page level script -->
        <!--/ END JAVASCRIPT SECTION -->
    </body>
    <!--/ END Body -->
</html>