<!DOCTYPE html>
<html class="backend">
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{{ $app_name }}}</title>
        <meta name="author" content="pampersdry.info">
        <meta name="description" content="Adminre is a clean and flat backend and frontend theme build with twitter bootstrap 3.1.1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/'.$theme.'/image/touch/apple-touch-icon-144x144-precomposed.png') }}}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/'.$theme.'/image/touch/apple-touch-icon-114x114-precomposed.png') }}}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/'.$theme.'/image/touch/apple-touch-icon-72x72-precomposed.png') }}}">
        <link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/'.$theme.'/image/touch/apple-touch-icon-57x57-precomposed.png') }}}">
        <link rel="shortcut icon" href="{{{ asset('assets/'.$theme.'/image/favicon.ico') }}}">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->
        @yield('additional_css')
        <!--/ Plugins stylesheet -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/library/bootstrap/css/bootstrap.min.css') }}}">
        <link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/stylesheet/layout.min.css') }}}">
        <link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/stylesheet/uielement.min.css') }}}">
        <!--/ Application stylesheet -->
        <!-- END STYLESHEETS -->

        <!-- START JAVASCRIPT SECTION - Load only modernizr script here -->
        <script>
        (function(_,e,rr,s){_errs=[s];var c=_.onerror;_.onerror=function(){var a=arguments;_errs.push(a);
        c&&c.apply(this,a)};var b=function(){var c=e.createElement(rr),b=e.getElementsByTagName(rr)[0];
        c.src="//beacon.errorception.com/"+s+".js";c.async=!0;b.parentNode.insertBefore(c,b)};
        _.addEventListener?_.addEventListener("load",b,!1):_.attachEvent("onload",b)})
        (window,document,"script","551377d9557dba16570006d8");
        </script>
        <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/library/modernizr/js/modernizr.min.js') }}}"></script>
        <!--/ END JAVASCRIPT SECTION -->
    </head>
    <!--/ END Head -->

    <!-- START Body -->
    <body>

        <!-- START Template Main -->
        @yield('main')
        <!--/ END Template Main -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Library script : mandatory -->

        <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/library/jquery/js/jquery.min.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/library/jquery/js/jquery-migrate.min.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/library/bootstrap/js/bootstrap.min.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/library/core/js/core.min.js') }}}"></script>
        <!--/ Library script -->

        <!-- App and page level script -->
        <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/app.min.js') }}}"></script>
        @yield('additional_scripts')
        <!--/ App and page level script -->
        <!--/ END JAVASCRIPT SECTION -->
    </body>
    <!--/ END Body -->
</html>