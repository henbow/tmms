<!DOCTYPE html>
<html class="backend">
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{{ $app_name }}}</title>
        <meta name="author" content="pampersdry.info">
        <meta name="description" content="Adminre is a clean and flat backend and frontend theme build with twitter bootstrap 3.1.1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/'.$theme.'/image/touch/apple-touch-icon-144x144-precomposed.png') }}}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/'.$theme.'/image/touch/apple-touch-icon-114x114-precomposed.png') }}}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/'.$theme.'/image/touch/apple-touch-icon-72x72-precomposed.png') }}}">
        <link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/'.$theme.'/image/touch/apple-touch-icon-57x57-precomposed.png') }}}">
        <link rel="shortcut icon" href="{{{ asset('assets/'.$theme.'/image/favicon.ico') }}}">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->
        @yield('additional_css')
        <!--/ Plugins stylesheet -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/library/bootstrap/css/bootstrap.min.css') }}}">
        <link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/stylesheet/layout.min.css') }}}">
        <link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/stylesheet/uielement.min.css') }}}">
        <!--/ Application stylesheet -->
        <!-- END STYLESHEETS -->

        <!-- START JAVASCRIPT SECTION - Load only modernizr script here -->
        <script>
        (function(_,e,rr,s){_errs=[s];var c=_.onerror;_.onerror=function(){var a=arguments;_errs.push(a);
        c&&c.apply(this,a)};var b=function(){var c=e.createElement(rr),b=e.getElementsByTagName(rr)[0];
        c.src="//beacon.errorception.com/"+s+".js";c.async=!0;b.parentNode.insertBefore(c,b)};
        _.addEventListener?_.addEventListener("load",b,!1):_.attachEvent("onload",b)})
        (window,document,"script","551377d9557dba16570006d8");
        </script>
        <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/library/modernizr/js/modernizr.min.js') }}}"></script>
        <!--/ END JAVASCRIPT SECTION -->
    </head>
    <!--/ END Head -->

    <!-- START Body -->
    <body>
        <!-- START Template Header -->
        <header id="header" class="navbar navbar-fixed-top">
            <!-- START navbar header -->
            <div class="navbar-header">
                <!-- Brand -->
                <a class="navbar-brand" href="{{{ route('dashboard.index') }}}">
                    <span class="logo-figure"></span>
                    <span class="logo-text"></span>
                </a>
                <!--/ Brand -->
            </div>
            <!--/ END navbar header -->

            <!-- START Toolbar -->
            <div class="navbar-toolbar clearfix">
                <!-- START Left nav -->
                <ul class="nav navbar-nav navbar-left">
                    <!-- Sidebar shrink -->
                    <li class="hidden-xs hidden-sm">
                        <a href="javascript:void(0);" class="sidebar-minimize" data-toggle="minimize" title="Minimize sidebar">
                            <span class="meta">
                                <span class="icon"></span>
                            </span>
                        </a>
                    </li>
                    <li class="navbar-main hidden-lg hidden-md hidden-sm">
                        <a href="javascript:void(0);" data-toggle="sidebar" data-direction="ltr" rel="tooltip" title="Menu sidebar">
                            <span class="meta">
                                <span class="icon"><i class="ico-paragraph-justify3"></i></span>
                            </span>
                        </a>
                    </li>
                    <!--/ Sidebar shrink -->
                </ul>
                <!--/ END Left nav -->


                <!-- START Right nav -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Profile dropdown -->
                    <li class="dropdown profile">
                        <a href="{{{ route('dashboard.index') }}}" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="meta">
                                <!-- <span class="avatar"><img src="{{{ asset('assets/'.$theme.'/image/avatar/avatar7.jpg') }}}" class="img-circle" alt="" /></span> -->
                                <span class="text hidden-xs hidden-sm pl5">{{{ $user_data }}}</span>
                                <span class="caret"></span>
                            </span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{{ route('auth.logout') }}}"><span class="icon"><i class="ico-exit"></i></span> Sign Out</a></li>
                        </ul>
                    </li>
                    <!-- Profile dropdown -->


                </ul>
                <!--/ END Right nav -->
            </div>
            <!--/ END Toolbar -->
        </header>
        <!--/ END Template Header -->

        <!-- START Template Sidebar (Left) -->
        <aside class="sidebar sidebar-left sidebar-menu">
            <!-- START Sidebar Content -->
            <section class="content slimscroll">
                <h5 class="heading">Main Menu</h5>
                <!-- START Template Navigation/Menu -->
                @include($theme . '._partials.left_sidebar')
                <!--/ END Template Navigation/Menu -->
            <!--/ END Sidebar Container -->
        </aside>
        <!--/ END Template Sidebar (Left) -->

        <!-- START Template Main -->
        @yield('main')
        <!--/ END Template Main -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Library script : mandatory -->

        <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/library/jquery/js/jquery.min.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/library/jquery/js/jquery-migrate.min.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/library/bootstrap/js/bootstrap.min.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/library/core/js/core.min.js') }}}"></script>
        <!--/ Library script -->

        <!-- App and page level script -->
        <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/app.min.js') }}}"></script>
        @yield('additional_scripts')
        <!--/ App and page level script -->
        <script type="text/javascript">
        (function(){
            $('.sidebar-minimize').click();
        })();
        </script>
        <!--/ END JAVASCRIPT SECTION -->
    </body>
    <!--/ END Body -->
</html>