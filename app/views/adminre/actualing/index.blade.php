@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/selectize/css/selectize.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui-timepicker.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li class="active">Project Actualing</li>
                    </ol>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel body with collapse capabale -->
                    <table class="table table-striped table-bordered table-hover" id="table-planning">
                        <thead>
                            <tr>
                                <th class="text-center" width="5%">No</th>
                                <th class="text-center">Master Plan</th>
                                <th class="text-center" width="10%"></th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 11px;">
                            <?php $no = 0; ?>
                            @foreach ($processes as $process)
                            <tr>
                                <td align="center">{{{ $no++ }}}</td>
                                <td align="left">{{{ $process->process }}}</td>
                                <td align="center">
                                    <a href="{{{ route('actualing.index.detail', $process->process) }}}" class="btn btn-sm btn-success"><i class="ico-stack2"></i>&nbsp;Details</a>
                                </td>
                            </tr> 
                            @endforeach
                        </tbody>
                    </table>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>        
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->
    
    <!-- START modal-lg -->
    <div id="bs-modal-lg" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content"></div>
        </div>
    </div>
    <!--/ END modal-lg -->
</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-timepicker.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-touch.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/xeditable/js/bootstrap-editable.min.js') }}}"></script>      
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/typeahead/bootstrap3-typeahead.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/element.js') }}}"></script>
@stop