<div class="col-md-12">
    <div class="tab-content panel">
        <div class="tab-pane" id="on-process">
            <table class="table table-striped table-bordered table-hover actualing-table" id="actualing-onprocess">
                <thead>
                    <tr>
                        <th class="text-center">Barcode</th>
                        <th class="text-center">Part Name</th>
                        <th class="text-center">Process Code</th>
                        <th class="text-center">Process Name</th>
                        <th class="text-center">Resource</th>
                        <th class="text-center">PIC</th>
                        <th class="text-center">Planning<br>Est. (Hours)</th>
                        <th class="text-center">Actualing<br>Start</th>
                        <th class="text-center">Qty</th>
                        <th class="text-center">Position</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody style="font-size: 11px"></tbody>
                </table>
        </div>
    </div>
</div>
