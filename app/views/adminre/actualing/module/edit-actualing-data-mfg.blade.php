<!-- START modal-lg -->
{{ Form::open(array('route' => array('actualing.do_edit_data'), 'id' => 'edit-actualing', 'class' => 'form-horizontal', 'data-parsley-validate')) }}
<div class="modal-header text-left">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <h3 class="semibold modal-title text-primary">Edit Actualing Data - <small>{{{ $prm->material_name }}}, {{{ $process->process }}}</small></h3>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">   
            <div class="panel-body"> 
                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-2 control-label">Resource</label>
                        <div class="col-sm-9">
                            <input type="text" name="resource-{{{ $random }}}" class="form-control" value="{{{ $resource->code." -> ".$resource->name }}}" required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-2 control-label">PIC</label>
                        <div class="col-sm-9">
                            <input type="text" name="pic-{{{ $random }}}" class="form-control" value="{{{ $pic->code." -> ".trim($pic->first_name." ".$pic->last_name) }}}" required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-2 control-label">Quantity</label>
                        <div class="col-sm-3">
                            <select multiple name="qty" id="qty" class="form-control" required>
                                @if(count($qty) > 0)
                                    @foreach($qty as $q)
                                    <option value="{{{ $q->id }}}">{{{ $q->qty_num }}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-2 control-label">Position</label>
                        <div class="col-sm-6">
                            <select multiple name="position[]" id="position" class="form-control" required></select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-2 control-label">Remark</label>
                        <div class="col-sm-9">
                            <textarea name="remark" class="form-control" disabled>{{{ $actualing->remark }}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-2 control-label">Start Date</label>
                        <div class="col-sm-3">
                            <input type="text" name="start" id="start" class="form-control datetime-picker" value="{{{ date('m/d/Y H:i', strtotime($actualing->start_date)) }}}" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-2 control-label">Finish Date</label>
                        <div class="col-sm-3">
                            <input type="text" name="finish" id="finish" class="form-control datetime-picker" value="{{{ date('m/d/Y H:i', strtotime($actualing->finish_date)) }}}" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-2 control-label">Finish</label>
                        <div class="col-sm-6">
                            <select name="finish" id="finish" class="form-control" required>
                                <option value="ONPROCESS">ON PROCESS</option>
                                <option value="COMPLETE">COMPLETE</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">

    <!-- START Hidden field -->
    <input type="hidden" name="actualing" value="{{{ $actualing->id }}}" />
    <input type="hidden" name="resource" value="{{{ $resource->id }}}" />
    <input type="hidden" name="pic" value="{{{ $pic->id }}}" />
    <input type="hidden" name="type" value="{{{ $type }}}" />
    <!--/END Hidden field -->
    
    <button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
    <button type="submit" class="btn btn-primary"> Update </button>
</div>
{{ Form::close() }}
<script type="text/javascript">
(function(){
    $(".datetime-picker").datetimepicker();
    $("[name=\"resource-{{{ $random }}}\"]").typeahead({
        source:function (query, resource) {
            resources = [];
            map = {};

            var data = <?php echo json_encode($resources); ?>;

            $.each(data, function (i, item) {
                var key = item.code+' -> '+item.name;
                map[key] = item;
                resources.push(key);
            });

            resource(resources);
        },
        updater: function (resource) {
            $('[name="resource"]').val(map[resource].id);
            return resource;
        }
    });
    $("[name=\"pic-{{{ $random }}}\"]").typeahead({
        source:function (query, pic) {
            pics = [];
            map = {};

            var data = <?php echo json_encode($pics); ?>;

            $.each(data, function (i, item) {
                var key = item.code+' -> '+item.first_name+' '+item.last_name;
                map[key] = item;
                pics.push(key);
            });

            pic(pics);
        },
        updater: function (pic) {
            $('[name="pic"]').val(map[pic].id);
            return pic;
        }
    });
})();
</script>
<!--/ END modal-lg