<div class="col-md-12">
    <!-- START panel -->
    <div class="panel panel-default">
        <table class="table table-striped table-bordered table-hover" id="ajax-source">
            <thead>
                <tr>
                    <th class="text-center" width="5%">No</th>
                    <th class="text-center">NOP</th>
                    <th class="text-center">Project Name</th>
                    <th class="text-center">Start Date</th>
                    <th class="text-center">Finish Date</th>
                    <th class="text-center" width="10%"></th>
                </tr>
            </thead>
            <tbody style="font-size: 11px">
            <?php $no = 1 ?>
            @foreach($planning_data as $planning)
                <?php
                $project = $planning->masterplan->project()->first();
                $masterplan = $planning->masterplan;
                if(isset($project->id)):
                ?>
                <tr>
                    <td class="text-center">{{{ $no++ }}}</td>
                    <td class="text-center">
                        <a href="{{{ route('actualing.pb-bom.detail', array('pb-bom', $planning->id)) }}}">{{{ $project->nop }}}</a>
                    </td>
                    <td class="text-left">{{{ $project->project_name }}}</td>
                    <td class="text-center">{{{ date('d F Y', strtotime($masterplan->start_date)) }}}</td>
                    <td class="text-center">{{{ strtotime($masterplan->finish_date) ? date('d F Y', strtotime($masterplan->finish_date)) : '-' }}}</td>
                    <td class="text-center"><a class="btn btn-success" href="{{{ route('actualing.pb-bom.detail', array('pb-bom', $planning->id)) }}}">Detail</a></td>
                </tr>
                <?php
                endif;
                ?>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
