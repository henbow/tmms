<div class="col-md-12">
    <!-- START panel -->
    <div class="panel panel-default">
        <table class="table table-striped table-bordered table-hover" id="ajax-source">
            <thead>
                <tr>
                    <th class="text-center" width="5%">No</th>
                    <th class="text-center" width="12%">Barcode</th>
                    <th class="text-center">Part Name</th>
                    <th class="text-center">PO No</th>
                    <th class="text-center">PO Date</th>
                    <th class="text-center">PO Qty</th>
                    <th class="text-center">TTB No</th>
                    <th class="text-center">TTB Date</th>
                    <th class="text-center">TTB Qty</th>
                    <th class="text-center" width="10%">Total Time (Hours)</th>
                </tr>
            </thead>
            <tbody style="font-size: 11px">
            <?php $no = 1; ?>
            @foreach($materials as $material)
                @foreach($material->actualing_pb_bom as $detail_actualing)
                <tr>
                    <td class="text-center">{{{ $no++ }}}</td>
                    <td class="text-center">{{{ $material->barcode }}}</td>
                    <td>{{{ $material->material_name }}}</td>
                    <td class="text-center">{{{ $detail_actualing->po_number }}}</td>
                    <td class="text-center">{{{ date('d-M-Y', strtotime($detail_actualing->po_date)) }}}</td>
                    <td class="text-center">{{{ $detail_actualing->po_qty }}}</td>
                    <td class="text-center">{{{ $detail_actualing->ttb_number }}}</td>
                    <td class="text-center">{{{ date('d-M-Y', strtotime($detail_actualing->ttb_date)) }}}</td>
                    <td class="text-center">{{{ $detail_actualing->ttb_qty }}}</td>
                    <td class="text-center">{{{ $detail_actualing->po_ttb_time }}}</td>
                </tr>
                @endforeach
            @endforeach
            </tbody>
        </table>
    </div>
</div>
