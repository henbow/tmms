
<table class="table table-striped table-bordered table-hover" id="ajax-source" style="font-size: 11px">
    <thead>
        <tr>
            <th class="text-center">NOP</th>
            <th class="text-center">Project</th>
            <th class="text-center">Process</th>
            <th class="text-center">Est.</th>
            <!--<th class="text-center">Priority</th>-->
            <th class="text-center">Resource</th>
            <th class="text-center" width="5%"><input type="checkbox" name="select_all" class="select_all" value="all" ></th>
        </tr>
    </thead>
    <tbody>
        <?php $priority_num = 1; $priority_number = array(); ?>
        @foreach($planning_data as $planning)
        <?php $priority = isset($planning->priority) ? $planning->priority : 0; ?>
        <?php $resource = Resource::find($planning->resource_id); ?>
        <tr>
            <!--<td class="text-center">{{{ @$planning->id }}} | {{{ @$planning->nop }}}</td>-->
            <td class="text-center">{{{ @$planning->nop }}}</td>
            <td class="text-left">{{{ @$planning->project }}}</td>
            <td class="text-center">{{{ @$planning->process }}}</td>
            <td class="text-center">{{{ @$planning->estimation }}}</td>
            <!--<td class="text-center"><a href="#" data-type="text" data-name="NON-MFG" data-pk="{{{ @$planning->id }}}" data-title="Edit Priority" class="edit_priority">{{{ $priority }}}</a></td>-->
            <td class="text-center">{{{ $resource->name }}}</td>
            <td class="text-center"><input type="checkbox" name="plan_id[]" class="plan_id" value="{{{ @$planning->id }}}" ></td>
        </tr>
        @endforeach
    </tbody>
</table>