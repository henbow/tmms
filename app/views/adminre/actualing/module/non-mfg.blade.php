<div class="col-md-12">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#waiting" data-toggle="tab">WAITING</a></li>
        <li><a href="#on-process" data-toggle="tab">ON PROCESS</a></li>
        <li><a href="#complete" data-toggle="tab">FINISH</a></li>
    </ul>

    <div class="tab-content panel">
        <div class="tab-pane active" id="waiting">
            <table class="table table-striped table-bordered table-hover actualing-table" id="actualing-waiting">
                <thead>
                    <tr>
                        <th class="text-center">NOP</th>
                        <th class="text-center">Project</th>
                        <th class="text-center">Process Code</th>
                        <th class="text-center">Process Name</th>
                        <th class="text-center">Resource Group</th>
                        <th class="text-center">Planning<br>Est. (Hours)</th>
                    </tr>
                </thead>
                <tbody style="font-size: 11px"></tbody>
            </table>
        </div>

        <div class="tab-pane" id="on-process">
            <table class="table table-striped table-bordered table-hover actualing-table" id="actualing-onprocess">
                <thead>
                    <tr>
                        <th class="text-center">NOP</th>
                        <th class="text-center">Project</th>
                        <th class="text-center">Process Code</th>
                        <th class="text-center">Process Name</th>
                        <th class="text-center">Resource</th>
                        <th class="text-center">PIC</th>
                        <th class="text-center">Planning<br>Est. (Hours)</th>
                        <th class="text-center">Actualing<br>Start</th>
                        <th class="text-center">Qty</th>
                        <th class="text-center">Position</th>
                    </tr>
                </thead>
                <tbody style="font-size: 11px"></tbody>
            </table>
        </div>

        <div class="tab-pane" id="complete">
            <table class="table table-striped table-bordered table-hover actualing-table" id="actualing-complete">
                <thead>
                    <tr>
                        <th class="text-center">NOP</th>
                        <th class="text-center">Project</th>
                        <th class="text-center">Process Code</th>
                        <th class="text-center">Process Name</th>
                        <th class="text-center">Resource</th>
                        <th class="text-center">PIC</th>
                        <th class="text-center">Planning<br>Est. (Hours)</th>
                        <th class="text-center">Actualing<br>Start</th>
                        <th class="text-center">Actualing<br>Finish</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Qty</th>
                        <th class="text-center">Position</th>
                    </tr>
                </thead>
                <tbody style="font-size: 11px"></tbody>
            </table>
        </div>
    </div>
</div>
