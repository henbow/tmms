<div class="col-md-12">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#material" data-toggle="tab">MATERIAL</a></li>
        <li><a href="#nop" data-toggle="tab">NOP</a></li>
    </ul>

    <div class="tab-content panel">
        <div class="tab-pane active" id="material">
            <input type="hidden" name="type" valu="material" />
            <table class="table table-striped table-bordered table-hover actualing-table" id="actualing-material">
                <thead>
                    <tr>
                        <th class="text-center">Barcode</th>
                        <th class="text-center">Part Name</th>
                        <th class="text-center">Process Code</th>
                        <th class="text-center">Process Name</th>
                        <th class="text-center">Resource</th>
                        <th class="text-center">PIC</th>
                        <th class="text-center">Planning<br>Est. (Hours)</th>
                        <th class="text-center">Actualing<br>Start</th>
                        <th class="text-center">Qty</th>
                        <th class="text-center">Position</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody style="font-size: 11px"></tbody>
                </table>
        </div>

        <div class="tab-pane" id="nop">
            <input type="hidden" name="type" valu="nop" />
            <table class="table table-striped table-bordered table-hover actualing-table" id="actualing-nop">
                <thead>
                    <tr>
                        <th class="text-center">NOP</th>
                        <th class="text-center">Project</th>
                        <th class="text-center">Process Code</th>
                        <th class="text-center">Process Name</th>
                        <th class="text-center">Resource</th>
                        <th class="text-center">PIC</th>
                        <th class="text-center">Planning<br>Est. (Hours)</th>
                        <th class="text-center">Actualing<br>Start</th>
                        <th class="text-center">Qty</th>
                        <th class="text-center">Position</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody style="font-size: 11px"></tbody>
            </table>
        </div>

    </div>
</div>
