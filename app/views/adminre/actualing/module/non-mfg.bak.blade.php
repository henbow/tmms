<div class="col-md-12">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#waiting" data-toggle="tab">WAITING</a></li>
        <li><a href="#on-process" data-toggle="tab">ON PROCESS</a></li>
        <li><a href="#complete" data-toggle="tab">FINISH</a></li>
    </ul>

    <div class="tab-content panel">
        <div class="tab-pane active" id="waiting">
            <table class="table table-striped table-bordered table-hover actualing-table" id="actualing-waiting">
                <thead>
                    <tr>
                        <th class="text-center">NOP</th>
                        <th class="text-center">Project</th>
                        <th class="text-center">Process</th>
                        <th class="text-center">Resource Group</th>
                        <th class="text-center">Planning<br>Est. (Hours)</th>
                    </tr>
                </thead>
                <tbody style="font-size: 11px">
                    @foreach($planning_data_waiting as $planning)
                    <?php $project = Project::find($planning->project_id); ?>
                    <?php $process = Process::find($planning->process_id); ?>
                    <?php $resource_group = ResourceGroup::find($planning->resource_group_id); ?>
                    @if(isset($project->nop) && isset($process->process) && isset($resource_group->name) && !$planning->status)
                    <tr>
                        <td class="text-center">{{{ $project->nop }}}</td>
                        <td class="text-left">{{{ $project->project_name }}}</td>
                        <td class="text-center">{{{ $process->process }}}</td>
                        <td class="text-center">{{{ $resource_group->name }}}</td>
                        <td class="text-center">{{{ $planning->estimation_hour }}}</td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="tab-pane" id="on-process">
            <table class="table table-striped table-bordered table-hover actualing-table" id="actualing-onprocess">
                <thead>
                    <tr>
                        <th class="text-center">NOP</th>
                        <th class="text-center">Project</th>
                        <th class="text-center">Process</th>
                        <th class="text-center">Resource</th>
                        <th class="text-center">PIC</th>
                        <th class="text-center">Planning<br>Est. (Hours)</th>
                        <th class="text-center">Actualing<br>Start</th>
                    </tr>
                </thead>
                <tbody style="font-size: 11px">
                    @foreach($planning_data_onprocess as $planning)
                    <?php $project = Project::find($planning->project_id); ?>
                    <?php $process = Process::find($planning->plan_process_id); ?>
                    <?php $resource = Resource::find($planning->resource_id); ?>
                    <?php $pic = Pic::find($planning->pic_id); ?>
                   @if(isset($project->nop) && isset($process->process) && isset($resource_group->name) )
                    <tr>
                        <td class="text-center">{{{ $project->nop }}}</td>
                        <td class="text-left">{{{ $project->project_name }}}</td>
                        <td class="text-center">{{{ $process->process }}}</td>
                        <td class="text-center">{{{ $resource->name }}}</td>
                        <td class="text-center">{{{ @$pic->first_name." ".@$pic->last_name }}}</td>
                        <td class="text-center">{{{ $planning->estimation_hour }}}</td>
                        <td class="text-center">{{{ $planning->start_date }}}</td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="tab-pane" id="complete">
            <table class="table table-striped table-bordered table-hover actualing-table" id="actualing-complete">
                <thead>
                    <tr>
                        <th class="text-center">NOP</th>
                        <th class="text-center">Project</th>
                        <th class="text-center">Process</th>
                        <th class="text-center">Resource</th>
                        <th class="text-center">PIC</th>
                        <th class="text-center">Planning<br>Est. (Hours)</th>
                        <th class="text-center">Actualing<br>Start</th>
                        <th class="text-center">Actualing<br>Finish</th>
                    </tr>
                </thead>
                <tbody style="font-size: 11px">
                    @foreach($planning_data_complete as $planning)
                    <?php $project = Project::find($planning->project_id); ?>
                    <?php $process = Process::find($planning->plan_process_id); ?>
                    <?php $resource = Resource::find($planning->resource_id); ?>
                    <?php $pic = Pic::find($planning->pic_id); ?>
                    @if(isset($project->nop) && isset($process->process) && isset($resource_group->name) )
                    <tr>
                        <td class="text-center">{{{ $project->nop }}}</td>
                        <td class="text-left">{{{ $project->project_name }}}</td>
                        <td class="text-center">{{{ $process->process }}}</td>
                        <td class="text-center">{{{ $resource->name }}}</td>
                        <td class="text-center">{{{ @$pic->first_name." ".@$pic->last_name }}}</td>
                        <td class="text-center">{{{ $planning->estimation_hour }}}</td>
                        <td class="text-center">{{{ $planning->start_date }}}</td>
                        <td class="text-center">{{{ $planning->finish_date }}}</td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
