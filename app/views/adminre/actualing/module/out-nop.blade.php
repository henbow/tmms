<div class="col-md-12">
    <!-- START panel -->
    <div class="panel panel-default">
        <table class="table table-striped table-bordered table-hover" id="ajax-source">
            <thead>
                <tr>
                    <th class="text-center">NOP</th>
                    <th class="text-center">Process</th>
                    <th class="text-center">Resource</th>
                    <th class="text-center">TSR No</th>
                    <th class="text-center">TSR Date</th>
                    <th class="text-center">TTB No</th>
                    <th class="text-center">TTB Date</th>
                    <th class="text-center">Total Time</th>
                </tr>
            </thead>
            <tbody style="font-size: 11px">
            @foreach($actualing_data as $actualing)
                <tr>
                    <td class="text-center">{{{ $actualing->nop }}}</td>
                    <td class="text-center">{{{ $actualing->process_name }}}</td>
                    <td class="text-center">{{{ $actualing->resource_name }}}</td>
                    <td class="text-center">{{{ $actualing->tsr_no }}}</td>
                    <td class="text-center">{{{ $actualing->tsr_date }}}</td>
                    <td class="text-center">{{{ $actualing->ttb_no }}}</td>
                    <td class="text-center">{{{ $actualing->ttb_date }}}</td>
                    <td class="text-center">{{{ $actualing->total_time }}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
