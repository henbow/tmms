<div class="col-md-12">
    <!-- START panel -->
    <div class="panel panel-default">
        <table class="table table-striped table-bordered table-hover" id="ajax-source">
            <thead>
                <tr>
                    <th class="text-center">Barcode</th>
                    <th class="text-center">Part Name</th>
                    <th class="text-center">Qty</th>
                    <th class="text-center">Process</th>
                    <th class="text-center">Resource</th>
                    <th class="text-center">TSR No</th>
                    <th class="text-center">TSR Date</th>
                    <th class="text-center">TTB No</th>
                    <th class="text-center">TTB Date</th>
                    <th class="text-center">Total Time</th>
                    <th class="text-center">Status</th>
                </tr>
            </thead>
            <tbody style="font-size: 10px">
            @foreach($actualing_data as $actualing)
                <tr>
                    <td class="text-center">{{{ $actualing->material_barcode }}}</td>
                    <td class="text-center">{{{ $actualing->material_name }}}</td>
                    <td class="text-center">{{{ $actualing->material_qty }}}</td>
                    <td class="text-center">{{{ $actualing->process_name }}}</td>
                    <td class="text-center">{{{ $actualing->resource_name }}}</td>
                    <td class="text-center">{{{ $actualing->tsr_no }}}</td>
                    <td class="text-center">{{{ $actualing->tsr_date ? date("d-M-Y", strtotime($actualing->tsr_date)) : '' }}}</td>
                    <td class="text-center">{{{ $actualing->ttb_no }}}</td>
                    <td class="text-center">{{{ $actualing->ttb_date ? date("d-M-Y", strtotime($actualing->ttb_date)) : '' }}}</td>
                    <td class="text-center">{{{ $actualing->total_time }}}</td>
                    <td class="text-center">-</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
