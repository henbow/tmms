@extends($theme.'._layouts.master-popup')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/selectize/css/selectize.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui-timepicker.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                    </div>
                    <div class="table-responsive panel-collapse pull out">
                        {{ Form::open(array('class' => 'form-horizontal', 'id' => 'filter-actualing-1', 'data-parsley-validate')) }}
                        <br>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Resource Group</label>
                            <div class="col-sm-3">
                                <select name="group_resource" class="form-control">
                                    @foreach($resource_group as $group)
                                    <option {{{ isset($groupres) ? $groupres == $group->id ? 'selected' : '' : '' }}} value="{{{ $group->id }}}">{{{ $group->name }}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">NOP</label>
                            <div class="col-sm-3"><input type="text" name="nop" class="form-control" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Process</label>
                            <div class="col-sm-3"><input type="text" name="process" class="form-control" /></div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Project Planning</h3>
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                    </div>
                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-striped table-bordered table-hover" id="project-planning-table" style="font-size: 10px">
                            <thead>
                                <tr>
                                    <th class="text-center">NOP</th>
                                    <th class="text-center">Project</th>
                                    <th class="text-center">Process</th>
                                    <th class="text-center">Est. (hours)</th>
                                    <th class="text-center">Resource</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-primary">  
                    <div class="table-responsive panel-collapse pull out col-md-5">
                        {{ Form::open(array('class' => 'form-horizontal', 'id' => 'filter-actualing-2', 'data-parsley-validate')) }}
                        <br>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Resource Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="resource_name" class="form-control" />
                                <input type="hidden" name="resource_id" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">PIC</label>
                            <div class="col-sm-8">
                                <input type="text" name="pic_name" class="form-control" />
                                <input type="hidden" name="pic_id" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Status</label>
                            <div class="col-sm-8">
                                <select name="status" class="form-control">
                                    <option>OPEN</option>
                                    <option>CLOSE</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Remark</label>
                            <div class="col-sm-8"><input type="text" name="remark" class="form-control" /></div>
                        </div>
                        <input type="hidden" name="selected_plan" />
                        {{ Form::close() }}
                    </div>
                    <div class="table-responsive panel-collapse pull out col-md-6">
                        {{ Form::open(array('class' => 'form-horizontal', 'id' => 'filter-actualing-2', 'data-parsley-validate')) }}
                        <br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Actual Working Time</label>
                        </div>                            
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-danger" id="actual-time" style="font-size:25px;font-weight:bold">00:00:00:00</label>
                        </div>
                        <br>
                        <div class="form-group col-sm-3">
                            <button type="button" class="btn btn-success" id="start-time" style="font-size:15px;width:120px;height:50px;"> START </button>
                        </div>
                        <div class="form-group col-sm-3">
                            <button type="button" class="btn btn-default" id="finish-plan" style="font-size:15px;width:120px;height:50px;"> PLAN </button>
                        </div>
                        <div class="form-group col-sm-3">
                            <button type="button" class="btn btn-default" id="finish-rework" style="font-size:15px;width:120px;height:50px;"> REWORK </button>
                        </div>
                        <div class="form-group col-sm-3">
                            <button type="button" class="btn btn-default" id="finish-guarantee" style="font-size:15px;width:120px;height:50px;"> GUARANTEE </button>
                        </div>
                        <input type="hidden" name="state" value="start" />
                        {{ Form::close() }}
                    </div>
                    <div style="clear:both"></div>
                </div>
            </div>
                
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Actual Process Log</h3>
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                    </div>
                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-striped table-bordered table-hover" id="project-actualing-table" style="font-size: 10px">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">NOP</th>
                                    <th class="text-center">Project</th>
                                    <th class="text-center">Process</th>
                                    <th class="text-center">Est. (hours)</th>
                                    <th class="text-center">Start</th>
                                    <th class="text-center">Finish</th>
                                    <th class="text-center">Resource</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Remark</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->
    <input type="hidden" name="actualing_id" />
</section>
<!--/ END modal-lg -->
@stop
 
@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-timepicker.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-touch.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/xeditable/js/bootstrap-editable.min.js') }}}"></script>      
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/typeahead/bootstrap3-typeahead.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/element.js') }}}"></script>    
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/jquery.runner.js') }}}"></script>
<script type="text/javascript">
(function(){
    resetAllButtons();
    $('[name="nop"]').keypress(function(e){
        resetAllButtons();
        var key = e.which;
        if(key == 13)
        {
            $('#project-planning-table tbody').empty();
            $('[name="process"]').focus();            
            //NProgress.start();
            $.post('{{{ route("actualing.planning.nop", $step) }}}', {nop: $(this).val()}, function(resp){
                //NProgress.done();                
                var resp = $.parseJSON(resp);
                if (resp.length > 0) {                    
                    $.each(resp, function (i, data) {
                        var row_content = "<tr onclick=\"getPlanResource("+data.resource_id+");getLastState("+data.id+");\" style=\"cursor:pointer\">"+
                                          "<td class=\"text-center\">"+data.nop+"</td>"+
                                          "<td class=\"text-center\">"+data.project+"</td>"+
                                          "<td class=\"text-center\">"+data.process+"</td>"+
                                          "<td class=\"text-center\">"+data.estimation_hour+"</td>"+
                                          "<td class=\"text-center\">"+data.resource+"</td>"+
                                          "</tr>";
                        $('#project-planning-table tbody').append(row_content);
                    });
                }
                else
                {
                    resetAllButtons();
                }
            });
        }
    });
    
    $('#actual-time').runner({milliseconds: false}).on('runnerStart', function(e, info){
        $('#start-time').removeClass('btn-success').addClass('btn-default').text(' PAUSE ');
        $('#finish-plan').removeClass('btn-default').addClass('btn-primary').prop("disabled", false);
        startActualing();
    }).on('runnerStop', function(e, info){
        $('#start-time').removeClass('btn-default').addClass('btn-success').text(' START ');
        $('#finish-plan').removeClass('btn-primary').addClass('btn-default').prop("disabled", true);
        $('[name="state"]').val('pause');
        pauseActualing();
    });
    $('#start-time').click(function(e){
        if ($(this).attr("disabled") != "disabled") {
            $('#actual-time').runner('toggle');
        }
    });
    $('#finish-plan').click(function(e){
        if ($(this).attr("disabled") != "disabled") {
            $('#actual-time').runner('reset', true);
            finishActualing('plan');
        }
    });
})();
function enableStartButton() {
    $('#start-time').prop("disabled", false); 
    $('#start-time').removeAttr("disabled"); 
    $('#start-time').removeClass('btn-default');
    $('#start-time').addClass('btn-success');
    $('[name="state"]').val('start');
}
function resetAllButtons() {
    $('#start-time').removeClass('btn-success').addClass('btn-default').prop("disabled", true); 
    $('#finish-plan').removeClass('btn-primary').addClass('btn-default').prop("disabled", true); 
    $('#finish-rework').removeClass('btn-danger').addClass('btn-default').prop("disabled", true); 
    $('#finish-guarantee').removeClass('btn-warning').addClass('btn-default').prop("disabled", true); 
    $('[name="state"]').val('start');
}
function getPlanResource(resource_id) {   
    $.get('{{{ route('actualing.index') }}}/'+resource_id+'/resource', function(resp){
        var resp = $.parseJSON(resp);
        $('[name="resource_name"]').val(resp.name);
        $('[name="resource_id"]').val(resp.id);
        $('[name="pic_name"]').val(resp.pic_name);
        $('[name="pic_id"]').val(resp.pic_id);
        enableStartButton(); 
    });
}
function getLastState(schedule_id) {
    $('[name="selected_plan"]').val(schedule_id);        
    /*
    $.get('{{{ route('actualing.index') }}}/'+schedule_id+'/last_state', function(resp){
        var resp = $.parseJSON(resp);        
        
    });
    */
}
function startActualing() {
    $.post('{{{ route('actualing.start_actualing', $step) }}}',
    {schedule:$('[name="selected_plan"]').val(),remark:$('[name="remark"]').val(),state:$('[name="state"]').val()},
    function(resp){
        var resp = $.parseJSON(resp);
        $('[name="actualing_id"]').val(resp.id);
        getActualingData();
    });
}
function pauseActualing() {
    //code
}
function finishActualing(type) {
    switch (type) {
        case 'plan':
            
            break;
        case 'rework':
        
            break;
        case 'guarantee':
        
            break;
        default:
            return false;
            break;
    }
}
function getActualingData() {
    $.post('{{{ route('actualing.actualing_data', $step) }}}',
    {actualing_id: $('[name="actualing_id"]').val()},
    function(resp){
        var resp = $.parseJSON(resp);
        if (resp.length > 0) {                    
            $.each(resp, function (i, data) {
                var row_content = "<tr>"+
                                  "<td class=\"text-center\">"+(i+1)+"</td>"+
                                  "<td class=\"text-center\">"+data.nop+"</td>"+
                                  "<td class=\"text-center\">"+data.process+"</td>"+
                                  "<td class=\"text-center\">"+data.estimation_hour+"</td>"+
                                  "<td class=\"text-center\">"+data.resource+"</td>"+
                                  "<td class=\"text-center\">"+data.nop+"</td>"+
                                  "<td class=\"text-center\">"+data.project+"</td>"+
                                  "<td class=\"text-center\">"+data.process+"</td>"+
                                  "<td class=\"text-center\">"+data.estimation_hour+"</td>"+
                                  "<td class=\"text-center\">"+data.resource+"</td>"+
                                  "</tr>";
                $('#project-actualing-table tbody').append(row_content);
            });
        }
    });
}
</script>
@stop