@extends($theme.'._layouts.master-popup')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/selectize/css/selectize.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui-timepicker.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">  
                    <div class="table-responsive panel-collapse pull out">
                        {{ Form::open(array('route' => 'scheduling.index', 'class' => 'form-horizontal', 'id' => 'filter-planning', 'data-parsley-validate')) }}
                        <br>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Workcenter Group</label>
                            <div class="col-sm-3">
                                <select name="group_resource" class="form-control">
                                    @foreach($resource_group as $group)
                                    <option {{{ isset($groupres) ? $groupres == $group->id ? 'selected' : '' : '' }}} value="{{{ $group->id }}}">{{{ $group->name }}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">NOP</label>
                            <div class="col-sm-3"><input type="text" name="nop" class="form-control" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Process</label>
                            <div class="col-sm-3"><input type="text" name="process" class="form-control" /></div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        @include($theme . '.actualing.popup.' . $module)
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->
    
</section>
<!--/ END modal-lg -->
@stop
 
@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-timepicker.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-touch.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/xeditable/js/bootstrap-editable.min.js') }}}"></script>      
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/typeahead/bootstrap3-typeahead.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/element.js') }}}"></script>    
<script type="text/javascript">
(function(){
    
})();
</script>
@stop