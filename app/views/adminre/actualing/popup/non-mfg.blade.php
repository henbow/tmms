@extends($theme.'._layouts.master-popup')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/selectize/css/selectize.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui-timepicker.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">@stop

@section('main')
<section id="main" role="main">
<!-- START Template Container -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title" id="actualing-title">ACTUALING</h3>
                    <div class="panel-toolbar text-right">
                        <!-- option -->
                        <!-- <div class="option">
                            <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                        </div> -->
                        <!--/ option -->
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-1 control-label">NOP</label>
                        <div class="col-sm-4">
                            <input type="text" name="nop" class="form-control" />
                            <span class="text-danger" id="nop-error-message"></span>
                        </div>
                        <label class="col-sm-1 control-label">Process</label>
                        <div class="col-sm-4">
                            <input type="text" name="process" class="form-control" />
                            <span class="text-danger" id="process-error-message"></span>
                        </div>
                        <div class="col-sm-2 text-right">
                            <button type="button" class="btn btn-primary" id="refresh-interface" onclick="refreshInterface()"><strong>REFRESH</strong></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <!-- START panel -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>All Process</h3>
                    <div class="panel-toolbar text-right">
                        <!-- option -->
                        <div class="option">
                            <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                        </div>
                        <!--/ option -->
                    </div>
                </div>
                <!-- panel body with collapse capabale -->
                <div class="table-responsive panel-collapse pull out">
                    <table class="table table-striped table-bordered table-hover actualing-table" id="project-planning-table">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">NOP</th>
                                <th class="text-center">Project</th>
                                <th class="text-center">Process</th>
                                <th class="text-center">Qty</th>
                                <th class="text-center">Position</th>
                                <th class="text-center">Est.</th>
                                <th class="text-center">Resource</th>
                                <th class="text-center">PIC</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 11px"></tbody>
                    </table>
                
                </div>
                <!--/ panel body with collapse capabale -->
            </div>
        </div>

    </div>
</div>
<!--/ END Template Container -->

<div id="start-btn-tpl" style="display: none">
    <div class="col-md-12">
        {{ Form::open(array('route' => array('actualing.start_actualing',$type), 'class' => 'form-horizontal', 'data-id' => 'start-form', 'onsubmit' => 'doStartActualing(this);return false;', 'data-parsley-validate')) }}
        <div class="form-group">
            <div class="row">
                <label class="col-sm-2 control-label">Resource</label>
                <div class="col-sm-9">
                    <input type="text" data-name="resource-{{{$random}}}" class="form-control" value="" required />
                    <span class="text-danger" data-id="resource-alert" style="display:none;">Error</span>
                    <input type="hidden" data-name="resource" resource-id-value="" required />
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label class="col-sm-2 control-label">PIC</label>
                <div class="col-sm-9">
                    <input type="text" data-name="pic-{{{$random}}}" class="form-control" value="" required />
                    <span class="text-danger" data-id="pic-alert" style="display:none;">Error</span>
                    <input type="hidden" data-name="pic" pic-id-value="" required />
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label class="col-sm-2 control-label">Quantity</label>
                <div class="col-sm-3">
                    <select multiple data-name="qty[]" data-id="qty" class="form-control" required></select>
                </div>
            </div>
        </div>
        <div class="form-group" style="text-align: center;">
            <div class="row">
                <button type="button" class="btn btn-default btn-lg" data-id="cancel-btn" data-dismiss="modal">Cancel</button>
                <button type="submit" id="start-btn" class="btn btn-primary btn-lg" >Start</button>
            </div>
        </div>
        
        
        <input type="hidden" data-name="planning_id" planning-id-value="" />
        <input type="hidden" data-name="nop" nop-value="" />
        <input type="hidden" data-name="process" process-value="" />
        <input type="hidden" data-name="actualing" actualing-value="" />
        {{ Form::close() }}
    </div>
    <div style="clear:both"></div>
</div>

<div id="finish-btn-tpl" style="display: none">
    <div class="col-md-12">
        {{ Form::open(array('route' => array('actualing.finish_actualing',$type), 'class' => 'form-horizontal', 'data-id' => 'finish-form', 'onsubmit' => 'doFinishActualing(this);return false;', 'data-parsley-validate')) }}
        <div class="form-group">
            <div class="row">
                <label class="col-sm-2 control-label">Position</label>
                <div class="col-sm-6">
                    <select multiple data-name="position[]" data-id="position" class="form-control"></select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label class="col-sm-2 control-label">Remark</label>
                <div class="col-sm-9">
                    <textarea data-name="remark" class="form-control"></textarea>
                </div>
            </div>
        </div>
        <div class="form-group" data-id="finish-type" style="text-align: center;">
            <div class="row">
                <button type="button" data-id="finish-plan-btn" onclick="selectPlanFinish('plan');" class="btn btn-lg" disabled>PLAN</button>
                <button type="submit" data-id="finish-rework-btn" onclick="subFinish('rework','complete');" class="btn btn-lg" disabled>REWORK</button>
                <button type="submit" data-id="finish-guarantee-btn" onclick="subFinish('guarantee','complete');" class="btn btn-lg" disabled>GUARANTEE</button>
            </div>
        </div>
        <div class="form-group" data-id="finish-sub-type" style="text-align: center; display:none;">
            <div class="row">
                <button type="submit" data-id="subfinish-onprocess" onclick="subFinish('plan','onprocess');" class="btn btn-success btn-lg" >ON PROCESS</button>
                <button type="submit" data-id="subfinish-complete" onclick="subFinish('plan','complete');" class="btn btn-danger btn-lg">COMPLETE</button>
            </div>
        </div>
        <input type="hidden" data-name="finish-type" finish-value="" />
        <input type="hidden" data-name="subfinish-type" subfinish-value="" />
        <input type="hidden" data-name="actualing" actualing-value="" />
        {{ Form::close() }}
    </div>
    <div style="clear:both"></div>
</div>

</section>
<!--/ END modal-lg -->
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-timepicker.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-touch.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/xeditable/js/bootstrap-editable.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/typeahead/bootstrap3-typeahead.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/element.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/jquery.runner.js') }}}"></script>
<script type="text/javascript">
jQuery.fn.preventDoubleSubmission = function() {
  $(this).on('submit',function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
  });

  // Keep chainability
  return this;
};
function selectPlanFinish(type) {
    $('[name="finish-type"]').val(type);
    $('#finish-type').fadeToggle('fast',function(){
        $('#finish-sub-type').fadeToggle('fast');
    });
}
function subFinish(type, subtype) {
    $('[name="finish-type"]').val(type);
    $('[name="subfinish-type"]').val(subtype);
    $('form#finish-form').submit();
}
function refreshInterface() {
    window.location.reload();
}
function startButtonTpl(planning_id,nop,process,actualing) {
    var tmpl = $('#start-btn-tpl').html()
        .replace(/data-id/g, 'id')
        .replace(/data-name/g, 'name')
        .replace('resource-id-value=""', 'value=""')
        .replace('pic-id-value=""', 'value=""')
        .replace('planning-id-value=""', 'value="'+planning_id+'"')
        .replace('nop-value=""', 'value="'+nop+'"')
        .replace('process-value=""', 'value="'+process+'"')
        .replace('actualing-value=""', 'value="'+actualing+'"');
    return tmpl;
}
function finishButtonTpl(finish,subfinish,actualing) {
    var tmpl = $('#finish-btn-tpl').html()
        .replace(/data-id/g, 'id')
        .replace(/data-name/g, 'name')
        .replace('finish-value=""', 'value="'+finish+'"')
        .replace('subfinish-value=""', 'value="'+subfinish+'"')
        .replace('actualing-value=""', 'value="'+actualing+'"');
    return tmpl;
}
var selectedRow = actualingId = null;
function startActualing(elem) {
    selectedRow = $(elem).parents('tr');

    var planning_id = selectedRow.data('id');
    var actualing = selectedRow.data('actualing');
    var nop = selectedRow.find('td:eq(1)').text();
    var process = selectedRow.data('process');
    var finish = selectedRow.data('actualing-finish');
    var subfinish = selectedRow.data('actualing-subfinish');
    $.post('{{{ route('actualing.quantity') }}}', {actualing_id:actualing,planning_id:planning_id,type:"{{{$type}}}"}, function(resp){
        $.each($.parseJSON(resp),function(i,item){
            $('#qty').append('<option value="'+item.id+'">'+item.qty_num+'</option>');
        });
    });
    if ($(elem).attr("disabled") != "disabled") {
        var modal = bootbox.dialog({
            title: "Start Actualing",
            message: startButtonTpl(planning_id,nop,process,actualing)
        });
        modal.on('shown.bs.modal', function(){
            $("[name=\"resource-{{{ $random }}}\"]").typeahead({
                source:function (query, resource) {
                    resources = [];
                    map = {};

                    var data = <?php echo json_encode($resources); ?>;

                    $.each(data, function (i, item) {
                        var key = item.code+' -> '+item.name;
                        map[key] = item;
                        resources.push(key);
                    });

                    resource(resources);
                },
                updater: function (resource) {
                    $('[name="resource"]').val(map[resource].code);
                    $("[name=\"pic-{{{ $random }}}\"]").focus();
                    return resource;
                }
            });
            $("[name=\"pic-{{{ $random }}}\"]").typeahead({
                source:function (query, pic) {
                    pics = [];
                    map = {};

                    var data = <?php echo json_encode($pics); ?>;

                    $.each(data, function (i, item) {
                        var key = item.code+' -> '+item.first_name+' '+item.last_name;
                        map[key] = item;
                        pics.push(key);
                    });

                    pic(pics);
                },
                updater: function (pic) {
                    $('[name="pic"]').val(map[pic].code);
                    return pic;
                }
            });
        });
    }
}
function doStartActualing(elem) {
    if(!$(elem).hasClass('submitted')) {
        var is_passed = false;
        if($(elem).find('[name="resource"]').val() && $(elem).find('[name="pic"]').val()) {
            is_passed = true;
        }
        if(is_passed) {            
            $(elem).addClass('submitted');
            $.post($(elem).attr('action'), $(elem).serialize(), function(data) {
                $('#cancel-btn').click();
                console.log(data);
                actualingId = data.id;
                selectedRow.attr('data-actualing',data.id);
                selectedRow.attr('data-actualing-qty',data.qty_id);
                selectedRow.attr('data-actualing-subfinish','ONPROCESS');
                selectedRow.find('td:eq(7)').text(data.resource_name);
                selectedRow.find('td:eq(8)').text(data.pic_name);
                selectedRow.find('.start').prop('disabled',true).removeClass('btn-success').addClass('btn-default');
                selectedRow.find('.finish').prop('disabled',false).removeClass('btn-default').addClass('btn-danger');

                var statusTxt = "";
                $.each(data.qty, function(i,qty_data){
                    statusTxt += "Qty "+qty_data.qty_num+": "+(qty_data.is_complete?"REWORK/GUARANTEE":"PLAN")+"";
                });
                selectedRow.find('td:eq(9)').html(statusTxt);

                $.post('{{{ route('actualing.set_status_log') }}}',{type:'nop',actualing_id:data.id,status:statusTxt},function(resp){});
            }, 'json');
        } else {
            $('#resource-alert').empty().text('Resource is required!').show();
            $('#pic-alert').empty().text('PIC is required!').show();
        }
    }
}
function finishActualing(elem) {
    selectedRow = $(elem).parents('tr');
    var actualing = $(elem).parents('tr').attr('data-actualing');
    var finish = $(elem).parents('tr').attr('data-actualing-finish');
    var subfinish = $(elem).parents('tr').attr('data-actualing-subfinish');
    $.post("{{{route('actualing.positions')}}}", {actualing_id:actualing,finish:finish,subfinish:subfinish,type:"<?php echo $type ?>"},function(resp){
        var resp = $.parseJSON(resp);      
        $('[data-id="position"]').empty();  
        $.each(resp, function(qty_id, data){
            var is_completed = data.is_complete;
            $.each(data.position, function(i, item){
                $('[data-id="position"]').append('<option class="text-left" data-status="'+(is_completed=='true'?'COMPLETE':'ONPROCESS')+'" onclick="selectedPosition(this)" value="'+item.id+'">'+item.label+' - '+(is_completed=='true'?'REWORK/GUARANTEE':'PLAN')+'</option>');
            });
        });
        
        if ($(elem).attr("disabled") != "disabled") {
            var modal = bootbox.dialog({
                title: "Finish Actualing",
                message: finishButtonTpl(finish,subfinish,actualing)
            });
        }
    });
}
function doFinishActualing(elem) {
    if(!$(elem).hasClass('submitted')) {
        $(elem).addClass('submitted');
        $.post($(elem).attr('action'), $(elem).serialize(), function(data) {
            $('.bootbox-close-button').click();
            selectedRow.find('.start').prop('disabled',false).addClass('btn-success').removeClass('btn-default');
            selectedRow.find('.finish').prop('disabled',true).addClass('btn-default').removeClass('btn-danger');
            // selectedRow.find('td:eq(9)').text(data.finish+" - "+data.finish_type);
            selectedRow.addClass(data.finish == 'PLAN' ? 'success' : 'danger');
            selectedRow.attr('data-actualing-finish', data.finish);
            selectedRow.attr('data-actualing-subfinish', data.finish_type);
        }, 'json');
    }
}
function getActualingData(post_data) {
    $('#project-planning-table tbody').empty();
    $.post('{{{ route("actualing.planning.data", $type) }}}', post_data, function(resp){
        var resp = $.parseJSON(resp);
        if (resp.length > 0) {
            $.each(resp, function (i, data) {
                var btn_group = "<div class=\"btn-group\">"+
                            "<button type=\"button\" onclick=\"startActualing(this)\" class=\"btn btn-success start\">Start</button>"+
                            "<button type=\"button\" onclick=\"finishActualing(this)\" class=\"btn btn-default finish\" disabled>Finish</button>"+
                            "</div>";
                if(data.actualing_status == 'ON PROCESS') {
                    btn_group = "<div class=\"btn-group\">"+
                            "<button type=\"button\" onclick=\"startActualing(this)\" class=\"btn btn-default start\" disabled>Start</button>"+
                            "<button type=\"button\" onclick=\"finishActualing(this)\" class=\"btn btn-danger finish\">Finish</button>"+
                            "</div>";
                }
                var row_content = '<tr class="'+(data.actualing_finish?(data.actualing_finish=='REWORK'||data.actualing_finish=='GUARANTEE'?"danger":"success"):"")+'" '+
                                    'data-id="'+data.id+'"'+
                                    'data-process="'+data.process_code+'" '+
                                    'data-actualing="'+data.actualing_id+'"'+
                                    'data-actualing-qty="'+(data.actualing_position?actualing_position:'')+'"'+
                                    'data-actualing-finish="'+data.actualing_finish+'"'+
                                    'data-actualing-subfinish="'+data.actualing_subfinish+'">'+
                    "<td class=\"text-center\" width=\"5%\">"+(i+1)+"</td>"+
                    "<td class=\"text-center\">"+data.nop+"</td>"+
                    "<td class=\"text-center\">"+data.project+"</td>"+
                    "<td class=\"text-center\">"+data.process_code+" - "+data.process+"</td>"+
                    "<td class=\"text-center\">"+data.qty+"</td>"+
                    "<td class=\"text-center\">"+(data.position>0?data.position:1)+"</td>"+
                    "<td class=\"text-center\">"+data.estimation+" hrs</td>"+
                    "<td class=\"text-center\">"+data.resource_name+"</td>"+
                    "<td class=\"text-center\">"+data.pic_name+"</td>"+
                    "<td class=\"text-center\">"+data.status_msg+"</td>"+
                    "<td class=\"text-center\">"+btn_group+"</td></tr>";
                    $('#project-planning-table tbody').append(row_content);
            });
        }
        else
        {                
            $('#nop-error-message').text('Planning data is not found!');
        }
    });
}
function selectedPosition(elem){
    var status = $(elem).data('status');
    if(status == 'COMPLETE'){
        $('form#finish-form')
            .find('button#finish-plan-btn').removeClass('btn-primary').prop('disabled',true).end()
            .find('button#finish-rework-btn').addClass('btn-primary').prop('disabled',false).end()
            .find('button#finish-guarantee-btn').addClass('btn-primary').prop('disabled',false).end();
    } else {
        $('form#finish-form')
            .find('button#finish-plan-btn').addClass('btn-primary').prop('disabled',false).end()
            .find('button#finish-rework-btn').removeClass('btn-primary').prop('disabled',true).end()
            .find('button#finish-guarantee-btn').removeClass('btn-primary').prop('disabled',true).end();
    }
}
(function(){
    getActualingData(null);
    $('[name="nop"]').focus();
    $('[name="nop"]').keypress(function(e){    
        $('#process-error-message').text('');
        $('#selected-item').empty();
        var key = e.which;
        if(key == 13)
        {
            $('#nop-error-message').text('');
            $('[name="process"]').focus();  
            var post_data = { nop: $(this).val() };
            getActualingData(post_data);
        }
    });
    $('[name="process"]').keypress(function(e){
        $('#selected-item').empty();
        var post_data = null;
        var key = e.which;
        if(key == 13)
        {        
            $('#process-error-message').text('');
            if($(this).val() == '') 
                post_data = { nop: $('[name="nop"]').val() };
            else 
                post_data = { nop: $('[name="nop"]').val(), process_code: $(this).val() };
            getActualingData(post_data);
        }
    });
    $('#position').click(function(e){
        var posId = $(this).val();
        
    });
    $('.start').click(function(e){startActualing(this)});
    $('.finish').click(function(e){finishActualing(this)});
    $('[name="nop"]').val('');
    $('[name="process"]').val('');
})();
</script>
@stop
