@extends($theme.'._layouts.master-popup')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/selectize/css/selectize.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui-timepicker.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title" id="actualing-title">ACTUALING</h3>
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <!-- <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                            </div> -->
                            <!--/ option -->
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-1 control-label">Barcode</label>
                            <div class="col-sm-4">
                                <input type="text" name="part_code" class="form-control" />
                                <span class="text-danger" id="part_code-error-message"></span>
                            </div>
                            <label class="col-sm-1 control-label">Process</label>
                            <div class="col-sm-4">
                                <input type="text" name="process" class="form-control" />
                                <span class="text-danger" id="process-error-message"></span>
                            </div>
                            <div class="col-sm-2 text-right">
                                <button type="button" class="btn btn-primary" id="refresh-interface" onclick="refreshInterface()"><strong>REFRESH</strong></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Planning Data</h3>
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                    </div>
                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-striped table-bordered table-hover" id="project-planning-table" style="font-size: 10px">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">NOP</th>
                                    <th class="text-center">Barcode</th>
                                    <th class="text-center">Part Name</th>
                                    <th class="text-center">Process</th>
                                    <th class="text-center">Process Code</th>
                                    <th class="text-center">Position</th>
                                    <th class="text-center">Qty</th>
                                    <th class="text-center">Resource Group</th>
                                </tr>
                            </thead>
                            <tbody style="border-bottom: 1px solid #CCCCCC;">
                            </tbody>
                        </table>
                    </div>

                    <div class="panel-body" style="font-size:11px;" id="selected-item"></div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        {{ Form::open(array('route' => array('actualing.start_actualing',$type), 'class' => 'form-horizontal', 'data-id' => 'start-form', 'onsubmit' => 'doStartActualing(this);return false;', 'data-parsley-validate')) }}
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Resource</label>
                                <div class="col-sm-3">
                                    <input type="text" name="resource_code" class="form-control" />
                                    <span class="text-danger" id="resource_code-error-message"></span>
                                </div>

                                <label class="col-sm-1 control-label">PIC</label>
                                <div class="col-sm-3">
                                    <input type="text" name="pic_code" class="form-control" />
                                    <span class="text-danger" id="pic_code-error-message"></span>
                                </div>
                            </div>
                            <p>&nbsp;</p>
                            <div class="row">
                                <label class="col-sm-2 control-label">Quantity</label>
                                <div class="col-sm-6">
                                    <select type="text" name="qty[]" id="qty" class="form-control" multiple></select>
                                    <span class="text-danger" id="qty-error-message"></span>
                                </div>
                            </div>
                            <p>&nbsp;</p>
                            <div class="row">
                                <div class="col-sm-9 text-right">
                                    <button type="button" class="btn btn-success" id="start-time" style="font-size: 15px; width: 200px; height: 50px;"> START </button>
                                    <span>&nbsp;&nbsp;</span>
                                    <button type="button" class="btn btn-danger" id="finish-time" style="font-size: 15px; width: 200px; height: 50px;"> FINISH </button>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="resource" value="" />
                        <input type="hidden" name="pic" value="" />
                        <input type="hidden" name="planning_id" value="" />
                        <input type="hidden" name="barcode" value="" />
                        <input type="hidden" name="process" value="" />
                        <input type="hidden" name="actualing" value="" />
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <dl class="dl-horizontal" style="padding-bottom: 0;margin-bottom: 0;">
                            <dt style="width:100px;">Project</dt><dd id="project_status" style="margin-left: 120px;">-</dd>
                            <dt style="width:100px;">Part Name</dt><dd id="material_status" style="margin-left: 120px;">-</dd>
                            <dt style="width:100px;">Process</dt><dd id="process_status" style="margin-left: 120px;">-</dd>
                            <dt style="width:100px;">Resource</dt><dd id="resource_status" style="margin-left: 120px;">-</dd>
                            <dt style="width:100px;">PIC</dt><dd id="pic_status" style="margin-left: 120px;">-</dd>
                            <!-- <dt style="width:100px;">Start Process</dt><dd id="start_time_status" style="margin-left: 120px;">-</dd> -->
                            <dt style="width:100px;">Status</dt><dd id="status_status" style="margin-left: 120px;">WAITING</dd>
                        </dl>
                    </div>
                </div>
            </div>

            <!--
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>Actual Process Log</h3>
                        <div class="panel-toolbar text-right">
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-striped table-bordered table-hover" id="project-actualing-table" style="font-size: 10px">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">NOP</th>
                                    <th class="text-center">Process</th>
                                    <th class="text-center">Est. (hours)</th>
                                    <th class="text-center">Start</th>
                                    <th class="text-center">Finish</th>
                                    <th class="text-center">Resource</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Remark</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            -->
        </div>
    </div>
    <!--/ END Template Container -->

    
<!--    <input type="hidden" name="state" value="start" />-->
    <div id="btn-tpl" style="display: none">
        <div class="col-md-12">

            <div class="form-group">
                <div class="row">
                    <label class="col-sm-2 control-label">Position</label>
                    <div class="col-sm-3">
                        <input type="text" name="position" class="form-control" />
                        <span class="text-danger" id="position-error-message"></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-sm-2 control-label">Remark</label>
                    <div class="col-sm-12">
                        <textarea name="remark" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group" style="text-align: center;">
                <div class="row">
                    <button type="button" id="finish-plan-btn" onclick="finishActualing('plan');" class="btn btn-primary btn-lg" >PLAN</button>
                    <button type="button" id="finish-rework-btn" onclick="finishActualing('rework');" class="btn btn-lg" disabled>REWORK</button>
                    <button type="button" id="finish-guarantee-btn" onclick="finishActualing('guarantee');" class="btn btn-lg" disabled>GUARANTEE</button>
                </div>
            </div>
            <input type="hidden" name="finish-type" finish-value="" />
            <input type="hidden" name="subfinish-type" subfinish-value="" />
            <input type="hidden" name="actualing" actualing-value="" />
        </div>
        <div style="clear:both"></div>
    </div>

</section>
<!--/ END modal-lg -->
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-timepicker.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-touch.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/xeditable/js/bootstrap-editable.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/typeahead/bootstrap3-typeahead.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/element.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/jquery.runner.js') }}}"></script>
<!-- <script type="text/javascript" src="http://tmms.tridaya-as.com:1421/socket.io/socket.io.js"></script> -->
<script type="text/javascript">
function getActualingLogData() {
    $.post('{{{ route('actualing.actualing_data.log', $type) }}}',
    {actualing_id: $('[name="actualing_id"]').val()},
    function(resp){
        var resp = $.parseJSON(resp);
        if (resp.length > 0) {
            $('#project-actualing-table tbody').empty();
            $.each(resp, function (i, data) {
               var row_content = "<tr data-id=\""+data.id+"\">"+
                                 "<td class=\"text-center\" width=\"5%\">"+(i+1)+"</td>"+
                                 "<td class=\"text-center\">"+data.nop+"</td>"+
                                 "<td class=\"text-center\">"+data.process+"</td>"+
                                 "<td class=\"text-center\">"+data.estimation_hour+"</td>"+
                                 "<td class=\"text-center\">"+data.start_date+"</td>"+
                                 "<td class=\"text-center\">"+(data.finish_date?data.finish_date:'')+"</td>"+
                                 "<td class=\"text-center\">"+data.resource+"</td>"+
                                 "<td class=\"text-center\">"+data.status+"</td>"+
                                 "<td class=\"text-center\">"+data.remark+"</td>"+
                                 "</tr>";
               $('#project-actualing-table tbody').append(row_content);
           });
       }
    });
}
function getLastState() {
    $.post('{{{ route('actualing.last_state.data', array('material')) }}}',
        {
            barcode:$('[name="part_code"]').val(),
            process_code:$('[name="process"]').val()
        },
        function (resp) {
            var response = $.parseJSON(resp);
            console.log(response);
            if(response.length > 0) {
                response = response[0];

                $('[name="actualing_id"]').val(response.id);
                $('[name="planning_id"]').val(response.planning_id);
                $('[name="resource_id"]').val(response.resource_id);
                $('[name="pic_id"]').val(response.pic_id);
                $('#resource_status').text(response.resource_name);
                $('#start_time_status').text(response.start_date);
                $('#status_status').text(response.status);

                if(response.status == 'ON PROCESS') {
                    disableStartButton();
                    enableFinishButton();

                    $('[name="resource_code"]').val(response.resource_code);
                    $('[name="pic_code"]').val(response.pic_code);
                }

                getActualingLogData();
                disableAllForm();

                if(response.finish_type == 'PLAN') {
                    $('#finish-plan-btn').removeClass('btn-primary').addClass('btn-default').prop("disabled", true);
                    $('#finish-rework-btn').removeClass('btn-default').addClass('btn-primary').prop("disabled", false);
                    $('#finish-guarantee-btn').removeClass('btn-primary').addClass('btn-default').prop("disabled", true);
                } else if(response.finish_type == 'REWORK') {
                    $('#finish-plan-btn').removeClass('btn-primary').addClass('btn-default').prop("disabled", true);
                    $('#finish-rework-btn').removeClass('btn-default').addClass('btn-primary').prop("disabled", false);
                    $('#finish-guarantee-btn').removeClass('btn-default').addClass('btn-primary').prop("disabled", false);
                } else {
                    $('#finish-plan-btn').removeClass('btn-default').addClass('btn-primary').prop("disabled", false);
                    $('#finish-rework-btn').removeClass('btn-primary').addClass('btn-default').prop("disabled", true);
                    $('#finish-guarantee-btn').removeClass('btn-primary').addClass('btn-default').prop("disabled", true);
                }
            } else {
                $('#finish-plan-btn').removeClass('btn-default').addClass('btn-primary').prop("disabled", false);
                $('#finish-rework-btn').removeClass('btn-primary').addClass('btn-default').prop("disabled", true);
                $('#finish-guarantee-btn').removeClass('btn-primary').addClass('btn-default').prop("disabled", true);
            }
        }
    );
}
function prepareForActualing(elem, planning_id) {
    console.log(elem);
    var nop = elem.children('td:eq(1)').text();
    var code = elem.children('td:eq(2)').text();
    var name = elem.children('td:eq(3)').text();
    var process = elem.children('td:eq(4)').text();
    var position = elem.children('td:eq(6)').text();
    var last_state = null;

    $('#selected-item').html('Part Code: <span class="text-primary"><strong>'+code+'</strong></span>, Part Name: <span class="text-success"><strong>'+name+'</strong></span>, Process: <span class="text-info"><strong>'+process+'</strong></span>');
    $('#project_status').html(nop);
    $('#barcode_status').html(code);
    $('#material_status').html(name);
    $('#process_status').html(process);
    $("[name=\"planning_id\"]").val(planning_id);
    $("[name=\"total_position\"]").val(position);
}
function startActualing() {
    console.log('Actualing is started...');
    $.post('{{{ route('actualing.start_actualing', 'material') }}}', {
        barcode: $('[name="part_code"]').val(),
        process_code: $('[name="process"]').val(),
        resource_code: $('[name="resource_code"]').val(),
        pic_code: $('[name="pic_code"]').val(),
        qty: $('#qty').val(),
        planning_id: $('[name="planning_id"]').val(),
        actualing_id: $('[name="actualing_id"]').val()
    }, function(resp){
        var response = $.parseJSON(resp);

        $('#status_status').text('ON PROCESS');
        $('[name="actualing_id"]').val(response.id);

        disableAllForm();
        disableStartButton();
        enableFinishButton();
        getActualingLogData();
    });
}
function finishActualing(finish_type) {
    console.log('Actualing is finished...');
    $.post('{{{ route('actualing.finish_actualing', 'material') }}}', {
        position: $('#position').val(),
        remark: $('#remark').val(),
        actualing_id: $('[name="actualing_id"]').val(),
        finish_type: finish_type,
    }, function(resp){
        refreshInterface();
    });
}

function refreshInterface() {
    window.location.reload();
}

function enableStartButton() {
    $('#start-time').prop("disabled", false);
    $('#start-time').removeClass('btn-default');
    $('#start-time').addClass('btn-success');
}
function disableStartButton() {
    $('#start-time').prop("disabled", true);
    $('#start-time').removeClass('btn-success');
    $('#start-time').addClass('btn-default');
}
function enableFinishButton() {
    $('#finish-time').prop("disabled", false);
    $('#finish-time').removeClass('btn-default');
    $('#finish-time').addClass('btn-danger');
}
function resetAllButtons() {
    $('#start-time').removeClass('btn-success').removeClass('btn-warning').addClass('btn-default').text(' START ').prop("disabled", true);
    $('#finish-time').removeClass('btn-danger').addClass('btn-default').prop("disabled", true);
}
function resetAllForm() {
    $('[type="text"]').val('');
    $('[type="number"]').val('');
    // $('[name="position"]').val('0');
    $('#status_status').text('WAITING').prop("disabled", true);
    $('#actual-time').text(toDDHHMMSS(0));
}
function enableAllForm() {
    $('[type="text"]').prop("disabled", false);
    $('[type="number"]').prop("disabled", false);
}
function disableAllForm() {
    $('[type="text"]').prop("disabled", true);
    $('[type="number"]').prop("disabled", true);
    $('[name="position"]').prop("disabled", false);
    $('[name="remark"]').prop("disabled", false);
}
function clearPlanningTable() {
    $('#project-planning-table tbody').empty();
    $('#selected-item').empty();
}
function clearActualingTable() {
    $('#project-actualing-table tbody').empty();
}
function finishButtonsTpl() {
    return $('#btn-tpl').html().replace('name="position"', 'name="position" id="position"').replace('name="remark"', 'name="remark" id="remark"');
}

function toDDHHMMSS(seconds) {
    var days    = Math.floor(seconds / (24 * 3600));
    var hours   = Math.floor((seconds - (days * (24 * 3600))) / 3600);
    var minutes = Math.floor((seconds - (hours * 3600)) / 60);
    var seconds = seconds - (hours * 3600) - (minutes * 60);

    if (days    < 10) {days    = "0" + days;}
    if (hours   < 10) {hours   = "0" + hours;}
    if (minutes < 10) {minutes = "0" + minutes;}
    if (seconds < 10) {seconds = "0" + seconds;}

    return days+':'+hours+':'+minutes+':'+seconds;
}

function getActualingData(post_data) {
    $('#project-actualing-table tbody').empty();
    $.post('{{{ route("actualing.planning.data", $type) }}}', post_data, function(resp){
        var resp = $.parseJSON(resp);
        if (resp.length > 0) {
            $.each(resp, function (i, data) {
                var row_content = "<tr onclick=\"prepareForActualing($(this),"+data.id+");\" style=\"cursor:pointer\">"+
                                                  "<td class=\"text-center\" width=\"5%\">"+(i+1)+"</td>"+
                                                  "<td class=\"text-center\">"+data.nop+"</td>"+
                                                  "<td class=\"text-center\">"+data.barcode+"</td>"+
                                                  "<td class=\"text-center\">"+data.material_name+"</td>"+
                                                  "<td class=\"text-center\">"+data.process+"</td>"+
                                                  "<td class=\"text-center\">"+data.process_code+"</td>"+
                                                  "<td class=\"text-center\">"+(data.position>0?data.position:1)+"</td>"+
                                                  "<td class=\"text-center\">"+(data.qty>0?data.qty:1)+"</td>"+
                                                  "<td class=\"text-center\">"+data.resource+"</td>"+
                                                  "</tr>";
                $('#project-planning-table tbody').append(row_content);
                if(i==0) getQuantity(null, data.id);
            });
            $('#project-planning-table tbody tr:eq(0)').click();
        }
        else
        {                
            resetAllButtons();
            if(post_data.barcode && post_data.process_code) 
                $('#process-error-message').text('Planning data is not found!');
            else
                $('#nop-error-message').text('Planning data is not found!');
        }
    });
}
function getQuantity(actualing_id, planning_id) {
    $('#qty').empty();
    $.post('{{{ route('actualing.quantity') }}}', {actualing_id:actualing_id,planning_id:planning_id,type:"{{{$type}}}"}, function(resp){
        $.each($.parseJSON(resp),function(i,item){
            $('#qty').append('<option value="'+item.id+'">Qty '+item.qty_num+'</option>');
        });
    });
}
(function(){
    resetAllButtons();
    $('[name="part_code"]').focus();
    $('[name="part_code"]').keypress(function(e){
        resetAllButtons();
        $('#project-planning-table tbody').empty();
        $('#process-error-message').text('');
        $('#selected-item').empty();
        var key = e.which;
        if(key == 13)
        {
            $('#part_code-error-message').text('');
            if($(this).val() == '')
            {
                $('#part_code-error-message').text('Barcode field is required!');
                return;
            }

            getActualingData({barcode: $(this).val()});
            $('[name="process"]').focus();
            $('[name="barcode"]').focus();
        }
    });
    $('[name="process"]').keypress(function(e){
        $('#selected-item').empty();
        var key = e.which;
        if(key == 13)
        {
            $('#process-error-message').text('');
            if($(this).val() == '')
            {
                $('#process-error-message').text('Process Code field is required!');
                return;
            }

            $('#project-planning-table tbody').empty();
            $('[name="resource_code"]').focus();
            getActualingData({ barcode: $('[name="part_code"]').val(), process_code: $(this).val() });
            getLastState();
        }
    });
    $('[name="resource_code"]').keypress(function(e){
        var key = e.which;
        if(key == 13)
        {
            $('[name="pic_code"]').focus();
            $.post('{{{ route('data.resource') }}}', {code:$(this).val()}, function(data){
                var data = $.parseJSON(data);
                $('#resource_status').text(data.name);
                $('[name="resource"]').val(data.id);
            });
            
        }
    });
    $('[name="pic_code"]').keypress(function(e){
        var key = e.which;
        if(key == 13)
        {
            $('#qty').focus();
            $.post('{{{ route('data.pic') }}}', {code:$(this).val()}, function(data){
                var data = $.parseJSON(data);
                $('#pic_status').text(data.first_name+" "+data.last_name);
                $('[name="pic"]').val(data.id);
                enableStartButton();
            });
        }
    });
    $('#start-time').click(function(e){
        startActualing();
    });
    $('#finish-time').click(function(e){
        if ($(this).attr("disabled") != "disabled") {
			bootbox.dialog({
                title: "Finish Actualing",
                message: finishButtonsTpl()
            });
        }
    });

})();
</script>
@stop
