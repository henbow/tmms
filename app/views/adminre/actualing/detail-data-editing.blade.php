@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/selectize/css/selectize.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui-timepicker.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatables/css/jquery.datatables.min.css') }}}">
<style type="text/css">
.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }
</style>
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">

            </div>
        </div>

        <div class="row">

        <!-- the content -->
        @include($theme . '.actualing.module.' . $module)
        <!--/ the content -->

        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>

<!-- START modal-lg -->
<div id="bs-modal-lg" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<!--/ END modal-lg -->
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-timepicker.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-touch.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/xeditable/js/bootstrap-editable.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/typeahead/bootstrap3-typeahead.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/tabletools.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/zeroclipboard.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/element.js') }}}"></script>
<script type="text/javascript">
function openForm(elem) {
    console.log('Clicked');
    $('#bs-modal-lg .modal-content').load($(elem).attr('href'),function(e){
        $('#bs-modal-lg').modal('show');
    });
    return false;
}
(function(){
    var oCache = {
        iCacheLower: -1
    };
     
    function fnSetKey( aoData, sKey, mValue )
    {
        for ( var i=0, iLen=aoData.length ; i<iLen ; i++ )
        {
            if ( aoData[i].name == sKey )
            {
                aoData[i].value = mValue;
            }
        }
    }
     
    function fnGetKey( aoData, sKey )
    {
        for ( var i=0, iLen=aoData.length ; i<iLen ; i++ )
        {
            if ( aoData[i].name == sKey )
            {
                return aoData[i].value;
            }
        }
        return null;
    }
     
    function fnDataTablesPipeline ( sSource, aoData, fnCallback ) {
        var iPipe = 3; /* Ajust the pipe size */
         
        var bNeedServer = false;
        var sEcho = fnGetKey(aoData, "sEcho");
        var iRequestStart = fnGetKey(aoData, "iDisplayStart");
        var iRequestLength = fnGetKey(aoData, "iDisplayLength");
        var iRequestEnd = iRequestStart + iRequestLength;

        oCache.iDisplayStart = iRequestStart;
         
        /* outside pipeline? */
        if ( oCache.iCacheLower < 0 || iRequestStart < oCache.iCacheLower || iRequestEnd > oCache.iCacheUpper )
        {
            bNeedServer = true;
        }
         
        /* sorting etc changed? */
        if ( oCache.lastRequest && !bNeedServer )
        {
            for( var i=0, iLen=aoData.length ; i<iLen ; i++ )
            {
                if ( aoData[i].name != "iDisplayStart" && aoData[i].name != "iDisplayLength" && aoData[i].name != "sEcho" )
                {
                    if ( aoData[i].value != oCache.lastRequest[i].value )
                    {
                        bNeedServer = true;
                        break;
                    }
                }
            }
        }
         
        /* Store the request for checking next time around */
        oCache.lastRequest = aoData.slice();
         
        if ( bNeedServer )
        {
            if ( iRequestStart < oCache.iCacheLower )
            {
                iRequestStart = iRequestStart - (iRequestLength*(iPipe-1));
                if ( iRequestStart < 0 )
                {
                    iRequestStart = 0;
                }
            }
             
            oCache.iCacheLower = iRequestStart;
            oCache.iCacheUpper = iRequestStart + (iRequestLength * iPipe);
            oCache.iDisplayLength = fnGetKey( aoData, "iDisplayLength" );
            fnSetKey( aoData, "iDisplayStart", iRequestStart );
            fnSetKey( aoData, "iDisplayLength", iRequestLength*iPipe );
             
            $.post( sSource, aoData, function (json) {
                var json = $.parseJSON(json);

                /* Callback processing */
                oCache.lastJson = jQuery.extend(true, {}, json);
                 
                if ( oCache.iCacheLower != oCache.iDisplayStart )
                {
                    json.aaData.splice( 0, oCache.iDisplayStart-oCache.iCacheLower );
                }
                json.aaData.splice( oCache.iDisplayLength, json.aaData.length );
                 
                fnCallback(json)
            } );
        }
        else
        {
            json = jQuery.extend(true, {}, oCache.lastJson);
            json.sEcho = sEcho; /* Update the echo for each response */
            json.aaData.splice( 0, iRequestStart-oCache.iCacheLower );
            json.aaData.splice( iRequestLength, json.aaData.length );
            fnCallback(json);
            return;
        }
    }
    var table1 = $("#actualing-material").dataTable({
        "sDom": "<'row'<'col-sm-6'Tl><'col-sm-6'f>><'table-responsive'rt><'row'<'col-sm-6'p><'col-sm-6'i>>",
        "oTableTools": {
            "sSwfPath": "{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/swf/copy_csv_xls_pdf.swf') }}}",
            "aButtons": [
            "print",
            "pdf",
            "csv"
            ]
        },
        "aoColumns": [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false }
        ],
        "iDisplayLength": 25,
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "{{{ route('actualing.data.editing') }}}",
        "sServerMethod": "POST",
        "fnServerParams": function ( aoData ) {
            aoData.push({ "name": "type", "value": "material" });
        },
        "fnServerData": fnDataTablesPipeline
    });
    var table2 = $("#actualing-nop").dataTable({
        "sDom": "<'row'<'col-sm-6'Tl><'col-sm-6'f>><'table-responsive'rt><'row'<'col-sm-6'p><'col-sm-6'i>>",
        "oTableTools": {
            "sSwfPath": "{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/swf/copy_csv_xls_pdf.swf') }}}",
            "aButtons": [
            "print",
            "pdf",
            "csv"
            ]
        },
        "aoColumns": [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false },
            { "sClass": "text-center", "bSearchable": false, "bSortable": false }
        ],
        "iDisplayLength": 25,
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "{{{ route('actualing.data.editing') }}}",
        "sServerMethod": "POST",
        "fnServerParams": function ( aoData ) {
            aoData.push({ "name": "type", "value": "nop" });
        },
        "fnServerData": fnDataTablesPipeline
    });
})();
</script>
@stop
