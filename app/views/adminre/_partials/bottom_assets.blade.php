<!-- Library script : mandatory -->
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/library/jquery/js/jquery.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/library/jquery/js/jquery-migrate.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/library/bootstrap/js/bootstrap.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/library/core/js/core.min.js') }}}"></script>
<!--/ Library script -->

<!-- App and page level script -->
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script><!-- will be use globaly as a summary on sidebar menu -->
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/app.min.js') }}}"></script>


<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/flot/jquery.flot.min.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/flot/jquery.flot.categories.min.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/flot/jquery.flot.tooltip.min.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/flot/jquery.flot.resize.min.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/flot/jquery.flot.spline.min.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/owl/js/owl.carousel.min.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jvectormap/js/jvectormap.min.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/maps/vector-world-mill.js') }}}"></script>

<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/pages/dashboard-v2.js') }}}"></script>

<!--/ App and page level script -->