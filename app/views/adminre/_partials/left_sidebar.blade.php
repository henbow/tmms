<ul class="topmenu topmenu-responsive" data-toggle="menu">
    @foreach(Menu::where('parent_id', '=', '0')->orderBy('order')->get() as $parent_menu)
    <!-- {{{ $parent_menu->menu_name }}} -->
    <?php $user_info = \Sentry::getUser(); ?>
    @if($user_info->hasAccess("menu-".$parent_menu->id))
    <?php $child_menu = Menu::where('parent_id', '=', $parent_menu->id)->orderBy('order'); ?>
    <li <?php echo ( $parent_active == $parent_menu->alias ? 'class="active open"' : '' ) ?>>
        <a href="{{{ $parent_menu->route == '#' ? 'javascript:void(0)' : route($parent_menu->route) }}}" <?php echo ($child_menu->count() > 0) ? 'data-target="#'.strtolower(str_replace(' ', '-', $parent_menu->menu_name)).'" data-toggle="submenu" data-parent=".topmenu"' : ''; ?>>
            <span class="figure"><i class="{{{ $parent_menu->class_attr }}}"></i></span>
            <span class="text">{{{ $parent_menu->menu_name }}}</span>
            <span class="arrow"></span>
        </a>
        @if($child_menu->count() > 0)
        <ul id="{{{ strtolower(str_replace(' ', '-', $parent_menu->menu_name)) }}}" class="submenu collapse {{{ ( $parent_active == $parent_menu->alias ? 'in' : '' ) }}}">
            <li class="submenu-header ellipsis">{{{ $parent_menu->menu_name }}}</li>
            @foreach($child_menu->get() as $child)
                @if($user_info->hasAccess("menu-".$child->id) OR $child->menu_name == "[LINE]")
                    @if($child->menu_name == "[LINE]")
                    <li><hr style="margin:0;padding:0;"></li>
                    @else
                    <?php $child_menu_2 = Menu::where('parent_id', '=', $child->id)->orderBy('order'); ?>
                    <?php $param1 = explode(',',$child->param); ?>
                    <li class="{{{ ( $child_active == $child->alias ? 'active' : '' ) }}}">
                        <a
                            href="{{{ $child->route == '#' ? 'javascript:void(0)' : (count($param1) > 0 ? route($child->route, $param1) : route($child->route)) }}}"
                            <?php echo $child_menu_2->count() > 0 ? 'data-toggle="submenu" data-target="#'.$child->alias.'"' : ''; ?>>
                            <span class="text">{{{ $child->menu_name }}}</span>
                            @if($child_menu_2->count() > 0) <span class="arrow"></span> @endif
                        </a>

                        @if($child_menu_2->count() > 0)
                        <ul id="{{{ $child->alias }}}" class="submenu collapse ">
                            @foreach($child_menu_2->get() as $child_2)
                                @if($user_info->hasAccess("menu-".$child_2->id) OR $child_2->menu_name == "[LINE]")
                                    @if($child_2->menu_name == "[LINE]")
                                    <li><hr style="margin:0;padding:0;"></li>
                                    @else
                                    <?php $param2 = explode(',',$child_2->param); ?>
                                    <li class="{{{ ( $child_active == $child_2->alias ? 'active' : '' ) }}}">
                                        <a href="{{{ $child_2->route == '#' ? 'javascript:void(0)' : (count($param2) > 0 ? route($child_2->route, $param2) : route($child_2->route)) }}}">
                                            <span class="text">{{{ $child_2->menu_name }}}</span>
                                        </a>
                                    </li>
                                    @endif
                                @endif
                            @endforeach
                        </ul>
                        @endif
                    </li>
                    @endif
                @endif
            @endforeach
        </ul>
        @endif
    </li>
    @endif
    @endforeach
</ul>
