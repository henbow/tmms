@extends($theme.'._layouts.master')


@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <!-- <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="#">Table</a></li>
                        <li class="active">Default</li>
                    </ol>
                </div> -->
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->
        
        <div class="row">
            <div class="col-md-12">
                {{ Notification::showAll() }}

                <!-- START panel -->
                <div class="panel panel-primary">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>{{{ $page_title }}}</h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <!--/ panel heading/header -->
                    
                    <!-- panel toolbar wrapper -->
                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <div class="panel-toolbar text-right">
                            <a href="{{{ route('process_group.create') }}}" class="btn btn-success mb5"><i class="ico-user22"></i> Add New Process Group</a>
                        </div>
                    </div>
                    <!--/ panel toolbar wrapper -->

                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-bordered table-hover" id="table1">
                            <thead>
                                <tr>
                                    <th class="text-center" width="5%">No</th>
                                    <th>Group</th>
                                    <th class="text-center">Type Process</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                @foreach ($groups as $group)
                                <tr>
                                    <td class="text-center">{{{ $no++ }}}</td>
                                    <td>{{{ $group->group }}}</td>
                                    <td class="text-center">{{{ strtoupper($group->type) }}}</td>
                                    <td width="10%" class="text-center">
                                        <!-- button toolbar -->
                                        <div class="toolbar">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-default">Action</button>
                                                <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="{{{ route('process_group.edit', $group->id) }}}"><i class="icon ico-pencil"></i>Update</a></li>
                                                    <li class="divider"></li>
                                                    <li>                                                        
                                                        <a href="#" data-id="{{{ $group->id }}}" class="delete-confirm text-danger"><i class="icon ico-remove3"></i>Delete</a>
                                                        {{ Form::open(array('route' => array('process_group.destroy', $group->id), 'method' => 'delete')) }}
                                                        <button type="submit" id="del-{{{ $group->id }}}" style="display:none"></button>
                                                        {{ Form::close() }}                                              
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--/ button toolbar -->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript">
(function(){
    $(".delete-confirm").on("click", function (event) {
        var self = $(this);
        bootbox.confirm("Are you sure to delete this process group?", function (result) {
            if(result){
                var groupid = self.attr('data-id');
                $('button#del-'+groupid).click();
            }
        });
        event.preventDefault();
    });
})();
</script>
<!-- <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/components/notification.js') }}}"></script> -->
@stop

