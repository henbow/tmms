@extends($theme.'._layouts.master')

@section('main')
<section id="main" role="main">
    {{ Notification::showAll() }}

    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Form::model($group, array('method' => 'put', 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate', 'route' => array('process_group.update', $group->id))) }}
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Group Name</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="group_name" id="process" value="{{{ $group->group }}}" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Type</label>
                            <div class="col-sm-6">
                                <select name="group_type" class="form-control">
                                    <option {{{ $group->type == 'nop' ? 'selected' : '' }}} value="nop">NOP</option>
                                    <option {{{ $group->type == 'material' ? 'selected' : '' }}} value="material">MATERIAL</option>
                                    <option {{{ $group->type == 'pb-bom' ? 'selected' : '' }}} value="pb-bom">PB-BOM</option>
                                </select>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-success">Update</button>
                            <button type="reset" class="btn btn-reset">Reset</button>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/parsley/js/parsley.min.js') }}}"></script>        
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/validation.js') }}}"></script>
@stop

