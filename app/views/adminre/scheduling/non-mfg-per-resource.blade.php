<table class="table table-striped table-bordered table-hover" id="schedule-per-resource-table" style="font-size: 11px">
    <thead>
        <tr>
            <!-- <th class="text-center">No</th> -->
            <th class="text-center">NOP</th>
            <th class="text-center">Project</th>
            <th class="text-center">Process</th>
            <th class="text-center">Est. (Hours)</th>
            <th class="text-center">Resource Group</th>
            <th class="text-center">Resource</th>
            <th class="text-center">Priority</th>
            <th class="text-center">Start Date</th>
            <th class="text-center">Finish Date</th>
        </tr>
    </thead>
    <tbody style="font-size: 11px"></tbody>
</table>