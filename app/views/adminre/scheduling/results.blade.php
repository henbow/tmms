@extends($theme.'._layouts.master')

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}} Results for {{{ strtoupper($resource->name) }}}</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="{{{ route('scheduling.index') }}}">Scheduling</a></li>
                        <li class="active">Results</li>
                    </ol>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->
        
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <div class="panel-toolbar text-right">
                            <a href="{{{ route('scheduling.gantt.open', array($resource->id, $step)) }}}" id="open-gantt-chart" class="btn btn-success mb5" style="font-weight: bold;"><i class="ico-chart"></i> Open Gantt Chart </a>
                        </div>
                    </div>

                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        @include($theme . '.scheduling.result.' . $module)
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>        
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->
    
    <!-- START modal-lg -->
    <div id="bs-modal-lg" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content"></div>
        </div>
    </div>
    <!--/ END modal-lg -->
</section>
@stop

@section('additional_scripts')
<script type="text/javascript">
(function(){
    $('#open-gantt-chart').click(function(e){
        e.preventDefault();
        window.open($(this).attr('href'), 'Scheduling results in Gantt Chart', 'fullscreen=1,location=0,menubar=0,status=0,toolbar=0');
        // $('#bs-modal-lg .modal-content').load($(this).attr('href'),function(e){
        //     $('#bs-modal-lg').modal('show');
        // });
    });
})();
</script>
@stop