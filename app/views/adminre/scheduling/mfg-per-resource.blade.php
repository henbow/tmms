<table class="table table-striped table-bordered table-hover" id="schedule-per-resource-table">
    <thead>
        <tr>
            <th class="text-center">Barcode</th>
            <th class="text-center">Material Name</th>
            <th class="text-center">Process</th>
            <th class="text-center">Qty</th>
            <th class="text-center" width="8%">Est. (Hours)</th>
            <th class="text-center">Resource Group</th>
            <th class="text-center">Resource</th>
            <th class="text-center" width="8%">Priority</th>
            <th class="text-center" width="10%">Start Date</th>
            <th class="text-center" width="10%">Finish Date</th>
        </tr>
    </thead>
    <tbody style="font-size: 11px"></tbody>
</table>