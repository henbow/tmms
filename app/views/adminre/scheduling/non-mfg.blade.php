<table class="table table-striped table-bordered table-hover" id="schedule-table" style="font-size: 11px">
    <thead>
        <tr>
            <!-- <th class="text-center">No</th> -->
            <th class="text-center">NOP</th>
            <th class="text-center">Project</th>
            <th class="text-center">Process</th>
            <th class="text-center">Est. (Hours)</th>
            <th class="text-center">Resource Group</th>
            <th class="text-center">Resource</th>
            <th class="text-center">Priority</th>
            <th class="text-center">Start Date</th>
            <th class="text-center">Finish Date</th>
            <th class="text-center" width="5%"><input type="checkbox" name="select_all" class="select_all" value="all" ></th>
        </tr>
    </thead>
    <tbody>
        <?php $old_priorities = array(); ?>
        @foreach($schedules as $key => $planning)
        <?php if($planning->schedule_id) { $old_priorities[] = $planning->schedule_id.":".$planning->priority; } ?>
        <?php  ?>
        <tr data-key="{{{ $key }}}" data-priority="{{{ $planning->priority }}}">
            <td class="text-center">{{{ $planning->nop }}}</td>
            <td class="text-left">{{{ $planning->project }}}</td>
            <td class="text-center">{{{ $planning->process }}}</td>
            <td class="text-center">{{{ $planning->estimation }}}</td>
            <td class="text-center"><a href="{{{ route('project.planning.detail.edit', array($planning->master_planning_id, $planning->planning_id)) }}}?is_planning=0" class="edit-process">{{{ $planning->resource_group }}}</a></td>
            <td class="text-center">{{{ $planning->resource }}}</td>
            <td class="text-center"><a href="#" data-pk="{{{ $planning->schedule_id }}}" data-name="nop" class="priority-number">{{{ $planning->priority }}}</a></td>
            <td class="text-center">{{{ $planning->start_date ? date('d M Y H:i:s', strtotime($planning->start_date)) : '' }}}</td>
            <td class="text-center">{{{ $planning->finish_date ? date('d M Y H:i:s', strtotime($planning->finish_date)) : '' }}}</td>
            <td class="text-center"><input type="checkbox" name="plan_id[]" class="plan_id" data-priority="{{{ $planning->schedule_id.":".$planning->priority }}}" value="{{{ $planning->planning_id }}}" ></td>
        </tr>
        @endforeach
    </tbody>
</table>
<input type="hidden" name="old_priority_order" value="{{{ implode(',',$old_priorities) }}}" />