<table class="table table-striped table-bordered table-hover" id="ajax-source">
    <thead>
        <tr>
            <th class="text-center">Material Code</th>
            <th class="text-center">Material Name</th>
            <th class="text-center">Dimension</th>
            <th class="text-center">Code</th>
            <th class="text-center">Process</th>
            <th class="text-center">Est. (Days)</th>
            <th class="text-center">Est. (Hours)</th>
            <th class="text-center">Resource</th>
            <th class="text-center">Scheduled Start</th>
            <th class="text-center">Scheduled Finish</th>
        </tr>
    </thead>
    <tbody style="font-size: 11px">
    @foreach($schedule_data as $schedule)
        <?php $material = ProjectBOM::find($schedule->material_id); ?>
        <?php $process = Process::find($schedule->process_id); ?>
        <?php $resource = Resource::find($schedule->resource_id); ?>
        <tr>
            <td class="text-center">{{{ $material->barcode }}}</td>
            <td class="text-center">{{{ $material->material_name }}}</td>
            <td class="text-center">{{{ "L: {$material->length}, W: {$material->width}, H: {$material->height}" }}}</td>
            <td class="text-center">{{{ $process->code }}}</td>
            <td class="text-center">{{{ $process->process }}}</td>
            <td class="text-center">{{{ floor($schedule->estimation_hour/24) }}}</td>
            <td class="text-center">{{{ $schedule->estimation_hour }}}</td>
            <td class="text-center"><strong>[{{{ $resource->code }}}]</strong> {{{ $resource->name }}}</td>
            <td class="text-center">{{{ date('d-M-Y H:i:s', strtotime($schedule->start_date)) }}}</td>
            <td class="text-center">{{{ date('d-M-Y H:i:s', strtotime($schedule->finish_date)) }}}</td>
        </tr>
    @endforeach
    </tbody>
</table>