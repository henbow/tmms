<table class="table table-striped table-bordered table-hover" id="ajax-source">
    <thead>
        <tr>
            <th class="text-center">Material Code</th>
            <th class="text-center">Material Name</th>
            <th class="text-center">Flag</th>
            <th class="text-center">PO Number</th>
            <th class="text-center">Qty</th>
            <th class="text-center">Est.</th>
            <th class="text-center">Resource</th>
            <th class="text-center" width="12%">Scheduled Start</th>
            <th class="text-center" width="12%">Scheduled Finish</th>
        </tr>
    </thead>
    <tbody style="font-size: 11px">
    @foreach($schedule_data as $schedule)
        <?php $material = ProjectBOM::find($schedule->material_id); ?>
        <?php $process = Process::find($schedule->process_id); ?>
        <?php $resource = Resource::find($schedule->resource_id); ?>
        <tr>
            <td class="text-center">{{{ @$material->part_code }}}</td>
            <td class="text-center">{{{ @$material->material_name }}}</td>
            <td class="text-left">{{{ @$schedule->flag_pb }}}</td>
            <td class="text-center">{{{ @$material->po_number }}}</td>
            <td class="text-center">{{{ @$material->qty }}}</td>
            <td class="text-center">{{{ @$schedule->estimation_hour }}}</td>
            <td class="text-center">{{{ $resource->name }}}</td>
            <td class="text-center">{{{ date('d-M-Y', strtotime($schedule->start_date)) }}}</td>
            <td class="text-center">{{{ date('d-M-Y', strtotime($schedule->finish_date)) }}}</td>
        </tr>
    @endforeach
    </tbody>
</table>