<table class="table table-striped table-bordered table-hover" id="schedule-table">
    <thead>
        <tr>
            <th class="text-center">Barcode</th>
            <th class="text-center">Material Name</th>
            <th class="text-center">Process</th>
            <th class="text-center">Qty</th>
            <th class="text-center" width="8%">Est. (Hours)</th>
            <th class="text-center">Resource Group</th>
            <th class="text-center">Resource</th>
            <th class="text-center" width="8%">Priority</th>
            <th class="text-center" width="10%">Start Date</th>
            <th class="text-center" width="10%">Finish Date</th>
            <th class="text-center" width="5%"><input type="checkbox" name="select_all" class="select_all" value="all" ></th>
        </tr>
    </thead>
    <tbody style="font-size: 11px">
        <?php $old_priorities = array(); ?>
        @foreach($schedules as $key => $planning)
        <?php if($planning->schedule_id) { $old_priorities[] = $planning->schedule_id.":".$planning->priority; } ?>
        <tr data-key="{{{ $key }}}" data-priority="{{{ $planning->priority }}}">
            <td class="text-center">{{{ $planning->barcode }}}</td>
            <td class="text-left">{{{ $planning->material }}}</td>
            <td class="text-center">{{{ $planning->process }}}</td>
            <td class="text-center">{{{ $planning->qty }}}</td>
            <td class="text-center">{{{ $planning->estimation }}}</td>
            <td class="text-center"><a href="{{{ route('project.planning.detail.edit_mfg', $planning->planning_id) }}}?is_planning=0" class="edit-process">{{{ $planning->resource_group }}}</a></td>
            <td class="text-center">{{{ $planning->resource }}}</td>
            <td class="text-center"><a href="#" data-pk="{{{ $planning->schedule_id }}}" data-name="material" class="priority-number">{{{ $planning->priority }}}</a></td>
            <td class="text-center">{{{ $planning->start_date ? date('d M Y H:i:s', strtotime($planning->start_date)) : '' }}}</td>
            <td class="text-center">{{{ $planning->finish_date ? date('d M Y H:i:s', strtotime($planning->finish_date)) : '' }}}</td>
            <td class="text-center"><input type="checkbox" name="plan_id[]" class="plan_id" value="{{{ $planning->planning_id }}}" ></td>
        </tr>
        @endforeach
    </tbody>
</table>
<input type="hidden" name="old_priority_order" value="{{{ implode(',',$old_priorities) }}}" />