<table class="table table-striped table-bordered table-hover" id="ajax-source">
    <thead>
        <tr>
            <th class="text-center">Material Code</th>
            <th class="text-center">Material Name</th>
            <th class="text-center">Flag</th>
            <th class="text-center">PO Number</th>
            <th class="text-center">Qty</th>
            <th class="text-center">Est.</th>
            <th class="text-center">Resource</th>
            <th class="text-center" width="5%"><input type="checkbox" name="select_all" class="select_all" value="all" ></th>
        </tr>
    </thead>
    <tbody style="font-size: 11px">
    @foreach($planning_data as $planning)
        <?php $priority = isset($planning->priority) ? $planning->priority : 0; ?>
        <?php $material = ProjectBOM::find($planning->material_id); ?>
        <?php $resource = Resource::find($planning->resource_id); ?>
        <tr>
            <td class="text-center">{{{ @$material->part_code }}}</td>
            <td class="text-center">{{{ @$material->material_name }}}</td>
            <td class="text-left">{{{ @$planning->flag_pb }}}</td>
            <td class="text-center">{{{ @$planning->po_number }}}</td>
            <td class="text-center">{{{ @$material->qty }}}</td>
            <td class="text-center">{{{ @$planning->estimation }}}</td>
            <td class="text-center">{{{ $resource->name }}}</td>
            <td class="text-center"><input type="checkbox" name="plan_id[]" class="plan_id" value="{{{ @$planning->id }}}" ></td>
        </tr>
    @endforeach
    </tbody>
</table>