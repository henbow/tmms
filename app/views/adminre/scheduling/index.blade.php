@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/selectize/css/selectize.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui-timepicker.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatables/css/jquery.datatables.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<style type="text/css">
.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }
</style>
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section"></div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">

                    <div class="table-responsive panel-collapse pull out">
                        {{ Form::open(array('route' => 'scheduling.index', 'class' => 'form-horizontal', 'id' => 'filter-planning', 'data-parsley-validate')) }}
                        <br>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Plan Type</label>
                            <div class="col-sm-3">
                                <select name="plan" class="form-control" required>
                                    <option {{{ isset($plan) ? $plan == 'IN PLAN' ? 'selected' : '' : '' }}}>IN PLAN</option>
                                    <!-- <option {{{ isset($plan) ? $plan == 'OUT PLAN' ? 'selected' : '' : '' }}}>OUT PLAN</option> -->
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Master Planning</label>
                            <div class="col-sm-3">
                                <select name="step" class="form-control" required>
                                    @foreach($process_groups as $process_group)
                                    <option {{{ isset($step) ? $step == $process_group->id ? 'selected' : '' : '' }}} value="{{{ $process_group->id }}}">
                                        {{{ $process_group->parent_id ? "&nbsp;&#8627;&nbsp;" : '' }}}{{{ $process_group->group }}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Resource Group</label>
                            <div class="col-sm-3">
                                <select name="resource_group" class="form-control">
                                    <option></option>
                                    @if(isset($active_resource_groups))
                                    @foreach($active_resource_groups as $resource_group)
                                    <option {{{ isset($selected_resource_group) ? $selected_resource_group == $resource_group->id ? 'selected' : '' : '' }}} value="{{{ $resource_group->id }}}">
                                        {{{ $resource_group->name }}}
                                    </option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Start Date</label>
                            <div class="col-sm-2">
                                <input type="text" name="start" id="datetime-picker" value="{{{ date('m/d/Y H:i', strtotime($start_date) < time() ? time() : strtotime($start_date)) }}}" class="form-control">
                            </div>
                            <!-- <div class="col-sm-1" style="margin:0;padding: 0;">
                                <input type="checkbox" value="1" name="is_manual" />&nbsp;<label class="control-label" for="is_manual">Manual</label>
                            </div> -->
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-3">
                                <button type="submit" id="search" class="btn btn-success">Filter Data</button>
                            </div>
                        </div>
                        <input type="hidden" name="process" value="{{{ isset($process_id) ? $process_id : '' }}}" />
                        {{ Form::close() }}
                    </div>
                </div>

                {{ Notification::showAll() }}

                <!-- START panel -->
                <div class="col-md-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#all" data-toggle="tab">All</a></li>
                        <li><a href="#per_resource" data-toggle="tab">Per Resource</a></li>
                    </ul>

                    <div class="tab-content panel">
                        <div class="tab-pane active" id="all">
                            {{ Form::open(array('route' => 'scheduling.process', 'id' => 'process-schedules', 'class' => 'form-horizontal')) }}
                            <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                                <div class="panel-toolbar text-right">
                                    <div class="col-sm-3">
                                        <select name="select_process" class="form-control">
                                            <option value="">Filter By Process</option>
                                            @foreach($processes as $process)
                                            <option {{{ isset($process_id) ? $process_id == $process->id ? 'selected' : '' : '' }}} value="{{{ $process->id }}}"><strong>{{{ $process->process }}}</strong></option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <?php $current_resource_group = isset($selected_resource_group) ? $selected_resource_group ? $selected_resource_group : '1' : '1'; ?>
                                    @if(@$selected_resource_group)
                                    <a href="{{{ route('scheduling.gantt.open', array(@$selected_resource_group, $step)) }}}" id="open-gantt-chart" class="btn btn-success mb5" style="font-weight: bold;" target="_blank">GANTT CHART</a>
                                    @endif
                                    <button type="button" id="refresh_data" class="btn btn-success mb5" style="font-weight: bold;">REFRESH DATA</button>
                                    <button type="submit" id="process_schedule" class="btn btn-primary mb5" style="font-weight: bold;">PROCESS SCHEDULE</button>
                                    <button type="submit" id="update_schedule" class="btn btn-danger mb5" style="font-weight: bold;">UPDATE SCHEDULE</button>

                                    <input type="hidden" name="step" value="{{{ $process_group_id }}}" />
                                    <input type="hidden" name="start_date" value="{{{ $start_date ? $start_date : date('Y-m-d H:i:s') }}}" />
                                    <input type="hidden" name="resource_group_id" value="{{{ $current_resource_group }}}" />
                                    <input type="hidden" name="use_priority_order" value="0" />
                                </div>
                            </div>

                            <!-- panel body with collapse capabale -->
                            <div class="table-responsive panel-collapse pull out">
                                @include($theme . '.scheduling.' . $template_step)
                            </div>
                            <!--/ panel body with collapse capabale -->
                            {{ Form::close() }}
                        </div>

                        <div class="tab-pane" id="per_resource">
                            <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                                <div class="panel-toolbar text-right">
                                    <div class="col-sm-3">
                                        <select name="select_resource" class="form-control">
                                            @foreach($selected_resources as $sr)
                                            <option value="{{{ $sr->id }}}">{{{ $sr->name }}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- panel body with collapse capabale -->
                            <div class="table-responsive panel-collapse pull out">
                                @include($theme . '.scheduling.' . $template_step . "-per-resource")
                            </div>
                            <!--/ panel body with collapse capabale -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
<!-- START modal-lg -->
<div id="bs-modal-lg" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<!--/ END modal-lg -->
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-timepicker.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-touch.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/xeditable/js/bootstrap-editable.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/typeahead/bootstrap3-typeahead.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/tabletools.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/zeroclipboard.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/element.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript">
function doSubmit(elem) {
    $.post($(elem).attr('action'),$(elem).serialize(),function(){
        window.location.reload();
    });
}
(function(){
    $('[name="is_manual"]').click(function(){
        if($(this).is(':checked'))
            $('[name="start"]').prop('disabled', false);
        else
            $('[name="start"]').prop('disabled', true);
    });
    var priorities = [];
    $('.priority-number').editable({
        url: "{{{ route('scheduling.edit_priority') }}}",
        params: function(param){
            param.old_priority_order = $('[name="old_priority_order"]').val();
            param.start_date = $('[name="start"]').val();
            return param;
        },
        title: "Edit Priority"
    });
    $('.priority-number').on('save', function(e, params){
        $('form#filter-planning').submit();
    });
    $('button#refresh_data').click(function(){
        NProgress.start();
        $('form#filter-planning').submit();
    });
    $('[name="step"]').change(function(){
        $('[name="process"]').empty();
        $('[name="resource"]').empty();
        $('[name="process"]').html('<option></option>');
        $.post('{{{ route('scheduling.data.process') }}}', {group_id:$(this).val()}, function(resp){
            var processes = $.parseJSON(resp);
            $.each(processes, function (i, process) {
                $('[name="process"]').append('<option value="'+process.id+'">'+process.process+'</option>');
            });
        });
    });
    $('[name="select_process"]').change(function(){
        $('[name="process"]').val($(this).val());
        $('form#filter-planning').submit();
    });
    $("#start").datepicker({
        changeMonth: true,
        changeYear: true
    });
    $('.select_all').click(function(){
        if ($(this).attr('checked')) {
            $('.plan_id').attr('checked', 'checked');
        } else {
            $('.plan_id').removeAttr('checked');
        }
    });
    $('.edit-process').click(function(e){
        e.preventDefault();
        var editUrl = $(this).attr('href');
        $('#bs-modal-lg .modal-content').load(editUrl,function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $('#update_schedule').click(function(e){
        e.preventDefault();
        $('[name="use_priority_order"]').val('1');
        $('form#process-schedules').submit();
    });
    $('#process_schedule').click(function(e){
        e.preventDefault();
        $('[name="use_priority_order"]').val('0');
        $('form#process-schedules').submit();
    });
    get_per_resource_data('{{{ @$selected_resources[0]->id }}}','{{{ $template_step == 'mfg' ?'material' : 'nop' }}}');
    $('[name="select_resource"]').change(function(e){
        var resource_id = $(this).val();
        get_per_resource_data(resource_id, '{{{ $template_step == 'mfg' ?'material' : 'nop' }}}');
    });
    function get_per_resource_data(resource_id, type) {
        $('#schedule-per-resource-table tbody').empty();
        $.post('{{{ route("scheduling.data.per_resources") }}}', {resource_id:resource_id,type:type}, function(resp){
            var resp = $.parseJSON(resp);
            if(type == 'material') {
                $.each(resp, function(i,item){
                    var row = '<tr>';
                    row += '<td class="text-center">'+item.barcode+'</td>';
                    row += '<td class="text-left">'+item.material+'</td>';
                    row += '<td class="text-center">'+item.process+'</td>';
                    row += '<td class="text-center">'+item.qty+'</td>';
                    row += '<td class="text-center">'+item.estimation+'</td>';
                    row += '<td class="text-center">'+item.resource_group+'</td>';
                    row += '<td class="text-center">'+item.resource+'</td>';
                    row += '<td class="text-center">'+item.priority+'</td>';
                    row += '<td class="text-center">'+item.start_date+'</td>';
                    row += '<td class="text-center">'+item.finish_date+'</td>';
                    row += '</tr>';
                    console.log(row);
                    $('#schedule-per-resource-table tbody').append(row);
                });
            } else {
                $.each(resp, function(i,item){
                    var row = '<tr>';
                    row += '<td class="text-center">'+item.nop+'</td>';
                    row += '<td class="text-left">'+item.project+'</td>';
                    row += '<td class="text-center">'+item.process+'</td>';
                    row += '<td class="text-center">'+item.estimation+'</td>';
                    row += '<td class="text-center">'+item.resource_group+'</td>';
                    row += '<td class="text-center">'+item.resource+'</td>';
                    row += '<td class="text-center">'+item.priority+'</td>';
                    row += '<td class="text-center">'+item.start_date+'</td>';
                    row += '<td class="text-center">'+item.finish_date+'</td>';
                    row += '</tr>';
                    $('#schedule-per-resource-table tbody').append(row);
                });
            }
        });
    }
})();
</script>
@stop
