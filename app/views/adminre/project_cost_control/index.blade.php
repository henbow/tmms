@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatables/css/jquery.datatables.min.css') }}}">
<style type="text/css">
.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }
</style>
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
            <!-- Toolbar -->

            <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Notification::showAll() }}
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>{{{ $sub_title }}}</h3>
                    </div>
                    <!--/ panel heading/header -->

                    <!-- panel body with collapse capabale -->
                    <table class="table table-striped table-bordered table-hover" id="project-list">
                        <thead>
                            <tr>
                                <th class="text-center" rowspan="2" width="1%">No</th>
                                <th class="text-center" rowspan="2" width="10%">NOP</th>
                                <th class="text-center" rowspan="2" width="5%">Order</th>
                                <th class="text-center" rowspan="2">Project</th>
                                <th class="text-center" rowspan="2">Customer</th>
                                <th class="text-center" rowspan="2">Qty</th>
                                <th class="text-center" rowspan="2">Price</th>
                                <th class="text-center" colspan="3">Budget</th>
                                <th class="text-center" colspan="3">Actual</th>
                                <th class="text-center" rowspan="2" width="5%">Action</th>
                            </tr>
                            <tr>
                                <th class="text-center">PB-BOM</th>
                                <th class="text-center">Process</th>
                                <th class="text-center">Overhead+Provit</th>
                                <th class="text-center">PB-BOM</th>
                                <th class="text-center">Process</th>
                                <th class="text-center">Overhead+Provit</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 10px">
                        <?php $no = $start_page+1; ?>
                        @foreach($projects as $project)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                                <td class="text-center">{{ $project->nop }}</td>
                                <td class="text-center">{{ $project->order_num }}</td>
                                <td class="text-left">{{ $project->project_name }}</td>
                                <td class="text-left">{{ $project->customer_name }}</td>
                                <td class="text-center">{{ $project->qty }}</td>
                                <td class="text-right">{{ number_format($project->price_total, 0, ',', '.') }}</td>
                                <td class="text-center"></td>
                                <td class="text-center"></td>
                                <td class="text-center"></td>
                                <td class="text-center"></td>
                                <td class="text-center"></td>
                                <td class="text-center"></td>
                                <td class="text-center"></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
        <div class="col-md-12 text-center"><?php echo $projects->links(); ?></div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>

<!-- START modal-lg -->
<div id="bs-modal-lg" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<!--/ END modal-lg -->
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/tabletools.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/zeroclipboard.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/gritter/js/jquery.gritter.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript">
(function(){
    $('button.action-btn').click(function(e){
        e.preventDefault();
        if($(this).attr('id') == 'update-btn') {
            $('[name="action-mode"]').val('update');
        } else {
            $('[name="action-mode"]').val('filter');
        }
        $(this).parents('form').submit();
    });
})();
function do_redirect(url)
{
    window.location.href = url;
}
function do_planning(elem)
{
    var self = $(elem);
    bootbox.dialog({
        message: "Link Start/Finish date from?",
        title: "Start/Finish Date Linking",
        buttons: {
            trial: {
                label: "Trial",
                className: "btn-success",
                callback: function () {
                    NProgress.start();
                    window.location.href= self.attr('data-trial-url');
                }
            },
            delivery: {
                label: "Delivery",
                className: "btn-primary",
                callback: function () {
                    NProgress.start();
                    window.location.href= self.attr('data-delivery-url');
                }
            }
        }
    });
}
</script>
@stop
