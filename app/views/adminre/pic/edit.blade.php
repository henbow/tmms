@extends($theme.'._layouts.master')

@section('main')
<section id="main" role="main">
    {{ Notification::showAll() }}

    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Form::model($pic, array('method' => 'put', 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate', 'route' => array('pic.update', $pic->id))) }}
                    <div class="panel-heading">
                        <h3 class="panel-title">PIC Details</h3>
                    </div>               
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">User Name</label>
                            <div class="col-sm-6">
                                <select name="userid" id="userid" class="form-control" parsley-required="true">
                                    <option value="">Choose Username</option>
                                    @foreach (Sentry::findAllUsers() as $user)
                                    <option {{{ $pic->user_id == $user->id ? 'selected' : '' }}} value="{{{ $user->id }}}">{{{ $user->username }}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">PIC Code</label>
                            <div class="col-sm-6">
                                <input type="text" name="code" id="code" class="form-control" value="{{{ $pic->code }}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">First Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="firstname" class="form-control" value="{{{ $pic->first_name }}}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Last Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="lastname" class="form-control" value="{{{ $pic->last_name }}}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Gender</label>
                            <div class="col-sm-6">
                                <select name="gender" id="gender" class="form-control">
                                    <option {{{ $pic->sex == 'm' ? 'selected' : '' }}} value="m">Male</option>
                                    <option {{{ $pic->sex == 'f' ? 'selected' : '' }}} value="f">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Department</label>
                            <div class="col-sm-6">
                                <select name="department" id="department" class="form-control" parsley-required="true">
                                    <option value=""></option>
                                    @foreach ($departments as $dept)
                                    <option {{{ $pic->dept_id == $dept->id ? 'selected' : '' }}} value="{{{ $dept->id }}}">{{{ $dept->name }}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Unit</label>
                            <div class="col-sm-6">
                                <select name="unit" id="unit" class="form-control" parsley-required="true">
                                    <option value=""></option>
                                    @foreach ($units as $unit)
                                    <option {{{ $pic->unit_id == $unit->id ? 'selected' : '' }}} value="{{{ $unit->id }}}">{{{ $unit->name }}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Edit PIC</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/parsley/js/parsley.min.js') }}}"></script>        
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/validation.js') }}}"></script>
@stop