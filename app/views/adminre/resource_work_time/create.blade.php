@extends($theme . '._layouts.master')

@section('main')
<section id="main" role="main">
    {{ Notification::showAll() }}

    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Form::open(array('route' => array('resource_work_time.store', $resource_id), 'id' => 'edit-work-time', 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate')) }}
                    <div class="panel-heading">
                        <h3 class="panel-title">Work Time For {{{ Resource::getName($resource_id) }}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Monday:</label>
                            <div class="col-sm-6"><input type="text" class="form-control day" name="mon" id="mon" value="{{{ @$resource_work_time->mon }}}"/></div><div class="clear"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tuesday:</label>
                            <div class="col-sm-6"><input type="text" class="form-control day" name="tue" id="tue" value="{{{ @$resource_work_time->tue }}}"/></div><div class="clear"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Wednesday:</label>
                            <div class="col-sm-6"><input type="text" class="form-control day" name="wed" id="wed" value="{{{ @$resource_work_time->wed }}}"/></div><div class="clear"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Thursday:</label>
                            <div class="col-sm-6"><input type="text" class="form-control day" name="thu" id="thu" value="{{{ @$resource_work_time->thu }}}"/></div><div class="clear"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Friday:</label>
                            <div class="col-sm-6"><input type="text" class="form-control day" name="fri" id="fri" value="{{{ @$resource_work_time->fri }}}"/></div><div class="clear"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Saturday:</label>
                            <div class="col-sm-6"><input type="text" class="form-control day" name="sat" id="sat" value="{{{ @$resource_work_time->sat }}}"/></div><div class="clear"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Sunday:</label>
                            <div class="col-sm-6"><input type="text" class="form-control day" name="sun" id="sun" value="{{{ @$resource_work_time->sun }}}"/></div><div class="clear"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Capacity Per Week:</label>
                            <div class="col-sm-6"><input type="text" class="form-control week" name="per_week" id="per_week" value="{{{ @$resource_work_time->per_week }}}"/></div><div class="clear"></div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success btn-update">Update</button>
                        <button type="reset" class="btn btn-reset">Reset</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/parsley/js/parsley.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/validation.js') }}}"></script>
<script type="text/javascript">
$('input.day').keyup(function(){
    var sum = 0;
    $('input.day').each(function() {
        sum += Number($(this).val());
    });
    $('input.week').val(sum);
});
$('.btn-update').click(function(e){
    e.preventDefault();
    NProgress.start();
    $('form#edit-work-time').submit();
});
</script>
@stop

