@extends($theme . '._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
            </div>
        </div>

        <!-- Planner detail -->
        <div class="row" id="work-plan-detail" style="display:none">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="panel-icon mr5"><i class="ico-table22"></i></span>
                            Work Plan For <span id="resource-name" style="font-weight: bold;"></span> in <span id="month-year" style="font-weight: bold;">January 2014</span>
                        </h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <div class="table-responsive panel-collapse pull out" id="calendar"></div>
                </div>
            </div>
        </div>

        <!-- Week here -->

        <!--/ Planner detail -->

        <!-- Planner list -->
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-primary">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>Resource Work Plan</h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <!--/ panel heading/header -->

                    <!-- panel toolbar wrapper -->
                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <div class="panel-toolbar pl10">
                            <div class="checkbox custom-checkbox pull-left">
                                <!--<input type="checkbox" id="customcheckbox-one0" value="1" data-toggle="checkall" data-target="#table1">  -->
                                <select name="sel-year" id="sel-year" class="form-control">
                                    @for($year = intval($start_year); $year <= $start_year + intval(GeneralOptions::getSingleOption('active_years')); $year++)
                                    <option value="{{{ $year }}}" {{{ $selected_year == $year ? 'selected' : '' }}}>{{{ $year }}}</option>
                                    @endfor
                                </select>
                        </div>
                        <div class="panel-toolbar text-right">
                            <a href="{{{ route('resource_planner.update.by_year', array($selected_year)) }}}" title="Update Resource Work Plan" class="btn btn-success mb5" id="update-work-time-plan">
                                <span>Update Resource Work Plan</span>
                            </a>
                        </div>
                    </div>
                    <!--/ panel toolbar wrapper -->

                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-bordered table-hover" id="table1">
                            <thead>
                                <tr>
                                    <th rowspan="3" class="text-center">Code</th>
                                    <th rowspan="3" class="text-center">Name</th>
                                    <th colspan="12" class="text-center">Work Time in {{{ $selected_year }}} (hours)</th>
                                </tr>
                                <tr>
                                    @foreach($months as $month)
                                    <th width="5%" class="text-center">{{{ substr($month, 0, 3) }}}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($resources as $resource)
                                <tr>
                                    <td align="center">{{{ $resource->code }}}</td>
                                    <td align="center"><a target="_blank" href="{{{ route('resource.show', $resource->id) }}}">{{{ $resource->name }}}</a></td>
                                    @foreach($months as $key => $month)
                                    <td align="center" class="month-select" title="Click for detail" data-resource="{{{ $resource->id }}}" data-month="{{{ $key + 1 }}}" data-year="{{{ $selected_year }}}">
                                        {{{ round(ResourcePlanner::getWorkTimePlanPerMonth($resource->id, $key + 1, $selected_year), 2) }}}
                                    </td>
                                    @endforeach
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>

        <div class="col-md-12 text-center"><?php echo $resources->links(); ?></div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<style type="text/css">
.month-select{cursor: pointer;}
.month-select:hover{font-weight: bold;}
</style>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/app.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/calendar.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/xeditable/js/bootstrap-editable.min.js') }}}"></script>
<script type="text/javascript">
(function(){
    $('#update-work-time-plan').click(function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        NProgress.start();
        window.location.href = url;
    });
    $('select#sel-year').change(function(e){redirect_to('{{{ route('resource_planner.index') }}}/'+$(this).val()+'/index');});
    $('td.month-select').click(function(){
        $('#calendar').empty();

        var resourceId = $(this).attr('data-resource');
        var month = $(this).attr('data-month');
        var year = $(this).attr('data-year');

        $('#work-plan-detail').fadeIn();
        $('#week-loader').show();
        $('#week-table').hide();

        $.get('{{{ URL::to('/resource_planner/') }}}/'+resourceId+'/'+month+'/'+year+'/work_plan', function(resp){
            Object.size = function(obj) {
                var size = 0, key;
                for (key in obj) {
                    if (obj.hasOwnProperty(key)) size++;
                }
                return size;
            };

            var num = 1;
            var response = $.parseJSON(resp);
            var work_times = response.work_time;

            $('#week-loader').fadeOut('fast', function(){$('#week-table').show();});
            $('#resource-name').text(response.resource);
            $('#month-year').text(response.month_year);

            var cal = new Calendar(parseInt(response.month_num), parseInt(response.year));
            cal.generateHTML();
            $('#calendar').html(cal.getHTML());

            for(var i=get_key(work_times); i <= get_key(work_times) + Object.size(work_times); i++){
                var tmp = work_times[i];
                var idworktime = tmp.split(',');

                $('#header'+num).text('Week '+i);
                $('#body'+num+' a').text(idworktime[1]);
                idworktime[1]==0.00?$('#body'+num+' span').removeClass('text-success').addClass('text-danger'):0;
                $('#body'+num+' a').attr('id', idworktime[0]);
                $('#body'+num+' a').attr('data-pk', idworktime[0]);
                $('#'+idworktime[0]).editable({url:"{{{ route('resource_planner.update_work_time_plan') }}}",title: "Edit Work Time"});
                $('#'+idworktime[0]).on('save', function(e, params){
                    redirect_to("{{{ route('resource_planner.index') }}}");
                });
                $('#'+idworktime[0]).on('shown', function(e, editable) {
                    editable.input.$input.val($(this).text());
                });
                num++;
            }
        });
    });

    function get_key(data) {
      for (var prop in data)
        return prop;
    }

    function redirect_to(url) {
        window.location.href = url;
    }
})();
</script>
<!-- <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/components/notification.js') }}}"></script> -->
@stop
