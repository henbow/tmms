@extends($theme . '._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<style type="text/css">
.resource-availability-item{cursor: pointer;}
.resource-availability-item-hover{font-weight: bold;}
</style>
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
            </div>
        </div>

        <!-- Planner detail -->
        <div class="row" id="availability-detail" style="display:none">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="panel-icon mr5"><i class="ico-table22"></i></span>
                            Capacity For <span id="resource-name" style="font-weight: bold;"></span> in <span id="month-year" style="font-weight: bold;">January 2014</span>
                        </h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->

                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <div class="table-responsive panel-collapse pull out" id="per-week"></div>
                </div>
            </div>
        </div>

        <!-- Week here -->

        <!--/ Planner detail -->

        <!-- Planner list -->
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-primary">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>Resources Availability</h3>
                    </div>
                    <!--/ panel heading/header -->

                    <!-- panel toolbar wrapper -->
                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <div class="panel-toolbar pl10">
                            <div class="checkbox custom-checkbox pull-left">
                                <!--<input type="checkbox" id="customcheckbox-one0" value="1" data-toggle="checkall" data-target="#table1">  -->
                                <select name="sel-year" id="sel-year" class="form-control">
                                    @for($year = intval($start_year); $year <= $start_year + intval(GeneralOptions::getSingleOption('active_years')); $year++)
                                    <option value="{{{ $year }}}" {{{ $selected_year == $year ? 'selected' : '' }}}>{{{ $year }}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" name="search_resource" class="form-control" value="{{ $keywords }}" placeholder="Search Resources">
                            </div>
                            <div class="btn-group" style="margin-bottom:5px;float:right;">
                                <button type="button" class="btn btn-primary">Export To</button>
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="javascript:void(0);">Excel</a></li>
                                    <li><a href="javascript:void(0);">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--/ panel toolbar wrapper -->

                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out horizontal-scrolling">
                        <table class="table table-bordered table-hover" id="table1">
                            <thead>
                                <tr>
                                    <th rowspan="3" class=" text-center" width="10">No</th>
                                    <th rowspan="3" class=" text-center" width="100">Code</th>
                                    <th rowspan="3" class=" text-center" width="100">Name</th>
                                    <th rowspan="3" class=" text-center" width="100">Group</th>
                                    <th rowspan="3" class=" text-center" width="100">Desc</th>
                                    <th colspan="48" class="text-center">Work Time in {{{ $selected_year }}} (hours)</th>
                                </tr>
                                <tr>
                                    @foreach($months as $month)
                                    <th width="5%" class="text-center">{{{ substr($month, 0, 3) }}}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody style="font-size: 10px;">
                                <?php $no = 1; ?>
                                @foreach($resources as $resource)
                                <?php $capacities = $workplans = $availability =  array(); ?>
                                <tr>
                                    <td rowspan="5" class="text-center">{{{ $no++ }}}</td>
                                    <td rowspan="5" class="text-center">{{{ $resource->code }}}</td>
                                    <td rowspan="5" class="text-center">{{{ $resource->name }}}</td>
                                    <td rowspan="5" class="text-center">{{{ $resource->code }}}</td>
                                </tr>
                                <tr>
                                    <td>Capacity</td>
                                    @foreach($months as $key => $month)
                                    <td align="center" class="resource-availability-item" title="Click for detail" data-resource="{{{ $resource->id }}}" data-month="{{{ $key + 1 }}}" data-year="{{{ $selected_year }}}" data-type="cp">
                                        <?php $cp = round(ResourcePlanner::getWorkTimePlanPerMonth($resource->id, $key + 1, $selected_year), 2); $capacities[] = $cp; echo $cp; ?>
                                    </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Work Plan</td>
                                    @foreach($months as $key => $month)
                                    <td align="center" class="resource-availability-item" title="Click for detail" data-resource="{{{ $resource->id }}}" data-month="{{{ $key + 1 }}}" data-year="{{{ $selected_year }}}" data-type="wp">
                                        <?php
                                        $sch_type = ResourceGroup::with('process')->where('id', '=', $resource->group->id)->first()->process->first()->process_type == 'nop' ? "schedulesNonMfg" : "schedulesMfg" ;
                                        $wp = $resource->{$sch_type}()->where(DB::raw('YEAR(start_date)'),'=',date('Y',strtotime("$selected_year-$month-1")))->where(DB::raw('MONTH(start_date)'),'=',date('m',strtotime("$selected_year-$month-1")))->sum('estimation_hour');
                                        $workplans[] = $wp; echo $wp;
                                        ?>
                                    </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Availability (H)</td>
                                    @for($i = 0; $i < count($capacities); $i++)
                                    <td align="center" class="resource-availability-item" title="Click for detail" data-resource="{{{ $resource->id }}}" data-month="{{{ $key + 1 }}}" data-year="{{{ $selected_year }}}" data-type="av">
                                        <?php $availability = $capacities[$i] - $workplans[$i]; echo $availability < 0 ? '<span class="text-danger">'.$availability.'</span>' : $availability ?>
                                    </td>
                                    @endfor
                                </tr>
                                <tr>
                                    <td>Availability (%)</td>
                                    @for($i = 0; $i < count($capacities); $i++)
                                    <td align="center" class="resource-availability-item" title="Click for detail" data-resource="{{{ $resource->id }}}" data-month="{{{ $key + 1 }}}" data-year="{{{ $selected_year }}}" data-type="avp">
                                        <?php $availability_percent = $capacities[$i] > 0 ? round(($capacities[$i] - $workplans[$i]) / $capacities[$i], 2) * 100 : 0; echo $availability_percent < 0 ? '<span class="text-danger">'.$availability_percent.'</span>' : $availability_percent ?>
                                    </td>
                                    @endfor
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>

        <div class="col-md-12 text-center"><?php echo $resources->links(); ?></div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/xeditable/js/bootstrap-editable.min.js') }}}"></script>
<script type="text/javascript">
(function(){
    $('[name="search_resource"]').keypress(function(e) {
        if(e.which == 13) {
            redirect_to('{{{ route('report.resource_availability') }}}?year={{ $selected_year }}&keywords='+$(this).val());
        }
    });
    $('select#sel-year').change(function(e){redirect_to('{{{ route('report.resource_availability') }}}?year='+$(this).val());});
    $('td.resource-availability-item').click(function(){
        $('#per-week').empty();

        var resourceId = $(this).attr('data-resource');
        var month = $(this).attr('data-month');
        var year = $(this).attr('data-year');
        var type = $(this).attr('data-type');

        $('#availability-detail').fadeIn();
        $('#week-loader').show();
        $('#week-table').hide();

        $.get('{{{ route('report.resource_availability.detail') }}}?resource_id='+resourceId+'&month='+month+'&year='+year+'&type='+type, function(resp){
            $('#availability-detail').html(resp);
        });

        $('.totop').click();
    });

    function get_key(data) {
      for (var prop in data)
        return prop;
    }

    function redirect_to(url) {
        window.location.href = url;
    }
})();
</script>
<!-- <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/components/notification.js') }}}"></script> -->
@stop
