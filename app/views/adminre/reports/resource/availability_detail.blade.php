<div class="col-md-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
                <span class="panel-icon mr5"><i class="ico-table22"></i></span>
                </span>
            </h3>
            <!-- panel toolbar -->
            <div class="panel-toolbar text-right">
                <!-- option -->
                <div class="option">
                    <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                    <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                </div>
                <!--/ option -->
            </div>
            <!--/ panel toolbar -->
        </div>
        <div class="table-responsive panel-collapse pull out">
            <table class="table table-bordered table-hover" id="table1">
                <thead>
                    <tr>
                        <th rowspan="3" class=" text-center" width="100">Code</th>
                        <th rowspan="3" class=" text-center" width="100">Name</th>
                        {{-- <th colspan="48" class="text-center">Capacity For <span id="resource-name" style="font-weight: bold;">{{ $resource_name }}</span> in <span id="week-year" style="font-weight: bold;">{{ $week_year }} (hours)</th> --}}
                    {{-- </tr>
                    <tr> --}}
                        @foreach($weeks as $week)
                        <th width="10%" class="text-center">Week {{{ substr($week, 0, 3) }}}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody style="font-size: 10px;">
                    <?php $capacities = $workplans = $availability =  array(); ?>
                     <tr>
                        <td rowspan="5" class="text-center">{{{ $resource->code }}}</td>
                        <td rowspan="5" class="text-center">{{{ $resource->name }}}</td>
                        @foreach($weeks as $week)
                        @if($type == 'cp')
                        <td class="text-center">{{{ round(ResourcePlanner::getWorkTimePlanPerWeek($resource->id, $week, $month, $year), 2) }}}</td>
                        @elseif($type == 'wp')
                        <td class="text-center">{{{ round(ResourcePlanner::getWorkTimePlanPerWeek($resource->id, $week, $month, $year), 2) }}}</td>
                        @elseif($type == 'av')
                        <td class="text-center">{{{ round(ResourcePlanner::getWorkTimePlanPerWeek($resource->id, $week, $month, $year), 2) }}}</td>
                        @else
                        <td class="text-center">{{{ round(ResourcePlanner::getWorkTimePlanPerWeek($resource->id, $week, $month, $year), 2) }}}</td>
                        @endif
                        @endforeach
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
