@extends($theme . '._layouts.master')

@section('additional_css')
<link rel="stylesheet" media="screen" href="{{{ asset('assets/'.$theme.'/plugins/handsontable/handsontable.full.css') }}}" />
<link rel="stylesheet" media="screen" href="{{{ asset('assets/'.$theme.'/plugins/handsontable/handsontable.bootstrap.css') }}}" />
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
            </div>
        </div>

        <!-- Planner detail -->
        <div class="row" id="work-plan-detail" style="display:none">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="panel-icon mr5"><i class="ico-table22"></i></span>
                            Work Plan For <span id="resource-name" style="font-weight: bold;"></span> in <span id="month-year" style="font-weight: bold;">January 2014</span>
                        </h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <div class="table-responsive panel-collapse pull out" id="calendar"></div>
                </div>
            </div>
        </div>

        <!-- Week here -->

        <!--/ Planner detail -->

        <!-- Planner list -->
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-primary">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>Resources Availability</h3>
                    </div>
                    <!--/ panel heading/header -->

                    <!-- panel toolbar wrapper -->
                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <div class="panel-toolbar pl10">
                            <div class="checkbox custom-checkbox pull-left">
                                <!--<input type="checkbox" id="customcheckbox-one0" value="1" data-toggle="checkall" data-target="#table1">  -->
                                <select name="sel-year" id="sel-year" class="form-control">
                                    @for($year = intval($start_year); $year <= $start_year + intval(GeneralOptions::getSingleOption('active_years')); $year++)
                                    <option value="{{{ $year }}}" {{{ $selected_year == $year ? 'selected' : '' }}}>{{{ $year }}}</option>
                                    @endfor
                                </select>
                        </div>
                    </div>
                    <!--/ panel toolbar wrapper -->

                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out horizontal-scrolling">
                        <div id="table"></div>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>

        <?php // <div class="col-md-12"><?php echo $resources->links(); </div> ?>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script src="{{{ asset('assets/'.$theme.'/plugins/handsontable/handsontable.full.js') }}}"></script>
<script type="text/javascript">
(function(){
    function getData() {
        var header1 = ['No','Resource','','','','Work Time in {{{ $selected_year }}}', <?php echo rtrim(str_repeat("'',", count($months)-1),','); ?>];
        var header2 = ['', 'Code', 'Name', 'Group', 'Desc', <?php echo "'".implode("','','','','",$months)."'"; ?>];
        var header3 = ['', '', '', '', '', <?php $weeks=array();for($i=1;$i<=48;$i++){$weeks[]="'".$i."'";}echo implode(',',$weeks); ?>];

        var data = [
            header1,
            header2,
            header3
        ];
        return data;
    }
    // Instead of creating a new Handsontable instance with the container element passed as an argument,
    // you can simply call .handsontable method on a jQuery DOM object.
    var $container = $("#table");

    $container.handsontable({
        data: getData(),
        startRows: 5,
        startCols: 5,
        minRows: 5,
        minCols: 5,
        maxRows: 10,
        maxCols: 10,
        rowHeaders: false,
        colHeaders: false,
        minSpareRows: 1,
        contextMenu: false,
        fixedColumnsLeft: 5,
        className: "htCenter",
        manualColumnFreeze: true,
        cell: [
            {row: 0, col: 0, className: "htCenter htMiddle"},
            {row: 1, col: 1, className: "htCenter htMiddle"},
            {row: 1, col: 2, className: "htCenter htMiddle"},
            {row: 1, col: 3, className: "htCenter htMiddle"},
            {row: 1, col: 4, className: "htCenter htMiddle"}
        ],
        mergeCells: [
          {row: 0, col: 0, rowspan: 3, colspan: 1},
          {row: 0, col: 1, rowspan: 1, colspan: 4},
          {row: 0, col: 5, rowspan: 1, colspan: <?php echo  count($weeks); ?>},

          {row: 1, col: 1, rowspan: 2, colspan: 1},
          {row: 1, col: 2, rowspan: 2, colspan: 1},
          {row: 1, col: 3, rowspan: 2, colspan: 1},
          {row: 1, col: 4, rowspan: 2, colspan: 1},

          {row: 1, col: 5, rowspan: 1, colspan: 4},
          {row: 1, col: 9, rowspan: 1, colspan: 4},
          {row: 1, col: 13, rowspan: 1, colspan: 4},
          {row: 1, col: 17, rowspan: 1, colspan: 4},
          {row: 1, col: 21, rowspan: 1, colspan: 4},
          {row: 1, col: 25, rowspan: 1, colspan: 4},
          {row: 1, col: 29, rowspan: 1, colspan: 4},
          {row: 1, col: 33, rowspan: 1, colspan: 4},
          {row: 1, col: 37, rowspan: 1, colspan: 4},
          {row: 1, col: 41, rowspan: 1, colspan: 4},
          {row: 1, col: 45, rowspan: 1, colspan: 4},
          {row: 1, col: 49, rowspan: 1, colspan: 4}
        ]
    });

    // This way, you can access Handsontable api methods by passing their names as an argument, e.g.:
    var hotInstance = $("#table").handsontable('getInstance');
})();
</script>
<!-- <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/components/notification.js') }}}"></script> -->
@stop
