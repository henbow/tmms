
<table class="table table-striped table-bordered table-hover" id="ajax-source" style="font-size: 11px">
    <thead>
        <tr>
            <th class="text-center">NOP</th>
            <th class="text-center">Project</th>
            <th class="text-center">Process</th>
            <th class="text-center">Est. (Hours)</th>
            <th class="text-center">Resource</th>
            <th class="text-center" width="12%">Scheduled Start</th>
            <th class="text-center" width="12%">Scheduled Finish</th>
        </tr>
    </thead>
    <tbody>
        @foreach($schedule_data as $schedule)
        <?php $resource = Resource::find($schedule->resource_id); ?>
        <tr>
            <td class="text-center">{{{ $schedule->nop }}}</td>
            <td class="text-left">{{{ $schedule->project }}}</td>
            <td class="text-center">{{{ $schedule->process }}}</td>
            <td class="text-center">{{{ $schedule->estimation_hour }}}</td>
            <td class="text-center">{{{ @$resource->name }}}</td>
            <td class="text-center">{{{ date('d-M-Y', strtotime($schedule->start_date)) }}}</td>
            <td class="text-center">{{{ date('d-M-Y', strtotime($schedule->finish_date)) }}}</td>
        </tr>
        @endforeach
    </tbody>
</table>