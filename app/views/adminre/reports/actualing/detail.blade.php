@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatable/jquery.dataTables.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/treegrid/css/jquery.treegrid.css') }}}">
<style type="text/css">
/*.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }*/
</style>
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
            <!-- Toolbar -->

            <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel body with collapse capabale -->
                    <div class="panel-collapse pull out">
                        <div class="panel-body">
                            <dl class="dl-horizontal" style="padding-bottom: 0;margin-bottom: 0;">
                                <dt style="width:100px;">NOP</dt><dd style="margin-left: 120px;">{{{ $project->nop }}}</dd>
                                <dt style="width:100px;">Order No</dt><dd style="margin-left: 120px;">{{{ $project->order_num }}}</dd>
                                <dt style="width:100px;">Project Name</dt><dd style="margin-left: 120px;">{{{ $project->project_name }}}</dd>
                                <dt style="width:100px;">Customer</dt><dd style="margin-left: 120px;">{{{ $project->customer->code ." - ". $project->customer->name }}}</dd>
                            </dl>
                        </div>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
                <!--/ END panel -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php // { Notification::showAll() }} ?>
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>{{{ $sub_title }}}</h3>
                    </div>
                    <!--/ panel heading/header -->

                    <!-- panel body with collapse capabale -->
                    <table class="table table-striped table-hover tree" id="report-detail">
                        <thead>
                            <tr>
                                <th class="text-center" width="10%">No</th>
                                <th class="text-left">Work / Process / Material</th>
                                <th class="text-center" width="6%">Est. (H)</th>
                                <th class="text-center" width="6%">Act. (H)</th>
                                <th class="text-center">Resource</th>
                                <th class="text-center">Plan</th>
                                <th class="text-center" width="7%">Status</th>
                            </tr>
                        </thead>
                        <tbody style="font-size:10px">
                            @foreach($masterplans as $masterplan)
                                <?php $process_group = $masterplan->process_group()->first(); ?>
                                @if($process_group->type == 'nop')
                                    <!-- By NOP -->
                                    <?php $detail_plans = $masterplan->detail_plans()->with('report')->get(); ?>
                                    <tr class="treegrid-{{{ md5($masterplan->id) }}} mp-{{ $masterplan->id }} success">
                                        <td colspan="7"><strong>{{{ $process_group->group }}}</strong></td>
                                    </tr>
                                    @if(count($detail_plans))
                                        <?php $no = 1; ?>
                                        @foreach($detail_plans as $detail_plan)
                                        {{-- @if($process_group->group == 'DESIGN') --}}
                                            @if(isset($detail_plan->process->process))
                                            <?php $report_non_mfg = $detail_plan->report()->first(); ?>
                                            <?php $resource = Resource::find($report_non_mfg['resource_id']); ?>
                                            <tr class="treegrid-{{{ $detail_plan->id }}} treegrid-parent-{{{ md5($masterplan->id) }}}">
                                                <td class="text-center">{{ $no++ }}</td>
                                                <td>{{ $detail_plan->process->process }}</td>
                                                <td class="text-center">{{ $detail_plan->estimation_hour }}</td>
                                                <td class="text-center">{{ $report_non_mfg['actualing_hour'] }}</td>
                                                <td class="text-center">{{ @$resource->name }}</td>
                                                <td class="text-center">{{ strtoupper(@$resource->plan_type) }}</td>
                                                <td class="text-center">{{ $report_non_mfg['status'] }}</td>
                                            </tr>
                                            @endif
                                        {{-- @endif --}}
                                        @endforeach
                                    @endif
                                @elseif($process_group->type == 'pb-bom')
                                    <!-- By PB-BOM -->
                                    <tr class="treegrid-{{{ md5($masterplan->id) }}} mp-{{ $masterplan->id }} success">
                                        <td colspan="7"><strong>{{{ $process_group->group }}}</strong></td>
                                    </tr>
                                    @if(count($project->boms))
                                        <?php $no = 1; ?>
                                        @foreach($project->boms()->get() as $bom)
                                            {{-- @if(isset($detail_plan->process->process)) --}}
                                            <?php $report_pbbom = ReportActualingPBBOM::where('material_id','=',$bom->id)->remember(60)->first(); ?>
                                            <?php $resource = Resource::find($report_pbbom['resource_id']); ?>
                                            <tr class="treegrid-{{{ $bom->id }}} treegrid-parent-{{{ md5($masterplan->id) }}}">
                                                <td class="text-center">{{ $no++ }}</td>
                                                <td>{{ $bom->material_name }}</td>
                                                <td class="text-center">{{ $masterplan->estimation_hour }}</td>
                                                <td class="text-center">{{ $report_pbbom['actualing_hour'] }}</td>
                                                <td class="text-center">{{ @$resource->name }}</td>
                                                <td class="text-center">{{ @$resource->plan_type }}</td>
                                                <td class="text-center">{{ $report_pbbom['status'] }}</td>
                                            </tr>
                                            {{-- @endif --}}
                                        @endforeach
                                    @endif
                                @else
                                    <!-- By Material -->
                                    <tr class="treegrid-{{{ md5($masterplan->id) }}} mp-{{ $masterplan->id }} success">
                                        <td colspan="7"><strong>{{{ $process_group->group }}}</strong></td>
                                    </tr>
                                    @if(count($project->boms))
                                        @foreach($project->boms()->with('planning_mfg')->where('type','<>','H')->remember(60)->get() as $bom)
                                            <?php $no = 1; $plannings = $bom->planning_mfg()->with('report')->where('project_id','=',$project->id)->get(); ?>
                                            @if(count($plannings))
                                                <tr class="treegrid-{{{ md5($masterplan->id.$bom->id) }}} treegrid-parent-{{{ md5($masterplan->id) }}}">
                                                    <td colspan="7"><strong>{{ $bom->material_name }}</strong></td>
                                                </tr>
                                                @foreach($plannings as $planning)
                                                @if($planning->process()->first()['process'])
                                                <?php $report_mfg = $planning->report()->first(); ?>
                                                <?php $resource = Resource::find($report_mfg['resource_id']); ?>
                                                <tr class="treegrid-parent-{{{ md5($masterplan->id.$bom->id) }}}">
                                                    <td class="text-center">{{ $no++ }}</td>
                                                    <td>{{ $planning->process()->first()['process'] }}</td>
                                                    <td class="text-center">{{ $planning->estimation_hour }}</td>
                                                    <td class="text-center">{{ $report_mfg['actualing_hour'] }}</td>
                                                    <td class="text-center">{{ @$resource->name }}</td>
                                                    <td class="text-center">{{ @$resource->plan_type }}</td>
                                                    <td class="text-center">{{ $report_mfg['status'] }}</td>
                                                </tr>
                                                @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>

<!-- START modal-lg -->
<div id="bs-modal-lg" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<!--/ END modal-lg -->
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatable/jquery.dataTables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatable/dataTables.bootstrap.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/gritter/js/jquery.gritter.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/treegrid/js/jquery.treegrid.js') }}}"></script>
<script>
(function(){
    $('.tree').treegrid();
})();
</script>
<!-- <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/components/notification.js') }}}"></script> -->
@stop
