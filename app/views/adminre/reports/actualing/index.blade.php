@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatables/css/jquery.datatables.min.css') }}}">
<style type="text/css">
.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }
</style>
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
            <!-- Toolbar -->

            <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel body with collapse capabale -->
                    {{ Form::open(array('route' => 'report.actualing.search', 'class' => 'form-horizontal', 'id' => 'filter-planning', 'data-parsley-validate')) }}
                    <br>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">NOP</label>
                        <div class="col-sm-3">
                            <input type="text" name="nop" class="form-control" value="{{ $nop }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Project</label>
                        <div class="col-sm-3">
                            <input type="text" name="project" class="form-control" value="{{ $project }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-3">
                            <button type="submit" id="search" class="btn btn-success">Search</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                    <!--/ panel body with collapse capabale -->
                </div>
                <!--/ END panel -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php // {{ Notification::showAll() }} ?>
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-calendar"></i></span>{{{ $sub_title }}}</h3>
                    </div>
                    <!--/ panel heading/header -->

                    <!-- panel body with collapse capabale -->
                    <?php //debug($project_data); ?>
                    <table class="table table-striped table-bordered table-hover" id="ajax-source">
                        <thead>
                            <tr>
                                <th class="text-center" rowspan="2" width="1%">No</th>
                                <th class="text-center" rowspan="2" width="10%">NOP</th>
                                <th class="text-center" rowspan="2" width="1%">Order</th>
                                <th class="text-center" rowspan="2" width="15%">Project</th>
                                <th class="text-center" rowspan="2">Customer</th>
                                {{-- <th class="text-center" rowspan="2">PO Num</th> --}}
                                {{-- <th class="text-center" colspan="3">Plan</th>
                                <th class="text-center" colspan="2">Progress</th>
                                <th class="text-center" rowspan="2">Status</th>
                            </tr>
                            <tr> --}}
                                <th class="text-center" width="10%">Start Date</th>
                                <th class="text-center" width="10%">First Date</th>
                                <th class="text-center" width="10%">Second. Date</th>
                                <th class="text-center">Progress Total (%)</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 10px">
                        <?php $no = 1; ?>
                        @foreach($project_data as $project)
                            @if(isset($project->customer->id) && $project->masterplans->count() > 0)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                                <td class="text-center"><a href="{{ route('report.actualing.detail') }}?id={{ $project->id }}">{{ $project->nop }}</a></td>
                                <td class="text-center">{{ $project->order_num }}</td>
                                <td class="text-left"><a href="{{ route('report.actualing.detail') }}?id={{ $project->id }}">{{ $project->project_name }}</a></td>
                                <td class="text-center">{{ $project->customer->code." - ".$project->customer->name }}</td>
                                {{-- <td class="text-center">{{ $project->po_num }}</td> --}}
                                <td class="text-center">{{ strtotime($project->start_date) > 0 ? date('d-M-Y', strtotime($project->start_date)) : '-' }}</td>
                                <td class="text-center">{{ strtotime($project->trial_date) > 0 ? date('d-M-Y', strtotime($project->trial_date)) : '-' }}</td>
                                <td class="text-center">{{ strtotime($project->delivery_date) > 0 ? date('d-M-Y', strtotime($project->delivery_date)) : '-' }}</td>
                                <td class="text-center"></td>
                                <td class="text-center">{{ $project->is_complete == 'yes' ? "COMPLETED" : "ON PROGRESS" }}</td>
                                <td class="text-center">{{ $project->is_complete == 'yes' ? "COMPLETED" : "ON PROGRESS" }}</td>
                            </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <div class="col-md-12 text-center"><?php //echo $projects->links(); ?></div>

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>

<!-- START modal-lg -->
<div id="bs-modal-lg" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<!--/ END modal-lg -->
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/tabletools.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/zeroclipboard.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/gritter/js/jquery.gritter.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript">
(function(){

})();
</script>
@stop
