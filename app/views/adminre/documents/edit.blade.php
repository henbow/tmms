@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatables/css/jquery.datatables.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/selectize/css/selectize.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/jqueryui/css/jquery-ui-timepicker.min.css') }}}">
<style type="text/css">
.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }
</style>
@stop

@section('main')
<section id="main" role="main">
    {{ Notification::showAll() }}

    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}} - <small>Upload New Document</small></h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Form::open(array('enctype' => 'multipart/form-data', 'method' => 'put', 'route' => array('document.update', $doc->id), 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate')) }}
                    <div class="panel-heading">
                        <h3 class="panel-title">Document Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Project</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <?php $project = Project::find($doc->project_id); ?>
                                    <input type="text" name="project" id="project" class="form-control" value="{{{ $project->nop.' -> '.$project->project_name }}}" disabled>
                                    <input type="hidden" name="project_id" id="project_id" value="{{{ $project->id }}}" required>
                                    <span class="input-group-btn">
                                        <a href="{{{ route('iqi.select_project') }}}" class="btn btn-primary" id="select-project" style="width:108px;">Select Project</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Issue</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <?php $issue = IssueManagement::find($doc->issue_id); ?>
                                    <input type="text" name="issue" id="issue" class="form-control" value="{{{ @$issue->issue_code }}}" disabled>
                                    <input type="hidden" name="issue_id" id="issue_id" value="{{{ @$issue->id }}}">
                                    <span class="input-group-btn">
                                        <a href="{{{ route('issue_management.select') }}}" class="btn btn-primary" id="select-issue" style="width:108px;">Select Issue</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Title</label>
                            <div class="col-sm-6">
                                <input type="text" name="title" class="form-control" value="{{{ $doc->title }}}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-9">
                                <textarea name="description" class="form-control" required>{{{ $doc->description }}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">File</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="file-text" readonly="">
                                    <span class="input-group-btn">
                                        <div class="btn btn-primary btn-file" style="width:108px;">
                                            <span class="icon iconmoon-file-3"></span> Browse <input name="document-file" type="file">
                                        </div>
                                    </span>
                                </div>
                                <?php $url = URL::to('/').'/'.GeneralOptions::getSingleOption('document_path')['value'].$project->nop.'/'.$doc->filename; ?>
                                Existing File: <a href="{{{ $url }}}" target="_blank">{{{ $doc->filename }}}</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary">Upload New Document</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
<!-- START modal-lg -->
<div id="bs-modal-lg" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<!--/ END modal-lg -->
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/parsley/js/parsley.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/tabletools.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/zeroclipboard.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/inputmask/js/inputmask.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-timepicker.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/jqueryui/js/jquery-ui-touch.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/typeahead/bootstrap3-typeahead.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/element.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/jquery.runner.js') }}}"></script>
<script type="text/javascript">
(function(){
    $('[name="document-file"]').change(function(){
        $('#file-text').val($(this).val());
    });
    $('#select-project').click(function(e){
        e.preventDefault();
        $('#bs-modal-lg .modal-content').load($(this).attr('href'),function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
    $('#select-issue').click(function(e){
        e.preventDefault();
        $('#bs-modal-lg .modal-content').load($(this).attr('href'),function(e){
            $('#bs-modal-lg').modal('show');
        });
    });
})();
</script>
@stop
