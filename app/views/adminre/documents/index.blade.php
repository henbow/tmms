@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/gritter/css/jquery.gritter.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/datatables/css/jquery.datatables.min.css') }}}">
<style type="text/css">
.dataTables_length{ float: left }
.DTTT{ float: left; }
.toolbar_search{ float: right; margin-right: 0px; }
</style>
@stop

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <!-- <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="#">Table</a></li>
                        <li class="active">Default</li>
                    </ol>
                </div> -->
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Notification::showAll() }}
                <!-- START panel -->
                <div class="panel panel-primary">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>{{{ $page_title }}}</h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <!--/ panel heading/header -->

                    <!-- panel toolbar wrapper -->
                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <!-- <div class="panel-toolbar pl10">
                            <div class="checkbox custom-checkbox pull-left">
                                <input type="checkbox" id="customcheckbox-one0" value="1" data-toggle="checkall" data-target="#table1">
                                <label for="customcheckbox-one0">&nbsp;&nbsp;Select all</label>
                            </div>
                        </div> -->
                        <div class="panel-toolbar text-right">
                            <a href="{{{ route('document.create') }}}" class="btn btn-success mb5"><i class="ico-user22"></i> Upload New Document</a>
                        </div>
                    </div>
                    <!--/ panel toolbar wrapper -->

                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-bordered table-hover" id="table1">
                            <thead>
                                <tr>
                                    <th class="text-center" width="5%">No</th>
                                    <th class="text-center">NOP</th>
                                    <th class="text-center">Project</th>
                                    <th class="text-center">Title</th>
                                    <th class="text-center">File Name</th>
                                    <th class="text-center">File Size</th>
                                    <th class="text-center">File Type</th>
                                    <th class="text-center">Uploaded Date</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody style="font-size:10px">
                            	@if(count($docs) > 0)
                            	<?php $no = 1; ?>
                            	@foreach($docs as $doc)
                            	<?php $project = Project::find($doc->project_id); ?>
                                <?php $url = URL::to('/').'/'.GeneralOptions::getSingleOption('document_path')['value'].$project->nop.'/'.$doc->filename; ?>
                            	<tr>
                                    <td class="text-center">{{{ $no++ }}}</td>
                                    <td class="text-center">{{{ $project->nop }}}</td>
                                    <td class="text-center">{{{ $project->project_name }}}</td>
                                    <td class="text-center">{{{ $doc->title }}}</td>
                                    <td class="text-center"><a href="{{{ $url }}}" target="_blank">{{{ $doc->filename }}}</a></td>
                                    <td class="text-center">{{{ $doc->size }}}</td>
                                    <td class="text-center">{{{ strtoupper($doc->filetype) }}}</td>
                                    <td class="text-center">{{{ date('d-M-Y H:i', strtotime($doc->created_at)) }}}</td>
                                    <td class="text-center">
			                            <div class="btn-group">
				                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="ico-paper-plane"></i> Action <span class="caret"></span></button>
				                            <ul class="dropdown-menu" role="menu">
				                                <!-- <li>
				                                	<a href="{{{ route('document.show', $doc->id) }}}" class="text-primary">Show Document</a>
				                                </li> -->
				                                <li>
				                                	<a href="{{{ route('document.edit', $doc->id) }}}" class="text-primary">Edit Document</a>
				                                </li>
				                                <li class="divider"></li>
				                                <!-- To Detail Planning Page -->
				                                <li>
                                                    <a href="#" onclick="$('#delete-form-{{{$doc->id}}}').submit();return false;" class="text text-danger">Delete</a>				                                </li>
                                                    {{ Form::open(array('route' => array('document.destroy', $doc->id), 'style' => 'display: none;', 'method' => 'delete', 'id' => 'delete-form-'.$doc->id, 'onsubmit' => "if(!confirm('Are you sure to delete?'))return false;")) }} {{ Form::close() }}
                                                </li>
				                            </ul>
				                        </div>

		                        	</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/sparkline/js/jquery.sparkline.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/tabletools.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/js/zeroclipboard.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/datatables/js/jquery.datatables-custom.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/gritter/js/jquery.gritter.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script>
(function(){
    // var table1 = $("#table1").dataTable({
    //     "sDom": "<'row'<'col-sm-6'Tl><'col-sm-6'f>><'table-responsive'rt><'row'<'col-sm-6'p><'col-sm-6'i>>",
    //     "oTableTools": {
    //         "sSwfPath": "{{{ asset('assets/'.$theme.'/plugins/datatables/tabletools/swf/copy_csv_xls_pdf.swf') }}}",
    //         "aButtons": [
    //             "print",
    //             "pdf",
    //             "csv"
    //         ]
    //     },
    //     "aoColumns": [
    //         { "sClass": "text-center" },
    //         { "sClass": "text-center" },
    //         { "sClass": "text-center" },
    //         { "sClass": "text-center" },
    //         { "sClass": "text-left" },
    //         { "sClass": "text-center" },
    //         { "sClass": "text-center" },
    //         { "sClass": "text-center" },
    //         { "sClass": "text-center" },
    //         { "sClass": "text-center", "bSearchable": false, "bSortable": false }
    //     ],
    //     "iDisplayLength": 50
    // });
    // table1.fnSort( [ [1,'desc'] ] );

})();
</script>
<!-- <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/components/notification.js') }}}"></script> -->
@stop
