@extends($theme.'._layouts.master')

@section('main')
<section id="main" role="main">
    {{ Notification::showAll() }}

    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}} - <small>Show Issue Details</small></h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Form::open(array('route' => array('iqi.update',$iqi->id), 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate')) }}
                    <div class="panel-heading">
                        <h3 class="panel-title">Issue Details</h3>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-2 control-label">Project</label>
                            <div class="col-sm-6">
                                <?php $project = Project::find($iqi->project_id); ?>
                                <input type="text" name="project" id="project" class="form-control" value="{{{ $project->nop." -> ". $project->project_name }}}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Part</label>
                            <div class="col-sm-6">
                                <?php $material = ProjectBOM::find($iqi->material_id); ?>
                                <input type="text" name="part_name" class="form-control" value="{{{ $material->barcode." -> ". $material->material_name }}}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">QC Staff</label>
                            <div class="col-sm-4">
                                <?php $qc_staff = Pic::find($iqi->qc_staff_id); ?>
                                <input type="text" class="form-control" value="{{{ $qc_staff->code . " -> ". $qc_staff->first_name." ".$qc_staff->last_name }}}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Problem Maker</label>
                            <div class="col-sm-4">
                                <?php $problem_maker = Pic::find($iqi->problem_maker_id); ?>
                                <input type="text" class="form-control" value="{{{ $problem_maker->code . " -> ". $problem_maker->first_name." ".$problem_maker->last_name }}}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Problem Description</label>
                            <div class="col-sm-9">
                                <textarea name="desc" class="form-control" disabled>{{{$iqi->description}}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Possible Causes</label>
                            <div class="col-sm-9">
                                <textarea name="causes" class="form-control" disabled>{{{$iqi->possible_causes}}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Corrective Action</label>
                            <div class="col-sm-9">
                                <textarea name="action" class="form-control" disabled>{{{$iqi->action}}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">PIC</label>
                            <div class="col-sm-3">
                                <?php $pic = Unit::find($iqi->pic_dept); ?>
                                <input type="text" class="form-control" value="{{{ $pic->name }}}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Remark</label>
                            <div class="col-sm-9">
                                <textarea name="remark" class="form-control" disabled>{{{$iqi->remark}}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Status</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" value="{{{ strtoupper($iqi->status) }}}" disabled>
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>

@stop

