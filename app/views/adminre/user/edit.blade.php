@extends($theme.'._layouts.master')
 
@section('main')
<section id="main" role="main">
    {{ Notification::showAll() }}

    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Form::model($user, array('method' => 'put', 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate', 'route' => array('user.update', $user->id))) }}
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit User Details</h3>
                    </div>               
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">User Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="username" class="form-control" value="{{{ $user->username }}}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Old Password</label>
                            <div class="col-sm-6">
                                <input type="password" name="password1" id="password1" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">New Password</label>
                            <div class="col-sm-6">
                                <input type="password" name="password2" id="password2" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Confirm New Password</label>
                            <div class="col-sm-6">
                                <input type="password" name="password3" id="password3" class="form-control" value="" data-parsley-equalto="#password2" data-equalto-message="Password doesn't match">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">First Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="firstname" class="form-control" value="{{{ $user->first_name }}}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Last Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="lastname" class="form-control" value="{{{ $user->last_name }}}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-6">
                                <input type="text" name="email" class="form-control" value="{{{ $user->email }}}" data-parsley-type="email" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-6">
                                <input type="text" name="phone" class="form-control" value="{{{ $user->phone_number }}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Level</label>
                            <div class="col-sm-6">
                                <select name="userlevel" id="userlevel" class="form-control" parsley-required="true">
                                    <option value=""></option>
                                    @foreach (Sentry::findAllGroups() as $group)
                                    <option {{{ $group_id == $group->id ? 'selected' : '' }}} value="{{{ $group->id }}}">{{{ $group->name }}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Update User</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop