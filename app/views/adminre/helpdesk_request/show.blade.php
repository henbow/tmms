@extends($theme.'._layouts.master')

@section('additional_css')
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/summernote/css/summernote.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/css/bootstrap-editable.min.css') }}}">
<link rel="stylesheet" href="{{{ asset('assets/'.$theme.'/plugins/xeditable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css') }}}">
@stop

@section('main')
<?php $user = \Sentry::getUser($request->request_user_id); ?>
<?php $ic_user = \Sentry::findUserById($request->incharge_id); ?>
<?php $department = Pic::where('user_id','=',$request->request_user_id)->get(); ?>

<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <!-- <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="#">Table</a></li>
                        <li class="active">Default</li>
                    </ol>
                </div> -->
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-6">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel heading/header -->

                    <!--/ panel heading/header -->
                    <!-- panel body with collapse capabale -->
                    <div class="panel-collapse pull out">
                        <div class="panel-body">
                            <dl class="dl-horizontal" style="margin-bottom: 0;">
                                <dt>Ticket ID</dt><dd><strong>{{{ $request->ticket_id }}}</strong></dd>
                                <dt>Category</dt><dd>{{{ HelpDeskCategory::find($request->category_id)->category }}}</dd>
                                <dt>Sub Category</dt><dd>{{{ strtoupper($request->category_type) }}}</dd>
                                <dt>Request By</dt><dd>{{{ $user->first_name . ' ' . $user->last_name }}}{{{ isset($department[0]) ? ' - '.Department::getName($department[0]->dept_id) : '' }}}</dd>
                            </dl>
                        </div>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
                <!--/ END panel -->
            </div>
            <div class="col-md-6">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel heading/header -->

                    <!--/ panel heading/header -->
                    <!-- panel body with collapse capabale -->
                    <div class="panel-collapse pull out">
                        <div class="panel-body">
                            <dl class="dl-horizontal" style="margin-bottom: 0;">
                                <dt>Priority</dt><dd class="{{{ HelpDeskPriority::find($request->priority_id)->class }}}"><strong>{{{ strtoupper(HelpDeskPriority::find($request->priority_id)->priority) }}}</strong></dd>
                                <dt>Process Manager</dt><dd>{{{ @\Sentry::findGroupByID($request->incharge_group_id)->name }}}</dd>
                                <dt>Person In Charge</dt><dd>{{{ $ic_user ? $ic_user->first_name . ' ' . $ic_user->last_name : '-' }}}</dd>
                                <dt>Status</dt><dd>
                                    <a href="#" data-type="select" data-pk="{{{ $request->status_id }}}" data-title="Change Request Status" id="edit-status">
                                        {{{ @HelpDeskPriority::find($request->status_id)->status }}}
                                    </a>
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
                <!--/ END panel -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel heading/header -->

                    <!--/ panel heading/header -->
                    <!-- panel body with collapse capabale -->
                    <div class="panel-collapse pull out">
                        <div class="panel-body">
                            <dl class="dl-horizontal" style="margin-bottom: 0;">
                                <dt>Subject</dt><dd>{{{ $request->subject }}}</dd>
                                <dt>Description</dt><dd><?php echo HelpDeskNotes::where('reply_to', '=', '0')->where('helpdesk_id', '=', $request->id)->get()[0]->message; ?></dd>
                            </dl>
                        </div>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
                <!--/ END panel -->
            </div>
        </div>

        @if($request->status_id != '4')
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                {{ Form::open(array('route' => array('helpdesk_request.add_note', $request->id), 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate')) }}
                <!-- panel heading/header -->
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-history2"></i></span>Add New Note</h3>
                </div>
                <!--/ panel heading/header -->
                <textarea class="addrequestnote" name="newnote" rows="5"></textarea>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
                {{ Form::close() }}
                <!--/ END panel -->
            </div>
        </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-default">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-history2"></i></span>History of Current Request</h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <!--/ panel heading/header -->
                    <!-- panel body with collapse capabale -->
                    <div class="panel-collapse pull out">
                        <div class="panel-body">
                            <table class="table table-bordered table-hover" id="table1" style="margin-bottom: 0;">
                                <tbody>
                                    @foreach($request_notes as $note)
                                    <tr>
                                        <td width="5%">
                                            <img width='32' src="{{{ asset('assets/default/images/icons/'.($note->reply_type == 'user' ? 'user-64.png' : 'mechanic-64.png')) }}}" />
                                        </td>
                                        <td>
                                            <strong>{{{ date('d M Y H:m:s', strtotime($note->created_at)) }}}</strong>
                                            [{{{ \Sentry::findUserByID($note->reply_by)->first_name.' '.\Sentry::findUserByID($request->request_user_id)->last_name }}}]<br>
                                            <?php echo $note->message; ?>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
                <!--/ END panel -->
            </div>
        </div>

    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop
@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/xeditable/js/bootstrap-editable.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/summernote/js/summernote.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/wysiwyg.js') }}}"></script>
<script type="text/javascript">
(function(){
    $('#edit-status').editable({
        url:"{{{ route('helpdesk_request.update_status', $request->id) }}}",
        value: <?php echo $request->status_id; ?>,
        source: <?php echo $request_status; ?>
    });
    $(".addrequestnote").summernote({
        height: 200,
        toolbar: [
            ["style", ["style"]],
            ["style", ["bold", "italic", "underline", "clear"]],
            ["fontsize", ["fontsize"]],
            ["color", ["color"]],
            ["para", ["ul", "ol", "paragraph"]],
            ["height", ["height"]],
            ["table", ["table"]]
        ]
    });
})();
</script>
@stop