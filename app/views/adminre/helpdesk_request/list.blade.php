@extends($theme.'._layouts.master')

@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">Requests From Users</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <!-- <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="#">Table</a></li>
                        <li class="active">Default</li>
                    </ol>
                </div> -->
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Notification::showAll() }}

                <!-- START panel -->
                <div class="panel panel-primary">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>Requests From Users</h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <!--/ panel heading/header -->

                    <!-- panel toolbar wrapper -->

                    <!--/ panel toolbar wrapper -->

                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-bordered table-hover" id="table1">
                            <thead>
                                <tr>
                                    <th class="text-center" width="5%">Ticket ID</th>
                                    <th class="text-center">Priority</th>
                                    <th class="text-center">Entry Date</th>
                                    <th class="text-center">From</th>
                                    <th class="text-center">Subject</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">In Charge</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($requests as $request)
                                <tr title="Priority is {{{ strtoupper(HelpDeskPriority::find($request->priority_id)->priority) }}}">
                                    <td align="center" width="10%"><a href="{{{ route('helpdesk_request.show', $request->id) }}}">{{{ $request->ticket_id }}}</a></td>
                                    <td class="{{{ HelpDeskPriority::find($request->priority_id)->class }}}" align="center" width="10%">{{{ strtoupper(HelpDeskPriority::find($request->priority_id)->priority) }}}</td>
                                    <td align="center" width="13%">{{{ date('d M Y H:m', strtotime($request->created_at)) }}}</td>
                                    <td align="center" width="13%">{{{ \Sentry::findUserByID($request->request_user_id)->first_name.' '.\Sentry::findUserByID($request->request_user_id)->last_name }}}</td>
                                    <td align="left">{{{ $request->subject }}}</td>
                                    <td align="center" width="10%">{{{ HelpDeskStatus::find($request->status_id)->status }}}</td>
                                    @if($request->incharge_group_id)
                                    <td align="center">{{{ @Sentry::findGroupByID($request->incharge_group_id)->name }}}</td>
                                    @else
                                    <td align="center"></td>
                                    @endif
                                    <td align="center">
                                        <!-- button toolbar -->
                                        <div class="toolbar">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-default">Action</button>
                                                <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="{{{ route('helpdesk_request.show', $request->id) }}}"><i class="icon ico-newspaper"></i>Update Request</a></li>
                                                    <!-- <li class="divider"></li>
                                                    <li>
                                                        <a href="#" data-id="{{{ $request->id }}}" class="delete-confirm text-danger"><i class="icon ico-remove3"></i>Delete</a>
                                                        {{ Form::open(array('route' => array('helpdesk_request.destroy', $request->id), 'method' => 'delete')) }}
                                                        <button type="submit" id="del-{{{ $request->id }}}" style="display:none"></button>
                                                        {{ Form::close() }}
                                                    </li> -->
                                                </ul>
                                            </div>
                                        </div>
                                        <!--/ button toolbar -->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript">
(function(){
    $(".delete-confirm").on("click", function (event) {
        var self = $(this);
        bootbox.confirm("Are you sure to cancel this request?", function (result) {
            if(result){
                var hdid = self.attr('data-id');
                $('button#del-'+hdid).click();
            }
        });
        event.preventDefault();
    });
})();
</script>
<!-- <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/components/notification.js') }}}"></script> -->
@stop