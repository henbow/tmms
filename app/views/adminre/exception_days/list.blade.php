@extends($theme.'._layouts.master')
 
@section('main')
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <!-- <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="#">Table</a></li>
                        <li class="active">Default</li>
                    </ol>
                </div> -->
                <!--/ Toolbar -->
            </div>
        </div>
        <!-- Page Header -->
        
        <div class="row">
            <div class="col-md-12">
                {{ Notification::showAll() }}

                <!-- START panel -->
                <div class="panel panel-primary">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>{{{ $page_title }}}</h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <!--/ panel heading/header -->
                    
                    <!-- panel toolbar wrapper -->
                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <div class="panel-toolbar text-right">
                            <a href="{{{ route('exception_days.create') }}}" class="btn btn-success mb5"><i class="ico-user22"></i> Add New Exception Day</a>
                        </div>
                    </div>
                    <!--/ panel toolbar wrapper -->

                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-bordered table-hover" id="table1">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">No</th>
                                    <th>Description</th>
                                    <th width="20%" class="text-center">Start Date</th>
                                    <th width="20%" class="text-center">Finish Date</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                @foreach ($exception_days as $day)
                                <tr>
                                    <td class="text-center">{{{ $no++ }}}</td>
                                    <td class="text-left">{{{ $day->description }}}</td>
                                    <td class="text-center">{{{ date('d-M-Y', strtotime("{$day->min_year}-{$day->min_month}-{$day->min_day}")) }}}</td>
                                    <td class="text-center">{{{ date('d-M-Y', strtotime("{$day->max_year}-{$day->max_month}-{$day->max_day}")) }}}</td>
                                    <td class="text-center" width="10%" class="text-center">
                                        <!-- button toolbar -->
                                        <div class="toolbar">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-default">Action</button>
                                                <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="{{{ route('exception_days.edit', $day->min_id.":".$day->max_id) }}}"><i class="icon ico-pencil"></i>Edit</a></li>
                                                    <li class="divider"></li>
                                                    <li>                                                        
                                                        <a href="#" data-id="{{{ $day->min_id."-".$day->max_id }}}" class="delete-confirm text-danger"><i class="icon ico-remove3"></i>Delete</a>
                                                        {{ Form::open(array('route' => array('exception_days.destroy', $day->min_id.":".$day->max_id), 'method' => 'delete')) }}
                                                        <button type="submit" id="del-{{{ $day->min_id."-".$day->max_id }}}" style="display:none"></button>
                                                        {{ Form::close() }}                                              
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--/ button toolbar -->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--/ panel body with collapse capabale -->
                </div>
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/bootbox/js/bootbox.min.js') }}}"></script>
<script type="text/javascript">
(function(){
    $(".delete-confirm").on("click", function (event) {
        var self = $(this);
        bootbox.confirm("Are you sure to delete this exception day?", function (result) {
            if(result){
                var excdayid = self.attr('data-id');
                $('button#del-'+excdayid).click();
            }
        });
        event.preventDefault();
    });
})();
</script>
<!-- <script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/components/notification.js') }}}"></script> -->
@stop