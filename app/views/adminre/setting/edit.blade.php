@extends($theme.'._layouts.master')

@section('main')
<section id="main" role="main">
    {{ Notification::showAll() }}

    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">{{{ $page_title }}}</h4>
            </div>
        </div>
        <!-- Page Header -->

        <div class="row">
            <div class="col-md-12">
                {{ Form::model($settings, array('method' => 'put', 'class' => 'panel panel-color-top panel-default form-horizontal form-bordered', 'data-parsley-validate', 'route' => array('setting.update', 1))) }}
                    <div class="panel-heading">
                        <h3 class="panel-title">Settings</h3>
                    </div>               
                    <div class="panel-body">
                        @foreach($settings as $setting)
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{{ $setting->alias }}}</label>
                            <div class="col-sm-6">
                                @if($setting->value_type == 'string')
                                <input type="text" class="form-control" name="{{{ $setting->name }}}" id="{{{ $setting->name }}}" value="{{{ $setting->value }}}" required />
                                @elseif($setting->value_type == 'split')
                                <?php $values = explode(',', $setting->value);?>
                                <a href="{{{ route($values[1]) }}}" title="{{{ $values[0] }}}" class="btn btn-success mb5"><span>{{{ $values[0] }}}</span></a>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Update Settings</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
@stop

@section('additional_scripts')
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/parsley/js/parsley.min.js') }}}"></script>        
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/plugins/selectize/js/selectize.min.js') }}}"></script>
<script type="text/javascript" src="{{{ asset('assets/'.$theme.'/javascript/forms/validation.js') }}}"></script>
@stop