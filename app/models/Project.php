<?php

class Project extends Eloquent
{

    protected $table = 'projects';

    public function masterplans()
    {
        return $this->hasMany('ProjectMasterPlan');
    }

    public function boms()
    {
        return $this->hasMany('ProjectBOM');
    }

    public function customer()
    {
        return $this->belongsTo('Customer', 'customer_id');
    }
}
