<?php

class GeneralOptions extends Eloquent
{

    protected $table = 'general_options';

    public static function getSingleOption($optname = '')
    {
        if ($optname == '')
            return FALSE;

        $optionData = self::where('name', '=', $optname)->take(1)
            ->get()
            ->toArray();

        return @$optionData[0];
    }

    public static function getAllOptions()
    {
        return self::all()->toArray();
    }
}