<?php

class ReportActualingNonMFG extends Eloquent
{

    protected $table = 'report_actualing_non_mfg';

    public function project()
    {
        return $this->belongsTo('Project', 'project_id', 'id');
    }

    public function planning()
    {
        return $this->belongsTo('PlanningNonMFG', 'planning_id', 'id');
    }

    public function actualing()
    {
        return $this->belongsTo('ActualingNonMFG', 'actualing_id', 'id');
    }

    public function process()
    {
        return $this->belongsTo('Process', 'process_id', 'id');
    }

    public function resource()
    {
        return $this->belongsTo('Resource', 'resource_id', 'id');
    }
}
