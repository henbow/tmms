<?php

class ResourceSpec extends Eloquent
{

    protected $table = 'master_resource_spec';

    public static function getByResource($resource_id)
    {
        $spec = self::where('resource_id', '=', $resource_id)->get();

        return @$spec[0];
    }
}