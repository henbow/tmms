<?php

class ReportActualingGeneral extends Eloquent
{

    protected $table = 'report_actualing_general';

    public function project()
    {
        return $this->belongsTo('Project', 'project_id', 'id');
    }

    public function planning()
    {
        return $this->belongsTo('ProjectMasterPlan', 'planning_id', 'id');
    }

    public function process_group()
    {
        return $this->belongsTo('ProcessGroup', 'process_group_id', 'id');
    }
}