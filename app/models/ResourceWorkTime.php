<?php

class ResourceWorkTime extends Eloquent
{

    protected $table = 'master_resource_work_time';

    public static function getByResource($resource_id)
    {
        $workTime = self::where('resource_id', '=', $resource_id);

        return $workTime->count() > 0 ? $workTime->get()[0] : FALSE;
    }
}