<?php

class ResourceActualUsed extends Eloquent
{

    protected $table = 'resource_actual_used';

    public function resource()
    {
        return $this->belongsTo('Resource', 'resource_id', 'id');
    }

}
