<?php

class Department extends Eloquent
{

    protected $table = 'master_department';

    public static function getSingle($id)
    {
        return self::find($id);
    }

    public static function getName($id)
    {
        $dept = self::find($id);

        return $dept->name;
    }

    public static function getAll()
    {
        return self::all();
    }
}