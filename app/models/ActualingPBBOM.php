<?php

class ActualingPBBOM extends Eloquent
{

    protected $table = 'actualing_pb_bom';

    public function planning()
    {
        return $this->belongsTo('PlanningPBBOM', 'planning_id', 'id');
    }

    public function pic()
    {
        return $this->belongsTo('Pic', 'pic_id', 'id');
    }

    public function material()
    {
        return $this->belongsTo('ProjectBOM', 'material_id', 'id');
    }

    public function used_resource()
    {
        return $this->hasOne('ResourceActualUsed', 'actualing_id', 'id');
    }
}
