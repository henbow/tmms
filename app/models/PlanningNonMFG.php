<?php

class PlanningNonMFG extends Eloquent
{

    protected $table = 'project_planning_detail_non_mfg';

    public function masterplan()
    {
        return $this->belongsTo('ProjectMasterPlan', 'master_planning_id', 'id');
    }

    public function actualing()
    {
        return $this->hasOne('ActualingNonMFG', 'planning_id', 'id');
    }

    public function report()
    {
        return $this->hasOne('ReportActualingNonMFG', 'planning_id', 'id');
    }

    public function process()
    {
        return $this->belongsTo('Process', 'process_id');
    }

    public function process_group()
    {
        return $this->belongsTo('ProcessGroup', 'process_group_id');
    }
}
