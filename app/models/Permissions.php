<?php

class Permissions extends Eloquent
{

    protected $table = 'permissions';

    public static function getPermissionNameByCode($code){
        $permission = self::where('code', '=', $code)->take(1)->get();
        return $permission[0]->name;
    }
}