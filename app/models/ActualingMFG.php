<?php

class ActualingMFG extends Eloquent
{

    protected $table = 'actualing_mfg';

    public function planning()
    {
        return $this->belongsTo('PlanningMFG', 'planning_id', 'id');
    }

    public function pic()
    {
        return $this->belongsTo('Pic', 'pic_id', 'id');
    }

    public function logs()
    {
        return $this->hasMany('ActualingMFGLog', 'actualing_id', 'id');
    }

    public function used_resource()
    {
        return $this->hasOne('ResourceActualUsed', 'actualing_id', 'id');
    }
}
