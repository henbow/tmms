<?php

class ProjectBOM extends Eloquent
{

    protected $table = 'project_bom';

    public function project()
    {
        return $this->belongsTo('Project', 'project_id', 'id');
    }

    public function planning_mfg()
    {
        return $this->hasMany('PlanningMFG', 'material_id', 'id');
    }

    public function planning_pb_bom()
    {
        return $this->hasOne('PlanningPBBOM', 'material_id', 'id');
    }

    public function actualing_mfg()
    {
        return $this->hasMany('ActualingMFG', 'material_id', 'id');
    }

    public function actualing_pb_bom()
    {
        return $this->hasMany('ActualingPBBOM', 'material_id', 'id');
    }
}
