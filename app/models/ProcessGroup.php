<?php

class ProcessGroup extends Eloquent
{

    protected $table = 'master_process_group';

    public function processes()
    {
        return $this->hasMany('Process', 'group_id', 'id');
    }

    public function masterplans()
    {
        return $this->hasMany('ProjectMasterPlan', 'process_group_id', 'id');
    }
}
