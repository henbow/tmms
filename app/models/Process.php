<?php

class Process extends Eloquent
{

    protected $table = 'master_process';

    public function group()
    {
        return $this->belongsTo('ProcessGroup', 'group_id');
    }

    public function resource_groups()
    {
        return $this->belongsToMany('ResourceGroup', 'process_resource_group', 'process_id', 'resource_group_id');
    }

    public function report_actualing_mfg()
    {
        return $this->hasMany('ReportActualingMFG', 'process_id', 'id');
    }

    public function planning_non_mfg(){
        return $this->hasMany('PlanningNonMFG', 'process_id', 'id');
    }

    public function planning_mfg(){
        return $this->hasMany('PlanningMFG', 'process_id', 'id');
    }
}
