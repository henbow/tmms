<?php

class ReportActualingPBBOM extends Eloquent
{

    protected $table = 'report_actualing_pb_bom';

    public function project()
    {
        return $this->belongsTo('Project', 'project_id', 'id');
    }

    public function planning()
    {
        return $this->belongsTo('PlanningMFG', 'planning_id', 'id');
    }

    public function actualing()
    {
        return $this->belongsTo('ActualingMFG', 'actualing_id', 'id');
    }

    public function material()
    {
        return $this->belongsTo('ProjectBOM', 'material_id', 'id');
    }

    public function process()
    {
        return $this->belongsTo('Process', 'process_id', 'id');
    }

    public function resource()
    {
        return $this->belongsTo('Resource', 'resource_id', 'id');
    }
}