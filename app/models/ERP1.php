<?php


use Eloquent, DB;

class ERP1 extends Eloquent
{
    public static function getOutPlanTSRData($limit = 20, $limit_type = "")
    {
		$limit_clause = $limit ? "TOP $limit $limit_type" : "";
        $sql = "SELECT $limit_clause
                    tsr.KdFak AS tsr_code,
                    tsr.Urt AS order_num,
                    tsr.Tgl AS tsr_date,
                    tsr.BOMNOPKdFak AS nop,
                    tsr.BOMKdPart AS part_code,
                    tsr.NamaBrgBOM AS part_name,
                    tsr.BarcodeNo AS barcode
                FROM TSR2N tsr
                ORDER BY Tgl DESC";
        return self::runQuery($sql);
    }

    private static function runQuery($sql)
    {
        return DB::connection('sqlsrv')->select($sql);
    }
}