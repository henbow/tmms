<?php

class Resource extends Eloquent
{

    protected $table = 'master_resource';

    public static function getName($id)
    {
    	$res = self::find($id);

    	return $res->name;
    }

    public static function isOffline($id)
    {
        $res = self::find($id);

        return $res->is_offline;
    }

    public static function isHuman($id)
    {
    	$res = self::find($id);

    	return $res->is_human;
    }

    public function group()
    {
        return $this->belongsTo('ResourceGroup', 'group_id', 'id');
    }

    public function schedulesMfg()
    {
        return $this->hasMany('SchedulingMFG', 'resource_id', 'id');
    }

    public function schedulesNonMfg()
    {
        return $this->hasMany('SchedulingNonMFG', 'resource_id', 'id');
    }

    public function actualing()
    {
        $actualing_mfg = ActualingMFG::where('resource_id','=',$this->id)->count();
        $actualing_non_mfg = ActualingNonMFG::where('resource_id','=',$this->id)->count();

        if($actualing_mfg)
        {
            return $this->hasMany('ActualingMFG', 'resource_id', 'id');
        }
        elseif($actualing_non_mfg)
        {
            return $this->hasMany('ActualingNonMFG', 'resource_id', 'id');
        }
        else
        {
            return $this->hasMany('ActualingPBBOM', 'resource_id', 'id');
        }
    }

}
