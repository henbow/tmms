<?php

class ActualingNonMFG extends Eloquent
{

    protected $table = 'actualing_non_mfg';

    public function planning()
    {
        return $this->belongsTo('PlanningNonMFG', 'planning_id', 'id');
    }

    public function pic()
    {
        return $this->belongsTo('Pic', 'pic_id', 'id');
    }

    public function logs()
    {
        return $this->hasMany('ActualingNonMFGLog', 'actualing_id', 'id');
    }

    public function used_resource()
    {
        return $this->hasOne('ResourceActualUsed', 'actualing_id', 'id');
    }
}
