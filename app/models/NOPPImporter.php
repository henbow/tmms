<?php

class NOPPImporter extends Eloquent
{
    public static function getProjects($limit = 20, $limit_type = "")
    {
		$limit_clause = $limit ? " TOP $limit $limit_type " : "";
        $sql = "SELECT $limit_clause
                    p.NOPKdFak AS nop, h.Urt AS order_num, h.KdPrioritas AS priority, h.NamaBrg AS project,
                    h.Qty AS qty, h.POKdFakDT as po_num, h.Hrg AS price_per_unit, h.PONilaiRp AS price_total,
                    p.CustKey AS customer_id, p.POKdFak AS po_num,
                    p.TglStart AS start_date, p.TglTrial AS trial_date, p.TglDelivery AS delivery_date,
                    p.Tgl AS insert_date
                FROM nopp p
                INNER JOIN noppdt h ON p.NOPKdFak=h.NOPKdFak
                WHERE
                    p.NOPKdFak LIKE 'T-%'
                ORDER BY p.NOPKdFak DESC";
        return self::runQuery($sql);
    }

	public static function getProjectByNOP($nop)
    {
        $sql = "SELECT
                    p.NOPKdFak AS nop, h.Urt AS order_num, h.KdPrioritas AS priority, h.NamaBrg AS project,
                    h.Qty AS qty, h.POKdFakDT as po_num, h.Hrg AS price_per_unit, h.PONilaiRp AS price_total,
                    p.CustKey AS customer_id, p.POKdFak AS po_num,
                    p.TglStart AS start_date, p.TglTrial AS trial_date, p.TglDelivery AS delivery_date,
                    p.Tgl AS insert_date
                FROM nopp p
                INNER JOIN noppdt h ON p.NOPKdFak=h.NOPKdFak
                WHERE
                    p.NOPKdFak = '{$nop}'
                ORDER BY p.TglDelivery DESC, p.Tgl DESC";
        return self::runQuery($sql);
    }

    public static function getCustomerByKey($key)
    {
        $sql = "SELECT * FROM Cust WHERE CustKey = '$key'";
        return self::runQuery($sql);
    }

    public static function getBomProject($nop, $order_num = '1')
    {
        $sql = "SELECT
                    NOPKdFak AS nop, Urt AS order_num, KdPrioritas AS priority, KdPart AS part_code,
                    AWLBarcodeNo AS barcode, AWLNamaBrgBOM AS material_name, AWLTipe AS type, AWLStandard AS standard,
                    AWLPanj AS length, AWLLebar AS width, AWLTinggi AS height, AWLDiameterAwl AS diameter_start,
                    AWLDiameterAkh AS diameter_finish, AWLUkuran AS dimension_desc, AWLQtyBOM AS qty, AWLSatuanBrg AS unit,
                    AWLKeterangan AS description, POBeliDTSCKey AS po_number, Tgl AS insert_date
                FROM noppbom WHERE NOPKdFak = '{$nop}' AND Urt = '{$order_num}' AND NamaBrgBOM <> ''";
        return self::runQuery($sql);
    }

    public static function getPOTTB($nop, $order_num = '1')
    {
        $sql = "SELECT
                    a.SCKey AS po_no, a.Urt AS po_order, a.Tgl AS po_date, a.Qty AS po_qty,
                    b.KdFak As ttb_no, b.Urt AS ttb_order, b.Tgl AS ttb_date, a.Qty AS ttb_qty
                FROM POBeliDT a INNER JOIN Ajust2 b ON a.BarcodeNo = b.BarcodeNo
                WHERE a.BOMNOPKdFak = '{$nop}' AND a.BOMUrt = '{$order_num}' AND a.NamaBrgBOM <> ''";
        return self::runQuery($sql);
    }

    private static function runQuery($sql)
    {
        return DB::connection('sqlsrv')->select($sql);
    }
}
