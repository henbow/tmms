<?php

class ResourceGroup extends Eloquent
{

    protected $table = 'master_resource_group';

    public static function getName($id)
    {
        $group = self::find($id);

        return $group->name;
    }

    public function resources()
    {
        return $this->hasMany('Resource', 'group_id', 'id');
    }

    public function process()
    {
        return $this->belongsToMany('Process', 'process_resource_group', 'resource_group_id', 'process_id');
    }
}
