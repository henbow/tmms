<?php

class UserInformation extends Eloquent
{

    protected $table = 'users';

    public static function getFullname($id)
    {
        $user = self::find($id);

        return @$user->first_name . ' ' . @$user->last_name;
    }

    public static function getUsers()
    {
        return self::get();
    }

    public static function getUser($id)
    {
        return self::find($id);
    }

    public static function getUserName($id)
    {
        $user = self::find($id);

        return @$user->username;
    }
}