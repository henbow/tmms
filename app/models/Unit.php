<?php

class Unit extends Eloquent
{

    protected $table = 'master_unit';

    public static function getSingle($id)
    {
        return self::find($id);
    }

    public static function getName($id)
    {
        $unit = self::find($id);

        return $unit->name;
    }

    public static function getAll()
    {
        return self::all();
    }
}