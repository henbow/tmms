<?php

class Pic extends Eloquent
{

    protected $table = 'master_pic';

    public static function getPICFullname($id)
    {
        $user = self::find($id);

        return @$user->first_name . ' ' . @$user->last_name;
    }

    public static function getPICs()
    {
        return self::get();
    }

    public static function getSinglePIC($id)
    {
        return self::find($id);
    }
}