<?php

class PlanningMFG extends Eloquent
{

    protected $table = 'project_planning_detail_mfg';

    public function masterplan()
    {
        return $this->belongsTo('ProjectMasterPlan', 'master_planning_id', 'id');
    }

    public function actualing()
    {
        return $this->hasOne('ActualingMFG', 'planning_id', 'id');
    }

    public function report()
    {
        return $this->hasOne('ReportActualingMFG', 'planning_id', 'id');
    }

    public function bom()
    {
        return $this->belongsTo('ProjectBOM', 'material_id');
    }

    public function process()
    {
        return $this->belongsTo('Process', 'process_id');
    }

    public function process_group()
    {
        return $this->belongsTo('ProcessGroup', 'process_group_id');
    }
}
