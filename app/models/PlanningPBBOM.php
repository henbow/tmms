<?php

class PlanningPBBOM extends Eloquent
{

    protected $table = 'project_planning_detail_pb_bom';

    public function masterplan()
    {
        return $this->belongsTo('ProjectMasterPlan', 'master_planning_id', 'id');
    }
}