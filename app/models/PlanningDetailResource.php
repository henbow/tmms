<?php

class PlanningDetailResource extends Eloquent
{
	protected $table = 'planningdetail_resource';
	public $timestamps = false;

	public function planning_detail_non_mfg() {
		return $this->belongsTo('PlanningNonMFG');
	}

	public function planning_detail_pb_bom() {
		return $this->belongsTo('PlanningPBBOM');
	}

	public function planning_detail_mfg() {
		return $this->belongsTo('PlanningMFG');
	}

	public function resource() {
		return $this->belongsTo('Resource');
	}
}