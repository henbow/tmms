<?php

class ResourcePlanner extends Eloquent
{

    protected $table = 'resource_work_plan';

    public static function isWorkPlanButtonEnabled()
    {
    	$thisYear = date('Y');
    	$workPlan = self::where('year', '=', $thisYear);

    	if($workPlan->count() > 0) return FALSE;

    	return TRUE;
    }

    public static function getWorkTimePlanPerMonth($resource_id, $month, $year)
    {
    	$workPlans = self::where('resource_id', '=', $resource_id)->where('month', '=', $month)->where('year', '=', $year);
    	$totalWorkPlans = 0;

    	if($workPlans->count() > 0)
    	{
    		foreach ($workPlans->get() as $workPlan) {
    			$totalWorkPlans += $workPlan->work_time;
    		}
    	}

    	return $totalWorkPlans;
    }

    public static function getWorkTimePlanPerWeek($resource_id, $week, $month, $year)
    {
        $first_day_of_week = date('z', strtotime("{$year}W{$week}1"));
        debug($first_day_of_week);
        $workPlans = self::where('resource_id', '=', $resource_id)
                        ->where('month', '=', $month)
                        ->where('year', '=', $year);
        $totalWorkPlans = 0;

        if($workPlans->count() > 0)
        {
            foreach ($workPlans->get() as $workPlan) {
                $totalWorkPlans += $workPlan->work_time;
            }
        }

        return $totalWorkPlans;
    }
}
