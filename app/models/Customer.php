<?php

class Customer extends Eloquent
{

    protected $table = 'customers';

    public function user()
    {
        return $this->belongsToMany('User', 'customer_user', 'customer_id', 'user_id');
    }

    public function projects()
    {
        return $this->hasMany('Project', 'customer_id', 'id');
    }
}