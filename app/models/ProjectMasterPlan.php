<?php


class ProjectMasterPlan extends Eloquent
{

    protected $table = 'project_master_plan';

    public function project()
    {
        return $this->belongsTo('Project', 'project_id', 'id');
    }

    public function process_group()
    {
        return $this->belongsTo('ProcessGroup', 'process_group_id', 'id');
    }

    public function has_children()
    {
        return $this->where('parent_id','=',$this->id)->count() > 0 ? true : false;
    }

    public function detail_plans()
    {
        $process_type = $this->process_group()->first()->type;
        if($process_type == 'material')
        {
            return $this->planning_mfg();
        }
        elseif($process_type == 'nop')
        {
            return $this->planning_non_mfg();
        }
        else
        {
            return $this->planning_pb_bom();
        }
    }

    public function planning_mfg()
    {
        return $this->hasMany('PlanningMFG', 'master_planning_id', 'id');
    }

    public function planning_non_mfg()
    {
        return $this->hasMany('PlanningNonMFG', 'master_planning_id', 'id');
    }

    public function planning_pb_bom()
    {
        return $this->hasMany('PlanningPBBOM', 'master_planning_id', 'id');
    }
}
