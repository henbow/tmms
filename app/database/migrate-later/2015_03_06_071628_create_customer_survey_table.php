<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerSurveyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_surveys', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('customer_id');
			$table->mediumText('title');
			$table->enum('type', array('1','2'))->default('1');
			$table->enum('status', array('1','0'))->default('0');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_surveys');
	}

}
