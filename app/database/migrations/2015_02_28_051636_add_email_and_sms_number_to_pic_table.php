<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailAndSmsNumberToPicTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('master_pic', function(Blueprint $table)
		{
			$table->string('email',100)->after('sex');
			$table->string('phone',12)->after('email');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('master_pic', function(Blueprint $table)
		{
			$table->dropColumn('email');
			$table->dropColumn('phone');
		});
	}

}
