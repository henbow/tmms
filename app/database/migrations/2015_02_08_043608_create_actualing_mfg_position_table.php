<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActualingMfgPositionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('actualing_mfg_positions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('position_id')->nullable()->default(0);
			$table->integer('actualing_id')->nullable()->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('actualing_mfg_positions');
	}

}
