<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMasterplanIdToMasterplanPositionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('masterplan_positions', function(Blueprint $table)
		{
			$table->integer('masterplan_id')->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('masterplan_positions', function(Blueprint $table)
		{
			$table->dropColumn('masterplan_id');
		});
	}

}
