<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportEmployeeCompensationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('report_employee_compensations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('lpk_code', 13);
			$table->integer('iqi_id');
			$table->mediumText('sanctions');
			$table->mediumText('penalty');
			$table->integer('hour_losts');
			$table->text('remarks');
			$table->integer('hr_id');
			$table->string('hr_name', 45);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('report_employee_compensations');
	}

}
