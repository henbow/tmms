<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStartStopDateToResourceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('master_resource', function(Blueprint $table)
		{
			$table->dateTime('start_date')->after('offline_reason');
			$table->dateTime('stop_date')->after('start_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('master_resource', function(Blueprint $table)
		{
			$table->dropColumn('start_date');
			$table->dropColumn('stop_date');
		});
	}

}
