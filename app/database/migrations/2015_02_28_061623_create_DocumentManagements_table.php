<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentManagementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('document_managements', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('project_id');
			$table->integer('issue_id');
			$table->mediumText('title');
			$table->text('description');
			$table->string('filename',50);
			$table->string('filetype',5);
			$table->mediumText('path');
			$table->integer('size')->default(0);
			$table->integer('upload_by');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('document_managements');
	}

}
