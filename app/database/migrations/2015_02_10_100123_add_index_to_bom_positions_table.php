<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToBomPositionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bom_positions', function(Blueprint $table)
		{
			$table->index('qty_id');
			$table->index('position_num');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bom_positions', function(Blueprint $table)
		{
			$table->dropIndex('qty_id');
			$table->dropIndex('position_num');
		});
	}

}
