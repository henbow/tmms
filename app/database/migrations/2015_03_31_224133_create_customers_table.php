<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('erp_code', 5);
			$table->string('code', 10);
			$table->mediumText('name');
			$table->text('address');
			$table->string('city', 50);
			$table->text('mailing_address');
			$table->text('tax_address');
			$table->text('contact_person');
			$table->text('phone');
			$table->text('fax');
			$table->text('mobile');
			$table->text('email');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers');
	}

}
