<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderFieldPbbom extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('actualing_pb_bom', function(Blueprint $table)
		{
			$table->integer('po_order')->after('po_number');
			$table->integer('ttb_order')->after('ttb_number');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('actualing_pb_bom', function(Blueprint $table)
		{
			$table->dropColumn('po_order');
			$table->dropColumn('ttb_order');
		});
	}

}
