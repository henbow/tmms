<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameMasterplanIdToQtyIdToMasterplanPositionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('masterplan_positions', function(Blueprint $table)
		{
			$table->renameColumn('masterplan_id', 'qty_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('masterplan_positions', function(Blueprint $table)
		{
			$table->renameColumn('qty_id', 'masterplan_id');
		});
	}

}
