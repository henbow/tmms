<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIssueManagementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('issue_managements', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('issue_code',13);
			$table->integer('project_id');
			$table->integer('owner_id');
			$table->integer('assigned_to');
			$table->mediumText('title');
			$table->text('description');
			$table->dateTime('due_date');
			$table->enum('status', array('open','close'))->default('open');
			$table->integer('priority');
			$table->enum('category', array('issue'))->default('issue');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('issue_managements');
	}

}
