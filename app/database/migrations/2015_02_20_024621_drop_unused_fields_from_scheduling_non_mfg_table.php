<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUnusedFieldsFromSchedulingNonMfgTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('scheduling_non_mfg', function(Blueprint $table)
		{
			$table->dropColumn('process_group_id');
			$table->dropColumn('type');
			$table->dropColumn('project_id');
			$table->dropColumn('process_id');
			$table->dropColumn('is_actualing');
			$table->dropColumn('score');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('scheduling_non_mfg', function(Blueprint $table)
		{
			$table->integer('process_group_id')->after('id');
			$table->string('type', 45)->nullable()->after('process_group_id');
			$table->integer('project_id')->nullable()->after('type');
			$table->integer('process_id')->nullable()->after('project_id');
			$table->integer('is_actualing')->nullable()->after('predecessor');
			$table->double('score', 8, 2)->nullable()->after('is_actualing');
		});
	}

}
