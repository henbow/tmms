<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActualingPbBomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('actualing_pb_bom', function(Blueprint $table)
		{
			$table->string('status', 8)->after('po_ttb_time')->default('COMPLETE');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('actualing_pb_bom', function(Blueprint $table)
		{
			$table->dropColumn('status');
		});
	}

}
