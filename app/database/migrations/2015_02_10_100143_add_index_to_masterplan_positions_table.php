<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToMasterplanPositionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('masterplan_positions', function(Blueprint $table)
		{
			$table->index('masterplan_id');
			$table->index('position_num');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('masterplan_positions', function(Blueprint $table)
		{
			$table->dropIndex('masterplan_id');
			$table->dropIndex('position_num');
		});
	}

}
