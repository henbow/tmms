<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToBomQuantitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bom_quantities', function(Blueprint $table)
		{
			$table->index('planning_id');
			$table->index('qty_num');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bom_quantities', function(Blueprint $table)
		{
			$table->dropIndex('planning_id');
			$table->dropIndex('qty_num');
		});
	}

}
