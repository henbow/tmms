<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HrdevToLpkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('report_employee_compensations', function(Blueprint $table)
		{
			$table->text('hr_dev');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('report_employee_compensations', function(Blueprint $table)
		{
			$table->dropColumn('hr_dev');
		});
	}

}
