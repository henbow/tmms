<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActualingReportNonMFGTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('report_actualing_non_mfg', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('project_id');
			$table->integer('planning_id');
			$table->integer('actualing_id');
			$table->integer('process_id');
			$table->integer('resource_id');
			$table->integer('estimation_hour');
			$table->integer('actualing_hour');
			$table->enum('status', array('COMPLETE','WAITING'))->default('WAITING');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('report_actualing_non_mfg');
	}

}
