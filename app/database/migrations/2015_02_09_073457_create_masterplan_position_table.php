<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterplanPositionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('masterplan_positions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('masterplan_id')->nullable()->default(0);
			$table->integer('position_num')->nullable()->default(0);
			$table->mediumText('description');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('masterplan_positions');
	}

}
