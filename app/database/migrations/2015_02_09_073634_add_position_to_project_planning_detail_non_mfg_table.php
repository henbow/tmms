<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPositionToProjectPlanningDetailNonMfgTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('project_planning_detail_non_mfg', function(Blueprint $table)
		{
			$table->integer('position')->nullable()->default(0)->after('priority');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('project_planning_detail_non_mfg', function(Blueprint $table)
		{
			$table->dropColumn('position');
		});
	}

}
