<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertUserTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alert_setting_users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('setting_id');
			$table->integer('user_id');
			$table->enum('send_email', array('1', '0'))->default('1');
			$table->enum('send_sms', array('1', '0'))->default('1');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('alert_setting_users');
	}

}
