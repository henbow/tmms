<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameMaterialIdInBomQuantitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bom_quantities', function(Blueprint $table)
		{
			$table->renameColumn('material_id', 'planning_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bom_quantities', function(Blueprint $table)
		{
			$table->renameColumn('planning_id', 'material_id');
		});
	}

}
