<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActualingPbBomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('actualing_pb_bom', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('planning_id');
			$table->integer('material_id');
			$table->string('po_number', 100);
			$table->date('po_date');
			$table->integer('po_qty');
			$table->string('ttb_number', 100);
			$table->date('ttb_date');
			$table->integer('ttb_qty');
			$table->integer('po_ttb_time');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('actualing_pb_bom');
	}

}
