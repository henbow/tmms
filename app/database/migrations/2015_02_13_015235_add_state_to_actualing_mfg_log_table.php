<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStateToActualingMfgLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('actualing_mfg_log', function(Blueprint $table)
		{
			$table->enum('state', array('START', 'ONPROCESS', 'COMPLETE'))->after('pic_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('actualing_mfg_log', function(Blueprint $table)
		{
			$table->dropColumn('state');
		});
	}

}
