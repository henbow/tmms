<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternalQualityIssuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('internal_quality_issues', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('project_id');
			$table->integer('material_id');
			$table->integer('qc_staff_id');
			$table->integer('problem_maker_id');
			$table->text('description');
			$table->text('possible_causes');
			$table->mediumText('action');
			$table->integer('pic_dept');
			$table->text('remark');
			$table->enum('status', array('open', 'close'))->default('open');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('internal_quality_issues');
	}

}
