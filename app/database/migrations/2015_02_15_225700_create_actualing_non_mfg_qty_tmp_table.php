<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActualingNonMfgQtyTmpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('actualing_non_mfg_qty_tmp', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('actualing_id')->default(0);
			$table->integer('qty_id')->nullable()->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('actualing_non_mfg_qty_tmp');
	}

}
