<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterResourceActualUsedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('resource_actual_used', function(Blueprint $table)
		{
			$table->dropColumn('is_used');
			$table->string('type', 8)->after('actualing_id')->default('material');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('resource_actual_used', function(Blueprint $table)
		{
			$table->integer('is_used')->after('estimation');
			$table->dropColumn('type');
		});
	}

}
