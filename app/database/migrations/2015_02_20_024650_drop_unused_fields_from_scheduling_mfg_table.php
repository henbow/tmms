<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUnusedFieldsFromSchedulingMfgTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('scheduling_mfg', function(Blueprint $table)
		{
			$table->dropColumn('material_id');
			$table->dropColumn('project_id');
			$table->dropColumn('process_id');
			$table->dropColumn('is_actualing');
			$table->dropColumn('score');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('scheduling_mfg', function(Blueprint $table)
		{
			$table->integer('material_id')->after('id');
			$table->integer('project_id')->nullable()->after('material_id');
			$table->integer('process_id')->nullable()->after('project_id');
			$table->integer('is_actualing')->nullable()->after('predecessor');
			$table->double('score', 8, 2)->nullable()->after('is_actualing');
		});
	}

}
