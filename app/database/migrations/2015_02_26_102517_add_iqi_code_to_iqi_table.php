<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIqiCodeToIqiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('internal_quality_issues', function(Blueprint $table)
		{
			$table->string('iqi_code',14)->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('internal_quality_issues', function(Blueprint $table)
		{
			$table->dropColumn('iqi_code');
		});
	}

}
