<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToActualingNonMfgPositionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('actualing_non_mfg_positions', function(Blueprint $table)
		{
			$table->enum('status', array('ONPROCESS', 'COMPLETE'))->default('ONPROCESS');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('actualing_non_mfg_positions', function(Blueprint $table)
		{
			$table->dropColumn('status');
			$table->dropTimestamps();
		});
	}

}
