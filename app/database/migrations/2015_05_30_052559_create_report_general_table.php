<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportGeneralTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('report_actualing_general', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('project_id');
			$table->integer('planning_id');
			$table->integer('process_group_id');
			$table->double('progress', 5, 2);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('report_actualing_general');
	}

}
