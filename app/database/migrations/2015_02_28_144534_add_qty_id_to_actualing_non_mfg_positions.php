<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQtyIdToActualingNonMfgPositions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('actualing_non_mfg_positions', function(Blueprint $table)
		{
			$table->integer('qty_id')->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('actualing_non_mfg_positions', function(Blueprint $table)
		{
			$table->dropColumn('qty_id');
		});
	}

}
