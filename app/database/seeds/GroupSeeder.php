<?php

class GroupSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->truncate();
        DB::table('groups')->insert(array(

            array(
                'id' => 1,
                'name' => 'Administrator',
                'permissions' => json_encode(array(
                   'menu-1' => 1,
                   'menu-6' => 1,
                   'menu-8' => 1,
                   'menu-7' => 1,
                   'menu-35' => 1,
                   'menu-18' => 1,
                   'menu-36' => 1,
                   'menu-41' => 1,
                   'menu-31' => 1,
                   'menu-19' => 1,
                   'menu-20' => 1,
                   'menu-33' => 1,
                   'menu-30' => 1,
                   'menu-43' => 1,
                   'menu-42' => 1,
                   'menu-32' => 1,
                   'menu-21' => 1,
                   'menu-23' => 1,
                   'menu-25' => 1,
                   'menu-45' => 1,
                   'menu-47' => 1,
                   'menu-46' => 1,
                   'menu-26' => 1,
                   'menu-27' => 1,
                   'menu-28' => 1,
                   'menu-44' => 1,
                   'menu-3' => 1,
                   'menu-4' => 1,
                   'menu-5' => 1,
                   'menu-9' => 1,
                   'menu-10' => 1,
                   'menu-11' => 1,
                   'menu-38' => 1,
                   'menu-15' => 1,
                   'menu-16' => 1,
                   'menu-17' => 1,
                   'menu-39' => 1,
                   'menu-13' => 1,
                   'menu-14' => 1,
                   'menu-40' => 1,
                   'menu-37' => 1,
                   'menu-48' => 1,
                   'menu-49' => 1,
                   'menu-50' => 1,
                   'menu-51' => 1,
                   'menu-52' => 1,
                   'menu-53' => 1,
                   'menu-54' => 1,
                   'menu-55' => 1,
                   'menu-56' => 1,
                   'menu-57' => 1,
                   'menu-58' => 1,
                   'menu-59' => 1,
                   'menu-60' => 1,
                   'menu-61' => 1,
                   'menu-62' => 1,
                   'menu-63' => 1,
                   'menu-64' => 1,
                   'menu-65' => 1,
                   'menu-66' => 1,
                   'menu-67' => 1,
                   'menu-68' => 1,
                   'menu-69' => 1,
                ))
            ),

            array(
                'id' => 2,
                'name' => 'Manager',
                'permissions' => json_encode(array(
                   'menu-1' => 1,
                   'menu-6' => 1,
                   'menu-8' => 1,
                   'menu-7' => 1,
                   'menu-35' => 1,
                   'menu-18' => 1,
                   'menu-36' => 1,
                   'menu-41' => 1,
                   'menu-31' => 1,
                   'menu-19' => 1,
                   'menu-20' => 1,
                   'menu-33' => 1,
                   'menu-30' => 1,
                   'menu-43' => 1,
                   'menu-42' => 1,
                   'menu-32' => 1,
                   'menu-21' => 1,
                   'menu-23' => 1,
                   'menu-25' => 1,
                   'menu-45' => 1,
                   'menu-47' => 1,
                   'menu-46' => 1,
                   'menu-26' => 1,
                   'menu-27' => 1,
                   'menu-28' => 1,
                   'menu-44' => 1,
                   'menu-3' => 1,
                   'menu-4' => 1,
                   'menu-5' => 1,
                   'menu-48' => 1,
                   'menu-49' => 1,
                   'menu-53' => 1,
                   'menu-62' => 1,
                   'menu-63' => 1,
                   'menu-64' => 1,
                   'menu-65' => 1,
                   'menu-66' => 1,
                   'menu-67' => 1,
                   'menu-68' => 1,
                   'menu-69' => 1,
                ))
            ),

            array(
                'id' => 4,
                'name' => 'Customer',
                'permissions' => json_encode(array(
                    "menu-1" => 1
                ))
            ),

            array(
                'id' => 6,
                'name' => 'Operator mesin',
                'permissions' => json_encode(array(
                   'menu-1' => 1,
                   'menu-21' => 1,
                   'menu-23' => 1,
                   'menu-47' => 1,
                   'menu-3' => 1,
                   'menu-4' => 1,
                   'menu-5' => 1,
                ))
            ),

            array(
                'id' => 7,
                'name' => 'Staff PPC',
                'permissions' => json_encode(array(
                   'menu-1' => 1,
                   'menu-6' => 1,
                   'menu-8' => 1,
                   'menu-7' => 1,
                   'menu-18' => 1,
                   'menu-36' => 1,
                   'menu-41' => 1,
                   'menu-31' => 1,
                   'menu-19' => 1,
                   'menu-20' => 1,
                   'menu-30' => 1,
                   'menu-43' => 1,
                   'menu-42' => 1,
                   'menu-32' => 1,
                   'menu-21' => 1,
                   'menu-23' => 1,
                   'menu-25' => 1,
                   'menu-45' => 1,
                   'menu-46' => 1,
                   'menu-26' => 1,
                   'menu-27' => 1,
                   'menu-28' => 1,
                   'menu-44' => 1,
                   'menu-3' => 1,
                   'menu-4' => 1,
                   'menu-5' => 1,
                ))
            ),

            array(
                'id' => 8,
                'name' => 'Staff CADCAM',
                'permissions' => json_encode(array(
                   'menu-31' => 1,
                   'menu-19' => 1,
                   'menu-32' => 1,
                   'menu-21' => 1,
                   'menu-23' => 1,
                   'menu-25' => 1,
                   'menu-45' => 1,
                   'menu-3' => 1,
                   'menu-4' => 1,
                   'menu-5' => 1,
                ))
            ),


));
    }
}
