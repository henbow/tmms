<?php

class MenuSeeder extends Seeder {
    public function run()
    {
        DB::table('menu')->truncate();
        DB::table('menu')->insert(array(

            array('id' => 1,  'order' => 10, 'parent_id' => 0,  'menu_name' => 'Dashboard', 'alias' => 'home', 'route' => 'dashboard.index', 'param' => NULL, 'class_attr' => 'ico-home2', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            array('id' => 6,  'order' => 11, 'parent_id' => 0,  'menu_name' => 'Resource Center', 'alias' => 'resource_center', 'route' => '#', 'param' => NULL, 'class_attr' => 'ico-group', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 8,  'order' => 12, 'parent_id' => 6,  'menu_name' => 'Resource List', 'alias' => 'resource_list', 'route' => 'resource.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 7,  'order' => 13, 'parent_id' => 6,  'menu_name' => 'Resource Planner', 'alias' => 'resource_planner', 'route' => 'resource_planner.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 72, 'order' => 14, 'parent_id' => 6,  'menu_name' => 'Resource Monitor', 'alias' => 'resource_monitor', 'route' => 'resource_monitor.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 73, 'order' => 15, 'parent_id' => 6,  'menu_name' => 'Resource Manager', 'alias' => 'resource_manager', 'route' => 'resource_manager.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 35, 'order' => 16, 'parent_id' => 6,  'menu_name' => '[LINE]', 'alias' => 'line', 'route' => NULL, 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 18, 'order' => 17, 'parent_id' => 6,  'menu_name' => 'Group', 'alias' => 'group', 'route' => 'resource_group.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 36, 'order' => 18, 'parent_id' => 6,  'menu_name' => 'Work Time Template', 'alias' => 'work_time_template', 'route' => 'work_time_template.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 41, 'order' => 19, 'parent_id' => 6,  'menu_name' => 'Exception Days', 'alias' => 'exception_days', 'route' => 'exception_days.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),


            array('id' => 31, 'order' => 21, 'parent_id' => 0,  'menu_name' => 'Planning', 'alias' => 'planning', 'route' => '#', 'param' => NULL, 'class_attr' => 'ico-th-list', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 19, 'order' => 22, 'parent_id' => 31, 'menu_name' => 'Project Planning', 'alias' => 'project_planning', 'route' => 'project.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 20, 'order' => 23, 'parent_id' => 31, 'menu_name' => 'Project Scheduling', 'alias' => 'project_scheduling', 'route' => 'scheduling.index', 'param' => NULL, 'class_attr' => 'ico-calendar', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 33, 'order' => 24, 'parent_id' => 31, 'menu_name' => '[LINE]', 'alias' => NULL, 'route' => '#', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 30, 'order' => 25, 'parent_id' => 31, 'menu_name' => 'Master Plan Template', 'alias' => 'master_plan_template', 'route' => 'template_master_plan.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 43, 'order' => 26, 'parent_id' => 31, 'menu_name' => 'Detail Plan Template', 'alias' => 'detail_plan_template', 'route' => 'template_mfg_process.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 42, 'order' => 27, 'parent_id' => 31, 'menu_name' => 'Master Plan Group', 'alias' => 'master_plan_group', 'route' => 'process_group.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 32, 'order' => 28, 'parent_id' => 31, 'menu_name' => 'Detail Plan Process', 'alias' => 'detail_plan_process', 'route' => 'processes.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),


            array('id' => 21, 'order' => 30, 'parent_id' => 0,  'menu_name' => 'Actualing', 'alias' => 'actualing', 'route' => '#', 'param' => NULL, 'class_attr' => 'ico-qrcode', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 23, 'order' => 31, 'parent_id' => 21, 'menu_name' => 'IN PLAN', 'alias' => 'in_plan', 'route' => '#', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 25, 'order' => 32, 'parent_id' => 23, 'menu_name' => 'By NOP', 'alias' => 'by_nop', 'route' => 'actualing.by_type.inplan', 'param' => 'nop,null', 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 45, 'order' => 33, 'parent_id' => 23, 'menu_name' => 'By Material Human', 'alias' => 'by_material_1', 'route' => 'actualing.by_type.inplan', 'param' => 'material,1', 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 47, 'order' => 34, 'parent_id' => 23, 'menu_name' => 'By Material Machine', 'alias' => 'by_material_2', 'route' => 'actualing.by_type.inplan', 'param' => 'material,2', 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 46, 'order' => 35, 'parent_id' => 23, 'menu_name' => 'PB-BOM', 'alias' => 'pb_bom', 'route' => 'actualing.by_type.inplan', 'param' => 'pb-bom,null', 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 51, 'order' => 36, 'parent_id' => 23, 'menu_name' => '[LINE]', 'alias' => NULL, 'route' => '#', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 52, 'order' => 37, 'parent_id' => 23, 'menu_name' => 'Internal Quality Issues', 'alias' => 'iqi', 'route' => 'iqi.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 26, 'order' => 38, 'parent_id' => 21, 'menu_name' => 'OUT PLAN', 'alias' => 'out_plan', 'route' => '#', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 27, 'order' => 39, 'parent_id' => 26, 'menu_name' => 'By NOP', 'alias' => 'by_nop', 'route' => 'actualing.by_type.outplan', 'param' => 'nop', 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 28, 'order' => 40, 'parent_id' => 26, 'menu_name' => 'By Material', 'alias' => 'by_material', 'route' => 'actualing.by_type.outplan', 'param' => 'material', 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),


            array('id' => 3,  'order' => 41, 'parent_id' => 0,  'menu_name' => 'Helpdesk', 'alias' => 'helpdesk', 'route' => '#', 'param' => NULL, 'class_attr' => 'ico-bubbles5', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 4,  'order' => 42, 'parent_id' => 3,  'menu_name' => 'My Requests', 'alias' => 'my_requests', 'route' => 'helpdesk.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 5,  'order' => 43, 'parent_id' => 3,  'menu_name' => 'Requests From Users', 'alias' => 'request_lists', 'route' => 'helpdesk_request.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),


            array('id' => 54, 'order' => 50, 'parent_id' => 0,  'menu_name' => 'Issue Management', 'alias' => 'issue_management', 'route' => '#', 'param' => NULL, 'class_attr' => 'ico-bubble-notification', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 55, 'order' => 51, 'parent_id' => 54, 'menu_name' => 'Issue Lists', 'alias' => 'issue_lists', 'route' => 'issue_management.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),


            array('id' => 56, 'order' => 55, 'parent_id' => 0,  'menu_name' => 'Documents', 'alias' => 'doc_management', 'route' => '#', 'param' => NULL, 'class_attr' => 'ico-files', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 57, 'order' => 56, 'parent_id' => 56, 'menu_name' => 'Document Lists', 'alias' => 'doc_lists', 'route' => 'document.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),


            array('id' => 48, 'order' => 60, 'parent_id' => 0,  'menu_name' => 'Self Services', 'alias' => 'self_services', 'route' => '#', 'param' => NULL, 'class_attr' => 'ico-group', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 49, 'order' => 61, 'parent_id' => 48, 'menu_name' => 'Filter Project Data', 'alias' => 'filter_project_data', 'route' => 'project.filter', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 50, 'order' => 62, 'parent_id' => 48, 'menu_name' => 'Edit Actualing Data', 'alias' => 'edit_actualing_data', 'route' => 'actualing.edit_data', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 61, 'order' => 63, 'parent_id' => 48, 'menu_name' => 'Reset Actualing Data', 'alias' => 'edit_actualing_data', 'route' => 'actualing.reset_data', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),


            array('id' => 70, 'order' => 70, 'parent_id' => 0,  'menu_name' => 'Project Cost Control', 'alias' => 'project_cost_control_parent', 'route' => '#', 'param' => NULL, 'class_attr' => 'ico-coins', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 71, 'order' => 71, 'parent_id' => 70,  'menu_name' => 'Project Cost Control', 'alias' => 'project_cost_control', 'route' => 'cost_control.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),


            array('id' => 58, 'order' => 73, 'parent_id' => 0,  'menu_name' => 'Customer', 'alias' => 'customer', 'route' => '#', 'param' => NULL, 'class_attr' => 'ico-address-book', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 59, 'order' => 74, 'parent_id' => 58, 'menu_name' => 'Reports', 'alias' => 'customer_report', 'route' => 'report.customer', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 60, 'order' => 75, 'parent_id' => 58, 'menu_name' => 'Survey', 'alias' => 'customer_survey', 'route' => 'survey.customer', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),


            array('id' => 44, 'order' => 80, 'parent_id' => 0,  'menu_name' => 'Reports', 'alias' => 'report', 'route' => '#', 'param' => NULL, 'class_attr' => 'ico-pie2', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 53, 'order' => 81, 'parent_id' => 44, 'menu_name' => 'Resource', 'alias' => 'report_resource', 'route' => '#', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 62, 'order' => 82, 'parent_id' => 53, 'menu_name' => 'Resource Availability', 'alias' => 'report_resource_availability', 'route' => 'report.resource_availability', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 63, 'order' => 83, 'parent_id' => 53, 'menu_name' => 'Resource Actual Used', 'alias' => 'report_resource_actual_used', 'route' => 'report.resource_actual_used', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 64, 'order' => 84, 'parent_id' => 44, 'menu_name' => 'Planning', 'alias' => 'report_planning', 'route' => '#', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 65, 'order' => 85, 'parent_id' => 64, 'menu_name' => 'Masterplan', 'alias' => 'report_planning_masterplan', 'route' => '#', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 66, 'order' => 86, 'parent_id' => 64, 'menu_name' => 'Per Project', 'alias' => 'report_planning_per_project', 'route' => '#', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 67, 'order' => 87, 'parent_id' => 64, 'menu_name' => 'Per Resource Group', 'alias' => 'report_planning_per_rg', 'route' => '#', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 68, 'order' => 88, 'parent_id' => 64, 'menu_name' => 'Gantt Chart', 'alias' => 'report_planning_gantt', 'route' => '#', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 69, 'order' => 89, 'parent_id' => 44, 'menu_name' => 'Actualing', 'alias' => 'report_actualing', 'route' => 'report.actualing', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),

            array('id' => 9,  'order' => 100, 'parent_id' => 0,  'menu_name' => 'Sysadmin', 'alias' => 'sysadmin', 'route' => '#', 'param' => NULL, 'class_attr' => 'ico-cogs2', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 10, 'order' => 101, 'parent_id' => 9,  'menu_name' => 'Users', 'alias' => 'user', 'route' => 'user.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 11, 'order' => 102, 'parent_id' => 9,  'menu_name' => 'User Level', 'alias' => 'user_level', 'route' => 'userlevel.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 38, 'order' => 103, 'parent_id' => 9,  'menu_name' => '[LINE]', 'alias' => 'line', 'route' => NULL, 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 15, 'order' => 104, 'parent_id' => 9,  'menu_name' => 'PIC', 'alias' => 'pic', 'route' => 'pic.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 16, 'order' => 105, 'parent_id' => 9,  'menu_name' => 'Unit Kerja', 'alias' => 'unit_kerja', 'route' => 'unit.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 17, 'order' => 106, 'parent_id' => 9,  'menu_name' => 'Department', 'alias' => 'department', 'route' => 'department.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 39, 'order' => 107, 'parent_id' => 9,  'menu_name' => '[LINE]', 'alias' => 'line', 'route' => NULL, 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 13, 'order' => 108, 'parent_id' => 9,  'menu_name' => 'Application Setting', 'alias' => 'setting', 'route' => 'setting.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 14, 'order' => 109, 'parent_id' => 9,  'menu_name' => 'Alert Settings', 'alias' => 'alert_setting', 'route' => 'alert_setting.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 40, 'order' => 110, 'parent_id' => 9,  'menu_name' => '[LINE]', 'alias' => 'line', 'route' => NULL, 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),
            array('id' => 37, 'order' => 111, 'parent_id' => 9,  'menu_name' => 'Menu', 'alias' => 'menu', 'route' => 'menu.index', 'param' => NULL, 'class_attr' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),),

        ));
    }
}
