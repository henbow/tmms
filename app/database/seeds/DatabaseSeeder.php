<?php

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('MenuSeeder');
        $this->command->info('Menu table seeded!');

        // $this->call('GroupSeeder');
        // $this->command->info('User group table seeded!');
    }
}
