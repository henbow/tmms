<?php


class GeneralOptionsSeeder extends Seeder {

	public function run()
	{
		DB::table('general_options')->truncate();
        DB::table('general_options')->insert(array(
        	array('id' => '1', 'name' => 'app_name','alias' => 'Application Name','value' => 'Tridaya Manufacturing Management System','value_type' => 'string','created_at' => '0000-00-00 00:00:00','updated_at' => '2014-08-22 20:23:45','updated_by' => NULL),
			array('id' => '2', 'name' => 'company_name','alias' => 'Company Name','value' => 'PT. Tridaya Artaguna Santara','value_type' => 'string','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00','updated_by' => NULL),
			array('id' => '3', 'name' => 'company_address','alias' => 'Company Address','value' => 'Jl. Tekno 2, Blok F2/5, Taman Tekno BSD','value_type' => 'string','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00','updated_by' => NULL),
			array('id' => '4', 'name' => 'company_phone','alias' => 'Company Phone','value' => '021-7561395','value_type' => 'string','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00','updated_by' => NULL),
			array('id' => '5', 'name' => 'company_fax','alias' => 'Company Fax','value' => '021-7561435','value_type' => 'string','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00','updated_by' => NULL),
			array('id' => '6', 'name' => 'company_email','alias' => 'Company Email','value' => 'marketing@tridaya-as.com','value_type' => 'string','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00','updated_by' => NULL),
			array('id' => '7', 'name' => 'company_website','alias' => 'Company Website','value' => 'http://www.tridaya-as.com/','value_type' => 'string','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00','updated_by' => NULL),
			array('id' => '8', 'name' => 'active_years','alias' => 'Active Year','value' => '2','value_type' => 'string','created_at' => '0000-00-00 00:00:00','updated_at' => '2014-08-22 20:23:45','updated_by' => NULL),
			array('id' => '9', 'name' => 'next_process_delay','alias' => 'Next Process Delay','value' => '1','value_type' => 'string','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00','updated_by' => NULL),
			array('id' => '10','name' => 'document_path','alias' => 'Document Path','value' => 'public/documents/','value_type' => 'string','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00','updated_by' => NULL)
    	));
	}

}