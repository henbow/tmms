<?php namespace App\Services\Validators;

class UserLevelValidator extends Validator {

	public static $rules = array(
		'levelname' => 'required',
		'permissions'  => 'required',
	);

}
