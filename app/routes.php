<?php
Event::listen('illuminate.query', function ($sql) {
    // var_dump($sql);
});

Route::get('/current_environment', array(
    'as' => 'test.environment',
    'uses' => 'TestController@environment'
));

Route::get('auth/logout', array(
    'as' => 'auth.logout',
    'uses' => 'AuthController@getLogout'
));
Route::get('auth/login', array(
    'as' => 'auth.login',
    'uses' => 'AuthController@getLogin'
));
Route::post('auth/login', array(
    'as' => 'auth.login.post',
    'uses' => 'AuthController@postLogin'
));

Route::group(array(
    'prefix' => 'project_importer'
        ), function () {
    Route::get('/', array(
        'as' => 'project_importer.index',
        'uses' => 'ProjectImporterController@index'
    ));
    Route::get('/sync_data_from_erp', array(
        'as' => 'project_importer.import_to_db',
        'uses' => 'ProjectImporterController@sync_data_from_erp'
    ));
});

Route::group(array('before' => 'auth.apps'), function () {
    Route::any('/', 'DashboardController@index');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::resource('user', 'UserController');
    Route::resource('userlevel', 'UserLevelController');
    Route::resource('department', 'DepartmentController');
    Route::resource('unit', 'UnitController');
    Route::resource('pic', 'PicController');
    Route::resource('resource', 'ResourceController');
    Route::resource('resource_group', 'ResourceGroupController');
    Route::resource('resource_planner', 'ResourcePlannerController');
    Route::resource('resource_monitor', 'ResourceMonitorController');
    Route::resource('resource_manager', 'ResourceManagerController');
    Route::resource('setting', 'SettingController');
    Route::resource('alert_setting', 'AlertSettingsController');
    Route::resource('email_recipient', 'SettingController');
    Route::resource('helpdesk', 'HelpDeskController');
    Route::resource('helpdesk_request', 'HelpDeskRequestController');
    Route::resource('processes', 'ProjectProcessController');
    Route::resource('process_group', 'ProcessGroupController');
    Route::resource('template_master_plan', 'TemplateMasterPlanController');
    Route::resource('template_mfg_process', 'TemplateMfgProcessController');
    Route::resource('work_time_template', 'ResourceWorkTimeTemplateController');
    Route::resource('menu', 'MenuController');
    Route::resource('exception_days', 'ExceptionDaysController');
    // Route::resource('issue_management', 'IssueManagementsController');
    Route::resource('document', 'DocumentManagementsController');

    // Route::group(array('prefix' => 'issue_management'), function () {
    //     Route::get('/sel', array(
    //         'as' => 'issue_management.select',
    //         'uses' => 'iqi@select_project'
    //     ));

    // });

    Route::group(array(
        'prefix' => 'template_master_plan/detail'
            ), function () {
        Route::get('/{header_id}/index', array(
            'as' => 'template_master_plan_detail.index',
            'uses' => 'TemplateMasterPlanDetailController@index'
        ));
        Route::get('/{header_id}/create', array(
            'as' => 'template_master_plan_detail.create',
            'uses' => 'TemplateMasterPlanDetailController@create'
        ));
        Route::get('/{header_id}/{detail_id}/create/child', array(
            'as' => 'template_master_plan_detail.create.child',
            'uses' => 'TemplateMasterPlanDetailController@create_child'
        ));
        Route::post('/{header_id}/store', array(
            'as' => 'template_master_plan_detail.store',
            'uses' => 'TemplateMasterPlanDetailController@store'
        ));
        Route::post('/{header_id}/{detail_id}/store', array(
            'as' => 'template_master_plan_detail.store.child',
            'uses' => 'TemplateMasterPlanDetailController@store_child'
        ));
        Route::get('/{detail_id}/edit', array(
            'as' => 'template_master_plan_detail.edit',
            'uses' => 'TemplateMasterPlanDetailController@edit'
        ));
        Route::post('/{detail_id}/update', array(
            'as' => 'template_master_plan_detail.update',
            'uses' => 'TemplateMasterPlanDetailController@update'
        ));
        Route::delete('/{detail_id}/destroy', array(
            'as' => 'template_master_plan_detail.destroy',
            'uses' => 'TemplateMasterPlanDetailController@destroy'
        ));
    });

    Route::group(array(
        'prefix' => 'template_mfg_process/detail'
            ), function () {
        Route::get('/{header_id}/index', array(
            'as' => 'template_mfg_process_detail.index',
            'uses' => 'TemplateMfgProcessDetailController@index'
        ));
        Route::get('/{header_id}/create', array(
            'as' => 'template_mfg_process_detail.create',
            'uses' => 'TemplateMfgProcessDetailController@create'
        ));
        Route::get('/{header_id}/{detail_id}/create/child', array(
            'as' => 'template_mfg_process_detail.create.child',
            'uses' => 'TemplateMfgProcessDetailController@create_child'
        ));
        Route::post('/{header_id}/store', array(
            'as' => 'template_mfg_process_detail.store',
            'uses' => 'TemplateMfgProcessDetailController@store'
        ));
        Route::get('/{detail_id}/edit', array(
            'as' => 'template_mfg_process_detail.edit',
            'uses' => 'TemplateMfgProcessDetailController@edit'
        ));
        Route::put('/{detail_id}/update', array(
            'as' => 'template_mfg_process_detail.update',
            'uses' => 'TemplateMfgProcessDetailController@update'
        ));
        Route::delete('/{detail_id}/destroy', array(
            'as' => 'template_mfg_process_detail.destroy',
            'uses' => 'TemplateMfgProcessDetailController@destroy'
        ));
    });

    Route::get('template_mfg_process/excel/import', array(
        'as' => 'template_mfg_process.excel.import',
        'uses' => 'TemplateMfgProcessController@import_excel'
    ));

    Route::post('template_mfg_process/excel/do_import', array(
        'as' => 'template_mfg_process.excel.do_import',
        'uses' => 'TemplateMfgProcessController@do_import_excel'
    ));

    Route::get('/', array(
        'as' => 'dashboard.index',
        'uses' => 'DashboardController@index'
    ));

    Route::get('resource/{id}/set_offline', array(
        'as' => 'resource_manager.set_offline',
        'uses' => 'ResourceManagerController@offline'
    ));

    Route::get('resource_planner/{year}/update', array(
        'as' => 'resource_planner.update.by_year',
        'uses' => 'ResourcePlannerController@create'
    ));

    Route::get('resource_planner/{year}/index', array(
        'as' => 'resource_planner.by_year',
        'uses' => 'ResourcePlannerController@index'
    ));

    Route::get('resource_planner/{resourceid}/{month}/{year}/work_plan', array(
        'as' => 'resource_planner.work_plan',
        'uses' => 'ResourcePlannerController@work_plan_per_month'
    ));

    Route::post('resource_planner/update_work_time_plan', array(
        'as' => 'resource_planner.update_work_time_plan',
        'uses' => 'ResourcePlannerController@update_work_time_plan'
    ));

    Route::post('helpdesk/{requestid}/add_note', array(
        'as' => 'helpdesk.add_note',
        'uses' => 'HelpDeskController@add_note'
    ));

    Route::post('helpdesk_request/{requestid}/update_status', array(
        'as' => 'helpdesk_request.update_status',
        'uses' => 'HelpDeskRequestController@update_status'
    ));

    Route::post('helpdesk_request/{requestid}/add_note', array(
        'as' => 'helpdesk_request.add_note',
        'uses' => 'HelpDeskRequestController@add_note'
    ));

    Route::group(array(
        'prefix' => 'resource_specification'
            ), function () {
        Route::get('/{resourceid}', array(
            'as' => 'resource_spec.index',
            'uses' => 'ResourceSpecificationController@index'
        ));
        Route::get('/{resourceid}/create', array(
            'as' => 'resource_spec.create',
            'uses' => 'ResourceSpecificationController@create'
        ));
        Route::post('/{resourceid}/store', array(
            'as' => 'resource_spec.store',
            'uses' => 'ResourceSpecificationController@store'
        ));
        Route::get('/{resourceid}/{id}/show', array(
            'as' => 'resource_spec.show',
            'uses' => 'ResourceSpecificationController@show'
        ));
        Route::get('/{resourceid}/{id}/edit', array(
            'as' => 'resource_spec.edit',
            'uses' => 'ResourceSpecificationController@edit'
        ));
        Route::post('/{resourceid}/{id}/update', array(
            'as' => 'resource_spec.update',
            'uses' => 'ResourceSpecificationController@update'
        ));
        Route::post('/{resourceid}/{id}/destroy', array(
            'as' => 'resource_spec.destroy',
            'uses' => 'ResourceSpecificationController@destroy'
        ));
    });

    Route::group(array(
        'prefix' => 'resource_work_time'
            ), function () {
        Route::get('/{resourceid}', array(
            'as' => 'resource_work_time.index',
            'uses' => 'ResourceWorkTimeController@index'
        ));
        Route::get('/{resourceid}/create', array(
            'as' => 'resource_work_time.create',
            'uses' => 'ResourceWorkTimeController@create'
        ));
        Route::post('/{resourceid}/store', array(
            'as' => 'resource_work_time.store',
            'uses' => 'ResourceWorkTimeController@store'
        ));
        Route::get('/{resourceid}/{id}/edit', array(
            'as' => 'resource_work_time.edit',
            'uses' => 'ResourceWorkTimeController@edit'
        ));
        Route::post('/{resourceid}/{id}/update', array(
            'as' => 'resource_work_time.update',
            'uses' => 'ResourceWorkTimeController@update'
        ));
        Route::post('/{resourceid}/{id}/destroy', array(
            'as' => 'resource_work_time.destroy',
            'uses' => 'ResourceWorkTimeController@destroy'
        ));
        Route::get('/{resource_id}/template/view', array(
            'as' => 'resource_work_time.view_template',
            'uses' => 'ResourceWorkTimeController@template'
        ));
        Route::post('/{resource_id}/template/process', array(
            'as' => 'resource_work_time.from_template',
            'uses' => 'ResourceWorkTimeController@from_template'
        ));
    });

    Route::group(array('prefix' => 'project'), function () {
        /* Route for Project */
        Route::get('/', array(
            'as' => 'project.index',
            'uses' => 'ProjectController@index'
        ));

        Route::get('/filter', array(
            'as' => 'project.filter',
            'uses' => 'ProjectController@filter'
        ));

        Route::post('/filter/do', array(
            'as' => 'project.filter.do',
            'uses' => 'ProjectController@do_filter'
        ));

        Route::get('/{id}', array(
            'as' => 'project.show',
            'uses' => 'ProjectController@show'
        ));

        Route::post('/data', array(
            'as' => 'project.get_data',
            'uses' => 'ProjectController@project_data'
        ));

        Route::get('/{id}/bom', array(
            'as' => 'project.view_bom',
            'uses' => 'ProjectController@view_bom'
        ));

        Route::get('/{id}/bom/select', array(
            'as' => 'project.select_bom_for_process',
            'uses' => 'ProjectController@select_bom_for_process'
        ));

        Route::post('/{id}/bom/select', array(
            'as' => 'project.select_bom_for_process.post',
            'uses' => 'ProjectController@do_select_bom_for_process'
        ));

        Route::get('/{id}/bom/process', array(
            'as' => 'project.process_bom',
            'uses' => 'ProjectController@process_bom'
        ));

        Route::post('/bom_data', array(
            'as' => 'project.bom_data',
            'uses' => 'ProjectController@bom_data'
        ));

        /* Route for Project Planning */
        Route::get('/{project_id}/{last_finish}/planning', array(
            'as' => 'project.planning',
            'uses' => 'ProjectPlanningController@index'
        ));

        Route::get('/{project_id}/planning', array(
            'as' => 'project.planning.index',
            'uses' => 'ProjectPlanningController@index'
        ));

        Route::get('/{project_id}/planning/template', array(
            'as' => 'project.planning.template',
            'uses' => 'ProjectPlanningController@template'
        ));

        Route::post('/planning/template/detail', array(
            'as' => 'project.planning.template.detail',
            'uses' => 'ProjectPlanningController@template_detail'
        ));

        Route::post('/{project_id}/planning/from_template', array(
            'as' => 'project.planning.from_template',
            'uses' => 'ProjectPlanningController@from_template'
        ));

        Route::get('/{project_id}/{type}/planning/create', array(
            'as' => 'project.planning.create',
            'uses' => 'ProjectPlanningController@create'
        ));

        Route::get('/{project_id}/{parent_id}/{type}/planning/create', array(
            'as' => 'project.planning.create.sub',
            'uses' => 'ProjectPlanningController@create'
        ));

        Route::post('/{project_id}/planning/store', array(
            'as' => 'project.planning.store',
            'uses' => 'ProjectPlanningController@store'
        ));

        Route::get('/{project_id}/{id}/{type}/planning/edit', array(
            'as' => 'project.planning.edit',
            'uses' => 'ProjectPlanningController@edit'
        ));

        Route::post('/{project_id}/{id}/planning/update', array(
            'as' => 'project.planning.update',
            'uses' => 'ProjectPlanningController@update'
        ));

        Route::get('/{project_id}/{type}/planning/refresh_date', array(
            'as' => 'project.planning.refresh_date',
            'uses' => 'ProjectPlanningController@refresh_date'
        ));

        Route::get('/{project_id}/{id}/planning/destroy', array(
            'as' => 'project.planning.destroy',
            'uses' => 'ProjectPlanningController@destroy'
        ));

        Route::post('/{project_id}/planning/edit_percentage', array(
            'as' => 'project.planning.edit_percentage',
            'uses' => 'ProjectPlanningController@edit_percentage'
        ));

        Route::post('/planning/edit_project_date', array(
            'as' => 'project.planning.edit_project_date',
            'uses' => 'ProjectPlanningController@edit_project_date'
        ));

        /* ======================================================================= */
        /* Route for Planning Detail */
        Route::get('/{process_group_id}/{id}/planning/detail', array(
            'as' => 'project.planning.detail',
            'uses' => 'ProjectPlanningDetailController@index'
        ));

        Route::post('/planning/detail/filter', array(
            'as' => 'project.planning.filter',
            'uses' => 'ProjectPlanningDetailController@filter'
        ));

        /* Create */
        Route::get('/{step}/{master_plan_id}/planning/detail/create', array(
            'as' => 'project.planning.detail.create',
            'uses' => 'ProjectPlanningDetailController@create'
        ));

        Route::get('/{material_id}/{master_plan_id}/planning/detail/create/pb_bom', array(
            'as' => 'project.planning.detail.create_pb_bom',
            'uses' => 'ProjectPlanningDetailController@create_pb_bom'
        ));

        Route::get('/{material_id}/{master_plan_id}/planning/detail/create/mfg', array(
            'as' => 'project.planning.detail.create_mfg',
            'uses' => 'ProjectPlanningDetailController@create_mfg'
        ));

        /* Store */
        Route::post('/{master_plan_id}/planning/detail/store', array(
            'as' => 'project.planning.detail.store',
            'uses' => 'ProjectPlanningDetailController@store'
        ));

        Route::post('/{material_id}/{master_plan_id}/planning/detail/store/pb_bom', array(
            'as' => 'project.planning.detail.store_pb_bom',
            'uses' => 'ProjectPlanningDetailController@store_pb_bom'
        ));

        Route::post('/{material_id}/{master_plan_id}/planning/detail/store/mfg', array(
            'as' => 'project.planning.detail.store_mfg',
            'uses' => 'ProjectPlanningDetailController@store_mfg'
        ));

        /* Edit */
        Route::get('/{master_plan_id}/{detail_id}/planning/detail/edit', array(
            'as' => 'project.planning.detail.edit',
            'uses' => 'ProjectPlanningDetailController@edit'
        ));

        Route::get('/{process_id}/planning/detail/edit/pb_bom', array(
            'as' => 'project.planning.detail.edit_pb_bom',
            'uses' => 'ProjectPlanningDetailController@edit_pb_bom'
        ));

        Route::get('/{process_id}/planning/detail/edit/mfg', array(
            'as' => 'project.planning.detail.edit_mfg',
            'uses' => 'ProjectPlanningDetailController@edit_mfg'
        ));

        /* Update */
        Route::post('/{detail_plan_id}/planning/detail/update', array(
            'as' => 'project.planning.detail.update',
            'uses' => 'ProjectPlanningDetailController@update'
        ));

        Route::post('/{process_id}/planning/detail/update/pb_bom', array(
            'as' => 'project.planning.detail.update_pb_bom',
            'uses' => 'ProjectPlanningDetailController@update_pb_bom'
        ));

        Route::post('/{detail_plan_id}/planning/detail/update/mfg', array(
            'as' => 'project.planning.detail.update_mfg',
            'uses' => 'ProjectPlanningDetailController@update_mfg'
        ));

        /* Delete */
        Route::get('/{process_type}/{detail_id}/planning/detail/destroy', array(
            'as' => 'project.planning.detail.destroy',
            'uses' => 'ProjectPlanningDetailController@destroy'
        ));
        /* ======================================================================= */

        Route::get('/{material_id}/{plan_id}/planning/detail/process', array(
            'as' => 'project.planning.material_process',
            'uses' => 'ProjectPlanningDetailController@material_process'
        ));

        Route::get('/{material_id}/{plan_id}/planning/detail/process/popover', array(
            'as' => 'project.planning.material_process.popover',
            'uses' => 'ProjectPlanningDetailController@get_process_popover_content'
        ));

        Route::get('/{material_id}/{plan_id}/planning/detail/mfg_process', array(
            'as' => 'project.planning.material_mfg_process',
            'uses' => 'ProjectPlanningDetailController@material_process'
        ));

        Route::get('/{plan_id}/planning/pb-bom/do_plan', array(
            'as' => 'project.planning.do_pb_bom_plan',
            'uses' => 'ProjectPlanningDetailController@do_pb_bom_plan'
        ));

        Route::post('/{master_plan_id}/planning/detail/search', array(
            'as' => 'project.planning.detail.search',
            'uses' => 'ProjectPlanningDetailController@index'
        ));

        Route::post('/planning/detail/available_resource', array(
            'as' => 'project.planning.detail.available_resource',
            'uses' => 'ProjectPlanningDetailController@get_available_resource'
        ));
    });

    Route::group(array('prefix' => 'template'), function () {
        Route::get('/{material_id}/{master_plan_id}/mfg', array(
            'as' => 'template.planning.mfg',
            'uses' => 'ProjectPlanningDetailController@mfg_process_template'
        ));

        Route::get('/{master_plan_id}/mfg/header', array(
            'as' => 'template.planning.mfg.header',
            'uses' => 'ProjectPlanningDetailController@mfg_process_template_header'
        ));

        Route::post('/mfg/detail', array(
            'as' => 'template.planning.mfg.detail',
            'uses' => 'ProjectPlanningDetailController@mfg_process_template_detail'
        ));

        Route::post('/{material_id}/{master_plan_id}/mfg/do_process', array(
            'as' => 'template.planning.mfg.do_process',
            'uses' => 'ProjectPlanningDetailController@from_mfg_process_template'
        ));

        Route::post('/{master_plan_id}/mfg/do_process', array(
            'as' => 'template.planning.mfg.do_process.header',
            'uses' => 'ProjectPlanningDetailController@from_mfg_process_template_header'
        ));
    });

    Route::group(array(
        'prefix' => 'scheduling'
            ), function () {
        Route::get('/', array(
            'as' => 'scheduling.index',
            'uses' => 'SchedulingController@index'
        ));

        Route::get('/{resource_id}/{step}/results', array(
            'as' => 'scheduling.results',
            'uses' => 'SchedulingController@results'
        ));

        Route::get('/{resource_id}/{step}/results_json', array(
            'as' => 'scheduling.results_json',
            'uses' => 'SchedulingController@results_json'
        ));

         Route::get('/{resource_id}/{step}/gantt', array(
            'as' => 'scheduling.gantt.open',
            'uses' => 'SchedulingController@open_gantt'
        ));

        Route::post('/', array(
            'as' => 'scheduling.index',
            'uses' => 'SchedulingController@index'
        ));

        Route::post('/data/per_resource', array(
            'as' => 'scheduling.data.per_resources',
            'uses' => 'DataController@get_per_resource_schedules'
        ));

        Route::post('/data/resources', array(
            'as' => 'scheduling.data.resources',
            'uses' => 'SchedulingController@get_resource'
        ));

        Route::post('/data/process', array(
            'as' => 'scheduling.data.process',
            'uses' => 'SchedulingController@get_process'
        ));

        Route::post('/do_process', array(
            'as' => 'scheduling.process',
            'uses' => 'SchedulingController@process'
        ));

        Route::post('/priority/edit', array(
            'as' => 'scheduling.edit_priority',
            'uses' => 'SchedulingController@edit_priority'
        ));
    });

    Route::group(array('prefix' => 'data'), function () {
        Route::get('/project', array(
            'as' => 'data.project',
            'uses' => 'DataController@get_project'
        ));

        Route::get('/material', array(
            'as' => 'data.material',
            'uses' => 'DataController@get_material'
        ));

        Route::post('/resource', array(
            'as' => 'data.resource',
            'uses' => 'DataController@get_resource'
        ));

        Route::post('/pic', array(
            'as' => 'data.pic',
            'uses' => 'DataController@get_pic'
        ));

        Route::post('/iqi', array(
            'as' => 'data.iqi',
            'uses' => 'DataController@get_iqi'
        ));

        Route::post('/issue', array(
            'as' => 'data.issue',
            'uses' => 'DataController@get_issue'
        ));

        Route::get('/issues', array(
            'as' => 'data.issues',
            'uses' => 'DataController@get_issues'
        ));
    });

    Route::group(array('prefix' => 'actualing'), function () {
        Route::get('/', array(
            'as' => 'actualing.index',
            'uses' => 'ActualingController@index'
        ));
        Route::get('/{type}/{ui_type}/inplan', array(
            'as' => 'actualing.by_type.inplan',
            'uses' => 'ActualingController@inplan_detail'
        ));
        Route::get('/reset_data', array(
            'as' => 'actualing.reset_data',
            'uses' => 'ActualingController@reset_data'
        ));
        Route::post('/do_reset_data', array(
            'as' => 'actualing.do_reset_data',
            'uses' => 'ActualingController@do_reset_data'
        ));
        Route::post('/data/reseting', array(
            'as' => 'actualing.data.reseting',
            'uses' => 'ActualingController@get_actualing_data_reset'
        ));
        Route::get('/edit_data', array(
            'as' => 'actualing.edit_data',
            'uses' => 'ActualingController@edit_data'
        ));
        Route::get('/{id}/{type}/edit_data/form', array(
            'as' => 'actualing.edit_data.form',
            'uses' => 'ActualingController@edit_data_form'
        ));
        Route::post('/edit_data/do', array(
            'as' => 'actualing.do_edit_data',
            'uses' => 'ActualingController@do_edit_data'
        ));

        Route::post('/data/editing', array(
            'as' => 'actualing.data.editing',
            'uses' => 'ActualingController@get_actualing_data_editing'
        ));
        Route::post('/data/inplan', array(
            'as' => 'actualing.data.inplan',
            'uses' => 'ActualingController@get_actualing_data'
        ));
        Route::get('/{type}/outplan', array(
            'as' => 'actualing.by_type.outplan',
            'uses' => 'ActualingController@outplan_detail'
        ));
        Route::get('/{type}/{nop}/pb-bom/detail', array(
            'as' => 'actualing.pb-bom.detail',
            'uses' => 'ActualingController@pbbom_detail'
        ));
        Route::get('/{type}/popup', array(
            'as' => 'actualing.do_actualing',
            'uses' => 'ActualingPopupController@index'
        ));
        Route::post('/{type}/planning/data', array(
            'as' => 'actualing.planning.data',
            'uses' => 'ActualingPopupController@get_project_planning'
        ));
        Route::post('/{type}/data', array(
            'as' => 'actualing.data',
            'uses' => 'ActualingPopupController@get_actualing_data'
        ));
        Route::post('/{type}/start', array(
            'as' => 'actualing.start_actualing',
            'uses' => 'ActualingPopupController@do_start_actualing'
        ));
        Route::post('/{type}/finish', array(
            'as' => 'actualing.finish_actualing',
            'uses' => 'ActualingPopupController@do_finish_actualing'
        ));
        Route::get('/{resource_id}/resource', array(
            'as' => 'actualing.resource',
            'uses' => 'ActualingPopupController@get_resource_data'
        ));
		Route::post('/{type}/last_state/data', array(
			'as' => 'actualing.last_state.data',
			'uses' => 'ActualingPopupController@get_last_state'
		));
        Route::post('/{type}/log/data', array(
            'as' => 'actualing.actualing_data.log',
            'uses' => 'ActualingPopupController@get_actualing_log_data'
        ));
        Route::post('/pic/by_resource/data', array(
            'as' => 'actualing.pic_data',
            'uses' => 'ActualingPopupController@get_pic_data'
        ));
        Route::post('/resource/by_planning/data', array(
            'as' => 'actualing.resource_data.by_planning',
            'uses' => 'ActualingPopupController@get_resource_data'
        ));
        Route::post('/position_data', array(
            'as' => 'actualing.positions',
            'uses' => 'ActualingPopupController@get_positions'
        ));
        Route::post('/quantity_data', array(
            'as' => 'actualing.quantity',
            'uses' => 'ActualingPopupController@get_quantity'
        ));
        Route::post('/is_quantity_complete', array(
            'as' => 'actualing.is_quantity_complete',
            'uses' => 'ActualingPopupController@is_quantity_complete'
        ));
        Route::post('/set_status_log', array(
            'as' => 'actualing.set_status_log',
            'uses' => 'ActualingPopupController@set_status_log'
        ));
    });

    Route::group(array('prefix' => 'iqi'), function () {
        Route::get('/', array(
            'as' => 'iqi.index',
            'uses' => 'IqisController@index'
        ));

        Route::get('/index', array(
            'as' => 'iqi.index',
            'uses' => 'IqisController@index'
        ));

        Route::get('/materials', array(
            'as' => 'iqi.materials',
            'uses' => 'IqisController@show_materials'
        ));

        Route::get('/lists', array(
            'as' => 'iqi.lists',
            'uses' => 'IqisController@iqi_lists'
        ));

        Route::get('/create', array(
            'as' => 'iqi.create',
            'uses' => 'IqisController@create'
        ));

        Route::post('/store', array(
            'as' => 'iqi.store',
            'uses' => 'IqisController@store'
        ));

        Route::get('/{id}/show', array(
            'as' => 'iqi.show',
            'uses' => 'IqisController@show'
        ));

        Route::get('/select_project', array(
            'as' => 'iqi.select_project',
            'uses' => 'IqisController@select_project'
        ));

        Route::get('/select_material', array(
            'as' => 'iqi.select_material',
            'uses' => 'IqisController@select_material'
        ));

        Route::get('/select_iqi', array(
            'as' => 'iqi.select_iqi',
            'uses' => 'IqisController@select_iqi'
        ));

        Route::get('/{id}/edit', array(
            'as' => 'iqi.edit',
            'uses' => 'IqisController@edit'
        ));

        Route::post('/{id}/update', array(
            'as' => 'iqi.update',
            'uses' => 'IqisController@update'
        ));

        Route::get('/{id}/set_status', array(
            'as' => 'iqi.set_status',
            'uses' => 'IqisController@set_status'
        ));

        Route::get('/{id}/destroy', array(
            'as' => 'iqi.destroy',
            'uses' => 'IqisController@destroy'
        ));
    });

    Route::group(array('prefix' => 'lpk'), function () {
        Route::get('/create', array(
            'as' => 'lpk.create',
            'uses' => 'EmployeeCompensationsController@create'
        ));

        Route::post('/store', array(
            'as' => 'lpk.store',
            'uses' => 'EmployeeCompensationsController@store'
        ));

        Route::get('/{id}/show', array(
            'as' => 'lpk.show',
            'uses' => 'EmployeeCompensationsController@show'
        ));

        Route::get('/{id}/edit', array(
            'as' => 'lpk.edit',
            'uses' => 'EmployeeCompensationsController@edit'
        ));

        Route::post('/{id}/update', array(
            'as' => 'lpk.update',
            'uses' => 'EmployeeCompensationsController@update'
        ));

        Route::get('/{id}/destroy', array(
            'as' => 'lpk.destroy',
            'uses' => 'EmployeeCompensationsController@destroy'
        ));
    });

    Route::group(array('prefix' => 'issue_management'), function () {
        Route::get('/', array(
            'as' => 'issue_management.index',
            'uses' => 'IssueManagementsController@index'
        ));

        Route::get('/index', array(
            'as' => 'issue_management.index',
            'uses' => 'IssueManagementsController@index'
        ));

        Route::get('/create', array(
            'as' => 'issue_management.create',
            'uses' => 'IssueManagementsController@create'
        ));

        Route::post('/store', array(
            'as' => 'issue_management.store',
            'uses' => 'IssueManagementsController@store'
        ));

        Route::get('/{id}/show', array(
            'as' => 'issue_management.show',
            'uses' => 'IssueManagementsController@show'
        ));

        Route::get('/select', array(
            'as' => 'issue_management.select',
            'uses' => 'IssueManagementsController@select'
        ));

        Route::get('/{id}/edit', array(
            'as' => 'issue_management.edit',
            'uses' => 'IssueManagementsController@edit'
        ));

        Route::post('/{id}/update', array(
            'as' => 'issue_management.update',
            'uses' => 'IssueManagementsController@update'
        ));

        Route::get('/{id}/destroy', array(
            'as' => 'issue_management.destroy',
            'uses' => 'IssueManagementsController@destroy'
        ));
    });

    Route::group(array('prefix' => 'cost_control'), function () {
        Route::get('/index', array(
            'as' => 'cost_control.index',
            'uses' => 'ProjectCostControlController@index'
        ));
        Route::get('/{id}/detail', array(
            'as' => 'cost_control.detail',
            'uses' => 'ProjectCostControlController@detail'
        ));
    });

    Route::group(array('prefix' => 'customer'), function () {
        Route::get('/survey', array(
            'as' => 'survey.customer',
            'uses' => 'CustomerController@survey'
        ));
    });

    Route::group(array('prefix' => 'alert_setting'), function () {
        Route::post('/set_receive_email', array(
            'as' => 'alert.set_receive_email',
            'uses' => 'AlertSettingsController@set_receive_email'
        ));

        Route::post('/set_receive_sms', array(
            'as' => 'alert.set_receive_sms',
            'uses' => 'AlertSettingsController@set_receive_sms'
        ));
    });



    /*
     * =================================================================
     * Report Routes
     * =================================================================
     */
    Route::group(array('prefix' => 'report/resource'), function () {
        Route::group(array('prefix' => '/availability'), function () {
            Route::get('/', array(
                'as' => 'report.resource_availability',
                'uses' => 'ReportResourceController@availability'
            ));
            Route::get('/detail', array(
                'as' => 'report.resource_availability.detail',
                'uses' => 'ReportResourceController@availability_detail'
            ));
        });

        Route::get('/actual_used', array(
            'as' => 'report.resource_actual_used',
            'uses' => 'ReportResourceController@actual_used'
        ));
    });

    Route::group(array('prefix' => 'report/customer'), function () {
        Route::get('/', array(
            'as' => 'report.customer',
            'uses' => 'CustomerController@report'
        ));
    });

    Route::group(array('prefix' => 'report/actualing'), function () {
        Route::get('/', array(
            'as' => 'report.actualing',
            'uses' => 'ActualingReportController@index'
        ));
        Route::post('/search', array(
            'as' => 'report.actualing.search',
            'uses' => 'ActualingReportController@search'
        ));
        Route::get('/detail', array(
            'as' => 'report.actualing.detail',
            'uses' => 'ActualingReportController@detail'
        ));
    });
});
