<?php

use Project;
use ProjectMasterPlan;
use ProcessGroup;
use NOPPImporter;
use TemplateMasterPlanHeader;
use TemplateMasterPlanDetail;
use GeneralOptions;

// use BaseController, Form, Input, Redirect, Sentry, View, Notification, DB;

class ProjectPlanningController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme') . '._layouts.master', function ($view)
        {
            $view->with('parent_active', 'project');
            $view->with('child_active', 'planning');
        });
    }

    public function index($project_id)
    {
        $project = Project::find($project_id);
        if (!is_bom_imported($project_id))
        {
            $materials = NOPPImporter::getBomProject($project->nop);

            if (count($materials) > 0)
            {
                foreach ($materials as $material)
                {
                    save_erp_material($project_id, $material);
                }
            }
        }

        $estimation_trial = (strtotime($project->trial_date) - strtotime($project->start_date)) / (24 * 3600);
        $estimation_delivery = (strtotime($project->delivery_date) - strtotime($project->start_date)) / (24 * 3600);
        $master_plan = DB::table('project_master_plan AS mp')
                        ->join('master_process_group AS pg', 'pg.id', '=', 'mp.process_group_id')
                        ->where('mp.project_id', '=', $project_id)->orderBy('mp.order')
                        ->orderBy('finish_date')->get(array('mp.*', 'pg.group'));
        // debug($master_plan);
        $view = View::make(Config::get('app.theme') . '.planning.index');
        $view->with('page_title', 'Project Planning');
        $view->with('sub_title', 'Project ' . $project->nop);
        $view->with('theme', Config::get('app.theme'));
        $view->with('processes', $master_plan);
        $view->with('project', $project);
        $view->with('estimation_trial', $estimation_trial);
        $view->with('estimation_delivery', $estimation_delivery);

        return $view;
    }

    public function create($project_id, $type = 'first')
    {
        $predecessor = ProjectMasterPlan::where('finish_date', '<>', '0000-00-00 00:00:00')->orderBy('id', 'DESC')->take(1)->get();
        $last_planning_date = isset($predecessor[0]) ? strtotime($predecessor[0]->finish_date) : 0;
        $last_planning = ProjectMasterPlan::where('project_id', '=', $project_id)->orderBy('id', 'DESC')->take(1)->get();
        $last_order_num = isset($last_planning[0]) ? $last_planning[0]->order : 0;
        $project = Project::find($project_id);
        $parent_plan = DB::table('project_master_plan AS mp')
                ->join('master_process_group AS pg', 'pg.id', '=', 'mp.process_group_id')
                ->where('mp.parent_id', '=', '0')
                ->where('project_id', '=', $project_id)
                ->get(array('mp.*', 'pg.group'));

        $view = View::make(Config::get('app.theme') . '.planning.add_planning');
        $view->with('project_id', $project_id);
        $view->with('random', mt_rand(0, 999));
        $view->with('min_date', date('Y-m-d H:i:s', $last_planning_date + 86400));
        $view->with('max_date', $project->delivery_date);
        $view->with('order_num', intval($last_order_num) + 1);
        $view->with('process_group', ProcessGroup::all());
        $view->with('parent_planning', $parent_plan);
        $view->with('type', $type);

        return $view;
    }

    public function store($project_id)
    {
        // debug($_POST);

        $parent_id = Input::get('parent_id');
        $planning = Input::get('group_id');
        $ordernum = Input::get('ordernum');
        $percent = Input::get('percent');
        $start_date = Input::get('start');
        $finish_date = Input::get('finish');
        $process_type = Input::get('type');

        self::_save_planning($project_id, $parent_id, $planning, $ordernum, $percent, $process_type, $start_date, $finish_date);

        Notification::success('New Master Plan was saved.');

        return Redirect::route('project.planning.index', $project_id);
    }

    public function edit($project_id, $id, $type = 'first')
    {
        $master_plan = DB::table('project_master_plan AS mp')
                ->join('master_process_group AS pg', 'pg.id', '=', 'mp.process_group_id')
                ->where('mp.id', '=', $id)
                ->get(array('mp.*', 'pg.group'));
        $parent_plan = DB::table('project_master_plan AS mp')
                ->join('master_process_group AS pg', 'pg.id', '=', 'mp.process_group_id')
                ->where('mp.parent_id', '=', '0')
                ->where('project_id', '=', $project_id)
                ->get(array('mp.*', 'pg.group'));

        $view = View::make(Config::get('app.theme') . '.planning.edit_planning');
        $view->with('project_id', $project_id);
        $view->with('planning', @$master_plan[0]);
        $view->with('process_group', ProcessGroup::all());
        $view->with('parent_planning', $parent_plan);
        $view->with('type', $type);

        return $view;
    }

    public function update($project_id, $id)
    {
        $project_plan = ProjectMasterPlan::find($id);

        $parent_id = Input::get('parent_id');
        $type = Input::get('type');
        $process_group_id = Input::get('group_id');
        $ordernum = Input::get('ordernum');
        $percent = Input::get('percent');
        $start_date_input = Input::get('start');
        $finish_date_input = Input::get('finish');

        $project_plan->project_id = $project_id;
        $project_plan->parent_id = $parent_id;
        $project_plan->process_group_id = $process_group_id;
        $project_plan->order = $ordernum;
        $project_plan->process_type = $type;

        if ($start_date_input)
        {
            list($smonth, $sdate, $syear) = explode('/', $start_date_input);
            $project_plan->start_date = "{$syear}-{$smonth}-{$sdate} 00:00:00";

            $predecessor = ProjectMasterPlan::find($project_plan->predecessor);
            $start_lags = $predecessor ? calculate_lags($project_plan->start_date, $predecessor->finish_date, 'start-finish') : 0;
        }

        if ($finish_date_input)
        {
            list($fmonth, $fdate, $fyear) = explode('/', $finish_date_input);
            $project_plan->finish_date = "{$fyear}-{$fmonth}-{$fdate} 00:00:00";

            $succesor = ProjectMasterPlan::where('predecessor', '=', $id)->get();
            if (isset($succesor[0]))
            {
                $finish_lags = calculate_lags($succesor[0]->start_date, $project_plan->finish_date, 'finish-start');
                $update_succesor = ProjectMasterPlan::find($succesor[0]->id);
                $update_succesor->lag = $finish_lags;
                $update_succesor->save();
            }
        }

        $project_plan->percent = $percent;
        $project_plan->pull_from = $type == 'first' ? 'trial' : 'delivery';
        $project_plan->lag = isset($start_lags) ? $start_lags : 0;
        $project_plan->save();

        return Redirect::route('project.planning.index', $project_id);
    }

    public function destroy($project_id, $planning_id)
    {
        $delete_plan = ProjectMasterPlan::find($planning_id);
        @$delete_plan->delete();

        return Redirect::route('project.planning', array($project_id, $planning_id));
    }

    public function template($project_id)
    {
        $view = View::make(Config::get('app.theme') . '.planning.template');
        $view->with('templates', TemplateMasterPlanHeader::all());
        $view->with('project_id', $project_id);
        return $view;
    }

    public function template_detail()
    {
        $template_details = DB::table('template_master_plan_detail AS tpl')
                ->join('master_process_group AS g', 'g.id', '=', 'tpl.process_group_id')
                ->where('tpl.header_id', '=', Input::get('tid'))
                ->orderBy('tpl.order');

        $data = $template_details->count() > 0 ? $template_details->get() : array();
        $response = Response::make($data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function from_template($project_id)
    {
        $template_id = Input::get('template-id');
        $template_header = TemplateMasterPlanHeader::find($template_id);
        $template_planning = TemplateMasterPlanDetail::where('header_id', '=', $template_id);
        if ($template_planning->count() > 0)
        {
            $old_planning = ProjectMasterPlan::where('project_id', '=', $project_id);
            if ($old_planning->count() > 0)
            {
                foreach ($old_planning->get() as $old_master_plan)
                {
                    DB::table('project_planning_detail_mfg')->where('master_planning_id', '=', $old_master_plan->id)->delete();
                    DB::table('project_planning_detail_non_mfg')->where('master_planning_id', '=', $old_master_plan->id)->delete();
                    DB::table('project_planning_detail_pb_bom')->where('master_planning_id', '=', $old_master_plan->id)->delete();
                }
            }

            if ($old_planning->delete() OR $old_planning->count() == 0)
            {
                $prev_id = 0;
                $prev_parent = 0;
                foreach ($template_planning->get() as $tpl_planning)
                {
                    $parent_id = 0;
                    if (intval($tpl_planning->parent_id) > 0)
                    {
                        $parent_id = $prev_id;
                    }

                    $process_group = $tpl_planning->process_group_id;
                    $ordernum = $tpl_planning->order;
                    $percent = $tpl_planning->percent;
                    $process_type = $tpl_planning->process_type;
                    $planning_id = self::_save_planning($project_id, $parent_id, $process_group, $ordernum, $percent, $process_type);

                    if ($parent_id == 0)
                    {
                        $prev_id = $planning_id;
                    }
                }
            }

            $this->refresh_date($project_id, 'first');
            $this->refresh_date($project_id, 'secondary');
        }

        return Redirect::back();
        //return Redirect::route('project.planning.refresh_date', array($project_id, strtolower($template_header->pull_date_from)));
    }

    public function edit_percentage()
    {
        $master_plan_id = Input::get('pk');
        $percentage = Input::get('value');
        $master_plan = ProjectMasterPlan::find($master_plan_id);
        $master_plan->percent = $percentage;
        $master_plan->save();

        return $percentage;
    }

    public function refresh_date($project_id, $type)
    {
        self::_update_start_finish_date($project_id, $type);
        return Redirect::route('project.planning.index', $project_id);
    }

    public function edit_project_date()
    {
        $id = Input::get('pk');
        $date = Input::get('value');
        $type = Input::get('type');

        $project = Project::find($id);

        if($type == 'start-date')
        {
            $project->start_date = $date;
        }
        elseif($type == 'first-date')
        {
            $project->trial_date = $date;
        }
        else
        {
            $project->delivery_date = $date;
        }

        return $project->save() ? $date : date('Y-m-d');
    }

    private static function _update_start_finish_date($project_id, $process_type = "first")
    {
        $master_plan = DB::table('project_master_plan AS mp')
                ->join('master_process_group AS pg', 'pg.id', '=', 'mp.process_group_id')
                ->select('mp.*', 'pg.group')
                ->where('mp.project_id', '=', $project_id)
                ->where('mp.process_type', '=', $process_type)
                ->orderBy('mp.order', 'DESC');

        $project_data = Project::find($project_id);
        $get_master_plan_data = $master_plan->get();

        if ($process_type == 'first')
        {
            $pull_date_from = isset($get_master_plan_data[0]->pull_from) ? $get_master_plan_data[0]->pull_from : '';
            $estimation_from = $project_data->{$pull_date_from . "_date"};
            $total_estimation = intval((strtotime("{$estimation_from} 23:59:59") - strtotime("{$project_data->start_date} 00:00:00")) / (24 * 3600));
        }
        else
        {
            $pull_date_from = 'delivery';
            $estimation_from = $project_data->delivery_date;
            $estimation_first_process = ceil(ProjectMasterPlan::where('process_type', '=', 'first')->where('process_group_id', '<>', '15')->where('project_id', '=', $project_id)->sum('real_estimation'));
            $estimation_all_process = intval((strtotime("{$estimation_from} 23:59:59") - strtotime("{$project_data->start_date} 00:00:00")) / (24 * 3600));
            $total_estimation = $estimation_all_process - $estimation_first_process;
        }

        $pull_from_unix = strtotime("{$estimation_from} 23:59:59");
        $master_plan_count = $master_plan->count();

        self::_batch_update_estimation($project_id, $total_estimation, $process_type);

        if ($master_plan_count > 0)
        {
            $master_plan_data = array();
            foreach ($master_plan->get() as $master)
            {
                $master_plan_data[] = array(
                    'id' => $master->id,
                    'parent_id' => $master->parent_id,
                    'order' => $master->order,
                    'project_id' => $master->project_id,
                    'process_group_id' => $master->process_group_id,
                    'group' => $master->group,
                    'percent' => $master->real_percent,
                    'estimation' => $master->real_estimation,
                    'estimation_hour' => $master->real_estimation_hour,
                    'process_type' => $master->process_type,
                    'lag' => $master->lag,
                    'predecessor' => $master->predecessor,
                );
            }

            $previous_order_num = count($master_plan_data);
            $previous_finish_date = 0;
            $previous_parent = 0;
            $parent_finish_date = 0;
            $total_child_estimation = 0;
            $total_child_percent = 0;
            $next_process_delay = GeneralOptions::getSingleOption('next_process_delay')['value'];
            foreach ($master_plan_data as $key => $plan)
            {
                $new_finish = $previous_finish_date == 0 ? $pull_from_unix : $previous_finish_date - ($next_process_delay * 3600);
                $estimation_in_seconds = intval($plan['estimation_hour'] * 3600);
                $new_start = $new_finish - $estimation_in_seconds;

                $the_plan = ProjectMasterPlan::find($plan['id']);
                $the_plan->order = $previous_order_num;

                $has_child = ProjectMasterPlan::where('parent_id', '=', $plan['id'])->count();
                if ($has_child == 0)
                {
                    if ($previous_parent == 0)
                    {
                        $parent_finish_date = $new_finish;
                    }

                    if ($plan['parent_id'] != 0)
                    {
                        $total_child_estimation += $plan['estimation'];
                        $total_child_percent += $plan['percent'];
                    }

                    $the_plan->start_date = date('Y-m-d H:i:s', $new_start);
                    $the_plan->finish_date = date('Y-m-d H:i:s', $new_finish);
                }
                else
                {
                    $the_plan->start_date = date('Y-m-d H:i:s', $previous_finish_date);
                    $the_plan->finish_date = date('Y-m-d H:i:s', $parent_finish_date);
                    $the_plan->real_estimation = $total_child_estimation;
                    $the_plan->percent = $total_child_percent;

                    $total_child_estimation = 0;
                    $total_child_percent = 0;
                }

                $the_plan->save();

                $previous_finish_date = $the_plan->start_date == '0000-00-00 00:00:00' ? 0 : strtotime($the_plan->start_date);
                $previous_parent = $plan['parent_id'];
                $previous_order_num--;
            }

            krsort($master_plan_data);
            $previous_plan_id = 0;
            foreach ($master_plan_data as $plan)
            {
                $the_plan = ProjectMasterPlan::find($plan['id']);
                $the_plan->predecessor = $previous_plan_id;
                $the_plan->save();
                $previous_plan_id = $plan['id'];
            }

            return TRUE;
        }

        return FALSE;
    }

    private static function _batch_update_estimation($project_id, $total_estimation, $process_type)
    {
        $master_plans = ProjectMasterPlan::where('project_id', '=', $project_id);
        $total_percentage = $master_plans->where('process_group_id', '<>', '15')->sum('percent');
        $total_percentage_type = $master_plans->where('process_group_id', '<>', '15')->where('process_type', '=', $process_type)->sum('percent');

        // debug("Total Estimation: ".$total_estimation, false);
        // debug("Total Percent: ".$total_percentage, false);
        // debug("Total Percent Type: ".$total_percentage_type, false);
        // debug("==============================", false);
        // debug(DB::getQueryLog());

        if ($master_plans->count() > 0 && $total_percentage > 0)
        {
            foreach ($master_plans->get() as $master_plan)
            {
                $has_child = ProjectMasterPlan::where('parent_id', '=', $master_plan->id)->count() > 0 ? true : false;
                $master_plan = ProjectMasterPlan::find($master_plan->id);

                if (!$has_child)
                {
                    $estimation = ($master_plan->percent / 100) * $total_estimation;
                    $estimation_hour = 24 * $estimation;

                    $real_percentage = ($master_plan->percent / $total_percentage_type) * 100;
                    $real_estimation = ($real_percentage / 100) * $total_estimation;
                    $real_estimation_hour = 24 * $real_estimation;

                    //debug("Real Percent: ".$real_percentage, false);
                    //debug("Real Estimation: ".$real_estimation, false);
                    //debug("Real Estimation Hour: ".$real_estimation_hour, false);
                    //debug("Percent: ".$master_plan->percent, false);
                    //debug("Estimation: ".$estimation, false);
                    //debug("Estimation Hour: ".$estimation_hour, false);
                    //debug(" ", false);

                    $master_plan->estimation = $estimation;
                    $master_plan->estimation_hour = $estimation_hour;
                    $master_plan->real_percent = $real_percentage;
                    $master_plan->real_estimation = $real_estimation;
                    $master_plan->real_estimation_hour = $real_estimation_hour;
                }
                else
                {
                    $master_plan->percent = 0;
                    $master_plan->estimation = 0;
                    $master_plan->estimation_hour = 0;
                    $master_plan->real_percent = 0;
                    $master_plan->real_estimation = 0;
                    $master_plan->real_estimation_hour = 0;
                }

                $master_plan->save();
            }
        }
        //die;
    }

    private static function _save_planning($project_id, $parent_id, $process_group_id, $ordernum, $percent, $process_type, $start_date_input = false, $finish_date_input = false)
    {
        $project_plan = new ProjectMasterPlan;
        $project_plan->project_id = $project_id;
        $project_plan->parent_id = $parent_id;
        $project_plan->process_group_id = $process_group_id;
        $project_plan->order = $ordernum;
        $project_plan->percent = $percent;
        $project_plan->process_type = $process_type;
        // $project_plan->pull_from = $pull_date_from;

        // Calculate start to finish lags
        $predecessor = ProjectMasterPlan::where('project_id', '=', $project_id)
                ->where('order', '=', ($ordernum - 1))
                ->get();
        if ($start_date_input)
        {
            if (!empty($parent_id))
            {
                $child_plan = ProjectMasterPlan::where('parent_id', '=', $parent_id)->count();
                if ($child_plan === 0)
                {
                    $predecessor = ProjectMasterPlan::find($predecessor[0]->predecessor);
                    $order = isset($predecessor->order) ? $predecessor->order + 2 : 2;
                    $predecessor_id = $parent_id;
                }
                else
                {
                    $child_predecessor = ProjectMasterPlan::where('project_id', '=', $project_id)
                            ->where('parent_id', '=', $parent_id)
                            ->orderBy('predecessor', 'desc')
                            ->get();
                    $predecessor = isset($child_predecessor) ? $child_predecessor[0] : 0;
                    $order = isset($predecessor->order) ? $predecessor->order + 1 : 1;
                    $predecessor_id = isset($predecessor->id) ? $predecessor->id : 0;
                }
            }
            else
            {
                $predecessor = isset($predecessor[0]) ? $predecessor[0] : 0;
                $order = isset($predecessor->order) ? $predecessor->order + 1 : 1;
                $predecessor_id = isset($predecessor->id) ? $predecessor->id : 0;
            }

            list($smonth, $sdate, $syear) = explode('/', $start_date_input);
            $project_plan->start_date = "{$syear}-{$smonth}-{$sdate} 00:00:00";
            $project_plan->lag = isset($predecessor->finish_date) ? calculate_lags($project_plan->start_date, $predecessor->finish_date, 'start-finish') : 0;
            $project_plan->predecessor = $predecessor_id;
            $project_plan->order = $order;
        }

        if ($finish_date_input)
        {
            list($fmonth, $fdate, $fyear) = explode('/', $finish_date_input);
            $project_plan->finish_date = "{$fyear}-{$fmonth}-{$fdate} 00:00:00";
        }

        $project_plan->save();

        // Update next planning lags
        if ($finish_date_input)
        {
            $successor = ProjectMasterPlan::where('predecessor', '=', $project_plan->id)->get();
            if (count($successor) > 0)
            {
                $start_date = $successor[0]->start_date;
                $finish_lags = calculate_lags($successor, $finish_date_input, 'finish-start');
                $update_succesor = ProjectMasterPlan::find($successor[0]->id);
                $update_succesor->lag = $finish_lags;
                $update_succesor->save();
            }
        }

        return isset($project_plan->id) ? $project_plan->id : false;
    }

}
