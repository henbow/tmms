<?php
use UserInformation;
use Department;
use Unit;
// // use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification;

class DepartmentController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'user_management');
            $view->with('child_active', 'department');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.department.list');
        $view->with('page_title', 'Department Management');
        $view->with('sub_title', 'Manage company department');
        $view->with('theme', Config::get('app.theme'));
        $view->with('departments', Department::getAll());

        return $view;
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.department.create');
        $view->with('page_title', 'Department Management');
        $view->with('sub_title', 'Create new department');
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function store()
    {
        $department = new Department;
        $department->code = Input::get('code');
        $department->name = Input::get('deptname');
        $department->desc = Input::get('desc');

        if($department->save())
            Notification::success('New department data was saved.');

        return Redirect::route('department.index');
    }

    public function edit($id)
    {
        $view = View::make(Config::get('app.theme').'.department.edit');
        $view->with('page_title', 'Department Management');
        $view->with('sub_title', 'Edit department data');
        $view->with('theme', Config::get('app.theme'));
        $view->with('department', Department::find($id));

        return $view;
    }

    public function update($id)
    {
        $department = Department::find($id);
        $department->code = Input::get('code');
        $department->name = Input::get('deptname');
        $department->desc = Input::get('desc');

        if($department->save())
            Notification::success('Department data changes was saved.');

        return Redirect::route('department.index');
    }

    public function destroy($id)
    {
        $department = Department::find($id);
        $department->delete();

        return Redirect::route('department.index');
    }
}
