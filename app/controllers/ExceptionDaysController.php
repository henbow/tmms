<?php
use ExceptionDays;

// use BaseController, Form, Input, Redirect, Sentry, View, Notification, DB;

class ExceptionDaysController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'resource_center');
            $view->with('child_active', 'exception_days');
        });
    }

	public function index()
    {
        $view = View::make(Config::get('app.theme').'.exception_days.list');
        $view->with('page_title', 'Exception Days');
        $view->with('sub_title', 'Manage Exception Days');
        $view->with('theme', Config::get('app.theme'));
        $view->with('exception_days', ExceptionDays::select(array(
			DB::raw('MIN(`id`) AS min_id'),
			DB::raw('MAX(`id`) AS max_id'),
			DB::raw('MIN(`year`) AS min_year'),
			DB::raw('MAX(`year`) AS max_year'),
			DB::raw('MIN(`month`) AS min_month'),
			DB::raw('MAX(`month`) AS max_month'),
			DB::raw('MIN(`day`) AS min_day'),
			DB::raw('MAX(`day`) AS max_day'),
			'description'
		))->groupBy('description')->orderBy('year', 'DESC')->paginate(20));

        return $view;
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.exception_days.create');
        $view->with('page_title', 'Add Exception Day');
        $view->with('sub_title', 'Add Exception Day');
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function store()
    {
		$description = Input::get('description');
		$start = explode('/', Input::get('start'));
		$finish = explode('/', Input::get('finish'));
		$start_unix = strtotime("{$start[2]}-{$start[0]}-{$start[1]} 00:00:00");
		$finish_unix = strtotime("{$finish[2]}-{$finish[0]}-{$finish[1]} 23:59:59");
		$duration_in_seconds = $finish_unix - $start_unix;
		$total_days = ceil($duration_in_seconds / (24 * 3600));
		$one_day = 86400;

		if($total_days > 0)
		{
			for($i = $start_unix; $i < $start_unix + ($total_days * $one_day); $i += $one_day)
			{
				list($year, $month, $day) = explode('-', date('Y-m-d', $i));
				$exception_day = new ExceptionDays;
				$exception_day->year = $year;
				$exception_day->month = $month;
				$exception_day->day = $day;
				$exception_day->description = $description;
				$exception_day->save();
			}
		}

		Notification::success('New exception day was saved.');

        return Redirect::route('exception_days.index');
    }

    public function edit($id)
    {
		list($min_id, $max_id) = explode(':', $id);
		$exc_days = ExceptionDays::select(array(
			DB::raw('MIN(`year`) AS min_year'),
			DB::raw('MAX(`year`) AS max_year'),
			DB::raw('MIN(`month`) AS min_month'),
			DB::raw('MAX(`month`) AS max_month'),
			DB::raw('MIN(`day`) AS min_day'),
			DB::raw('MAX(`day`) AS max_day'),
			'description'
		))
		->whereBetween('id', array($min_id, $max_id))
		->groupBy('description')->get();
		$min_date = $exc_days[0]->min_month."/".$exc_days[0]->min_day."/".$exc_days[0]->min_year;
		$max_date = $exc_days[0]->max_month."/".$exc_days[0]->max_day."/".$exc_days[0]->max_year;
		$description = $exc_days[0]->description;

        $view = View::make(Config::get('app.theme').'.exception_days.edit');
        $view->with('page_title', 'Exception Day');
        $view->with('sub_title', 'Edit exception day');
        $view->with('theme', Config::get('app.theme'));
        $view->with('min_date', $min_date);
        $view->with('max_date', $max_date);
        $view->with('description', $description);
        $view->with('id', $id);

        return $view;
    }

    public function update($id)
    {
		list($min_id, $max_id) = explode(':', $id);
		$ids = range($min_id, $max_id);
        $description = Input::get('description');
		$start = explode('/', Input::get('start'));
		$finish = explode('/', Input::get('finish'));
		$start_unix = strtotime("{$start[2]}-{$start[0]}-{$start[1]} 00:00:00");
		$finish_unix = strtotime("{$finish[2]}-{$finish[0]}-{$finish[1]} 23:59:59");
		$duration_in_seconds = $finish_unix - $start_unix;
		$total_days = ceil($duration_in_seconds / (24 * 3600));
		$one_day = 86400;

		if(count($ids) > 0)
		{
			foreach($ids as $the_id)
			{
				$e_day = ExceptionDays::find($the_id);
				if($e_day)
				{
					$e_day->delete();
				}
			}
		}

		if($total_days > 0)
		{
			for($i = $start_unix; $i < $start_unix + ($total_days * $one_day); $i += $one_day)
			{
				list($year, $month, $day) = explode('-', date('Y-m-d', $i));
				$exception_day = new ExceptionDays;
				$exception_day->year = $year;
				$exception_day->month = $month;
				$exception_day->day = $day;
				$exception_day->description = $description;
				$exception_day->save();
			}
		}

        Notification::success('Exception day changes was saved.');
        return Redirect::route('exception_days.index');
    }

    public function destroy($id)
    {
		list($min_id, $max_id) = explode(':', $id);
		$ids = range($min_id, $max_id);

        if(count($ids) > 0)
		{
			foreach($ids as $the_id)
			{
				$e_day = ExceptionDays::find($the_id);
				if($e_day)
				{
					$e_day->delete();
				}
			}
		}

		Notification::success('Exception day changes was deleted.');
        return Redirect::route('exception_days.index');
    }
}
