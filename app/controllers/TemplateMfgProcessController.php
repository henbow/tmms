<?php

use TemplateMfgProcessHeader;
use TemplateMfgProcessDetail;
use Process;

class TemplateMfgProcessController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'planning');
            $view->with('child_active', 'mfg_process_template');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.template.mfg_process.header.index');
        $view->with('page_title', 'Template Manufacturing Process');
        $view->with('sub_title', 'Manage Template');
        $view->with('theme', Config::get('app.theme'));
        $view->with('headers', TemplateMfgProcessHeader::all());

        return $view;
    }

    public function import_excel()
    {
        $view = View::make(Config::get('app.theme').'.template.mfg_process.header.import_excel');
        $view->with('page_title', 'Import Template From Excel');
        $view->with('sub_title', 'Manage Template');
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function do_import_excel()
    {
        if(Input::hasFile('file_xls'))
        {
            $tpl_file_path = __DIR__.'/../../public/excel/template/mfg_process';
            $file_xls = Input::file('file_xls')->move($tpl_file_path, 'mfg_tpl_'.date('YmdHis').'.'.Input::file('file_xls')->getClientOriginalExtension());
            Excel::load($file_xls, function($reader) {
                $raw_tpl_data = array();
                $header_tpl = '';
                foreach ($reader->toArray() as $key => $sheet) {
                    if($key == 0)
                    {
                        $prefix = "MOLD";
                    }
                    else
                    {
                        $prefix = "DIE";
                    }

                    foreach($sheet as $row)
                    {
                        if($row['process'] && empty($row['small']) && empty($row['medium']) && empty($row['large']))
                        {
                            $header_tpl = $row['process'];
                        }
                        else
                        {
                            if($row['small'])
                            {
                                $raw_tpl_data[$prefix . ' ' . $header_tpl.' SMALL'][trim($row['process'])] = $row['small'];
                            }

                            if($row['medium'])
                            {
                                $raw_tpl_data[$prefix . ' ' . $header_tpl.' MEDIUM'][trim($row['process'])] = $row['medium'];
                            }

                            if($row['large'])
                            {
                                $raw_tpl_data[$prefix . ' ' . $header_tpl.' LARGE'][trim($row['process'])] = $row['large'];
                            }
                        }
                    }
                }

                foreach ($raw_tpl_data as $header => $data) {
                    $existing_template = TemplateMfgProcessHeader::where('name','LIKE','%'.$header.'%')->count() > 0 ? true : false;
                    if($existing_template === false)
                    {
                        $template = new TemplateMfgProcessHeader;
                        $template->name = $header;
                        if($template->save())
                        {
                            foreach($data as $process => $estimation)
                            {
                                $process_data = Process::where('process', '=', ucwords($process))->get();
                                if(count($process_data) > 0)
                                {
                                    $existing_process = TemplateMfgProcessDetail::where('process_id', '=', $process_data[0]->id)
                                                        ->where('header_id', '=', $template->id)->count() > 0 ? true : false;
                                    if($existing_process === false)
                                    {
                                        $planning = new TemplateMfgProcessDetail;
                                        $planning->header_id = $template->id;
                                        $planning->process_group_id = $process_data[0]->group_id;
                                        $planning->process_id = $process_data[0]->id;
                                        $planning->estimation = round($estimation / 24, 2);
                                        $planning->estimation_hour = $estimation;
                                        $planning->save();
                                    }
                                }
                            }

                        }

                    }
                }
            });
        }

        return Redirect::route('template_mfg_process.index');
    }

    public function create()
    {
	    $view = View::make(Config::get('app.theme').'.template.mfg_process.header.add');
        $view->with('page_title', 'Template Manufacturing Process');
        $view->with('sub_title', 'Manage Template');
        $view->with('theme', Config::get('app.theme'));
        $view->with('random', mt_rand(100, 999));
        //$view->with('process_data', $process_data);

        return $view;
    }

    public function store()
    {
        $template = new TemplateMfgProcessHeader;
        $template->name = Input::get('header_name');

        if($template->save())
            Notification::success('New template was saved.');

        return Redirect::back();
    }

    public function edit($id)
    {
        $view = View::make(Config::get('app.theme').'.template.mfg_process.header.edit');
        $view->with('page_title', 'Template Manufacturing Process');
        $view->with('sub_title', 'Edit template data');
        $view->with('theme', Config::get('app.theme'));
        $view->with('template', TemplateMfgProcessHeader::find($id));

        return $view;
    }

    public function update($id)
    {
        $template = TemplateMfgProcessHeader::find($id);
        $template->name = Input::get('header_name');

        if($template->save())
	{
            Notification::success('Template data changes was saved.');
	}

        return Redirect::back();
    }

    public function destroy($id)
    {
        $template = TemplateMfgProcessHeader::find($id);
	$template_details = TemplateMfgProcessDetail::where('header_id', '=', $id);
	if($template_details->count() > 0)
	{
	    foreach($template_details->get(array('id')) as $detail)
	    {
		$tpl_detail = TemplateMfgProcessDetail::find($detail->id);
		$tpl_detail->delete();
	    }
	}
        $template->delete();
        return Redirect::back();
    }
}
