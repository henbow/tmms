<?php

class ResourceManagerController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'resource_center');
            $view->with('child_active', 'resource_manager');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.resource_manager.list');
        $view->with('page_title', 'Resource Manager');
        $view->with('sub_title', 'Start and/or stop resources');
        $view->with('theme', Config::get('app.theme'));
        $view->with('in_plan_resources', Resource::where('plan_type', '=', 'in plan')->get());
        $view->with('out_plan_resources', Resource::where('plan_type', '=', 'out plan')->get());
        $view->with('breadcrumb', array(
            'Dashboard' => array(route('dashboard.index')),
            'Resource Manager' => array(route('resource_manager.index'), 'last')
        ));

        return $view;
    }

    public function offline($id)
    {
        $resource = Resource::find($id);

        if ($resource->is_offline == '1') {
            $resource->is_offline = '0';
            $resource->start_date = date('Y-m-d H:i:s', time());
            $message = 'Resource is set to ONLINE.';
        } else {
            $resource->is_offline = '1';
            $resource->stop_date = date('Y-m-d H:i:s', time());
            $message = 'Resource is set to OFFLINE.';
        }

        $resource->save();

        Notification::success($message);

        return Redirect::route('resource_manager.index');
    }
}

/**
 * End of file
 *
 */
