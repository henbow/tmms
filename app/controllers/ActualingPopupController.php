<?php

class ActualingPopupController extends BaseController
{
    private $user = null;

    public function __construct()
    {
        $this->user = Sentry::getUser();
    }

    public function index($type)
    {
        $ui_type = Input::get('ui-type') ? Input::get('ui-type') : 2;
        switch($type)
        {
            case 'material':
                Activity::log([
                    'contentId'   => $this->user->id,
                    'contentType' => 'Actualing',
                    'action'      => 'Do actualing',
                    'description' => 'Do actualing by material (type '.$ui_type.')',
                    'details'     => 'Do actualing by material (type '.$ui_type.')',
                    'updated'     => true,
                ]);

                if($ui_type == 1) {
                    $view = View::make(Config::get('app.theme').'.actualing.popup.mfg1');
                } else {
                    $view = View::make(Config::get('app.theme').'.actualing.popup.mfg2');
                    $planning_data = DB::table('project_planning_detail_mfg AS d')->join('projects AS p','p.id','=','d.project_id')->where('p.is_complete','<>','1')->select('d.*')->get();
                    $view->with('planning_data', $planning_data);
                }
                $view->with('page_title', 'Project Actualing for Material Type');
                break;

            default:
                Activity::log([
                    'contentId'   => $this->user->id,
                    'contentType' => 'Actualing',
                    'action'      => 'Do actualing',
                    'description' => 'Do actualing by NOP',
                    'details'     => 'Do actualing by NOP',
                    'updated'     => true,
                ]);

                $view = View::make(Config::get('app.theme').'.actualing.popup.non-mfg');
                $planning_data = DB::table('project_planning_detail_non_mfg AS d')->join('projects AS p','p.id','=','d.project_id')->where('p.is_complete','<>','1')->select('d.*')->get();
                $view->with('page_title', 'Project Actualing for NOP Type');
                $view->with('module', 'non-mfg');
                $view->with('planning_data', $planning_data);


                break;
        }

        $view->with('theme', Config::get('app.theme'));
        $view->with('type', $type);
        $view->with('resources', Resource::where('is_offline','<>','1')->where('plan_type','=','in plan')->get());
        $view->with('pics', Pic::all());
        $view->with('random', mt_rand(100, 999));

        return $view;
    }

    public function get_quantity()
    {
        $actualing_id = Input::get('actualing_id');
        $planning_id = Input::get('planning_id');
        $type = Input::get('type');
        $quantities = array();
        if($type == 'material')
        {
            $quantities = BomQuantity::where('planning_id','=',$planning_id)->get();
        }
        else
        {
            $quantities = MasterplanQuantity::where('planning_id','=',$planning_id)->get();
        }

        $response = Response::make($quantities, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_positions()
    {
        $actualing_id= Input::get('actualing_id');
        $finish = Input::get('finish');
        $subfinish = Input::get('subfinish');
        $type = Input::get('type');
        $position_datas = array();

        try{
            if($type == 'material')
            {
                $tmp_qty = ActualingMfgQtyTmp::where('actualing_id','=',$actualing_id)->get();
                $tmp_qty_ids = array();
                foreach ($tmp_qty as $qty_dt)
                {
                    $tmp_qty_ids[] = $qty_dt->qty_id;
                }
                if(count($tmp_qty_ids) > 0)
                {
                    foreach ($tmp_qty_ids as $tmp_qty_id)
                    {
                        $qry_position_data = ActualingMFGPosition::where('actualing_id','=',$actualing_id)->where('qty_id','=',$tmp_qty_id);

                        // Is all positions for current QTY complete?
                        $is_complete = is_mfg_qty_complete($tmp_qty_id);
                        if($is_complete === false)
                        {
                            $qry_position_data->where('status','=','ONPROCESS');
                        }

                        $available_positions = $qry_position_data->get();
                        $qty_data = BomQuantity::find($tmp_qty_id);

                        foreach ($available_positions as $available_pos) {
                            $pos_data = BomPosition::find($available_pos->position_id);
                            $position_datas[$tmp_qty_id]['position'][] = array(
                                'id' => $pos_data->id,
                                'label' => 'Qty: '.$qty_data->qty_num.'; Position: '.$pos_data->position_num
                            );
                        }

                        $position_datas[$tmp_qty_id]['is_complete'] = $is_complete ? 'true' : 'false';
                    }
                    // Log::debug($position_datas);
                }
                // die;
            }
            else
            {
                $tmp_qty = ActualingNonMfgQtyTmp::where('actualing_id','=',$actualing_id)->get();
                $tmp_qty_ids = array();
                foreach ($tmp_qty as $qty_dt)
                {
                    $tmp_qty_ids[] = $qty_dt->qty_id;
                }
                if(count($tmp_qty_ids) > 0)
                {
                    foreach ($tmp_qty_ids as $tmp_qty_id)
                    {

                        $qry_position_data = ActualingNonMFGPosition::where('actualing_id','=',$actualing_id)->where('qty_id','=',$tmp_qty_id);

                        // Is all positions for current QTY complete?
                        $is_complete = is_non_mfg_qty_complete($tmp_qty_id);
                        if($is_complete === false)
                        {
                            $qry_position_data->where('status','=','ONPROCESS');
                        }

                        $available_positions = $qry_position_data->get();
                        $qty_data = MasterplanQuantity::find($tmp_qty_id);

                        foreach ($available_positions as $available_pos) {
                            $pos_data = MasterplanPosition::find($available_pos->position_id);
                            $position_datas[$tmp_qty_id]['position'][] = array(
                                'id' => $pos_data->id,
                                'label' => 'Qty: '.$qty_data->qty_num.'; Position: '.$pos_data->position_num
                            );
                        }

                        $position_datas[$tmp_qty_id]['is_complete'] = $is_complete ? 'true' : 'false';
                    }
                    // Log::debug($position_datas);
                }
                // die;
            }

            $response = Response::make($position_datas, 200);
            $response->header('Content-Type', 'Application/JSON');
            return $response;
        }
        catch (ErrorException $e)
        {
            Log::debug('No positions. Submitted data: '.json_encode($_POST));
        }
    }

    public function get_project_planning($type)
    {
        $barcode = trim(Input::get('barcode'));
        $process_code = trim(Input::get('process_code'));
        $response_data = array();
        switch($type)
        {
            case 'material':
                $planning_data =  DB::table('project_planning_detail_mfg AS p')
                    ->join('project_bom AS b', 'b.id', '=', 'p.material_id')
                    ->join('master_process AS m', 'm.id', '=', 'p.process_id')
                    ->join('projects AS pr', 'pr.id', '=', 'p.project_id')
                    ->join('master_resource_group AS mr', 'mr.id', '=', 'p.resource_group_id')
                    ->where(function($query) {
                        $barcode = trim(Input::get('barcode'));
                        $process_code = trim(Input::get('process_code'));

                        if($barcode)
                        {
                            $query->where('b.barcode', '=', $barcode);
                        }

                        if($process_code)
                        {
                            $query->where('m.code', '=', $process_code);
                        }

                        $query->where('b.will_processed', '=', '1');
                    })
                    ->select('p.*',
                            'b.barcode',
                            'b.material_name',
                            'b.qty',
                            'pr.nop',
                            'pr.project_name AS project',
                            'm.code AS process_code',
                            'm.process',
                            'mr.code AS resource_code',
                            'mr.name AS resource')
                    ->groupBy('m.code')
                    ->get();

                if(count($planning_data) > 0)
                {
                    $material_data = ProjectBOM::where('barcode','=',$barcode)->get();
                    foreach($planning_data as $planning)
                    {
                        $actualing = ActualingMFG::where('planning_id','=',$planning->id)->get();
                        $planning->resource_name = '';
                        $planning->pic_name = '';
                        $planning->actualing_id = '';
                        $planning->actualing_status = '';
                        $planning->actualing_finish = '';
                        $planning->actualing_subfinish = '';
                        $planning->status_msg = '';
                        if(count($actualing) > 0)
                        {
                            $actpos_query = ActualingMFGPosition::where('actualing_id','=', $actualing[0]->id);
                            $actpos_total = $actpos_query->count();
                            $actpos_complete_total = $actpos_query->where('status','=','COMPLETE')->count();

                            $planning->pos_total = $actpos_total;
                            $planning->pos_complete = $actpos_complete_total;

                            $resource = Resource::find($actualing[0]->resource_id);
                            $pic = Pic::find($actualing[0]->pic_id);

                            $actualing_log = ActualingMFGLog::where('actualing_id','=',$actualing[0]->id)->orderBy('created_at','desc')->take(1)->get();
                            $last_status = count($actualing_log) > 0 ? $actualing_log[0]->state : '';

                            $planning->resource_name = isset($resource->name) ? $resource->name : '';
                            $planning->pic_name = isset($pic->id) ? $pic->first_name.' '.$pic->last_name : '';
                            $planning->actualing_id = $actualing[0]->id;
                            $planning->actualing_status = $actualing[0]->status == 'ON PROCESS' ? ($last_status == 'START' ? 'ON PROCESS' : 'COMPLETE') : $actualing[0]->status;
                            $planning->actualing_finish = $actualing[0]->finish_type ? $actualing[0]->finish_type : '';
                            $planning->actualing_subfinish = $actpos_total > 0 ? ($actpos_complete_total < $actpos_total ? 'ONPROCESS' : 'COMPLETE') : '';

                            $last_status = ActualingMFGStatusLog::where('actualing_id','=',$actualing[0]->id)->orderBy('id','desc')->take(1)->get();
                            $planning->status_msg = count($last_status) ? $last_status[0]->status : '';
                        }

                        $response_data[] = $planning;
                    }
                }
                break;

            default:
                $planning_data =  DB::table('project_planning_detail_non_mfg AS p')
                    ->join('master_process AS m', 'm.id', '=', 'p.process_id')
                    ->join('projects AS pr', 'pr.id', '=', 'p.project_id')
                    ->join('master_resource_group AS mr', 'mr.id', '=', 'p.resource_group_id')
                    ->where(function($query){
                        $nop = trim(Input::get('nop'));
                        $process_code = trim(Input::get('process_code'));

                        if($nop)
                        {
                            $query->where('pr.nop', 'LIKE', $nop.'%');
                        }

                        if($process_code)
                        {
                            $query->where('m.code', '=', $process_code);
                        }
                    })
                    ->select('p.*',
                        'pr.nop',
                        'pr.qty',
                        'pr.project_name AS project',
                        'm.code AS process_code',
                        'm.process',
                        'p.estimation_hour AS estimation')
                    ->get();
                if(count($planning_data) > 0)
                {
                    $nop = trim(Input::get('nop'));
                    $project_data = Project::where('nop','=',$nop)->get();
                    foreach($planning_data as $planning)
                    {
                        $actualing = ActualingNonMFG::where('planning_id','=',$planning->id)->get();
                        $planning->resource_name = '';
                        $planning->pic_name = '';
                        $planning->actualing_id = '';
                        $planning->actualing_status = '';
                        $planning->actualing_finish = '';
                        $planning->actualing_subfinish = '';
                        $planning->status_msg = '';
                        if(count($actualing) > 0)
                        {
                            $actpos_query = ActualingNonMFGPosition::where('actualing_id','=', $actualing[0]->id);
                            $actpos_total = $actpos_query->count();
                            $actpos_complete_total = $actpos_query->where('status','=','COMPLETE')->count();

                            $planning->pos_total = $actpos_total;
                            $planning->pos_complete = $actpos_complete_total;

                            $resource = Resource::find($actualing[0]->resource_id);
                            $pic = Pic::find($actualing[0]->pic_id);

                            $actualing_log = ActualingNonMFGLog::where('actualing_id','=',$actualing[0]->id)->orderBy('created_at','desc')->take(1)->get();
                            $last_status = count($actualing_log) > 0 ? $actualing_log[0]->state : '';

                            $planning->resource_name = isset($resource->name) ? $resource->name : '';
                            $planning->pic_name = isset($pic->id) ? $pic->first_name.' '.$pic->last_name : '';
                            $planning->actualing_id = $actualing[0]->id;
                            $planning->actualing_status = $actualing[0]->status == 'ON PROCESS' ? ($last_status == 'START' ? 'ON PROCESS' : 'COMPLETE') : $actualing[0]->status;
                            $planning->actualing_finish = $actualing[0]->finish_type ? $actualing[0]->finish_type : '';
                            $planning->actualing_subfinish = $actpos_total > 0 ? ($actpos_complete_total < $actpos_total ? 'ONPROCESS' : 'COMPLETE') : '';

                            $last_status = ActualingNonMFGStatusLog::where('actualing_id','=',$actualing[0]->id)->orderBy('id','desc')->take(1)->get();
                            $planning->status_msg = count($last_status) ? $last_status[0]->status : '';
                        }

                        $response_data[] = $planning;
                    }
                }
                break;
        }

        $response = Response::make($response_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function do_start_actualing($type)
    {
        $actualing_id = Input::get('actualing');
        $resource_code = Input::get('resource');
        $planning_id = Input::get('planning_id');
        $pic_code = Input::get('pic');
        $qty = Input::get('qty');
        $resource_by_code = Resource::where('code', '=', $resource_code)->first();
        $pic_by_code = Pic::where('code', '=', $pic_code)->first();

        if($resource_code && $pic_code)
        {
            switch($type)
            {
                case 'material':
                    $barcode = trim(Input::get('barcode'));
                    $process_code = trim(Input::get('process'));

                    $material_data = ProjectBOM::where('barcode','=',$barcode)->first();
                    $process_data = Process::where('code','=',$process_code)->first();
                    $planning = PlanningMFG::find($planning_id);

                    if($actualing_id)
                    {
                        $actualing = ActualingMFG::find($actualing_id);
                        $start_type = $actualing->finish_type == 'PLAN' && $actualing->subfinish_type == 'COMPLETE' ? 'REWORK' : $actualing->finish_type;
                    }
                    else
                    {
                        $actualing = new ActualingMFG;
                        $start_type = 'PLAN';
                    }

                    if($resource_by_code)
                    {
                        $actualing->resource_id = $resource_by_code->id;
                        $actualing_data['resource_name'] = $resource_by_code->name;
                    }

                    if($pic_by_code)
                    {
                        $actualing->pic_id = $pic_by_code->id;
                        $actualing_data['pic_name'] = $pic_by_code->first_name." ".$pic_by_code->last_name;
                    }

                    if($material_data)
                    {
                        $actualing->material_id = $material_data->id;
                        $actualing->plan_qty = $material_data->qty;
                    }

                    if($process_data)
                    {
                        $actualing->process_id = $process_data->id;
                    }

                    $actualing->planning_id = $planning->id;
                    $actualing->total_start = 1;
                    $actualing->plan_duration = $planning->estimation_hour;
                    $actualing->actual_duration = 0;
                    $actualing->start_date = date('Y-m-d H:i:s');
                    $actualing->status = "ON PROCESS";
                    $actualing->user_id = $this->user->id;
                    $actualing->save();

                    save_actualing_mfg_position($actualing->id);

                    if(count($qty) > 0)
                    {
                        foreach($qty as $qty)
                        {
                            $act_qty_tmp = ActualingMfgQtyTmp::where('actualing_id','=',$actualing->id)->where('qty_id','=',$qty)->get();
                            if(count($act_qty_tmp) == 0)
                            {
                                $new_act_qty_tmp = new ActualingMfgQtyTmp;
                                $new_act_qty_tmp->actualing_id = $actualing->id;
                                $new_act_qty_tmp->qty_id = $qty;
                                $new_act_qty_tmp->save();
                            }

                            $qry = ActualingMFGPosition::where('qty_id','=',$qty);
                            $total_positions = $qry->count();
                            $total_complete_positions = $qry->where('status','=','COMPLETE')->count();

                            $qty_data = BomQuantity::find($qty);

                            $actualing_data['qty'][] = array(
                                'id' => $qty,
                                'qty_num' => $qty_data->qty_num,
                                'is_complete' => $total_positions == $total_complete_positions ? true : false
                            );
                        }
                    }

                    $actualing_log = new ActualingMFGLog;
                    $actualing_log->actualing_id = $actualing->id;
                    $actualing_log->state = "START";

                    if(count($resource_by_code) > 0)
                    {
                        $actualing_log->resource_id = $resource_by_code->id;
                    }

                    if(count($pic_by_code))
                    {
                        $actualing_log->pic_id = $pic_by_code->id;
                    }

                    $actualing_log->save();

    				$planning->is_actualing = 1;
    				$planning->save();

                    $project_data = Project::find($planning->project_id);
                    $project_data->is_actualing = 'yes';

                    $message_format = "Project:%s\nMaterial:%s\nProcess:%s\nResource:%s\nPIC:%s\nStatus:%s\nType:%s\nTime:%s\n";
                    $message = sprintf(
                        $message_format,
                        "[".@$project_data->nop."] ".$project_data->project_name,
                        @$material_data->material_name,
                        @$process_data->process,
                        @$actualing_data['resource_name'],
                        @$actualing_data['pic_name'],
                        'START',
                        $start_type,
                        date('d-M-Y H:i')
                    );

                    $subject = "TMMS ALERT - [START] " . @$actualing_data['resource_name'];

                    $project_data->save();

                    $resource_used = new ResourceActualUsed;
                    $resource_used->resource_id = $resource_by_code->id;
                    $resource_used->actualing_id = $actualing->id;
                    $resource_used->type = 'material';
                    $resource_used->save();
                    break;

                default:
                    $nop = trim(Input::get('nop'));
                    $process_code = trim(Input::get('process'));
                    $project_data = Project::where('nop','=',$nop)->first();
                    $process_data = Process::where('code','=',$process_code)->first();
                    $planning = PlanningNonMFG::find($planning_id);

                    if($actualing_id)
                    {
                        $actualing = ActualingNonMFG::find($actualing_id);
                        $start_type = $actualing->finish_type == 'PLAN' && $actualing->subfinish_type == 'COMPLETE' ? 'REWORK' : $actualing->finish_type;
                    }
                    else
                    {
                        $actualing = new ActualingNonMFG;
                        $start_type = 'PLAN';
                    }

                    if(count($project_data) > 0)
                    {
                        $actualing->project_id = $project_data->id;
                    }

                    if(count($resource_by_code) > 0)
                    {
                        $actualing->resource_id = $resource_by_code->id;
                        $actualing_data['resource_name'] = $resource_by_code->name;
                    }

                    if(count($pic_by_code))
                    {
                        $actualing->pic_id = $pic_by_code->id;
                        $actualing_data['pic_name'] = $pic_by_code->first_name." ".$pic_by_code->last_name;
                    }

                    if(count($process_data) > 0)
                    {
                        $actualing->process_id = $process_data->id;
                    }

                    $actualing->planning_id = $planning->id;
                    $actualing->total_start = 1;
                    $actualing->plan_duration = $planning->estimation_hour;
                    $actualing->actual_duration = 0;
                    $actualing->start_date = date('Y-m-d H:i:s');
                    $actualing->status = "ON PROCESS";
                    $actualing->user_id = $this->user->id;
                    $actualing->save();

                    save_actualing_non_mfg_position($actualing->id);

                    if(count($qty) > 0)
                    {
                        foreach($qty as $qty)
                        {
                            $act_qty_tmp = ActualingNonMfgQtyTmp::where('actualing_id','=',$actualing->id)->where('qty_id','=',$qty)->get();
                            if(count($act_qty_tmp) == 0)
                            {
                                $new_act_qty_tmp = new ActualingNonMfgQtyTmp;
                                $new_act_qty_tmp->actualing_id = $actualing->id;
                                $new_act_qty_tmp->qty_id = $qty;
                                $new_act_qty_tmp->save();
                            }

                            $qry = ActualingNonMFGPosition::where('qty_id','=',$qty);
                            $total_positions = $qry->count();
                            $total_complete_positions = $qry->where('status','=','COMPLETE')->count();

                            $qty_data = MasterplanQuantity::find($qty);

                            $actualing_data['qty'][] = array(
                                'id' => $qty,
                                'qty_num' => $qty_data->qty_num,
                                'is_complete' => $total_positions == $total_complete_positions ? true : false
                            );
                        }
                    }

                    $actualing_log = new ActualingNonMFGLog;
                    $actualing_log->actualing_id = $actualing->id;
                    $actualing_log->state = 'START';

                    if(count($resource_by_code) > 0)
                    {
                        $actualing_log->resource_id = $resource_by_code->id;
                    }

                    if(count($pic_by_code))
                    {
                        $actualing_log->pic_id = $pic_by_code->id;
                    }

                    $actualing_log->save();

    				$planning->is_actualing = 1;
    				$planning->save();

                    $project_data = Project::find($planning->project_id);
                    $project_data->is_actualing = 1;

                    $message_format = "Project:%s\nProcess:%s\nResource:%s\nPIC:%s\nStatus:%s\nType:%s\nTime:%s\n";
                    $message = sprintf(
                        $message_format,
                        "[".@$project_data->nop."] ".$project_data->project_name,
                        @$process_data->process,
                        @$actualing_data['resource_name'],
                        @$actualing_data['pic_name'],
                        'START',
                        $start_type,
                        date('d-M-Y H:i')
                    );

                    $subject = "TMMS ALERT - [START] ". @$actualing_data['resource_name'];

                    $project_data->save();

                    $resource_used = new ResourceActualUsed;
                    $resource_used->resource_id = $resource_by_code->id;
                    $resource_used->actualing_id = $actualing->id;
                    $resource_used->type = 'nop';
                    $resource_used->save();
                    break;
            }

            $alert_users = AlertSettingUser::where('setting_id','=','1')->get();

            if(count($alert_users) > 0)
            {
                foreach ($alert_users as $alert_user)
                {
                    $user = UserInformation::find($alert_user->user_id);
                    if($alert_user->send_sms == '1' && @$user->phone_number)
                    {
                        // SMS Notification
                        $sms_queue = new SmsQueue;
                        $sms_queue->send_to = $user->phone_number;
                        $sms_queue->message = $message;
                        $sms_queue->save();
                    }

                    if($alert_user->send_email == '1' && @$user->email)
                    {
                        // Email Notification
                        $email_message = nl2br($message);
                        Mail::queue(Config::get('app.theme').'.mails.main', array('content' => $email_message), function($msg) use ($user, $subject)
                        {
                            $msg->from('alert@tmms.tridaya-as.com', 'TMMS Alert');
                            $msg->to($user->email, $user->first_name." ".@$user->last_name)->subject($subject);
                        });
                    }
                }
            }

            $actualing_data['id'] = $actualing->id;
            $actualing_data['pic_id'] = @$actualing->pic_id;
            $actualing_data['resource_id'] = @$actualing->resource_id;
            $actualing_data['status'] = 'SUCCESS';
        }
        else
        {
            $actualing_data = array(
                'status' => 'FAILED'
            );
        }

        if($actualing_data['pic_id'] == '')
        {
            Bugsnag::notifyException(new Exception("Actualing with No PIC"));
        }

        Activity::log([
            'contentId'   => $actualing->id,
            'contentType' => 'Actualing',
            'action'      => 'Start actualing',
            'description' => "Start actualing {$type} type",
            'details'     => json_encode($actualing_data),
            'updated'     => true,
        ]);

        $response = Response::make($actualing_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

	public function do_finish_actualing($type)
    {
        $actualing_id = Input::get('actualing');
		$positions = Input::get('position');
		$remark = Input::get('remark');
        $finish_type = strtoupper(Input::get('finish-type'));
		$subfinish_type = strtoupper(Input::get('subfinish-type'));

        switch($type)
        {
            case 'material':
                $actualing = ActualingMFG::find($actualing_id);

                $resource_used = ResourceActualUsed::where('actualing_id','=',$actualing->id)->where('type','=','material')->first();
                @$resource_used->delete();

				$start_time = strtotime($actualing->start_date);
                $duration = time() - $start_time;

                $actualing->actual_duration = $duration;
                $actualing->finish_type = $finish_type;
                $actualing->finish_date = date('Y-m-d H:i:s');
                $actualing->subfinish_type = $subfinish_type;

                if($finish_type == 'PLAN')
                {
                    $actualing->total_finish = 1;
                }
                elseif($finish_type == 'REWORK')
                {
                    $actualing->total_rework = $actualing->total_rework + 1;
                }
                else
                {
                    $actualing->total_guarantee = $actualing->total_guarantee + 1;
                }

				$status = "ON PROCESS";
                if(count($positions) > 0)
                {
                    foreach($positions as $position)
                    {
                        if($subfinish_type == 'COMPLETE')
                        {
                            update_actualing_mfg_position_status($actualing->id, $position, $subfinish_type);
                        }
                    }

                    $completed_positions = ActualingMFGPosition::where('actualing_id','=',$actualing->id)->where('status','=','COMPLETE')->count();
                    $total_positions =  ActualingMFGPosition::where('actualing_id','=',$actualing->id)->count();

                    if($completed_positions === $total_positions)
                    {
                        $status = "COMPLETE";
                    }

                    DB::table('actualing_mfg_qty_tmp')->where('actualing_id','=',$actualing->id)->delete();
                }

                $actualing->status = $status;
                $actualing->remark = $remark;

                if($actualing->save())
                {
                    $actualing_log = new ActualingMFGLog;
                    $actualing_log->actualing_id = $actualing_id;
                    $actualing_log->state = "COMPLETE";
                    $actualing_log->finish_type = $finish_type;
                    $actualing_log->remark = $remark;
                    $actualing_log->last_duration = $duration;
                    $actualing_log->resource_id = $actualing->resource_id;
                    $actualing_log->pic_id = $actualing->pic_id;
                    $actualing_log->save();
                }

                $actualing_data['id'] = $actualing->id;
                $actualing_data['finish'] = $finish_type;
                $actualing_data['finish_type'] = str_replace(' ', '', $status);

                $planning = PlanningMFG::find($actualing->planning_id);
                $project_data = Project::find($planning->project_id);
                $material_data = ProjectBOM::find($actualing->material_id);
                $process_data = Process::find($planning->process_id);
                $resource = Resource::find($actualing->resource_id);
                $pic = Pic::find($actualing->pic_id);
                $all_duration = ActualingMFGLog::where('actualing_id','=',$actualing->id)->sum('last_duration');
                $message_format = "Project:%s\nMaterial:%s\nProcess:%s\nResource:%s\nPIC:%s\nStatus:%s\nType:%s\nDuration:%s/%s\nTime:%s\n";
                $message = sprintf(
                    $message_format,
                    @$project_data->nop,
                    @$material_data->material_name,
                    @$process_data->process,
                    @$resource->name,
                    @$pic->first_name." ".substr(@$pic->last_name,0,1),
                    'FINISH',
                    $finish_type,
                    round($duration/3600,1).'hrs',
                    round($all_duration/3600,1).'hrs',
                    date('d-M-Y H:i')
                );
                $subject = "TMMS ALERT - [FINISH] ".@$resource->name;
                break;

            default:
                $actualing = ActualingNonMFG::find($actualing_id);
                $start_time = strtotime($actualing->start_date);
                $duration = time() - $start_time;

                $resource_used = ResourceActualUsed::where('actualing_id','=',$actualing->id)->where('type','=','nop')->first();
                @$resource_used->delete();

                $actualing->actual_duration = $duration;
                $actualing->finish_type = $finish_type;
                $actualing->finish_date = date('Y-m-d H:i:s');
                $actualing->subfinish_type = $subfinish_type;

                if($finish_type == 'PLAN')
                {
                    $actualing->total_finish = 1;
                }
                elseif($finish_type == 'REWORK')
                {
                    $actualing->total_rework = $actualing->total_rework + 1;
                }
                else
                {
                    $actualing->total_guarantee = $actualing->total_guarantee + 1;
                }

                $status = "ON PROCESS";
                if(count($positions) > 0)
                {
                    foreach($positions as $position)
                    {
                        if($subfinish_type == 'COMPLETE')
                        {
                            update_actualing_non_mfg_position_status($actualing->id, $position, $subfinish_type);
                        }
                    }

                    $completed_positions = ActualingNonMFGPosition::where('actualing_id','=',$actualing->id)->where('status','=','COMPLETE')->count();
                    $total_positions =  ActualingNonMFGPosition::where('actualing_id','=',$actualing->id)->count();

                    if($completed_positions === $total_positions)
                    {
                        $status = "COMPLETE";
                    }

                    DB::table('actualing_non_mfg_qty_tmp')->where('actualing_id','=',$actualing->id)->delete();
                }

                $actualing->status = $status;
                $actualing->remark = $remark;

                if($actualing->save())
                {
                    $last_actualing_log = ActualingNonMFGLog::where('actualing_id','=',$actualing_id)->orderBy('created_at','desc')->take(1)->get();
                    $actualing_log = new ActualingNonMFGLog;
                    $actualing_log->actualing_id = $actualing->id;
                    $actualing_log->state = 'COMPLETE';
                    $actualing_log->last_duration = $duration;
                    $actualing_log->remark = $remark;
                    $actualing_log->resource_id = $actualing->resource_id;
                    $actualing_log->pic_id = $actualing->pic_id;
                    $actualing_log->finish_type = $finish_type;
                    $actualing_log->save();
                }

                $actualing_data['id'] = $actualing->id;
                $actualing_data['finish'] = $finish_type;
                $actualing_data['finish_type'] = str_replace(' ', '', $status);

                $planning = PlanningNonMFG::find($actualing->planning_id);
                $project_data = Project::find($planning->project_id);
                $material_data = ProjectBOM::find($actualing->material_id);
                $process_data = Process::find($planning->process_id);
                $resource = Resource::find($actualing->resource_id);
                $pic = Pic::find($actualing->pic_id);
                $all_duration = ActualingMFGLog::where('actualing_id','=',$actualing->id)->sum('last_duration');
                $message_format = "Project:%s\nProcess:%s\nResource:%s\nPIC:%s\nStatus:%s\nType:%s\nDuration:%s\nTime:%s\n";
                $message = sprintf(
                    $message_format,
                    "[".@$project_data->nop."]".@$project_data->project_name,
                    @$process_data->process,
                    @$resource->name,
                    @$pic->first_name." ".@$pic->last_name,
                    'FINISH',
                    $finish_type,
                    round($duration/3600,1).'hrs/'.round($all_duration/3600,1).'hrs',
                    date('d-M-Y H:i')
                );
                $subject = "TMMS ALERT - [FINISH] ".@$resource->name;
                break;
        }

        $alert_users = AlertSettingUser::where('setting_id','=','1')->get();

        if(count($alert_users) > 0)
        {
            foreach ($alert_users as $alert_user)
            {
                $user = UserInformation::find($alert_user->user_id);
                if($alert_user->send_sms == '1' && @$user->phone_number)
                {
                    // SMS Notification
                    $sms_queue = new SmsQueue;
                    $sms_queue->send_to = $user->phone_number;
                    $sms_queue->message = $message;
                    $sms_queue->save();
                }

                if($alert_user->send_email == '1' && @$user->email)
                {
                    // Email Notification
                    $email_message = nl2br($message);
                    Mail::queue(Config::get('app.theme').'.mails.main', array('content' => $email_message), function($msg) use ($user, $subject)
                    {
                        $msg->from('alert@tmms.tridaya-as.com', 'TMMS Alert');
                        $msg->to($user->email, $user->first_name." ".@$user->last_name)->subject($subject);
                    });
                }
            }
        }

        Activity::log([
            'contentId'   => $actualing_id,
            'contentType' => 'Actualing',
            'action'      => 'Start actualing',
            'description' => "Start actualing {$type} type",
            'details'     => json_encode($actualing_data),
            'updated'     => true,
        ]);

        $response = Response::make($actualing_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
	}

    public function get_actualing_log_data($type)
    {
        $actualing_id = Input::get('actualing_id');
        $response_data = array();
        switch($type)
        {
            case 'material':
                $actualing_data = DB::table('actualing_mfg AS a')
                    ->join('project_planning_detail_mfg AS s', 's.id', '=', 'a.planning_id')
                    ->join('master_resource AS r', 'r.id', '=', 's.resource_id')
                    ->join('master_process AS c', 'c.id', '=', 's.process_id')
                    ->join('projects AS p', 'p.id', '=', 's.project_id')
                    ->where('a.id', '=', $actualing_id)
                    ->select('a.*', 'c.process', 's.process_id', 's.estimation_hour', 'r.name AS resource', 'p.nop', 'p.project_name AS project')
                    ->get();
                break;

            default:
                $actualing_data = DB::table('actualing_non_mfg AS a')
                    ->join('project_planning_detail_non_mfg AS s', 's.id', '=', 'a.planning_id')
                    ->join('master_resource AS r', 'r.id', '=', 's.resource_id')
                    ->join('master_process AS c', 'c.id', '=', 's.process_id')
                    ->join('projects AS p', 'p.id', '=', 's.project_id')
                    ->where('a.id', '=', $actualing_id)
                    ->select('a.*', 'c.process', 's.process_id', 's.estimation_hour', 'r.name AS resource', 'p.nop', 'p.project_name AS project')
                    ->get();
                break;
        }

        $response = Response::make($actualing_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_actualing_data($type)
    {
        $actualing_id = Input::get('actualing_id');
        $response_data = array();
        switch($type)
        {
            case 'material':
                $actualing_data = DB::table('actualing_mfg AS a')
                    ->join('project_planning_detail_mfg AS s', 's.id', '=', 'a.planning_id')
                    ->join('master_resource AS r', 'r.id', '=', 's.resource_id')
                    ->join('projects AS p', 'p.id', '=', 's.project_id')
                    ->where('a.id', '=', $actualing_id)
                    ->select('a.*', 's.process_id', 's.estimation_hour', 'r.name AS resource', 'p.project_name AS project')
                    ->get();
                break;

            default:
                $actualing_data = DB::table('actualing_non_mfg AS a')
                    ->join('project_planning_detail_non_mfg AS s', 's.id', '=', 'a.planning_id')
                    ->join('master_resource AS r', 'r.id', '=', 's.resource_id')
                    ->join('projects AS p', 'p.id', '=', 's.project_id')
                    ->where('a.id', '=', $actualing_id)
                    ->select('a.*', 's.process_id', 's.estimation_hour', 'r.name AS resource', 'p.project_name AS project')
                    ->get();
                break;
        }
        $response = Response::make($actualing_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_resource_data()
    {
        $resource = Resource::where(function($query){
            $planning_id = Input::get('planning_id');
            $type = Input::get('type');

            if ($type == 'material') $planning_data = PlanningMFG::find($planning_id);
            elseif ($type == 'pb-bom') $planning_data = PlanningPBBOM::find($planning_id);
            else $planning_data = PlanningNonMFG::find($planning_id);

//            debug($planning_data);

            $resource_for_process = ProcessResourceGroup::where('process_id','=',$planning_data->process_id)->get();
            $resources = array();
            if(count($resource_for_process)) {
                foreach ($resource_for_process as $data) {
                    $resources[] = $data->resource_id;
                }
            }

            $query->whereIn('id', $resources);
        })->get();
        $response = Response::make($resource, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_pic_data()
    {
        $resource_id = Input::get('resource_id');
        $pic_data = DB::table('resource_pic AS rp')->join('master_pic AS pic', 'pic.id', '=', 'rp.pic_id')
						->where('rp.resource_id', '=', $resource_id)->select('pic.*')->get();
        $response = Response::make($pic_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function set_status_log()
    {
        $type = Input::get('type');
        $actualing_id = Input::get('actualing_id');
        $status = Input::get('status');

        $status_log = $type == 'material' ? new ActualingMFGStatusLog : new ActualingNonMFGStatusLog;
        $status_log->actualing_id = $actualing_id;
        $status_log->status = $status;
        $status_log->save();
    }
}
