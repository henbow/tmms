<?php
use ProcessGroup;

class ProcessGroupController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'planning');
            $view->with('child_active', 'process_group');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.project_process_group.index');
        $view->with('page_title', 'Master Plan Group Management');
        $view->with('sub_title', 'Manage Master Plan Group');
        $view->with('theme', Config::get('app.theme'));
        $view->with('groups', ProcessGroup::all());

        return $view;
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.project_process_group.create');
        $view->with('page_title', 'Master Plan Group Management');
        $view->with('sub_title', 'Create new Master Plan Group');
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function store()
    {
        $process_group = new ProcessGroup;
        $process_group->group = Input::get('group_name');
        $process_group->type = Input::get('group_type');

        $last_group_order = ProcessGroup::select('order')->take(1)->orderBy('order', 'desc')->get()[0]->order;

        $process_group->order = $last_group_order + 1;

        if($process_group->save())
            Notification::success('New Master Plan Group data was saved.');

        return Redirect::route('process_group.index');
    }

    public function edit($id)
    {
        $view = View::make(Config::get('app.theme').'.project_process_group.edit');
        $view->with('page_title', 'Master Plan Group Management');
        $view->with('sub_title', 'Edit Master Plan Group data');
        $view->with('theme', Config::get('app.theme'));
        $view->with('group', ProcessGroup::find($id));

        return $view;
    }

    public function update($id)
    {
        $process_group = ProcessGroup::find($id);
        $process_group->group = Input::get('group_name');
        $process_group->type = Input::get('group_type');

        if($process_group->save())
            Notification::success('Master Plan Group data changes was saved.');

        return Redirect::route('process_group.index');
    }

    public function destroy($id)
    {
        $process_group = ProcessGroup::find($id);
        $process_group->delete();

        return Redirect::route('process_group.index');
    }
}
