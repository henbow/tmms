<?php
use ResourceSpec;
use Resource;
// use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification;

class ResourceSpecificationController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'resource_center');
            $view->with('child_active', 'resource_management');
        });
    }

    public function create($resource_id)
    {
        $view = View::make(Config::get('app.theme').'.resource_spec.create');
        $view->with('page_title', 'Resource Specification Management');
        $view->with('sub_title', 'Specify resource specification for ' . strtoupper(Resource::getName($resource_id)));
        $view->with('theme', Config::get('app.theme'));
        $view->with('resource_id', $resource_id);

        return $view;
    }

    public function store($resource_id)
    {
        $resource_spec = new ResourceSpec;
        $resource_spec->resource_id = $resource_id;
        $resource_spec->min_table_load = Input::get('mintabload');
        $resource_spec->max_table_load = Input::get('maxtabload');
        $resource_spec->table_size = Input::get('tablesize');
        $resource_spec->travel_range = Input::get('travelrange');
        $resource_spec->accuracy = Input::get('accuracy');
        $resource_spec->min_mould_size = Input::get('minmouldsize');
        $resource_spec->max_mould_size = Input::get('maxmouldsize');
        $resource_spec->std_rate_per_hour = Input::get('stdrate');
        $resource_spec->ovt_rate_per_hour = Input::get('ovtrate');

        if ($resource_spec->save())
            Notification::success('Resource specification for ' . Resource::getName($resource_id) . ' was saved.');

        return Redirect::route('resource.show', $resource_id);
    }

    public function edit($resource_id, $id)
    {
        $view = View::make(Config::get('app.theme').'.resource_spec.edit');
        $view->with('page_title', 'Resource Specification Management');
        $view->with('sub_title', 'Edit resource specification for ' . strtoupper(Resource::getName($resource_id)));
        $view->with('theme', Config::get('app.theme'));
        $view->with('spec', ResourceSpec::find($id));
        $view->with('resource_id', $resource_id);

        return $view;
    }

    public function update($resource_id, $id)
    {
        $resource_spec = ResourceSpec::find($id);
        $resource_spec->resource_id = $resource_id;
        $resource_spec->min_table_load = Input::get('mintabload');
        $resource_spec->max_table_load = Input::get('maxtabload');
        $resource_spec->table_size = Input::get('tablesize');
        $resource_spec->travel_range = Input::get('travelrange');
        $resource_spec->accuracy = Input::get('accuracy');
        $resource_spec->min_mould_size = Input::get('minmouldsize');
        $resource_spec->max_mould_size = Input::get('maxmouldsize');
        $resource_spec->std_rate_per_hour = Input::get('stdrate');
        $resource_spec->ovt_rate_per_hour = Input::get('ovtrate');

        if ($resource_spec->save())
            Notification::success('Resource specification changes for ' . Resource::getName($resource_id) . ' was saved.');

        return Redirect::route('resource.show', $resource_id);
    }
}