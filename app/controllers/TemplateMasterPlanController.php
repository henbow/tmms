<?php

use TemplateMasterPlanHeader;
use TemplateMasterPlanDetail;

// use BaseController, Form, Input, Redirect, Sentry, View, Notification;

class TemplateMasterPlanController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'planning');
            $view->with('child_active', 'master_plan_template');
        });
    }

	public function index()
    {
        $view = View::make(Config::get('app.theme').'.template.master_plan.header.index');
        $view->with('page_title', 'Template Master Plan');
        $view->with('sub_title', 'Manage Template');
        $view->with('theme', Config::get('app.theme'));
        $view->with('headers', TemplateMasterPlanHeader::all());

        return $view;
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.template.master_plan.header.add');
        $view->with('page_title', 'Template Master Plan');
        $view->with('sub_title', 'Manage Template');
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function store()
    {
        $template = new TemplateMasterPlanHeader;
        $template->name = Input::get('header_name');
        $template->pull_date_from = Input::get('date_from');

        if($template->save())
            Notification::success('New template was saved.');

        return Redirect::back();
    }

    public function edit($id)
    {
        $view = View::make(Config::get('app.theme').'.template.master_plan.header.edit');
        $view->with('page_title', 'Template Master Plan');
        $view->with('sub_title', 'Edit template data');
        $view->with('theme', Config::get('app.theme'));
        $view->with('template', TemplateMasterPlanHeader::find($id));

        return $view;
    }

    public function update($id)
    {
        $template = TemplateMasterPlanHeader::find($id);
        $template->name = Input::get('header_name');
        $template->pull_date_from = Input::get('date_from');

        if($template->save())
            Notification::success('Template data changes was saved.');

        return Redirect::back();
    }

    public function destroy($id)
    {
        $template = TemplateMasterPlanHeader::find($id);

		$template_details = TemplateMasterPlanDetail::where('header_id', '=', $id);
		if($template_details->count() > 0)
		{
			foreach($template_details->get(array('id')) as $detail)
			{
				$tpl_detail = TemplateMasterPlanDetail::find($detail->id);
				$tpl_detail->delete();
			}
		}

        $template->delete();

        return Redirect::back();
    }
}
