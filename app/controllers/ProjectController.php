<?php
class ProjectController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'planning');
            $view->with('child_active', 'do_planning');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.project.list');
        $view->with('page_title', 'Project');
        $view->with('sub_title', 'Project List');
        $view->with('filtered', true);
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function filter()
    {
        $view = View::make(Config::get('app.theme').'.project.list');
        $view->with('page_title', 'Filter Project');
        $view->with('sub_title', 'Project List');
        $view->with('filtered', false);
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function do_filter()
    {
        $mode = Input::get('action-mode');

        if($mode == 'filter')
        {
            $ids = Input::get('filter');
            if(count($ids) > 0)
            {
                foreach ($ids as $id) {
                    $project = Project::find($id);
                    $project->is_filtered = 'yes';
                    $project->save();
                }
            }
            else
            {
                $ids = array();
            }

            $filtered_projects = Project::where('is_filtered', '=', 'yes')->get();
            if(count($filtered_projects) > 0)
            {
                foreach ($filtered_projects as $filtered_project) {
                    $project = Project::find($filtered_project->id);
                    $project->is_filtered = in_array($filtered_project->id, $ids) ? 'yes' : 'no';
                    $project->save();
                }
            }
        }
        else
        {
            $ids = Input::get('update');
            Queue::push('ERPSync', array('ids' => $ids));
            Notification::success('Update project data process queue was submitted. Please wait about 10 minutes for project data get updated.<br>Mohon tunggu sekitar 10 menit agar proses update data project selesai.');
        }

        return Redirect::route('project.filter');
    }

    public function project_data()
    {
        $start_time = time();

        $filtered = Input::get('filtered');
        $mode = Input::get('mode');

        $project_qry = Project::take(250)
                    ->where('is_planning', '=', 'yes')
                    ->orderBy('nop', 'desc');
        if($filtered == 1)
        {
            $project_qry->with('masterplans')->where('is_filtered', '<>', 'yes');
        }
        $projects = $project_qry->get();

        $project_data = array(
            "iTotalRecords" => count($projects),
            "iTotalDisplayRecords" => count($projects)
        );

        if($filtered == 1)
        {
            foreach ($projects as $project)
            {
                if(isset($project->customer->code) && isset($project->customer->name))
                {
                    $customer_code = $project->customer->code;
                    $customer_name = $project->customer->name;
                    $has_bom = is_bom_imported($project->id);
                    $button_class = $has_bom ? 'btn-primary' : 'btn-danger';
                    $master_plan_data = $project->masterplans()->where('process_group_id', '=', '15')->first();
                    $detail_url = isset($master_plan_data->id) ? route('project.planning.detail', array($master_plan_data->process_group_id, $master_plan_data->id)) : route('project.planning.index', $project->id);
                    $project_data['aaData'][] = array(
                        $project->nop,
                        $project->order_num,
                        $project->project_name,
                        $customer_code." - ".$customer_name,
                        strtotime($project->start_date) < 0 ? '-' : date('d M Y', strtotime($project->start_date)),
                        strtotime($project->trial_date) < 0 ? '-' : date('d M Y', strtotime($project->trial_date)),
                        strtotime($project->delivery_date) < 0 ? '-' : date('d M Y', strtotime($project->delivery_date)),
                        $project->masterplans->count() ? "Planned" : "-",
                        '<div class="btn-group">
                            <button type="button" class="btn '.$button_class.' dropdown-toggle" data-toggle="dropdown"><i class="ico-paper-plane"></i> Action <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#" onclick="$(\'#bs-modal-lg .modal-content\').load(\''.route('project.show', $project->id).'\',function(e){$(\'#bs-modal-lg\').modal(\'show\');});"><i class="ico-profile"></i> Show Details</a></li>
                                <li><a href="#" onclick="NProgress.start();window.location.href=\''.route('project.view_bom', $project->id).'\';" class="view-bom"><i class="ico-unite"></i> View BOM</a></li>
                                <li class="divider"></li>
                                <!-- To Master Plannning Page -->
                                <li><a href="javascript:void(0)" onclick="do_redirect(\''.route('project.planning.index', $project->id).'\')" class="master-planning">
                                    <i class="ico-screen3"></i> <strong>Master Plan</strong></a>
                                </li>
                                <!-- To Detail Planning Page -->
                                <li><a href="'.$detail_url.'" class="detail-planning">
                                    <i class="ico-screen3"></i> <strong>Detail Plan</strong></a>
                                </li>
                            </ul>
                        </div>'
                    );
                }
            }
        }
        else
        {
            foreach ($projects as $project)
            {
                $has_bom = is_bom_imported($project->id);
                $button_class = $has_bom ? 'btn-primary' : 'btn-danger';
                $project_data['aaData'][] = array(
                    $project->nop,
                    $project->order_num,
                    $project->project_name,
                    (trim($project->customer_code) != '' ? $project->customer_code.' - ' : '') . $project->customer_name,
                    '<input type="checkbox" '.($project->is_filtered == 'yes' ? 'checked="checked"' : '').' value="'.$project->id.'" name="filter[]" class="filter-project" />',
                    '<input type="checkbox" value="'.$project->id.'" name="update[]" class="update-project" />',
                    '<a href="'.route('project.select_bom_for_process', $project->id).'" class="btn btn-sm '.$button_class.'">Filter BOM</a>'
                );
            }
        }

        $response = Response::make($project_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        $end_time = time();
        $total_time = $end_time - $start_time;

        return $response;
    }

    public function show($id)
    {
        $project = Project::find($id);
        $html = "
            <div class=\"modal-header text-left\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\">×</button>
                <h3 class=\"semibold modal-title text-primary\">Project Details [{$project->nop}]</h3>
            </div>
            <div class=\"modal-body\">
                <dl class=\"dl-horizontal\">
                    <dt>NOP</dt><dd>{$project->nop}</dd>
                    <dt>Project Name</dt><dd>{$project->project_name}</dd>
                    <dt class='text-left'>Order Number</dt><dd>{$project->order_num}</dd>
                    <dt>Priority</dt><dd>".($project->priority ? $project->priority : '-')."</dd>
                    <dt>Quantity</dt><dd>{$project->qty} unit</dd>
                    <dt>Price Per Unit</dt><dd>Rp ".number_format($project->price_per_unit, 2, ',', '.')."</dd>
                    <dt>Price Total</dt><dd>Rp ".number_format($project->price_total, 2, ',', '.')."</dd>
                    <dt>Customer Code</dt><dd>".(strlen(trim($project->customer_code)) > 0 ? $project->customer_code : '-')."</dd>
                    <dt>Customer Name</dt><dd>{$project->customer_name}</dd>
                    <dt>Start Date</dt><dd>".date('d M Y', strtotime($project->start_date))."</dd>
                    <dt>Trial Date</dt><dd>".date('d M Y', strtotime($project->trial_date))."</dd>
                    <dt>Delivery Date</dt><dd>".date('d M Y', strtotime($project->delivery_date))."</dd>
                </dl>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>
            </div>";
        return $html;
    }

    public function view_bom($id)
    {
        $project = Project::find($id);

        if( !is_bom_imported($id) )
        {
            $materials = NOPPImporter::getBomProject($project->nop);

            if(count($materials) > 0)
            {
                foreach ($materials as $material) {
                    save_erp_material($id, $material);
                }
            }
        }

        $view = View::make(Config::get('app.theme').'.project.bom');
        $view->with('page_title', 'BOM Lists');
        $view->with('sub_title', 'Project BOM ['.$project->nop.']');
        $view->with('theme', Config::get('app.theme'));
        $view->with('project', $project);
        $view->with('mode', 'view');

        return $view;
    }

    public function select_bom_for_process($id)
    {
        $project = Project::find($id);

        if( !is_bom_imported($id) )
        {
            $materials = NOPPImporter::getBomProject($project->nop);
            if(count($materials) > 0)
            {
                foreach ($materials as $material)
                {
                    save_erp_material($id, $material);
                }
            }
        }

        $view = View::make(Config::get('app.theme').'.project.select_bom');
        $view->with('page_title', 'Select BOM for Material Process');
        $view->with('sub_title', 'Project BOM ['.$project->nop.']');
        $view->with('theme', Config::get('app.theme'));
        $view->with('project', $project);
        $view->with('mode', 'select');

        return $view;
    }

    public function do_select_bom_for_process($id)
    {
        $bom_ids = Input::get('material_id');
        $bom_id_values = Input::get('material_id_value');

        if(count($bom_ids) > 0)
        {
            $i = 0;
            foreach ($bom_ids as $bom_id) {
                $current_bom = ProjectBOM::find($bom_id);
                if(in_array($bom_id, $bom_id_values))
                {
                    $current_bom->will_processed = 1;
                }
                else
                {
                    $current_bom->will_processed = 0;
                }

                $current_bom->save();
            }
        }

        return Redirect::route('project.select_bom_for_process', $id);
    }

    public function bom_data()
    {
        $mode = Input::get('mode');
        $project_id = Input::get('project_id');
        $boms = ProjectBOM::where('project_id','=',$project_id);

        if($mode != 'iqi')
        {
            $boms->where('type','<>','H');
        }

        $boms = $boms->get();

        $bom_data = array();
        foreach ($boms as $bom)
        {
            if($mode == 'select')
            {
                $bom_data['aaData'][] = array(
                    $bom->barcode,
                    $bom->material_name,
                    material_type($bom->type),
                    trim($bom->standard) ? $bom->standard : '-',
                    $bom->diameter_start ?
                        'Diam. Start: '.$bom->diameter_start.', '.'Diam. Finish: '.$bom->diameter_finish :
                        'L: '.$bom->length.', W: '.$bom->width.', H: '.$bom->height,
                    $bom->qty,
                    trim($bom->unit) ? strtoupper($bom->unit) : '-',
                    '<input type="hidden" name="material_id[]" class="material_id" value="'.$bom->id.'" />
                     <input type="checkbox" name="material_id_value[]" class="material_id_value" value="'.$bom->id.'" '.($bom->will_processed == '1' ? 'checked' : '').' />'
                );
            }
            elseif($mode == 'iqi')
            {
                $bom_data['aaData'][] = array(
                    $bom->barcode,
                    $bom->material_name,
                    material_type($bom->type),
                    trim($bom->standard) ? $bom->standard : '-',
                    $bom->diameter_start ?
                        'Diam. Start: '.$bom->diameter_start.', '.'Diam. Finish: '.$bom->diameter_finish :
                        'L: '.$bom->length.', W: '.$bom->width.', H: '.$bom->height,
                    $bom->qty,
                    trim($bom->unit) ? strtoupper($bom->unit) : '-',
                    '<a href="'.route('iqi.lists').'?project_id='.$bom->project_id.'&material_id='.$bom->id.'" class="btn btn-primary">View and Manage IQI</a>'
                );
            }
            else
            {
                $bom_data['aaData'][] = array(
                    $bom->barcode,
                    $bom->material_name,
                    material_type($bom->type),
                    trim($bom->standard) ? $bom->standard : '-',
                    $bom->diameter_start ?
                        'Diam. Start: '.$bom->diameter_start.', '.'Diam. Finish: '.$bom->diameter_finish :
                        'L: '.$bom->length.', W: '.$bom->width.', H: '.$bom->height,
                    $bom->qty,
                    trim($bom->unit) ? strtoupper($bom->unit) : '-',
                );
            }
        }

        $response = Response::make($bom_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }
}
