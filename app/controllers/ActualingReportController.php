<?php

class ActualingReportController extends BaseController {

	public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'report');
            $view->with('child_active', 'project');
        });
    }

	public function index()
	{
		$view = View::make(Config::get('app.theme').'.reports.actualing.index');
        $view->with('page_title', 'Actualing Report');
        $view->with('sub_title', 'General Report');
        $view->with('nop', '');
        $view->with('project', '');
        $view->with('filtered', true);
        $view->with('theme', Config::get('app.theme'));
        $view->with('project_data', Project::with('masterplans')->where('is_filtered', '<>', 'yes')->orderBy('nop')->orderBy('order_num')->get());

        return $view;
	}

    public function search()
    {
        $nop = Input::get('nop');
        $project = Input::get('project');

        $results = Project::where(function ($query) use ($nop, $project) {
            if($nop) { $query->where('nop', 'like' , '%'.$nop.'%'); }
            if($project) { $query->orWhere('project_name', 'like' , '%'.$project.'%'); }
        })->where('is_filtered', '<>', 'yes')->orderBy('nop')->orderBy('order_num')->get();

        // debug($results);

        $view = View::make(Config::get('app.theme').'.reports.actualing.index');
        $view->with('page_title', 'Actualing Report');
        $view->with('sub_title', 'General Report');
        $view->with('nop', $nop);
        $view->with('project', $project);
        $view->with('filtered', true);
        $view->with('theme', Config::get('app.theme'));
        $view->with('project_data', $results);

        if($results) Notification::success(count($results).' projects found.');

        return $view;
    }

	public function detail()
	{
		$project_id = Input::get('id');
        $project = Project::find($project_id);

        $masterplans = $project->masterplans()->with(array('process_group','detail_plans'))->where('parent_id','=','0')->orderBy(DB::raw('process_type,predecessor'))->get();
        $view = View::make(Config::get('app.theme').'.reports.actualing.detail');
        $view->with('page_title', 'Actualing Report');
        $view->with('sub_title', 'Detail Actualing Report');
        $view->with('theme', Config::get('app.theme'));
        $view->with('project', $project);
        $view->with('masterplans', $masterplans);

        return $view;
	}

	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /actualingreport/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /actualingreport/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /actualingreport/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
