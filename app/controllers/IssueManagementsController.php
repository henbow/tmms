<?php
use Project;
use Pic;
use Department;
use IssueManagement;

class IssueManagementsController extends BaseController {

	public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'issue_management');
            $view->with('child_active', 'issue_lists');
        });
    }

	public function index()
    {
    	$issue_data = DB::table('issue_managements AS i')->join('projects AS p','p.id','=','i.project_id')->orderBy('i.created_at','desc')->select('i.*','p.id AS pid','p.nop','p.project_name')->get();
    	$issues = array();
    	if(count($issue_data))
    	{
    		foreach ($issue_data as $issue) {
    			$issues[$issue->nop." - ".$issue->project_name][] = $issue;
    		}
    	}
    	// debug($issues);
        $view = View::make(Config::get('app.theme').'.issues.index');
        $view->with('page_title', 'Issues Management');
        $view->with('sub_title', 'View and Manage Issues');
        $view->with('theme', Config::get('app.theme'));
        $view->with('issues', $issues);

        return $view;
    }

	/**
	 * Show the form for creating a new resource.
	 * GET /issuemanagements/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$view = View::make(Config::get('app.theme').'.issues.create');
        $view->with('page_title', 'Issues Management');
        $view->with('sub_title', 'Create new issue');
        $view->with('theme', Config::get('app.theme'));
        $view->with('pics', Pic::all());
        $view->with('departments', Department::all());
        $view->with('random', mt_rand(1,999));

        return $view;
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /issuemanagements
	 *
	 * @return Response
	 */
	public function store()
	{
		list($date, $time) = explode(' ', Input::get('due_date'));
		list($month, $day, $year) = explode('/', $date);

		$issue = new IssueManagement;
		$issue->issue_code = Input::get('issue_code');
		$issue->project_id = Input::get('project_id');
		$issue->title = Input::get('title');
		$issue->owner_id = Input::get('owner');
		$issue->due_date = "{$year}-{$month}-{$day} {$time}";
		$issue->assigned_to = Input::get('assigned_to');
		$issue->description = Input::get('desc');
		$issue->category = Input::get('category');
		$issue->priority = Input::get('priority');
		$issue->status = Input::get('status');
		$issue->save();

		// submit_alert_queues();

		return Redirect::route('issue_management.index');
	}

	/**
	 * Display the specified resource.
	 * GET /issuemanagements/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	public function select()
	{
		$issues = IssueManagement::all();
        $view = View::make(Config::get('app.theme') . '.issues.select');
        $view->with('issues', $issues);
        return $view;
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /issuemanagements/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$view = View::make(Config::get('app.theme').'.issues.edit');
        $view->with('page_title', 'Issues Management');
        $view->with('sub_title', 'Create new issue');
        $view->with('theme', Config::get('app.theme'));
        $view->with('issue', IssueManagement::find($id));
        $view->with('pics', Pic::all());
        $view->with('random', mt_rand(1,999));

        return $view;
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /issuemanagements/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		list($date, $time) = explode(' ', Input::get('due_date'));
		list($month, $day, $year) = explode('/', $date);

		$issue = IssueManagement::find($id);
		$issue->issue_code = Input::get('issue_code');
		$issue->project_id = Input::get('project_id');
		$issue->title = Input::get('title');
		$issue->owner_id = Input::get('owner');
		$issue->due_date = "{$year}-{$month}-{$day} {$time}";
		$issue->assigned_to = Input::get('assigned_to');
		$issue->description = Input::get('desc');
		$issue->category = Input::get('category');
		$issue->priority = Input::get('priority');
		$issue->status = Input::get('status');
		$issue->save();

		return Redirect::route('issue_management.index');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /issuemanagements/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$issue = IssueManagement::find($id);
		$issue->delete();

		return Redirect::route('issue_management.index');
	}

}