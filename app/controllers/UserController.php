<?php
use UserInformation;
use Department;
use Unit;
// // use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification;

class UserController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'user_management');
            $view->with('child_active', 'user');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.user.list');
        $view->with('page_title', 'User Management');
        $view->with('sub_title', 'Manage application users');
        $view->with('user_lists', UserInformation::getUsers());
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.user.create');
        $view->with('page_title', 'User Management');
        $view->with('sub_title', 'Create new user');
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function store()
    {
        // print '<pre>';print_r(Sentry::findGroupById(Input::get('userlevel')));die;
        $user = Sentry::createUser(array(
            'username' => Input::get('username'),
            'password' => Input::get('password1'),
            'first_name' => Input::get('firstname'),
            'last_name' => Input::get('lastname'),
            'email' => Input::get('email'),
            'phone_number' => Input::get('phone'),
            'activated' => true
        ));

        $user->addGroup(Sentry::findGroupById(Input::get('userlevel')));

        Notification::success('Successfully add new user.');

        return Redirect::route('user.edit', $user->id);
    }

    public function edit($id)
    {
        $view = View::make(Config::get('app.theme').'.user.edit');
        $view->with('page_title', 'User Management');
        $view->with('sub_title', 'Edit user ' . UserInformation::getFullname($id));
        $view->with('theme', Config::get('app.theme'));
        $view->with('user', Sentry::findUserByID($id));
        $view->with('group_id', @Sentry::findUserByID($id)->getGroups()[0]->id);

        return $view;
    }

    public function update($id)
    {
        try
        {
            $user = Sentry::findUserById($id);

            $user->username = Input::get('username');
            $user->first_name = Input::get('firstname');
            $user->last_name = Input::get('lastname');
            $user->email = Input::get('email');
            $user->phone_number = Input::get('phone');

            if (Input::get('password1'))
            {
                if ($user->checkPassword(Input::get('password1')) && (Input::get('password2') == Input::get('password3')))
                {
                    $user->password = Input::get('password2');
                }
            }

            $user_groups = $user->getGroups();
            if(count($user_groups) > 0)
            {
                foreach($user_groups as $group)
                {
                    if ($user->removeGroup(Sentry::findGroupById($group->id)))
                    {
                        $user->addGroup(Sentry::findGroupById(Input::get('userlevel')));
                    }
                }
            }

            $user->save();

            Notification::success('User data changes was saved.');

            return Redirect::route('user.index', $user->id);
        }
        catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
        {
            Notification::error('<strong>New password</strong> and <strong>repeat password</strong> field is required.');

            return Redirect::back()->withInput();
        }
    }

    public function destroy($id)
    {
        $user = UserInformation::find($id);
        $user->delete();

        return Redirect::route('user.index');
    }
}
