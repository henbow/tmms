<?php

use Iqi;
use EmployeeCompensation;
use Pic;
use Project;
use ProjectBOM;
use Department;
use Unit;

class IqisController extends BaseController {

	public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'actualing');
            $view->with('child_active', 'iqi');
        });
    }

	public function index()
    {
    	$mode = Input::get('mode');

        $view = View::make(Config::get('app.theme').'.iqi.index');
        $view->with('page_title', 'Internal Quality Issues (IQI)');
        $view->with('sub_title', 'View and Manage IQI issues');
        $view->with('theme', Config::get('app.theme'));
        $view->with('mode', $mode ? $mode :'iqi');
        $view->with('iqis', Iqi::orderBy('created_at','desc')->get());
        $view->with('lpks', EmployeeCompensation::orderBy('created_at','desc')->get());

        return $view;
    }

	public function create()
	{
		$view = View::make(Config::get('app.theme').'.iqi.create');
        $view->with('page_title', 'Internal Quality Issues (IQI)');
        $view->with('sub_title', 'Create new issue');
        $view->with('theme', Config::get('app.theme'));
        $view->with('departments', Department::all());
        $view->with('units', Unit::all());
        $view->with('qc_staffs', Pic::where('unit_id','=','6')->get());
        $view->with('pics', Pic::all());
        $view->with('random', mt_rand(1,999));

        return $view;
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /iqis
	 *
	 * @return Response
	 */
	public function store()
	{
		$iqi = new Iqi;
		$iqi->iqi_code = Input::get('iqi_code');
		$iqi->project_id = Input::get('project_id');
		$iqi->material_id = Input::get('material_id');
		$iqi->qc_staff_id = Input::get('qc_staff');
		$iqi->problem_maker_id = Input::get('problem_maker');
		$iqi->description = Input::get('desc');
		$iqi->possible_causes = Input::get('causes');
		$iqi->action = Input::get('action');
		$iqi->pic_dept = Input::get('pic');
		$iqi->remark = Input::get('remark');
		$iqi->status = Input::get('status');
		$iqi->save();

		return Redirect::route('iqi.index');
	}

	/**
	 * Display the specified resource.
	 * GET /iqis/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$view = View::make(Config::get('app.theme').'.iqi.show');
        $view->with('page_title', 'Internal Quality Issues (IQI)');
        $view->with('sub_title', 'Create new issue');
        $view->with('theme', Config::get('app.theme'));
        // $view->with('departments', Department::all());
        // $view->with('units', Unit::all());
        // $view->with('qc_staffs', Pic::where('unit_id','=','6')->get());
        $view->with('pics', Pic::all());
        $view->with('iqi', Iqi::find($id));

        return $view;
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /iqis/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$view = View::make(Config::get('app.theme').'.iqi.edit');
        $view->with('page_title', 'Internal Quality Issues (IQI)');
        $view->with('sub_title', 'Create new issue');
        $view->with('theme', Config::get('app.theme'));
        $view->with('departments', Department::all());
        $view->with('units', Unit::all());
        $view->with('qc_staffs', Pic::where('unit_id','=','6')->get());
        $view->with('pics', Pic::all());
        $view->with('iqi', Iqi::find($id));
        $view->with('random', mt_rand(1,999));

        return $view;
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /iqis/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$iqi = Iqi::find($id);
		$iqi->iqi_code = Input::get('iqi_code');
		$iqi->project_id = Input::get('project_id');
		$iqi->material_id = Input::get('material_id');
		$iqi->qc_staff_id = Input::get('qc_staff');
		$iqi->problem_maker_id = Input::get('problem_maker');
		$iqi->description = Input::get('desc');
		$iqi->possible_causes = Input::get('causes');
		$iqi->action = Input::get('action');
		$iqi->pic_dept = Input::get('pic');
		$iqi->remark = Input::get('remark');
		$iqi->status = Input::get('status');
		$iqi->save();

		return Redirect::route('iqi.index');
	}

	public function set_status($id)
	{
		$iqi = Iqi::find($id);
		$iqi->status = Input::get('status');
		$iqi->save();

		return Redirect::route('iqi.index');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /iqis/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$iqi = Iqi::find($id);
		$iqi->delete();

		return Redirect::route('iqi.index');
	}

	public function select_project()
	{
		$projects = Project::take(300)->orderBy('nop','desc')->get();
        $view = View::make(Config::get('app.theme') . '.iqi.select-project');
        $view->with('projects', $projects);
        return $view;
	}

	public function select_material()
	{
		$materials = ProjectBOM::where('project_id','=',Input::get('project_id'))->get();
        $view = View::make(Config::get('app.theme') . '.iqi.select-material');
        $view->with('materials', $materials);
        return $view;
	}

	public function select_iqi()
	{
		$iqis = Iqi::all();
        // debug($iqis);
        $view = View::make(Config::get('app.theme') . '.iqi.select-iqi');
        $view->with('iqis', $iqis);
        return $view;
	}

}