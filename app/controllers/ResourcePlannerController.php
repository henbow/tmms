<?php
set_time_limit(0);

use ResourcePlanner;
use ResourceWorkTime;
use Resource;

class ResourcePlannerController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'resource_center');
            $view->with('child_active', 'resource_planner');
        });
    }

    public function index($year = '')
    {
        if($year == '')
        {
            $year = date('Y');
        }

        $view = View::make(Config::get('app.theme').'.resource_planner.list');
        $view->with('page_title', 'Resource Planner');
        $view->with('sub_title', 'Resource Work Plans in ' . date('Y'));
        $view->with('theme', Config::get('app.theme'));
        $view->with('resources', Resource::paginate(10));
        $view->with('months', $this->_generate_month());
        $view->with('selected_year', intval($year));
        $view->with('start_year', date('Y'));
        $view->with('last_year', intval(date('Y')) + 5);
        $view->with('work_plan_button_enabled', ResourcePlanner::isWorkPlanButtonEnabled());
        $view->with('breadcrumb', array(
            'Dashboard' => array(route('dashboard.index')),
            'Resource Planner' => array(route('resource_planner.index'), 'last')
        ));

        return $view;
    }

    public function create($year)
    {
        Queue::push('ResourcePlannerJob', array('year' => $year));

        Notification::success('Work plan is on process update!');

        return Redirect::route('resource_planner.index');
    }

    public function update_work_time_plan()
    {
        $work_plan = ResourcePlanner::find(Input::get('pk'));
        $work_plan->work_time = Input::get('value');
        $work_plan->save();

        $response = Response::make(array(Input::get('value')), 200);
        $response->header('Content-Type', 'Application/JSON');

        return $response;
    }

    public function work_plan_per_month($resource_id, $month, $year)
    {
        $days = $this->_generate_days($month, $year);
        $contents = array();

        foreach($days as $day)
        {
            $resource_name = Resource::getName($resource_id);
            $work_time = ResourcePlanner::where('resource_id', '=', $resource_id)
                            ->where('month', '=', $month)
                            ->where('year', '=', $year)
                            ->where('day_num', '=', $day)
                            ->get();
            $contents['resource'] = $resource_name;
            $contents['month_year'] = date('F Y', strtotime("{$year}-{$month}-1"));
            $contents['month_num'] = $month;
            $contents['year'] = $year;
            $contents['work_time'][$day] = $work_time[0]->id . ',' . $work_time[0]->work_time;
        }

        $response = Response::make($contents, 200);
        $response->header('Content-Type', 'Application/JSON');

        return $response;
    }

    private function _generate_month()
    {
        $months = array();

        foreach (range(1, 12) as $month) {
            $months[] = date('F', mktime(0, 0, 0, $month, 1, date('Y')));
        }

        return $months;
    }

    private function _generate_weeks($month)
    {
        $month = intval(date('m', strtotime(date('Y') . '-' . $month . '-1')));
        $weekStart = ($month * 4) - 3;
        $weeks = array();
        $months = range(1, 12);

        $month = intval($month);
        for($i = $weekStart; $i <= $weekStart + 3; $i++){
            $weeks[] = $i;
        }

        return $weeks;
    }

    private function _generate_days($month, $year)
    {
        $day_num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $days = array();
        for($i = 1; $i <= $day_num; $i++)
        {
            $days[] = $i;
        }

        return $days;
    }

}

/**
 * End of file
 *
 */
