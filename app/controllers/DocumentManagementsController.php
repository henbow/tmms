<?php
use DocumentManagement;
use GeneralOptions;
use Project;

class DocumentManagementsController extends \BaseController {

	public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'doc_management');
            $view->with('child_active', 'doc_lists');
        });
    }

	public function index()
    {
        $view = View::make(Config::get('app.theme').'.documents.index');
        $view->with('page_title', 'Documents Management');
        $view->with('sub_title', 'Manage project documents.');
        $view->with('theme', Config::get('app.theme'));
        $view->with('docs', DocumentManagement::all());

        return $view;
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.documents.create');
        $view->with('page_title', 'Documents Management');
        $view->with('sub_title', 'Upload New Document');
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function store()
    {
    	$doc = new DocumentManagement();
        $doc->project_id = Input::get('project_id');
        $doc->issue_id = Input::get('issue_id');
        $doc->title = Input::get('title');
        $doc->description = Input::get('description');
        $doc->upload_by = Sentry::getUser()->id;

        $upload_path = (Object) GeneralOptions::getSingleOption('document_path');
        $doc_file = Input::file('document-file');
        $project = Project::find($doc->project_id);
        $directory = $upload_path->value . $project->nop;
        $new_file_name = Str::slug($doc->title);

        if(Input::hasFile('document-file'))
        {
            if(!file_exists($directory))
            {
                mkdir($directory);
            }

            $size = Input::file('document-file')->getSize();
            $extension = Input::file('document-file')->getClientOriginalExtension();
            $move_file = Input::file('document-file')->move($directory, $new_file_name.'.'.$extension);
            if($move_file)
            {
                $doc->filename = $new_file_name.'.'.$extension;
                $doc->filetype = $extension;
                $doc->path = $directory;
                $doc->size = $size;
            }
        }

        if($doc->save())
        {
            Notification::success('New Document was uploaded.');
            return Redirect::route('document.index');
        }

        Notification::error('New upload new Document.');
        return Redirect::back();
    }

    public function edit($id)
    {
        $view = View::make(Config::get('app.theme').'.documents.edit');
        $view->with('page_title', 'Documents Management');
        $view->with('sub_title', 'Edit Uploaded Document');
        $view->with('theme', Config::get('app.theme'));
        $view->with('doc', DocumentManagement::find($id));

        return $view;
    }

    public function update($id)
    {
        $doc = DocumentManagement::find($id);
        $doc->project_id = Input::get('project_id');
        $doc->issue_id = Input::get('issue_id');
        $doc->title = Input::get('title');
        $doc->description = Input::get('description');
        $doc->upload_by = Sentry::getUser()->id;

        $upload_path = (Object) GeneralOptions::getSingleOption('document_path');
        $doc_file = Input::file('document-file');
        $project = Project::find($doc->project_id);
        $directory = $upload_path->value . $project->nop;
        $new_file_name = Str::slug($doc->title);

        if(Input::hasFile('document-file'))
        {
            if(file_exists($doc->path.'/'.$doc->filename))
            {
                @unlink($doc->path.'/'.$doc->filename);
            }

            if(!file_exists($directory))
            {
                mkdir($directory);
            }

            $size = Input::file('document-file')->getSize();
            $extension = Input::file('document-file')->getClientOriginalExtension();
            $move_file = Input::file('document-file')->move($directory, $new_file_name.'.'.$extension);
            if($move_file)
            {
                $doc->filename = $new_file_name.'.'.$extension;
                $doc->filetype = $extension;
                $doc->path = $directory;
                $doc->size = $size;
            }
        }

        if($doc->save())
        {
            Notification::success('Uploaded Document was changed.');
            return Redirect::route('document.index');
        }

        Notification::error('Uploaded Document changes was failed.');
        return Redirect::back();
    }

    public function destroy($id)
    {
        $doc = DocumentManagement::find($id);

        if(file_exists($doc->path.'/'.$doc->filename))
        {
            @unlink($doc->path.'/'.$doc->filename);
        }

        $doc->delete();

        return Redirect::route('document.index');
    }

}