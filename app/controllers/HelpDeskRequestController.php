<?php
use HelpDesk;
use HelpDeskCategory;
use HelpDeskPriority;
use HelpDeskStatus;
use HelpDeskNotes;
use AlertSetting;
use AlertSettingUser;
use UserInformation;

class HelpDeskRequestController extends BaseController
{
    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'helpdesk');
            $view->with('child_active', 'request_lists');
        });
    }

    public function index()
    {
        $is_admin = @Sentry::getUser()->getGroups()[0]->name == 'Administrator' ? true : false;
        if($is_admin)
        {
            $helpdesk = HelpDesk::orderBy('created_at','desc')->get();
        }
        else
        {
            $helpdesk = HelpDesk::where('incharge_group_id', '=', @Sentry::getUser()->getGroups()[0]->id)->orderBy('created_at','desc')->get();
        }
        $view = View::make(Config::get('app.theme').'.helpdesk_request.list');
        $view->with('page_title', 'Helpdesk');
        $view->with('sub_title', 'Requests from users.');
        $view->with('theme', Config::get('app.theme'));
        $view->with('requests', $helpdesk);

        return $view;
    }

    public function show($id)
    {
        $request = HelpDesk::find($id);
        $request->incharge_id =  Sentry::getUser()->id;
        $request->start_request_attendance =  date('Y-m-d H:i:s');
        $request->save();

        $view = View::make(Config::get('app.theme').'.helpdesk_request.show');
        $view->with('page_title', 'Helpdesk Request From Employee');
        $view->with('sub_title', 'Show request detail.');
        $view->with('theme', Config::get('app.theme'));
        $view->with('request', HelpDesk::find($id));
        $view->with('request_notes', HelpDeskNotes::where('helpdesk_id', '=', $id)->orderBy('id', 'DESC')->get());
        $view->with('request_status', json_encode(HelpDeskStatus::all(array('id as value', 'status as text'))->toArray()));

        return $view;
    }

    public function edit($id)
    {
        $view = View::make(Config::get('app.theme').'.helpdesk_request.edit');
        $view->with('page_title', 'Helpdesk');
        $view->with('sub_title', 'Edit request data');
        $view->with('theme', Config::get('app.theme'));
        $view->with('request', HelpDesk::find($id));

        return $view;
    }

    public function update($id)
    {
        $request = HelpDesk::find($id);
        $request->code = Input::get('code');
        $request->name = Input::get('requestname');
        $request->desc = Input::get('desc');

        if($request->save())
        {
            Notification::success('HelpDesk data changes was saved.');
        }

        return Redirect::route('helpdesk_request.index');
    }

    public function destroy($id)
    {
        $request = HelpDesk::find($id);
        $request_notes = HelpDeskNotes::find($request->helpdesk_id);

        $request->delete();
        $request_notes->delete();

        return Redirect::route('helpdesk_request.index');
    }

    public function add_note($request_id)
    {
        $parent_request_note = HelpDeskNotes::where('reply_to', '=', '0')->where('helpdesk_id', '=', $request_id)->take(1)->get();

        $request_note = new HelpDeskNotes;
        $request_note->helpdesk_id = $request_id;
        $request_note->reply_to = $parent_request_note[0]->id;
        $request_note->reply_by = Sentry::getUser()->id;
        $request_note->reply_type = 'pm';
        $request_note->message = Input::get('newnote');

        if($request_note->save()) {
            $this->_send_alert($request_note->helpdesk_id);

            Notification::success('New note was added to this request.');
        }

        return Redirect::route('helpdesk_request.show', $request_id);
    }

    public function update_status($id)
    {
        $status = Input::get('value');
        $request = HelpDesk::find($id);
        $request->status_id = $status;
        $request->save();

        if(!$request->save())
        {
            $status = "NEW";
        }

        $response = Response::make(array($status), 200);
        $response->header('Content-Type', 'Application/JSON');

        return $response;
    }

    private function _send_alert($id)
    {
        $request = HelpDesk::find($id);
        $request_note = HelpDeskNotes::where('helpdesk_id','=',$request->id)->orderBy('id','desc')->take(1)->get()[0];
        $priority_text = HelpDeskPriority::find($request->priority_id);
        $category_text = HelpDeskCategory::find($request->category_id);
        $from = UserInformation::find($request->request_user_id);
        $from_name = $from->first_name." ".$from->last_name;

        $messages_for_me = (Object) array(
            'subject'=> "TMMS Helpdesk Your Request - {$request->ticket_id}",
            'content' => "Ticket ID: {$request->ticket_id}<br>
                        Priority: {$priority_text->priority}<br>
                        Category: {$category_text->category}<br>
                        Subcategory: {$request->category_type}<br>
                        Subject: {$request->subject}<br>
                        Timestamp: ".date('d-M-Y H:i:s')."<br>
                        Message: {$request_note->message}"
        );
        $from = (Object) array(
            'fullname' => trim($from_name),
            'email' => trim($from->email)
        );
        submit_email_alert_queues($from, $messages_for_me);

        $to_group = Sentry::findGroupById($request->incharge_group_id);
        $to_s = Sentry::findAllUsersInGroup($to_group);
        if(count($to_s))
        {
            foreach ($to_s as $to)
            {
                $sms_message = "TMMS Helpdesk [{$priority_text->priority}]\nTicket ID: {$request->ticket_id}\nFrom: {$from_name}\nSubject: {$request->subject}\n";
                $email_mesage = (Object) array(
                    'subject'=> "TMMS Helpdesk User Request - {$request->ticket_id} [{$priority_text->priority}]",
                    'content' => "Ticket ID: {$request->ticket_id}<br>
                                Priority: {$priority_text->priority}<br>
                                Category: {$category_text->category}<br>
                                Subcategory: {$request->category_type}<br>
                                Subject: {$request->subject}<br>
                                Timestamp: ".date('d-M-Y H:i:s')."<br>
                                From: {$from_name}<br>
                                Message: {$request_note->message}"
                );
                $phone_number = $to->phone_number;
                $recipient = (Object) array (
                    'fullname' => trim($to->first_name." ".@$to->last_name),
                    'email' => trim($to->email)
                );
                $alert_setting = AlertSettingUser::where('user_id','=',$to->id)->where('setting_id','=',2)->get();
                $is_sms = count($alert_setting) > 0 ? $alert_setting[0]->send_sms ? true : false : true;
                $is_email = count($alert_setting) > 0 ? $alert_setting[0]->send_email ? true : false : true;

                if($is_sms) submit_sms_alert_queues($phone_number, $sms_message);
                if($is_email) submit_email_alert_queues($recipient, $email_mesage);
            }
        }
    }
}
