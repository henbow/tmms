<?php
use GeneralOptions;
// // use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification;

class SettingController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'settings');
            $view->with('child_active', 'setting');
        });
    }

    public function index()
    {
        return $this->create();
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.setting.edit');
        $view->with('page_title', 'Application Settings');
        $view->with('sub_title', 'Manage settings');
        $view->with('theme', Config::get('app.theme'));
        $view->with('settings', GeneralOptions::all());

        return $view;
    }

    public function update($id)
    {
        foreach (GeneralOptions::all() as $opt)
        {
            if($opt->value_type == 'string')
            {
                $setting = GeneralOptions::find($opt->id);
                $setting->value = Input::get($opt->name);
                $setting->save();
            }
        }

        Notification::success('Setting changes was saved.');

        return Redirect::route('setting.index');
    }
}
