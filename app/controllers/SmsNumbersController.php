<?php
use SmsNumbers;
// // use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification;

class SmsNumbersController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'settings');
            $view->with('child_active', 'sms_numbers');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.sms_number.list');
        $view->with('page_title', 'SMS Alert Recipient Numbers');
        $view->with('sub_title', 'Manage SMS alert recipient numbers');
        $view->with('theme', Config::get('app.theme'));
        $view->with('sms_numbers', SmsNumbers::all());

        return $view;
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.sms_number.create');
        $view->with('page_title', 'SMS Alert Recipient Numbers');
        $view->with('sub_title', 'Create new SMS number');
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function store()
    {
        $sms_number = new SmsNumbers;
        $sms_number->name = Input::get('recipient');
        $sms_number->number = Input::get('number');

        if($sms_number->save())
            Notification::success('New SMS number data was saved.');

        return Redirect::route('sms_number.index');
    }

    public function edit($id)
    {
        $view = View::make(Config::get('app.theme').'.sms_number.edit');
        $view->with('page_title', 'SMS Alert Recipient Numbers');
        $view->with('sub_title', 'Edit SMS number data');
        $view->with('theme', Config::get('app.theme'));
        $view->with('sms_number', SmsNumbers::find($id));

        return $view;
    }

    public function update($id)
    {
        $sms_number = SmsNumbers::find($id);
        $sms_number->name = Input::get('recipient');
        $sms_number->number = Input::get('number');

        if($sms_number->save())
            Notification::success('SMS number data changes was saved.');

        return Redirect::route('sms_number.index');
    }

    public function destroy($id)
    {
        $sms_number = SmsNumbers::find($id);
        $sms_number->delete();

        Notification::success('SMS number data was deleted.');

        return Redirect::route('sms_number.index');
    }
}
