<?php
class ProjectProcessController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'planning');
            $view->with('child_active', 'standard_process');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.project_process.list');
        $view->with('page_title', 'Standard Process');
        $view->with('sub_title', 'Manage Standard Process');
        $view->with('theme', Config::get('app.theme'));
        $view->with('processes', Process::with('resource_groups')->get());

        return $view;
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.project_process.create');
        $view->with('page_title', 'Standard Process');
        $view->with('sub_title', 'Create New Standard Process');
        $view->with('theme', Config::get('app.theme'));
        $view->with('resource_groups', ResourceGroup::orderBy('name')->get());

        return $view;
    }

    public function store()
    {
        $process = new Process;
        $process->code = Input::get('code');
        $process->group_id = Input::get('group');
        $process->process = Input::get('process');
        $process->process_type = Input::get('process_type');

        if($process->save())
        {
            $resource_groups = Input::get('resource_groups');
            $process_id = $process->id;
            if(count($resource_groups) > 0)
            {
                foreach($resource_groups as $resource_group_id)
                {
                    $process_resource = new ProcessResourceGroup;
                    $process_resource->process_id = $process_id;
                    $process_resource->resource_group_id = $resource_group_id;
                    $process_resource->save();
                }
            }

            Notification::success('New standard process data was saved.');
        }

        return Redirect::route('processes.index');
    }

    public function edit($id)
    {
        $view = View::make(Config::get('app.theme').'.project_process.edit');
        $view->with('page_title', 'Standard Process');
        $view->with('sub_title', 'Edit standard process data');
        $view->with('theme', Config::get('app.theme'));
        $view->with('process', Process::find($id));
        $view->with('resource_groups', ResourceGroup::orderBy('name')->get());
        $view->with('process_resource_group', ProcessResourceGroup::where('process_id', '=', $id)->get());

        return $view;
    }

    public function update($id)
    {
        $process = Process::find($id);
        $process->code = Input::get('code');
        $process->group_id = Input::get('group');
        $process->process = Input::get('process');
        $process->process_type = Input::get('process_type');

        if($process->save())
        {
            DB::table('process_resource_group')->where('process_id', '=', $id)->delete();

            $resource_groups = Input::get('resource_groups');
            $process_id = $process->id;
            if(count($resource_groups) > 0)
            {
                foreach($resource_groups as $resource_group_id)
                {
                    $process_resource = new ProcessResourceGroup;
                    $process_resource->process_id = $process_id;
                    $process_resource->resource_group_id = $resource_group_id;
                    $process_resource->save();
                }
            }

            Notification::success('Standard process data changes was saved.');
        }

        return Redirect::route('processes.index');
    }

    public function destroy($id)
    {
        $process = Process::find($id);
        $process->delete();

        Notification::success('Standard process data was deleted.');

        return Redirect::route('processes.index');
    }
}
