<?php
use ResourceGroup;

class ResourceGroupController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'resource_center');
            $view->with('child_active', 'resource_group');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.resource_group.list');
        $view->with('page_title', 'Resource Group Management');
        $view->with('sub_title', 'Manage resource group');
        $view->with('theme', Config::get('app.theme'));
        $view->with('resource_groups', ResourceGroup::all());

        return $view;
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.resource_group.create');
        $view->with('page_title', 'Resource Group Management');
        $view->with('sub_title', 'Create new resource group');
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function store()
    {
        $resource_group = new ResourceGroup();
        $resource_group->code = Input::get('code');
        $resource_group->name = Input::get('res_group_name');
        $resource_group->desc = Input::get('desc');

        if ($resource_group->save())
        {
            Notification::success('New resource group data was saved.');
        }

        return Redirect::route('resource_group.index');
    }

    public function edit($id)
    {
        $view = View::make(Config::get('app.theme').'.resource_group.edit');
        $view->with('page_title', 'Resource Group Management');
        $view->with('sub_title', 'Edit resource group data');
        $view->with('theme', Config::get('app.theme'));
        $view->with('resource_group', ResourceGroup::find($id));

        return $view;
    }

    public function update($id)
    {
        $resource_group = ResourceGroup::find($id);
        $resource_group->code = Input::get('code');
        $resource_group->name = Input::get('res_group_name');
        $resource_group->desc = Input::get('desc');

        if ($resource_group->save())
            Notification::success('Resource Group data changes was saved.');

        return Redirect::route('resource_group.index');
    }

    public function destroy($id)
    {
        $resource_group = ResourceGroup::find($id);
        $resource_group->delete();

        return Redirect::route('resource_group.index');
    }
}
