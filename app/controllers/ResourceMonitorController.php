<?php

use ResourcePlanner;
use ResourceWorkTime;
use Resource;

class ResourceMonitorController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'resource_center');
            $view->with('child_active', 'resource_monitor');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.resource_monitor.list');
        $view->with('page_title', 'Resource Monitor');
        $view->with('sub_title', 'Resource Status Monitoring');
        $view->with('theme', Config::get('app.theme'));
        $view->with('in_resources', Resource::with(array('actualing' => function($query){
            $query->where('subfinish_type','=','ONPROCESS')->where('status','=','ON PROCESS');
        }))->where('plan_type','=','in plan')->orderBy('code')->get());
        $view->with('out_resources', Resource::with(array('actualing' => function($query){
            $query->where('subfinish_type','=','ONPROCESS')->where('status','=','ON PROCESS');
        }))->where('plan_type','<>','in plan')->orderBy('code')->get());
        $view->with('breadcrumb', array(
            'Dashboard' => array(route('dashboard.index')),
            'Resource Monitor' => array(route('resource_monitor.index'), 'last')
        ));

        return $view;
    }

    public function show($id)
    {
        $view = View::make(Config::get('app.theme').'.resource_monitor.detail');
        $view->with('page_title', 'Resource Monitor');
        $view->with('sub_title', 'Resource Status Monitoring Detail');
        $view->with('theme', Config::get('app.theme'));
        $view->with('resource', Resource::find($id));
        $view->with('details', Resource::find($id)->actualing);
        $view->with('breadcrumb', array(
            'Dashboard' => array(route('dashboard.index')),
            'Resource Monitor' => array(route('resource_monitor.index')),
            'Resource Monitor Detail' => array(route('resource_monitor.show', $id), 'last')
        ));

        return $view;
    }
}

/**
 * End of file
 *
 */
