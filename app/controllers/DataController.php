<?php
use Project;
use ProjectBOM;
use Resource;
use ResourceGroup;
use Pic;
use Iqi;
use EmployeeCompensation;
use IssueManagement;
use Process;
use ProcessGroup;
use ProcessResourceGroup;
use PlanningMFG;
use PlanningNonMFG;
use ProjectMasterPlan;
use SchedulingMFG;
use SchedulingNonMFG;
use SchedulingPBBOM;
use GeneralOptions;

class DataController extends BaseController
{
    public function get_project()
    {
        $id = Input::get('id');
        $response_data = Project::find($id);
        $response = Response::make($response_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_material()
    {
        $id = Input::get('id');
        $response_data = ProjectBOM::find($id);
        $response = Response::make($response_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_all_resources()
    {
        $response_data = Resource::all();
        $response = Response::make($response_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_all_pics()
    {
        $response_data = Resource::all();
        $response = Response::make($response_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_resource()
    {
        $id = Input::get('id');
        $code = Input::get('code');
        $response_data = $code ? @Resource::where('code','=',$code)->get()[0] : Resource::find($id);
        $response = Response::make($response_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_pic()
    {
        $id = Input::get('id');
        $code = Input::get('code');
        $response_data = $code ? @Pic::where('code','=',$code)->get()[0] : Pic::find($id);
        $response = Response::make($response_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_iqi()
    {
        $id = Input::get('id');
        if($id)
        {
            $response_data = Iqi::find($id);
        }
        else
        {
            $response_data = Iqi::orderBy('created_at', 'desc')->get();
        }

        $response = Response::make($response_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_issue()
    {
        $id = Input::get('id');
        $response_data = IssueManagement::find($id);
        $response = Response::make($response_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_issues()
    {
        $issues = DB::table('issue_managements AS i')
            ->join('projects AS p','p.id','=','i.project_id')
            ->join('helpdesk_priority AS hp','hp.id','=','i.priority')
            ->select('i.*','p.nop','p.project_name','hp.priority AS pr_text','hp.class AS pr_class')
            ->get();

        $response_data = array();
        if(count($issues) > 0)
        {
            $raw = array();
            $i = 0;
            foreach ($issues as $issue) {
                $raw[$issue->nop][$i] = $issue;
                $raw[$issue->nop][$i][data] = $issue;
                $i++;
            }
        }
        $response = Response::make($response_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_per_resource_schedules()
    {
        $resource = Input::get('resource_id');
        $type = Input::get('type');
        $schedules = $type == 'material' ? SchedulingMFG::where('resource_id','=',$resource)->orderBy('priority')->get(): SchedulingNonMFG::where('resource_id','=',$resource)->orderBy('priority')->get();
        $response_data = array();
        if(count($schedules))
        {
            $priority = 1;
            foreach ($schedules as $schedule) {
                $planning = $type == 'material' ? PlanningMFG::find($schedule->planning_id) : PlanningNonMFG::find($schedule->planning_id);
                $resource_group = ResourceGroup::find($schedule->resource_group_id);
                $resource = Resource::find($schedule->resource_id);
                $process = Process::find($planning->process_id);

                $data = array();
                if($type == 'material')
                {
                    $material = ProjectBOM::find($planning->material_id);
                    $data['barcode'] = $material->barcode;
                    $data['material'] = $material->material_name;
                    $data['qty'] = $material->qty;
                }
                else
                {
                    $project = Project::find($planning->project_id);
                    $data['nop'] = $project->nop;
                    $data['project'] = $project->project_name;
                }

                $data['planning_id'] = $planning->id;
                $data['master_planning_id'] = $planning->master_planning_id;
                $data['process'] = $process->process;
                $data['estimation'] = $planning->estimation_hour;
                $data['resource_group'] = $resource_group->name;
                $data['schedule_id'] = $schedule->id;
                $data['resource'] = $resource ? $resource->name : '';
                $data['priority'] = $priority++;
                $data['start_date'] = date('d-m-Y', strtotime($schedule->start_date));
                $data['finish_date'] = date('d-m-Y', strtotime($schedule->finish_date));

                $response_data[] = $data;
            }
        }

        $response = Response::make($response_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }
}
