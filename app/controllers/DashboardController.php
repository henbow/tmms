<?php
//// // use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification;

class DashboardController extends BaseController 
{

	public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'home');
            $view->with('child_active', 'home');           
        });
    }

	public function index()
	{
		$view = View::make(Config::get('app.theme').'.dashboard.main');
		$view->with('theme', Config::get('app.theme'));

		return $view;
	}

}
