<?php
use Project;
use ProjectBOM;
use ProjectMasterPlan;

use Process;
use ProcessGroup;
use ProcessResource;

use MasterPlanTemplate;
use MasterPlanMFGProcess;
use PlanningMFG;
use PlanningNonMFG;
use PlanningPBBOM;
use TemplateMfgProcessHeader;
use TemplateMfgProcessDetail;

use Resource;

class ProjectPlanningDetailController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'planning');
            $view->with('child_active', 'project_planning');
        });
    }

    public function index($process_group_id, $master_plan_id)
    {
        $master_plan_data = ProjectMasterPlan::find($master_plan_id);
        $group_data = ProcessGroup::find($process_group_id);

        if($master_plan_data->parent_id > 0)
        {
            $parent_plan = ProjectMasterPlan::find($master_plan_data->parent_id);
        }
        else
        {
            $parent_plan = $master_plan_data;
        }

        $project_data = Project::find($parent_plan->project_id);

        switch ($group_data->type)
        {
            case 'material':
                $bom = ProjectBOM::where('project_id', '=', $parent_plan->project_id)->where('will_processed', '=', '1')->where('type','<>','H');
                $view = View::make(Config::get('app.theme').'.planning_detail.list.header_material_mfg');
                $view->with('materials', $bom->get());
                break;

            case 'pb-bom':
                $bom = ProjectBOM::where('project_id', '=', $parent_plan->project_id);
                $view = View::make(Config::get('app.theme').'.planning_detail.list.header_material_pb_bom');
                $view->with('materials', $bom->get());
                break;

            default:
                $model = PlanningNonMFG::where('master_planning_id', '=', $master_plan_id);
                $view = View::make(Config::get('app.theme').'.planning_detail.list.detail_non_mfg');
                $view->with('planning_details', $model->get());
                break;
        }

        $estimation = round($master_plan_data->real_estimation, 2);
        $start_date = $master_plan_data->start_date;
        $finish_date = $master_plan_data->finish_date;

        if(intval($master_plan_data->estimation) == 0)
        {
            $current_plan = DB::table('project_master_plan')->where('parent_id', '=', $master_plan_id);
            $start_date_data = $current_plan->orderBy('start_date')->get();
            $finish_date_data = $current_plan->orderBy('finish_date', 'DESC')->get();
            $estimation = @$current_plan->sum('estimation');
            $start_date = isset($start_date_data[0]->start_date) ? $start_date_data[0]->start_date : $start_date;
            $finish_date = isset($finish_date_data[0]->finish_date) ? $finish_date_data[0]->finish_date : $finish_date;
        }

        if(strtotime($start_date) === strtotime($finish_date))
        {
            $start_date = date('Y-m-d 00:00:00', strtotime($start_date));
            $finish_date = date('Y-m-d 23:59:59', strtotime($finish_date));
        }

        $view->with('page_title', 'Detail Planning '.$group_data->group);
        $view->with('sub_title', $group_data->group);
        $view->with('theme', Config::get('app.theme'));
        $view->with('steps', ProjectMasterPlan::where('project_id', '=', $master_plan_data->project_id)->where('parent_id', '=', '0')->get());
        $view->with('step', $parent_plan->process_group_id);
        $view->with('nop', $project_data->nop);
        $view->with('project', $project_data);
        $view->with('master_plan_id', $master_plan_id);
        $view->with('estimation',  $estimation);
        $view->with('start_date',  $start_date);
        $view->with('finish_date',  $finish_date);

        return $view;
    }

    public function material_process($material_id, $master_plan_id)
    {
        $material = ProjectBOM::find($material_id);
        $master_plan_data = ProjectMasterPlan::find($master_plan_id);
        $project_data = Project::find($master_plan_data->project_id);
        $group_data = ProcessGroup::find($master_plan_data->process_group_id);

        if($master_plan_data->parent_id > 0)
        {
            $master_plan_data = ProjectMasterPlan::find($master_plan_data->parent_id);
        }

        switch($group_data->type)
        {
            case 'material':
                $process_data = PlanningMFG::where('material_id', '=', $material_id);
                if($master_plan_data->parent_id > 0)
                {
                    $process_data->where('process_group_id', '=', $group_data->id);
                }
                else
                {
                    $process_data->where('master_planning_id', '=', $master_plan_data->id);
                }

                $view = View::make(Config::get('app.theme') . '.planning_detail.list.detail_mfg');
                $view->with('processes', $process_data->get());
                break;

            case 'pb-bom':
                $process_data = DB::table('project_planning_detail_pb_bom AS p')
                        ->join('project_master_plan AS m', 'm.id', '=', 'p.master_planning_id')
                        ->join('projects AS pr', 'p.project_id', '=', 'pr.id')
                        ->where('p.material_id', '=', $material_id)
                        ->where('p.master_planning_id','=',$master_plan_id)
                        ->select('p.*')
                        ->get();
                $view = View::make(Config::get('app.theme') . '.planning_detail.list.detail_pb_bom');
                $view->with('processes', $process_data);
                break;

            default:
                return Redirect::route('project.planning.detail', array($master_plan_data->process_group_id, $master_plan_id));
        }

        $view->with('page_title', $group_data->group);
        $view->with('sub_title', "Process for Material");
        $view->with('theme', Config::get('app.theme'));
        $view->with('step', $master_plan_data->process_group_id);
        $view->with('nop', $project_data->nop);
        $view->with('project', $project_data);
        $view->with('process_group_id', $master_plan_data->process_group_id);
        $view->with('master_plan_id', $master_plan_id);
        $view->with('estimation', $master_plan_data->estimation);
        $view->with('start_date',  $master_plan_data->start_date);
        $view->with('finish_date',  $master_plan_data->finish_date);
        $view->with('material', $material);
        return $view;
    }

    public function mfg_process_template($material_id, $master_plan_id)
    {
        $material = ProjectBOM::find($material_id);
        $search = isset($material->material_name) ? $material->material_name : FALSE;
        $template_headers = TemplateMfgProcessHeader::all();
        $view = View::make(Config::get('app.theme') . '.planning_detail.list.template');
        $view->with('templates', $template_headers);
        $view->with('material_id', $material_id);
        $view->with('init_search', $search);
        $view->with('master_plan_id', $master_plan_id);
        return $view;
    }

    public function mfg_process_template_header($master_plan_id)
    {
        $template_headers = TemplateMfgProcessHeader::all();
        $view = View::make(Config::get('app.theme') . '.planning_detail.list.template_header');
        $view->with('templates', $template_headers);
        $view->with('master_plan_id', $master_plan_id);
        return $view;
    }

    public function mfg_process_template_detail()
    {
        $header_id = Input::get('tpl_id');
        $template_details = DB::table('template_mfg_process_detail AS tpl')
		    ->join('master_process_group AS g', 'g.id', '=', 'tpl.process_group_id')
		    ->join('master_process AS p', 'p.id', '=', 'tpl.process_id')
		    ->where('tpl.header_id', '=', $header_id)
		    ->get(array('tpl.*', 'p.process', 'g.group'));
        $data = array();
        if(count($template_details) > 0)
        {
            foreach($template_details as $detail)
            {
                $resources = DB::table('process_resource AS pr')
                            ->join('master_resource AS r', 'r.id', '=', 'pr.resource_id')
                            ->where('pr.process_id', '=', $detail->process_id)
                            ->get(array('r.*'));
                $resource = array();
                if(count($resources) > 0)
                {
                    foreach($resources as $res)
                    {
                        $resource[] = ucwords($res->name);
                    }
                }

                $data[] = array(
                    'id' => $detail->id,
                    'header_id' => $detail->header_id,
                    'process_group_id' => $detail->process_group_id,
                    'process_id' => $detail->process_id,
                    'group' => $detail->group,
                    'process' => $detail->process,
                    'estimation' => $detail->estimation,
                    'estimation_hour' => $detail->estimation_hour,
                    'position' => $detail->position,
                    'specification' => $detail->specification,
                    'resource' => implode(', ', $resource)
                );
            }
        }

        $response = Response::make($data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function from_mfg_process_template($material_id, $master_plan_id)
    {
        $template_id = Input::get('template-id');
        $template_data = TemplateMfgProcessDetail::where('header_id', '=', $template_id);
        if($template_data->count() > 0)
        {
            DB::table('project_planning_detail_mfg')
                ->where('master_planning_id', '=', $master_plan_id)
                ->where('material_id', '=', $material_id)
                ->delete();

            $master_plan_data = ProjectMasterPlan::find($master_plan_id);
            if($master_plan_data->parent_id)
            {
                $master_plan_data = ProjectMasterPlan::find($master_plan_data->parent_id);
            }

            $project_id = $master_plan_data->project_id;

            foreach($template_data->get() as $template)
            {
                $mfg_process = new PlanningMFG;
                $mfg_process->project_id = $project_id;
                $mfg_process->master_planning_id = $master_plan_data->id;
                $mfg_process->material_id = $material_id;
                $mfg_process->process_id = $template->process_id;
                $mfg_process->process_group_id = $template->process_group_id;
                $mfg_process->estimation = $template->estimation;
                $mfg_process->estimation_hour = $template->estimation_hour;
                $mfg_process->position = $template->position > 0 ? $template->position : 1;
                $mfg_process->specification = $template->specification;
                $mfg_process->resource_group_id = select_resource_group_randomly($template->process_id);
                $mfg_process->save();

                save_mfg_qty($mfg_process->id);
                save_mfg_position($mfg_process->id);
            }
        }
        return Redirect::back();
    }

    public function from_mfg_process_template_header($master_plan_id)
    {
        $template_id = Input::get('template-id');
        $material_ids = Input::get('material-ids');
        $template_data = TemplateMfgProcessDetail::where('header_id', '=', $template_id);
        if($template_data->count() > 0)
        {
            foreach(explode(',', $material_ids) as $material_id)
            {
                DB::table('project_planning_detail_mfg')
                    ->where('master_planning_id', '=', $master_plan_id)
                    ->where('material_id', '=', $material_id)
                    ->delete();

                $master_plan_data = ProjectMasterPlan::find($master_plan_id);
                if($master_plan_data->parent_id)
                {
                    $master_plan_data = ProjectMasterPlan::find($master_plan_data->parent_id);
                }

                $project_id = $master_plan_data->project_id;

                foreach($template_data->get() as $template)
                {
                    $mfg_process = new PlanningMFG;
                    $mfg_process->project_id = $project_id;
                    $mfg_process->master_planning_id = $master_plan_data->id;
                    $mfg_process->material_id = $material_id;
                    $mfg_process->process_id = $template->process_id;
                    $mfg_process->process_group_id = $template->process_group_id;
                    $mfg_process->estimation = $template->estimation;
                    $mfg_process->estimation_hour = $template->estimation_hour;
                    $mfg_process->position = $template->position > 0 ? $template->position : 1;
                    $mfg_process->specification = $template->specification;
                    $mfg_process->resource_group_id = select_resource_group_randomly($template->process_id);
                    $mfg_process->save();

                    save_mfg_qty($mfg_process->id);
                    save_mfg_position($mfg_process->id);
                }
            }
        }
        return Redirect::back();
    }

    public function filter()
    {
        $process_group_id = Input::get('step');
        $project_id = Input::get('project_id');
        $master_plan = ProjectMasterPlan::where('project_id', '=', $project_id)->where('process_group_id', '=', $process_group_id)->get(array('id'));

        return Redirect::route('project.planning.detail', array($process_group_id, $master_plan[0]->id));
    }

    public function create($process_group_id, $master_plan_id)
    {
        $master_plan_data = ProjectMasterPlan::find($master_plan_id);

        if($master_plan_data->parent_id != 0)
        {
            $plan = ProjectMasterPlan::find($master_plan_data->parent_id);
        }
        else
        {
            $plan = $master_plan_data;
        }

        $project_data = Project::find($plan->project_id);

        $view = View::make(Config::get('app.theme') . '.planning_detail.create.create_non_mfg_modal');
        $view->with('processes', Process::whereIn('group_id', array('1','2','9','11','12'))->get());
        $view->with('project', $project_data);
        $view->with('master_plan_id', $master_plan_id);
        $view->with('min_start_date', $master_plan_data->start_date);
        $view->with('max_finish_date', $master_plan_data->finish_date);
        $view->with('max_estimation', $master_plan_data->real_estimation);
        $view->with('step_name', ProcessGroup::find($process_group_id)->group);
        $view->with('step', $process_group_id);
        $view->with('random', mt_rand(0, 999));

        return $view;
    }

    public function store($master_plan_id)
    {
        $process_id = Input::get('process_id');
        $project_id = Input::get('project_id');
        $group_id = Input::get('group_id');
        $specification = Input::get('specification');
        $position = Input::get('position') > 0 ? Input::get('position') : 1;
        $estimation = Input::get('estimation');
        $start_date = Input::get('start');
        $finish_date = Input::get('finish');

        if(Input::get('resource_select') == 'auto')
        {
            $selected_resource = select_resource_group_randomly($process_id);
        }
        else
        {
            $selected_resource = Input::get('resource_group');
        }

        $process_group_id = Input::get('step');
        $planning = new PlanningNonMFG;
        $planning->project_id = $project_id;
        $planning->process_id = $process_id ;
        $planning->specification = $specification;
        $planning->resource_group_id = $selected_resource;
        $planning->position = $position;
        $planning->estimation_hour = $estimation;
        $planning->master_planning_id = $master_plan_id;
        $planning->process_group_id = $group_id;

        if($planning->save())
        {
            save_non_mfg_qty($planning->id);
            save_non_mfg_position($planning->id);
            Notification::success('New Planning was saved.');
        }
        else
        {
            Notification::error('New Planning was not saved!');
        }

        return Redirect::route('project.planning.detail', array($process_group_id, $master_plan_id));
    }

    public function do_pb_bom_plan($master_plan_id)
    {
        $master_plan_data = ProjectMasterPlan::find($master_plan_id);
        $project_bom = ProjectBOM::where('project_id', '=', $master_plan_data->project_id);

        if($project_bom->count() > 0)
        {
            DB::table('project_planning_detail_pb_bom')->where('master_planning_id', '=', $master_plan_id)->delete();

            foreach($project_bom->get() as $bom)
            {
                $this->_store_pb_bom($master_plan_data->project_id, $bom->id, $master_plan_id, $master_plan_data->start_date, $master_plan_data->finish_date);
            }
        }

        return Redirect::route('project.planning.detail', array(2, $master_plan_id));
    }

    private function _store_pb_bom($project_id, $material_id, $master_plan_id, $start_date, $finish_date)
    {
        $planning = new PlanningPBBOM;
        $planning->project_id = $master_plan_id;
        $planning->master_planning_id = $master_plan_id;
        $planning->process_id = 44;
        $planning->material_id = $material_id;
        $planning->resource_id = 43;
        $planning->save();
    }

    public function create_mfg($material_id, $master_plan_id)
    {
        $master_plan_data = ProjectMasterPlan::find($master_plan_id);

        if($master_plan_data->parent_id != 0)
        {
            $plan = ProjectMasterPlan::find($master_plan_data->parent_id);
        }
        else
        {
            $plan = $master_plan_data;
        }

        $project_data = Project::find($plan->project_id);
        if($plan->process_group_id == 15)
        {
            // $processes = Process::whereNotIn('group_id', array('1','2','9','10','11','12','15','18','19','20','22'))->get();
            $processes = Process::all();
        }
        else
        {
            $processes = Process::where('group_id', '=', $plan->process_group_id)->get();
        }

        $process_data = array();
        foreach($processes as $process)
        {
            $process_data[] = array(
                'id' => $process->id,
                'group_id' => $process->group_id,
                'process_name' => $process->process,
                'code' => $process->code
            );
        }

        $view = View::make(Config::get('app.theme') . '.planning_detail.create.create_mfg_modal');
        $view->with('material', ProjectBOM::find($material_id));
        $view->with('process_data', $process_data);
        $view->with('project', $project_data);
        $view->with('plan_id', $master_plan_id);
        $view->with('min_start_date', $master_plan_data->start_date);
        $view->with('max_finish_date', $master_plan_data->finish_date);
        $view->with('random', mt_rand(0, 999));

        return $view;
    }

    public function store_mfg($material_id, $master_plan_id)
    {
        $position = Input::get('position') > 0 ? Input::get('position') : 1;
        $specification = Input::get('specification');
        $process_id = Input::get('process_id');
        $project_id = Input::get('project_id');
        $group_id = Input::get('group_id');
        $estimation = Input::get('estimation');
        $start_date = Input::get('start');
        $finish_date = Input::get('finish');

        if(Input::get('resource_select') == 'auto')
        {
            $selected_resource = select_resource_group_randomly($process_id);
        }
        else
        {
            $selected_resource = Input::get('resource_group');
        }

        $is_process_exist = PlanningMFG::where('project_id', '=', $project_id)
                    ->where('master_planning_id', '=', $master_plan_id)
                    ->where('material_id', '=', $material_id)
                    ->where('process_id', '=', $process_id)
                    ->count() > 0 ? true : false;

        if($is_process_exist === false)
        {
            $master_plan_data = ProjectMasterPlan::find($master_plan_id);
            if($master_plan_data->parent_id)
            {
                $master_plan_data = ProjectMasterPlan::find($master_plan_data->parent_id);
            }

            $planning = new PlanningMFG;
            $planning->project_id = $project_id;
            $planning->master_planning_id = $master_plan_data->id;
            $planning->material_id = $material_id;
            $planning->resource_group_id = $selected_resource;
            $planning->process_id = $process_id;
            $planning->process_group_id = $group_id;
            $planning->position = $position;
            $planning->specification = $specification;
            $planning->estimation = round($estimation / 24, 2);
            $planning->estimation_hour = $estimation;
            $planning->save();

            save_mfg_qty($planning->id);
            save_mfg_position($planning->id);
        }

        Notification::success('New process was saved.');

        return Redirect::route('project.planning.material_process', array($material_id, $master_plan_id));
    }

    public function edit($master_plan_id, $detail_plan_id)
    {
        $master_plan_data = ProjectMasterPlan::find($master_plan_id);
        $group_data = ProcessGroup::find($master_plan_data->process_group_id);

        if($master_plan_data->parent_id != 0)
        {
            $plan = ProjectMasterPlan::find($master_plan_data->parent_id);
        }
        else
        {
            $plan = $master_plan_data;
        }

        $project_data = Project::find($plan->project_id);

        switch ($group_data->type) {
            case 'material':
                $planning = PlanningMFG::find($detail_plan_id);

                $view = View::make(Config::get('app.theme') . '.planning_detail.edit.edit_mfg_modal');
                $view->with('materials', ProjectBOM::where('project_id', '=', $master_plan_data->project_id)->get()->toArray());
                $view->with('material', ProjectBOM::find($planning->material_id));
                $view->with('processes', Process::whereNotIn('group_id', array('1','2','9','11','12'))->get());
                $view->with('process', Process::find($planning->process_id));
                break;

            default:
                $planning = PlanningNonMFG::find($detail_plan_id);
                $process_template = Process::find($planning->process_id);

                $view = View::make(Config::get('app.theme') . '.planning_detail.edit.edit_non_mfg_modal');
                $view->with('processes', Process::whereIn('group_id', array('1','2','9','11','12'))->get());
                $view->with('planning_process', $process_template->process);
                $view->with('group_id', $master_plan_data->process_group_id);
                $view->with('type', $master_plan_data->process_group_id);
                break;
        }

        // $view->with('available_resource', @get_available_resource_by_process_group($group_data->id, $master_plan_data->start_date, $master_plan_data->finish_date));
        $view->with('project', $project_data);
        $view->with('planning_data', $planning);
        $view->with('min_start_date', $master_plan_data->start_date);
        $view->with('max_finish_date', $master_plan_data->finish_date);
        $view->with('master_plan_id', $master_plan_id);
        $view->with('is_planning', intval(Input::get('is_planning')) ? true : false);
        $view->with('random', mt_rand(0, 999));

        return $view;
    }

    public function update($detail_plan_id)
    {
        $random_num = Input::get('random');
        $process_id = Input::get('process_id');
        $project_id = Input::get('project_id');
        $process_group_id = Input::get('group_id');
        $specification = Input::get('specification');
        $estimation = Input::get('estimation');
        $start_date = Input::get('start');
        $finish_date = Input::get('finish');

        if(Input::get('resource_select') == 'auto')
        {
            $selected_resource = select_resource_group_randomly($process_id);
        }
        else
        {
            $selected_resource = Input::get('resource_group');
        }

        $planning_type = Input::get('planning_type');
        switch ($planning_type) {
            case 'material':
                $planning = PlanningMFG::find($detail_plan_id);
                $planning->project_id = $project_id;
                $planning->material_id = Input::get('material_id');
                $planning->process_id = $process_id;
                $planning->resource_group_id = $selected_resource;
                $planning->position = Input::get('position') > 0 ? Input::get('position') : 1;
                $planning->specification = Input::get('specification');
                $planning->estimation = round($estimation / 24, 2);
                $planning->estimation_hour = $estimation;
                break;

            default:
                $planning = PlanningNonMFG::find($detail_plan_id);
                $planning->project_id = $project_id;
                $planning->process_id = $process_id;
                $planning->specification = $specification;
                $planning->resource_group_id = $selected_resource;
                $planning->position = Input::get('position') > 0 ? Input::get('position') : 1;
                $planning->estimation_hour = $estimation;

                save_non_mfg_qty($planning->id);
                save_non_mfg_position($planning->id);
                break;
        }

        if($planning->save())
        {
            Notification::success('Planning changes was saved.');
        }
        else
        {
            Notification::error('Planning changes was not saved!');
        }

        return Redirect::route('project.planning.detail',   array($process_group_id,  $planning->master_planning_id));
    }

    public function edit_mfg($process_id)
    {
        $planning_data = PlanningMFG::find($process_id);

        $master_plan_data = ProjectMasterPlan::find($planning_data->master_planning_id);

        if($master_plan_data->parent_id != 0)
        {
            $plan = ProjectMasterPlan::find($master_plan_data->parent_id);
        }
        else
        {
            $plan = $master_plan_data;
        }

        $resource = Resource::find($planning_data->resource_id);
        $project_data = Project::find($plan->project_id);

        if($plan->process_group_id == 15)
        {
            $processes = Process::whereNotIn('group_id', array('1','2','9','10','11','12','15','18','19','20','22'))->get();
        }
        else
        {
            $processes = Process::where('group_id', '=', $plan->process_group_id)->get();
        }

        $process_data = array();
        foreach($processes as $process)
        {
            $process_data[] = array(
                'id' => $process->id,
                'group_id' => $process->group_id,
                'process_name' => $process->process,
                'code' => $process->code
            );
        }

        $project_data = Project::find($plan->project_id);
        $the_planning = DB::table('project_planning_detail_mfg AS p')
                        ->join('master_process AS m', 'm.id', '=', 'p.process_id')
                        ->select('p.*', 'm.id AS pid', 'm.process', 'm.code')
                        ->where('p.id', '=', $process_id)
                        ->get();
        $resources = DB::table('process_resource AS p')
                        ->join('master_resource AS m', 'm.id', '=', 'p.resource_id')
                        ->select('m.*')
                        ->where('p.process_id', '=', $planning_data->process_id)
                        ->get();

        // debug(DB::getQueryLog());

        $view = View::make(Config::get('app.theme') . '.planning_detail.edit.edit_mfg_modal');
        $view->with('material', ProjectBOM::find($planning_data->material_id));
        $view->with('process_data', $process_data);
        $view->with('resources', $resources);
        $view->with('project', $project_data);
        $view->with('planning_data', isset($the_planning[0]) ? $the_planning[0] : null);
        $view->with('min_start_date', $master_plan_data->start_date);
        $view->with('max_finish_date', $master_plan_data->finish_date);
        $view->with('plan_id', $master_plan_data->id);
        $view->with('process_group_id', $plan->process_group_id);
        $view->with('is_planning', intval(Input::get('is_planning')) ? true : false);
        $view->with('random', mt_rand(0, 999));

        return $view;
    }

    public function update_mfg($planning_id)
    {
        $position = Input::get('position') > 0 ? Input::get('position') : 1;
        $specification = Input::get('specification');
        $process_id = Input::get('process_id');
        $project_id = Input::get('project_id');
        $group_id = Input::get('group_id');
        $estimation = Input::get('estimation');
        $start_date = Input::get('start');
        $finish_date = Input::get('finish');
        $sub_plannings = Input::get('sub_planning');

        if(Input::get('resource_select') == 'auto')
        {
            $selected_resource_group = select_resource_group_randomly($process_id);
        }
        else
        {
            $selected_resource_group = Input::get('resource_group');
        }

        $planning = PlanningMFG::find($planning_id);
        $planning->resource_group_id = $selected_resource_group;
        $planning->process_id = $process_id;
        $planning->position = $position;
        $planning->specification = $specification;
        $planning->estimation = round($estimation / 24, 2);
        $planning->estimation_hour = $estimation;

        if($planning->save())
        {
            save_mfg_qty($planning->id);
            save_mfg_position($planning->id);

            Notification::success('Process changes was saved.');
        }
        else
        {
            Notification::error('Process changes was not saved!');
        }

        return Redirect::route('project.planning.material_process', array($planning->material_id, $planning->master_planning_id));
    }

    public function destroy($process_type, $detail_id)
    {
        switch ($process_type)
        {
            case 'material':
                $planning = PlanningMFG::find($detail_id);
                delete_planning_qty($planning->id);
                delete_mfg_position($planning->id);
                $planning->delete();
                break;

            default:
            case 'nop':
                $planning = PlanningNonMFG::find($detail_id);
                delete_non_mfg_position($planning->id);
                $planning->delete();
                break;
        }

        return Redirect::back();
    }

    public function get_available_resource()
    {
        $process_id = Input::get('group_id');
        $start_date = explode('/', Input::get('start'));
        $finish_date = explode('/', Input::get('finish'));
        $available_resources = get_available_resource_by_process_group($process_id, date('Y-m-d 00:00:00', strtotime($start_date[2].'-'.$start_date[1].'-'.$start_date[0])), date('Y-m-d 00:00:00', strtotime($finish_date[2].'-'.$finish_date[1].'-'.$finish_date[0])));
        $response = Response::make($available_resources, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function planning_data($id)
    {
        $master_plan = ProjectMasterPlan::find($id);
        $planning_data = PlanningNonMFG::where('master_planning_id', '=', $id)->get();

        $response = Response::make($planning_data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_process_popover_content()
    {
        $view = View::make(Config::get('app.theme') . '.planning_detail.list.process_popover');
        return $view;
    }

    private function get_master_process($master_plan_id)
    {
        $master_plan_data = ProjectMasterPlan::find($master_plan_id);
        $master_process = MasterPlanTemplate::where('process', '=', $master_plan_data->process_group_id)->get();
        return @$master_process[0];
    }

}
