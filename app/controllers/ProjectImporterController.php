<?php
use Project;
use ProjectBOM;
use NOPPImporter;

class ProjectImporterController extends BaseController
{

    public function index()
    {
        $projects = NOPPImporter::getProjects();
        return json_encode($projects);
    }

    public function sync_data_from_erp()
    {
        $projects = NOPPImporter::getProjects();
        $new_projects = 0;
        $update_projects = 0;
        foreach ($projects as $project)
        {
            $existing_project = Project::where('nop', '=', $project->nop);
            if($existing_project->count() == 0)
            {
                $new_project = new Project;
                $new_project++;
            }
            else
            {
                $existing_project = $existing_project->get()[0];
                $new_project = Project::find($existing_project->id);
                $update_projects++;
            }

            $new_project->nop = $project->nop;
            $new_project->order_num = $project->order_num;
            $new_project->priority = $project->priority;
            $new_project->project_name = $project->project;
            $new_project->qty = $project->qty;
            $new_project->price_per_unit = $project->price_per_unit;
            $new_project->price_total = $project->price_total;
            $new_project->customer_id = $project->customer_id;
            $new_project->customer_code = $project->customer_code;
            $new_project->customer_name = $project->customer;
            $new_project->is_planning = '1';
            $new_project->start_date = date('Y-m-d', strtotime($project->start_date));
            $new_project->trial_date = date('Y-m-d', strtotime($project->trial_date));
            $new_project->delivery_date = date('Y-m-d', strtotime($project->delivery_date));
            $new_project->save();

            $materials = NOPPImporter::getBomProject($project->nop, $project->order_num);

            if(count($materials) > 0)
            {
                foreach ($materials as $material) {
                    $bom_data = ProjectBOM::where('nop', '=', $material->nop);
                    if($bom_data->count() > 0)
                    {
                        foreach($bom_data->get() as $bom)
                        {
                            if(trim($bom->barcode) == trim($material->barcode))
                            {
                                update_materials_to_db($bom->id, $new_project->id, $material);
                            }
                        }
                    }
                    else
                    {
                        save_erp_material($new_project->id, $material);
                    }
                }
            }

        }

        $response = Response::make(array('new_projects' => $new_projects), 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

}
