<?php
use Pic;
use Unit;
// // use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification;
use Department;

class PicController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'user_management');
            $view->with('child_active', 'pic');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.pic.list');
        $view->with('page_title', 'PIC Management');
        $view->with('sub_title', 'You can manage Resource/Work Center PIC with this page');
        $view->with('theme', Config::get('app.theme'));
        $view->with('pic_lists', Pic::getPICs());

        return $view;
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.pic.create');
        $view->with('page_title', 'PIC Management');
        $view->with('sub_title', 'Create new PIC');
        $view->with('theme', Config::get('app.theme'));
        $view->with('departments', Department::all());
        $view->with('units', Unit::all());

        return $view;
    }

    public function store()
    {
        $pic = new Pic();
        $pic->user_id = Input::get('userid');
        $pic->code = Input::get('code');
        $pic->first_name = Input::get('firstname');
        $pic->last_name = Input::get('lastname');
        $pic->dept_id = Input::get('department');
        $pic->unit_id = Input::get('unit');
        $pic->sex = Input::get('gender');

        if($pic->save())
            Notification::success('New PIC was saved.');

        return Redirect::route('pic.index');
    }

    public function edit($id)
    {
        $view = View::make(Config::get('app.theme').'.pic.edit');
        $view->with('page_title', 'PIC Management');
        $view->with('sub_title', 'Edit PIC ' . Pic::getPICFullname($id));
        $view->with('theme', Config::get('app.theme'));
        $view->with('pic', Pic::find($id));
        $view->with('departments', Department::all());
        $view->with('units', Unit::all());

        return $view;
    }

    public function update($id)
    {
        $pic = Pic::find($id);

        $pic->user_id = Input::get('userid');
        $pic->code = Input::get('code');
        $pic->first_name = Input::get('firstname');
        $pic->last_name = Input::get('lastname');
        $pic->dept_id = Input::get('department');
        $pic->unit_id = Input::get('unit');
        $pic->sex = Input::get('gender');

        if($pic->save())
            Notification::success('PIC data changes was saved.');

        return Redirect::route('pic.index', $id);
    }

    public function destroy($id)
    {
        $pic = Pic::find($id);
        $pic->delete();

        return Redirect::route('pic.index');
    }
}
