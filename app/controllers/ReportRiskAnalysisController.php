<?php

use Process;
use ProcessGroup;
use ProcessResource;

use PlanningMFG;
use PlanningNonMFG;

use ProjectMasterPlan;

use SchedulingMFG;
use SchedulingNonMFG;
use SchedulingPBBOM;

use Resource;
use ResourceActualUsed;
use ResourcePlanner;

use GeneralOptions;

use ReportRiskAnalysis;
use ReportRiskAnalysisMaterial;
use ReportRiskAnalysisNOP;

class ReportRiskAnalysisController extends BaseController {

    public function __construct() {
        View::composer(Config::get('app.theme') . '._layouts.master', function ($view) {
            $view->with('parent_active', 'report');
            $view->with('child_active', 'risk_analysis');
        });
    }

    public function index() {
        $view = View::make(Config::get('app.theme') . '.reports.risk_analysis.index');
        $view->with('page_title', 'Report Risk Analysis');
        $view->with('theme', Config::get('app.theme'));
        $view->with('report_data', ReportRiskAnalysis::orderBy('created_at', 'desc');

        return $view;
    }


}
