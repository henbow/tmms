<?php
class ProjectCostControlController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'project_cost_control');
            $view->with('child_active', 'project_cost_control');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.project_cost_control.index');
        $view->with('page_title', 'Project Cost Control');
        $view->with('sub_title', 'Project List');
        $view->with('theme', Config::get('app.theme'));
        $view->with('projects', Project::where('is_filtered','<>','1')->orderBy('id','desc')->paginate(20));
        $view->with('start_page', (intval(Input::get('page')) - 1) * 20);

        return $view;
    }
}
