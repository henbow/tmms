<?php

use ResourceWorkTimeTemplate;

class ResourceWorkTimeTemplateController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'resource_center');
            $view->with('child_active', 'work_time_template');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.template.work_time.list');
        $view->with('page_title', 'Resource Work Time Template');
        $view->with('sub_title', 'Manage Resource Work Time Template');
        $view->with('theme', Config::get('app.theme'));
        $view->with('templates', ResourceWorkTimeTemplate::paginate(20));

        return $view;
    }

    public function show($template_id)
    {
        // do nothing
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.template.work_time.create');
        $view->with('page_title', 'Resource Work Time Template');
        $view->with('sub_title', '');
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function store()
    {
        $resource_work_time_template = new ResourceWorkTimeTemplate;
        $resource_work_time_template->name = Input::get('name');
        $resource_work_time_template->mon = Input::get('mon');
        $resource_work_time_template->tue = Input::get('tue');
        $resource_work_time_template->wed = Input::get('wed');
        $resource_work_time_template->thu = Input::get('thu');
        $resource_work_time_template->fri = Input::get('fri');
        $resource_work_time_template->sat = Input::get('sat');
        $resource_work_time_template->sun = Input::get('sun');

        if ($resource_work_time_template->save())
        {
            Notification::success('Work time template was saved.');
        }

        return Redirect::route('work_time_template.index');
    }

    public function edit($id)
    {
        $view = View::make(Config::get('app.theme').'.template.work_time.edit');
        $view->with('page_title', 'Resource Work Time Template');
        $view->with('sub_title', '');
        $view->with('template', ResourceWorkTimeTemplate::find($id));
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function update($id)
    {
        $resource_work_time_template = ResourceWorkTimeTemplate::find($id);
        $resource_work_time_template->name = Input::get('name');
        $resource_work_time_template->mon = Input::get('mon');
        $resource_work_time_template->tue = Input::get('tue');
        $resource_work_time_template->wed = Input::get('wed');
        $resource_work_time_template->thu = Input::get('thu');
        $resource_work_time_template->fri = Input::get('fri');
        $resource_work_time_template->sat = Input::get('sat');
        $resource_work_time_template->sun = Input::get('sun');

        if ($resource_work_time_template->save())
        {
            Notification::success('Work time template changes was saved.');
        }

        return Redirect::route('work_time_template.index');
    }

    public function destroy($id)
    {
        $resource = ResourceWorkTimeTemplate::find($id);
        $resource->delete();

        Notification::success('Work time template changes was deleted.');

        return Redirect::route('work_time_template.index');
    }

}
