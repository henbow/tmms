<?php
use AlertSetting;
use AlertSettingUser;

class AlertSettingsController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'settings');
            $view->with('child_active', 'alert_settings');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.alert_setting.list');
        $view->with('page_title', 'Alert Settings');
        $view->with('sub_title', 'Manage Alert Setting');
        $view->with('theme', Config::get('app.theme'));
        $view->with('alert_settings', AlertSetting::all());

        return $view;
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.alert_setting.create');
        $view->with('page_title', 'Alert Settings');
        $view->with('sub_title', 'Create new setting');
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function store()
    {
        $alert_setting = new AlertSetting;
        $alert_setting->setting = Input::get('setting_name');

        if($alert_setting->save())
        {
        	$users = Input::get('recipients');
        	foreach ($users as $user) {
        	   	$alert_users = new AlertSettingUser;
        	   	$alert_users->setting_id =$alert_setting->id;
        	   	$alert_users->user_id = $user;
        	   	$alert_users->send_email = '1';
        	   	$alert_users->send_sms = '1';
        	   	$alert_users->save();
        	}

            Notification::success('New Alert Setting was saved.');
        }

        return Redirect::route('alert_setting.index');
    }

    public function edit($id)
    {
        $view = View::make(Config::get('app.theme').'.alert_setting.edit');
        $view->with('page_title', 'Alert Settings');
        $view->with('sub_title', 'Edit Alert Setting');
        $view->with('theme', Config::get('app.theme'));
        $view->with('alert_setting', AlertSetting::find($id));

        $alert_users = AlertSettingUser::where('setting_id','=',$id)->select('user_id')->get();
        $users = array();
        if(count($alert_users))
        {
            foreach ($alert_users as $alert_user) {
                $users[] = $alert_user->user_id;
            }
        }

        $view->with('alert_users', $users);

        return $view;
    }

    public function update($id)
    {
        $alert_setting = AlertSetting::find($id);
        $alert_setting->setting = Input::get('setting_name');

        if($alert_setting->save())
        {
            DB::table('alert_setting_users')->where('setting_id','=',$alert_setting->id)->delete();
            $users = Input::get('recipients');
            foreach ($users as $user) {
                $alert_users = new AlertSettingUser;
                $alert_users->setting_id =$alert_setting->id;
                $alert_users->user_id = $user;
                $alert_users->send_email = '1';
                $alert_users->send_sms = '1';
                $alert_users->save();
            }

            Notification::success('Alert Setting was updated.');
        }

        return Redirect::route('alert_setting.index');
    }

    public function destroy($id)
    {
        $alert_setting = AlertSetting::find($id);
        $alert_setting->delete();

        DB::table('alert_setting_users')->where('setting_id','=',$alert_setting->id)->delete();

        Notification::success('Alert Setting was deleted.');

        return Redirect::route('alert_setting.index');
    }

    public function set_receive_email()
    {
        $id = Input::get('pk');
        $value = Input::get('value');
        $alert_user = AlertSettingUser::find($id);
        $alert_user->send_email = $value;
        $alert_user->save();
    }

    public function set_receive_sms()
    {
        $id = Input::get('pk');
        $value = Input::get('value');
        $alert_user = AlertSettingUser::find($id);
        $alert_user->send_sms = $value;
        $alert_user->save();
    }
}
