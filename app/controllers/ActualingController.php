<?php

class ActualingController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'actualing');
            $view->with('child_active', '');
        });
    }

    public function inplan_detail($step,$ui_type='')
    {
        $view = View::make(Config::get('app.theme').'.actualing.detail');

        switch($step)
        {
            case 'material':
                $view->with('page_title', 'Project Actualing by Material for '.($ui_type == '1' ? 'HUMAN' : 'MACHINE'));
                $view->with('module', 'mfg');
                $view->with('child_active', 'manufacturing');
                break;

            case 'pb-bom':
                $planning = PlanningPBBOM::groupBy('master_planning_id')->get();
                $view->with('page_title', 'Project Actualing by PB-BOM');
                $view->with('module', 'pb-bom');
                $view->with('planning_data', $planning);
                $view->with('child_active', 'manufacturing');
                break;

            case 'nop':
            default:
                $view->with('page_title', 'Project Actualing by NOP');
                $view->with('module', 'non-mfg');
                $view->with('child_active', 'nop');
                break;
        }

        $view->with('theme', Config::get('app.theme'));
        $view->with('step', $step);
        $view->with('ui_type', $ui_type);

        return $view;
    }

    public function get_actualing_data()
    {
        $plan_type = Input::get('plan_type');
        $step = Input::get('step');
        $state = Input::get('state');
        $s_echo = Input::get('sEcho');
        $i_columns = Input::get('iColumns');
        $s_columns = Input::get('sColumns');
        $i_display_start = Input::get('iDisplayStart');
        $i_display_length = Input::get('iDisplayLength');
        $m_data_prop_0 = Input::get('mDataProp_0');
        $m_data_prop_1 = Input::get('mDataProp_1');
        $m_data_prop_2 = Input::get('mDataProp_2');
        $m_data_prop_3 = Input::get('mDataProp_3');
        $m_data_prop_4 = Input::get('mDataProp_4');
        $m_data_prop_5 = Input::get('mDataProp_5');
        $m_data_prop_6 = Input::get('mDataProp_6');
        $m_data_prop_7 = Input::get('mDataProp_7');
        $m_data_prop_8 = Input::get('mDataProp_8');
        $m_data_prop_9 = Input::get('mDataProp_9');
        $m_data_prop_10 = Input::get('mDataProp_10');
        $m_data_prop_11= Input::get('mDataProp_11');
        $s_search = Input::get('sSearch');
        $b_regex = Input::get('bRegex');
        $s_search_0 = Input::get('sSearch_0');
        $b_regex_0 = Input::get('bRegex_0');
        $b_searchable_0 = Input::get('bSearchable_0');
        $s_search_1 = Input::get('sSearch_1');
        $b_regex_1 = Input::get('bRegex_1');
        $b_searchable_1 = Input::get('bSearchable_1');
        $s_search_2 = Input::get('sSearch_2');
        $b_regex_2 = Input::get('bRegex_2');
        $b_searchable_2 = Input::get('bSearchable_2');
        $s_search_3 = Input::get('sSearch_3');
        $b_regex_3 = Input::get('bRegex_3');
        $b_searchable_3 = Input::get('bSearchable_3');
        $s_search_4 = Input::get('sSearch_4');
        $b_regex_4 = Input::get('bRegex_4');
        $b_searchable_4 = Input::get('bSearchable_4');
        $s_search_5 = Input::get('sSearch_5');
        $b_regex_5 = Input::get('bRegex_5');
        $b_searchable_5 = Input::get('bSearchable_5');
        $s_search_6 = Input::get('sSearch_6');
        $b_regex_6 = Input::get('bRegex_6');
        $b_searchable_6 = Input::get('bSearchable_6');
        $s_search_7 = Input::get('sSearch_7');
        $b_regex_7 = Input::get('bRegex_7');
        $b_searchable_7 = Input::get('bSearchable_7');
        $s_search_8 = Input::get('sSearch_8');
        $b_regex_8 = Input::get('bRegex_8');
        $b_searchable_8 = Input::get('bSearchable_8');
        $s_search_9 = Input::get('sSearch_9');
        $b_regex_9 = Input::get('bRegex_9');
        $b_searchable_9 = Input::get('bSearchable_9');
        $s_search_10 = Input::get('sSearch_10');
        $b_regex_10 = Input::get('bRegex_10');
        $b_searchable_10 = Input::get('bSearchable_10');
        $s_search_11 = Input::get('sSearch_11');
        $b_regex_11 = Input::get('bRegex_11');
        $b_searchable_11 = Input::get('bSearchable_11');
        $i_sort_col_0 = Input::get('iSortCol_0');
        $i_sort_dir_0 = Input::get('sSortDir_0');
        $i_sorting_cols = Input::get('iSortingCols');
        $b_sortable_0 = Input::get('bSortable_0');
        $b_sortable_1 = Input::get('bSortable_1');
        $b_sortable_2 = Input::get('bSortable_2');
        $b_sortable_3 = Input::get('bSortable_3');
        $b_sortable_4 = Input::get('bSortable_4');
        $b_sortable_5 = Input::get('bSortable_5');
        $b_sortable_6 = Input::get('bSortable_6');
        $b_sortable_7 = Input::get('bSortable_7');
        $b_sortable_8 = Input::get('bSortable_8');
        $b_sortable_9 = Input::get('bSortable_9');
        $b_sortable_10 = Input::get('bSortable_10');
        $b_sortable_11 = Input::get('bSortable_11');
        $output = array();

        switch($step)
        {
            case 'material':
                if($state == 'waiting')
                {
                    $a_columns = array(
                        'm.barcode',
                        'm.material_name',
                        'pc.code',
                        'pc.process',
                        'rg.name',
                        'p.estimation_hour'
                    );

                    $s_index_column = "id";

                    $s_limit = $s_filter = "";
                    if (isset($i_display_start) && $i_display_length != '-1' )
                    {
                        $s_limit .= " LIMIT " . intval( $i_display_start ) . ", " . intval( $i_display_length );
                    }

                    $waiting_sql  = "SELECT
                        " . implode(", ", $a_columns) . "
                    FROM
                        project_planning_detail_mfg p
                    LEFT OUTER JOIN
                        actualing_mfg a ON a.planning_id = p.id
                    JOIN
                        master_process pc ON pc.id=p.process_id
                    JOIN
                        project_bom m ON m.id=p.material_id
                    JOIN
                        master_resource_group rg ON rg.id=p.resource_group_id";
                    $all_data = DB::select(DB::raw($waiting_sql));
                    $total_all_data = count($all_data);


                    $s_where = "";
                    if ( isset($s_search) && $s_search != "" )
                    {
                        $s_where = "WHERE (";
                        for ( $i=0 ; $i < 6 ; $i++ )
                        {
                            $s_where .= $a_columns[$i]." LIKE '%".( $s_search )."%' OR ";
                        }
                        $s_where = substr_replace( $s_where, "", -3 );
                        $s_where .= ')';
                    }

                    $s_order = "";
                    if ( isset( $i_sort_col_0 ) )
                    {
                        $s_order = " ORDER BY  ";
                        for ( $i=0 ; $i<intval( $i_sorting_cols ) ; $i++ )
                        {
                            if ( ${"b_sortable_".intval(${"i_sort_col_{$i}"})} == "true" )
                            {
                                $s_order .= $a_columns[ intval( ${"i_sort_col_{$i}"} ) ]." ".
                                    (${"i_sort_dir_{$i}"}==='asc' ? 'asc' : 'desc') .", ";
                            }
                        }

                        $s_order = rtrim(trim( $s_order ), ',');
                        if ( $s_order == "ORDER BY" )
                        {
                            $s_order = "";
                        }
                    }

                    $s_filter .= " " . $s_where . " " . $s_order;

                    $waiting_sql .= $s_filter;
                    $filtered_data = DB::select(DB::raw($waiting_sql));
                    $total_filtered_data = count($filtered_data);

                    $waiting_sql .= $s_limit;
                    $planning_data = DB::select(DB::raw($waiting_sql));

                    // Log::debug($waiting_sql);

                    if($total_filtered_data > 0)
                    {
                        foreach ($planning_data as $planning) {
                            if(isset($planning->barcode) && isset($planning->code))
                            {
                                $output[] = array(
                                    $planning->barcode,
                                    $planning->material_name,
                                    $planning->code,
                                    $planning->process,
                                    $planning->name,
                                    $planning->estimation_hour
                                );
                            }
                        }
                    }
                }
                elseif($state == 'onprocess')
                {
                    $a_columns = array(
                        'm.barcode',
                        'm.material_name',
                        'pc.code',
                        'pc.process',
                        'r.name',
                        'pic.first_name',
                        'p.estimation_hour',
                        'a.start_date'
                    );

                    $s_index_column = "id";

                    $s_limit = $s_filter = "";
                    if (isset($i_display_start) && $i_display_length != '-1' )
                    {
                        $s_limit .= " LIMIT " . intval( $i_display_start ) . ", " . intval( $i_display_length );
                    }

                    $onprocess_sql  = "SELECT
                        p.id AS pid, a.id AS aid," . implode(", ", $a_columns) . ", pic.last_name
                    FROM
                        project_planning_detail_mfg p
                    JOIN
                        actualing_mfg a ON a.planning_id=p.id
                    JOIN
                        master_process pc ON pc.id=p.process_id
                    JOIN
                        project_bom m ON m.id=p.material_id
                    JOIN
                        master_resource r ON r.id=a.resource_id
                    JOIN
                        master_pic AS pic ON pic.id=a.pic_id";
                    $all_data = DB::select(DB::raw($onprocess_sql));
                    $total_all_data = count($all_data);

                    $s_where = "WHERE a.status = 'ON PROCESS'";
                    if ( isset($s_search) && $s_search != "" )
                    {
                        $s_where .= " AND (";
                        for ( $i=0 ; $i < 6 ; $i++ )
                        {
                            $s_where .= $a_columns[$i]." LIKE '%".( $s_search )."%' OR ";
                        }
                        $s_where = substr_replace( $s_where, "", -3 );
                        $s_where .= ')';
                    }

                    $s_order = "";
                    if ( isset( $i_sort_col_0 ) )
                    {
                        $s_order = " ORDER BY  ";
                        for ( $i=0 ; $i<intval( $i_sorting_cols ) ; $i++ )
                        {
                            if ( ${"b_sortable_".intval(${"i_sort_col_{$i}"})} == "true" )
                            {
                                $s_order .= $a_columns[ intval( ${"i_sort_col_{$i}"} ) ]." ".
                                    (${"i_sort_dir_{$i}"}==='asc' ? 'asc' : 'desc') .", ";
                            }
                        }

                        $s_order = rtrim(trim( $s_order ), ',');
                        if ( $s_order == "ORDER BY" )
                        {
                            $s_order = "";
                        }
                    }

                    $s_filter .= " " . $s_where . " " . $s_order;

                    $onprocess_sql .= $s_filter;
                    $filtered_data = DB::select(DB::raw($onprocess_sql));
                    $total_filtered_data = count($filtered_data);

                    $onprocess_sql .= $s_limit;
                    $planning_data = DB::select(DB::raw($onprocess_sql));

                    if(count($planning_data) > 0)
                    {
                        foreach ($planning_data as $planning) {
                            $total_qty = BomQuantity::where('planning_id','=',$planning->pid)->count();
                            $total_complete_qty = DB::table('actualing_mfg_positions AS ap')
                                            ->join('bom_positions AS bp','bp.id','=','ap.position_id')
                                            ->join('bom_quantities AS bq','bq.id','=','bp.qty_id')
                                            ->where('ap.actualing_id','=',$planning->aid)
                                            ->where('ap.status','=','COMPLETE')
                                            ->groupBy('bq.id')
                                            ->count();
                            $total_position = ActualingMFGPosition::where('actualing_id','=',$planning->aid)->count();
                            $total_complete_position = ActualingMFGPosition::where('actualing_id','=',$planning->aid)->where('status','=','COMPLETE')->count();
                            $output[] = array(
                                $planning->barcode,
                                $planning->material_name,
                                $planning->code,
                                $planning->process,
                                $planning->name,
                                isset($planning->first_name) ? $planning->first_name." ".$planning->last_name : '',
                                $planning->estimation_hour,
                                $planning->start_date,
                                $total_complete_qty."/".($total_qty > 0 ? $total_qty : 1),
                                $total_complete_position."/".($total_position > 0 ? $total_position : 1)
                            );
                        }
                    }
                }
                else
                {
                    $a_columns = array(
                        'm.barcode',
                        'm.material_name',
                        'pc.code',
                        'pc.process',
                        'r.name',
                        'pic.first_name',
                        'p.estimation_hour',
                        'a.start_date',
                        'a.finish_date',
                        'a.status'
                    );

                    $s_index_column = "id";

                    $s_limit = $s_filter = "";
                    if (isset($i_display_start) && $i_display_length != '-1' )
                    {
                        $s_limit .= " LIMIT " . intval( $i_display_start ) . ", " . intval( $i_display_length );
                    }

                    $finish_sql  = "SELECT
                        p.id AS pid, a.id AS aid, pic.last_name, " . implode(", ", $a_columns) . "
                    FROM
                        project_planning_detail_mfg p
                    JOIN
                        actualing_mfg a ON a.planning_id=p.id
                    JOIN
                        master_process pc ON pc.id=p.process_id
                    JOIN
                        project_bom m ON m.id=p.material_id
                    JOIN
                        master_resource r ON r.id=a.resource_id
                    JOIN
                        master_pic AS pic ON pic.id=a.pic_id";
                    $all_data = DB::select(DB::raw($finish_sql));
                    $total_all_data = count($all_data);

                    $s_where = "WHERE a.status = 'COMPLETE' AND a.subfinish_type = 'COMPLETE'";
                    if ( isset($s_search) && $s_search != "" )
                    {
                        $s_where .= " AND (";
                        for ( $i=0 ; $i < 6 ; $i++ )
                        {
                            $s_where .= $a_columns[$i]." LIKE '%".( $s_search )."%' OR ";
                        }
                        $s_where = substr_replace( $s_where, "", -3 );
                        $s_where .= ')';
                    }

                    $s_order = "";
                    if ( isset( $i_sort_col_0 ) )
                    {
                        $s_order = " ORDER BY  ";
                        for ( $i=0 ; $i<intval( $i_sorting_cols ) ; $i++ )
                        {
                            if ( ${"b_sortable_".intval(${"i_sort_col_{$i}"})} == "true" )
                            {
                                $s_order .= $a_columns[ intval( ${"i_sort_col_{$i}"} ) ]." ".
                                    (${"i_sort_dir_{$i}"}==='asc' ? 'asc' : 'desc') .", ";
                            }
                        }

                        $s_order = rtrim(trim( $s_order ), ',');
                        if ( $s_order == "ORDER BY" )
                        {
                            $s_order = "";
                        }
                    }

                    $s_filter .= " " . $s_where . " " . $s_order;

                    $finish_sql .= $s_filter;
                    $filtered_data = DB::select(DB::raw($finish_sql));
                    $total_filtered_data = count($filtered_data);

                    $finish_sql .= $s_limit;
                    $planning_data = DB::select(DB::raw($finish_sql));

                    if(count($planning_data) > 0)
                    {
                        foreach ($planning_data as $planning) {
                            $total_qty = BomQuantity::where('planning_id','=',$planning->pid)->count();
                            $total_position = ActualingMFGPosition::where('actualing_id','=',$planning->aid)->count();
                            $output[] = array(
                                $planning->barcode,
                                $planning->material_name,
                                $planning->code,
                                $planning->process,
                                $planning->name,
                                isset($planning->first_name) ? $planning->first_name." ".$planning->last_name : '',
                                $planning->estimation_hour,
                                $planning->start_date,
                                $planning->finish_date,
                                $planning->status,
                                ($total_qty > 0 ? $total_qty : 1)."/".($total_qty > 0 ? $total_qty : 1),
                                ($total_position > 0 ? $total_position : 1)."/".($total_position > 0 ? $total_position : 1)
                            );
                        }
                    }
                }
                break;

            case 'nop':
            default:
                if($state == 'waiting')
                {
                    $a_columns = array(
                        'pj.nop',
                        'pj.project_name',
                        'pc.code',
                        'pc.process',
                        'rg.name',
                        'p.estimation_hour'
                    );

                    $s_index_column = "id";

                    $s_limit = $s_filter = "";
                    if (isset($i_display_start) && $i_display_length != '-1' )
                    {
                        $s_limit .= " LIMIT " . intval( $i_display_start ) . ", " . intval( $i_display_length );
                    }

                    $waiting_sql  = "SELECT
                        " . implode(", ", $a_columns) . "
                    FROM
                        project_planning_detail_non_mfg p
                    LEFT OUTER JOIN
                        actualing_non_mfg a ON a.planning_id = p.id
                    JOIN
                        master_process pc ON pc.id=p.process_id
                    JOIN
                        projects pj ON pj.id=p.project_id
                    JOIN
                        master_resource_group rg ON rg.id=p.resource_group_id";
                    $all_data = DB::select(DB::raw($waiting_sql));
                    $total_all_data = count($all_data);


                    $s_where = "";
                    if ( isset($s_search) && $s_search != "" )
                    {
                        $s_where = "WHERE (";
                        for ( $i=0 ; $i < 6 ; $i++ )
                        {
                            $s_where .= $a_columns[$i]." LIKE '%".( $s_search )."%' OR ";
                        }
                        $s_where = substr_replace( $s_where, "", -3 );
                        $s_where .= ')';
                    }

                    $s_order = "";
                    if ( isset( $i_sort_col_0 ) )
                    {
                        $s_order = " ORDER BY  ";
                        for ( $i=0 ; $i<intval( $i_sorting_cols ) ; $i++ )
                        {
                            if ( ${"b_sortable_".intval(${"i_sort_col_{$i}"})} == "true" )
                            {
                                $s_order .= $a_columns[ intval( ${"i_sort_col_{$i}"} ) ]." ".
                                    (${"i_sort_dir_{$i}"}==='asc' ? 'asc' : 'desc') .", ";
                            }
                        }

                        $s_order = rtrim(trim( $s_order ), ',');
                        if ( $s_order == "ORDER BY" )
                        {
                            $s_order = "";
                        }
                    }

                    $s_filter .= " " . $s_where . " " . $s_order;

                    $waiting_sql .= $s_filter;
                    $filtered_data = DB::select(DB::raw($waiting_sql));
                    $total_filtered_data = count($filtered_data);

                    $waiting_sql .= $s_limit;
                    $planning_data = DB::select(DB::raw($waiting_sql));

                    if($total_filtered_data > 0)
                    {
                        foreach ($planning_data as $planning) {
                            if(isset($planning->nop) && isset($planning->code))
                            {
                                $output[] = array(
                                    $planning->nop,
                                    $planning->project_name,
                                    $planning->code,
                                    $planning->process,
                                    $planning->name,
                                    $planning->estimation_hour
                                );
                            }
                        }
                    }
                }
                elseif($state == 'onprocess')
                {
                    $a_columns = array(
                        'pj.nop',
                        'pj.project_name',
                        'pc.code',
                        'pc.process',
                        'r.name',
                        'pic.first_name',
                        'p.estimation_hour',
                        'a.start_date'
                    );

                    $s_index_column = "id";

                    $s_limit = $s_filter = "";
                    if (isset($i_display_start) && $i_display_length != '-1' )
                    {
                        $s_limit .= " LIMIT " . intval( $i_display_start ) . ", " . intval( $i_display_length );
                    }

                    $onprocess_sql  = "SELECT
                        p.id AS pid, a.id AS aid," . implode(", ", $a_columns) . ", pic.last_name
                    FROM
                        project_planning_detail_non_mfg p
                    JOIN
                        actualing_non_mfg a ON a.planning_id=p.id
                    JOIN
                        master_process pc ON pc.id=p.process_id
                    JOIN
                        projects pj ON pj.id=p.project_id
                    JOIN
                        master_resource r ON r.id=a.resource_id
                    JOIN
                        master_pic AS pic ON pic.id=a.pic_id
                    WHERE (a.status = 'ON PROCESS' OR a.subfinish_type = 'ONPROCESS')";

                    $all_data = DB::select(DB::raw($onprocess_sql));
                    $total_all_data = count($all_data);

                    $s_where = "";
                    if ( isset($s_search) && $s_search != "" )
                    {
                        $s_where .= " AND (";
                        for ( $i=0 ; $i < 6 ; $i++ )
                        {
                            $s_where .= $a_columns[$i]." LIKE '%".( $s_search )."%' OR ";
                        }
                        $s_where = substr_replace( $s_where, "", -3 );
                        $s_where .= ')';
                    }

                    $s_order = "";
                    if ( isset( $i_sort_col_0 ) )
                    {
                        $s_order = " ORDER BY  ";
                        for ( $i=0 ; $i<intval( $i_sorting_cols ) ; $i++ )
                        {
                            if ( ${"b_sortable_".intval(${"i_sort_col_{$i}"})} == "true" )
                            {
                                $s_order .= $a_columns[ intval( ${"i_sort_col_{$i}"} ) ]." ".
                                    (${"i_sort_dir_{$i}"}==='asc' ? 'asc' : 'desc') .", ";
                            }
                        }

                        $s_order = rtrim(trim( $s_order ), ',');
                        if ( $s_order == "ORDER BY" )
                        {
                            $s_order = "";
                        }
                    }

                    $s_filter .= " " . $s_where . " " . $s_order;

                    $onprocess_sql .= $s_filter;
                    $filtered_data = DB::select(DB::raw($onprocess_sql));
                    $total_filtered_data = count($filtered_data);

                    $onprocess_sql .= $s_limit;
                    $planning_data = DB::select(DB::raw($onprocess_sql));

                    // Log::debug($onprocess_sql);

                    if(count($planning_data) > 0)
                    {
                        foreach ($planning_data as $planning) {
                            $total_qty = MasterplanQuantity::where('planning_id','=',$planning->pid)->count();
                            $total_complete_qty = DB::table('actualing_non_mfg_positions AS ap')
                                            ->join('masterplan_positions AS bp','bp.id','=','ap.position_id')
                                            ->join('masterplan_quantities AS bq','bq.id','=','bp.qty_id')
                                            ->where('ap.actualing_id','=',$planning->aid)
                                            ->where('ap.status','=','COMPLETE')
                                            ->groupBy('bq.id')
                                            ->count();

                            $queries = DB::getQueryLog();
                            // Log::debug(end($queries));

                            $total_position = ActualingNonMFGPosition::where('actualing_id','=',$planning->aid)->count();
                            $total_complete_position = ActualingNonMFGPosition::where('actualing_id','=',$planning->aid)->where('status','=','COMPLETE')->count();
                            $output[] = array(
                                $planning->nop,
                                $planning->project_name,
                                $planning->code,
                                $planning->process,
                                $planning->name,
                                isset($planning->first_name) ? $planning->first_name." ".$planning->last_name : '',
                                $planning->estimation_hour,
                                $planning->start_date,
                                $total_complete_qty."/".($total_qty > 0 ? $total_qty : 1),
                                $total_complete_position."/".($total_position > 0 ? $total_position : 1)
                            );
                        }
                    }
                }
                else
                {
                    $a_columns = array(
                        'pj.nop',
                        'pj.project_name',
                        'pc.code',
                        'pc.process',
                        'r.name',
                        'pic.first_name',
                        'p.estimation_hour',
                        'a.start_date',
                        'a.finish_date',
                        'a.status'
                    );

                    $s_index_column = "id";

                    $s_limit = $s_filter = "";
                    if (isset($i_display_start) && $i_display_length != '-1' )
                    {
                        $s_limit .= " LIMIT " . intval( $i_display_start ) . ", " . intval( $i_display_length );
                    }

                    $finish_sql  = "SELECT
                        p.id AS pid, a.id AS aid," . implode(", ", $a_columns) . ", pic.last_name
                    FROM
                        project_planning_detail_non_mfg p
                    JOIN
                        actualing_non_mfg a ON a.planning_id=p.id
                    JOIN
                        master_process pc ON pc.id=p.process_id
                    JOIN
                        projects pj ON pj.id=p.project_id
                    JOIN
                        master_resource r ON r.id=a.resource_id
                    JOIN
                        master_pic AS pic ON pic.id=a.pic_id";
                    $all_data = DB::select(DB::raw($finish_sql));
                    $total_all_data = count($all_data);

                    $s_where = "WHERE a.status = 'COMPLETE' AND a.subfinish_type = 'COMPLETE'";
                    if ( isset($s_search) && $s_search != "" )
                    {
                        $s_where .= " AND (";
                        for ( $i=0 ; $i < 6 ; $i++ )
                        {
                            $s_where .= $a_columns[$i]." LIKE '%".( $s_search )."%' OR ";
                        }
                        $s_where = substr_replace( $s_where, "", -3 );
                        $s_where .= ')';
                    }

                    $s_order = "";
                    if ( isset( $i_sort_col_0 ) )
                    {
                        $s_order = " ORDER BY  ";
                        for ( $i=0 ; $i<intval( $i_sorting_cols ) ; $i++ )
                        {
                            if ( ${"b_sortable_".intval(${"i_sort_col_{$i}"})} == "true" )
                            {
                                $s_order .= $a_columns[ intval( ${"i_sort_col_{$i}"} ) ]." ".
                                    (${"i_sort_dir_{$i}"}==='asc' ? 'asc' : 'desc') .", ";
                            }
                        }

                        $s_order = rtrim(trim( $s_order ), ',');
                        if ( $s_order == "ORDER BY" )
                        {
                            $s_order = "";
                        }
                    }

                    $s_filter .= " " . $s_where . " " . $s_order;

                    $finish_sql .= $s_filter;
                    $filtered_data = DB::select(DB::raw($finish_sql));
                    $total_filtered_data = count($filtered_data);

                    $finish_sql .= $s_limit;
                    $planning_data = DB::select(DB::raw($finish_sql));

                    if(count($planning_data) > 0)
                    {
                        foreach ($planning_data as $planning) {
                            $total_qty = MasterplanQuantity::where('planning_id','=',$planning->pid)->count();
                            $total_position = ActualingNonMFGPosition::where('actualing_id','=',$planning->aid)->count();
                            $output[] = array(
                                $planning->nop,
                                $planning->project_name,
                                $planning->code,
                                $planning->process,
                                $planning->name,
                                isset($planning->first_name) ? $planning->first_name." ".$planning->last_name : '',
                                $planning->estimation_hour,
                                $planning->start_date,
                                $planning->finish_date,
                                $planning->status,
                                ($total_qty > 0 ? $total_qty : 1)."/".($total_qty > 0 ? $total_qty : 1),
                                ($total_position > 0 ? $total_position : 1)."/".($total_position > 0 ? $total_position : 1)
                            );
                        }
                    }
                }
                break;
        }

        $data = array(
            "sEcho" => intval($s_echo),
            "iTotalRecords" => $total_all_data,
            "iTotalDisplayRecords" => $total_filtered_data,
            "aaData" => $output
        );

        $response = Response::make($data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function pbbom_detail($step, $planning_id)
    {
        $view = View::make(Config::get('app.theme').'.actualing.detail');

        $project = PlanningPBBOM::find($planning_id)->masterplan->project;
        $materials = ProjectBOM::with('actualing_pb_bom')->where('project_id','=',$project->id)->get();

        $view->with('page_title', 'Project Actualing by PB-BOM Type [NOP: '.$project->nop.']');
        $view->with('module', 'pb-bom-detail');
        $view->with('materials', $materials);
        $view->with('child_active', 'manufacturing');
        $view->with('theme', Config::get('app.theme'));
        $view->with('step', $step);

        return $view;
    }

    public function outplan_detail($step)
    {
        $view = View::make(Config::get('app.theme').'.actualing.detail');

        switch($step)
        {
            case 'material':
                $actualing_data = DB::table('actualing_out_material')->orderBy('ttb_date', 'desc')->get();
                $view->with('page_title', 'Out Plan Project Actualing by Material Type');
                $view->with('module', 'out-material');
                $view->with('actualing_data', $actualing_data);
                $view->with('child_active', 'manufacturing');
                break;

            default:
                $actualing_data = DB::table('actualing_out_nop')->orderBy('ttb_date', 'desc')->get();
                $view->with('page_title', 'Out Plan Project Actualing by NOP Type');
                $view->with('module', 'out-nop');
                $view->with('actualing_data', $actualing_data);
                $view->with('child_active', 'manufacturing');
                break;
        }

        $view->with('theme', Config::get('app.theme'));
        $view->with('step', $step);

        return $view;
    }

    public function reset_data()
    {
        $view = View::make(Config::get('app.theme').'.actualing.detail-data-reseting');
        $view->with('page_title', 'Actualing Data Reseting for Manager');
        $view->with('parent_active', 'self-services');
        $view->with('child_active', 'reset_actualing_data');
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function do_reset_data()
    {
        // debug($_POST);
        $ids = Input::get('aid');
        $type = Input::get('type');

        if(count($ids) > 0)
        {
            if($type == 'material')
            {
                foreach ($ids as $id)
                {
                    $reset_data1 = DB::table('actualing_mfg')->where('id','=',$id)->delete();
                    $reset_data2 = DB::table('actualing_mfg_log')->where('actualing_id','=',$id)->delete();
                    $reset_data3 = DB::table('actualing_mfg_status_logs')->where('actualing_id','=',$id)->delete();
                    $reset_data4 = DB::table('actualing_mfg_positions')->where('actualing_id','=',$id)->delete();
                    $reset_data5 = DB::table('actualing_mfg_qty_tmp')->where('actualing_id','=',$id)->delete();

                    Activity::log([
                        'contentId'   => $id,
                        'contentType' => 'Self-Services',
                        'action'      => 'Reset actualing data',
                        'description' => "Reset actualing data by material",
                        'details'     => "Reset actualing data: $reset_data1, Reset actualing log: $reset_data2, Reset status logs: $reset_data3, Reset positions: $reset_data4, Reset qty tmp: $reset_data5",
                        'updated'     => true,
                    ]);
                }
            }
            else
            {
                foreach ($ids as $id)
                {
                    $reset_data1 = DB::table('actualing_non_mfg')->where('id','=',$id)->delete();
                    $reset_data2 = DB::table('actualing_non_mfg_log')->where('actualing_id','=',$id)->delete();
                    $reset_data3 = DB::table('actualing_non_mfg_status_logs')->where('actualing_id','=',$id)->delete();
                    $reset_data4 = DB::table('actualing_non_mfg_positions')->where('actualing_id','=',$id)->delete();
                    $reset_data5 = DB::table('actualing_non_mfg_qty_tmp')->where('actualing_id','=',$id)->delete();

                    Activity::log([
                        'contentId'   => $id,
                        'contentType' => 'Self-Services',
                        'action'      => 'Reset actualing data',
                        'description' => "Reset actualing data by NOP",
                        'details'     => "Reset actualing data: $reset_data1, Reset actualing log: $reset_data2, Reset status logs: $reset_data3, Reset positions: $reset_data4, Reset qty tmp: $reset_data5",
                        'updated'     => true,
                    ]);
                }
            }
        }

        return Redirect::route('actualing.reset_data');
    }

    public function edit_data()
    {
        $view = View::make(Config::get('app.theme').'.actualing.detail-data-editing');
        $view->with('page_title', 'Actualing Data Editing for Manager');
        $view->with('module', 'actualing-data-editing');
        $view->with('child_active', 'edit_actualing_data');
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function edit_data_form($id, $type)
    {
        $actualing = $type == 'material' ? ActualingMFG::find($id) : ActualingNonMFG::find($id);
        $planning = $type == 'material' ? PlanningMFG::find($actualing->planning_id) : PlanningNonMFG::find($actualing->planning_id);
        $qty = $type == 'material' ? BomQuantity::where('planning_id','=',$planning->id)->get() : MasterplanQuantity::where('planning_id','=',$planning->id)->get();
        $prm = $type == 'material' ? ProjectBOM::find($planning->material_id) : Project::find($planning->project_id);
        $resource = Resource::find($actualing->resource_id);
        $pic = Pic::find($actualing->pic_id);
        $process = Process::find($planning->process_id);

        // debug($prm->project_name);

        $view = View::make(Config::get('app.theme').'.actualing.module.'.($type == 'material' ? 'edit-actualing-data-mfg' : 'edit-actualing-data-nonmfg'));
        $view->with('type', $type);
        $view->with('actualing', $actualing);
        $view->with('planning', $planning);
        $view->with('random', mt_rand(1, 100));
        $view->with('prm', $prm);
        $view->with('process', $process);
        $view->with('resource', $resource);
        $view->with('pic', $pic);
        $view->with('qty', $qty);
        $view->with('resources', Resource::where('is_offline','<>','1')->where('plan_type','=','in plan')->get());
        $view->with('pics', Pic::all());
        return $view;
    }

    public function do_edit_data()
    {
        $type = Input::get('type');
        switch($type)
        {
            case 'material':

                break;

            default:

                break;
        }
    }

    public function get_actualing_data_reset()
    {
        $type = Input::get('type');
        $s_echo = Input::get('sEcho');
        $i_columns = Input::get('iColumns');
        $s_columns = Input::get('sColumns');
        $i_display_start = Input::get('iDisplayStart');
        $i_display_length = Input::get('iDisplayLength');
        $s_search = Input::get('sSearch');
        $b_regex = Input::get('bRegex');
        $i_sort_col_0 = Input::get('iSortCol_0');
        $i_sort_dir_0 = Input::get('sSortDir_0');
        $i_sorting_cols = Input::get('iSortingCols');
        $b_sortable_0 = Input::get('bSortable_0');
        $b_sortable_1 = Input::get('bSortable_1');
        $b_sortable_2 = Input::get('bSortable_2');
        $b_sortable_3 = Input::get('bSortable_3');
        $b_sortable_4 = Input::get('bSortable_4');
        $b_sortable_5 = Input::get('bSortable_5');
        $b_sortable_6 = Input::get('bSortable_6');
        $b_sortable_7 = Input::get('bSortable_7');
        $b_sortable_8 = Input::get('bSortable_8');
        $b_sortable_9 = Input::get('bSortable_9');
        $b_sortable_10 = Input::get('bSortable_10');
        $b_sortable_11 = Input::get('bSortable_11');
        $output = array();

        switch($type)
        {
            case 'material':
                $a_columns = array(
                    'm.barcode',
                    'm.material_name',
                    'pc.code',
                    'pc.process',
                    // 'r.name',
                    // 'pic.first_name',
                    'p.estimation_hour',
                    'a.start_date'
                );

                $s_index_column = "id";

                $s_limit = $s_filter = "";
                if (isset($i_display_start) && $i_display_length != '-1' )
                {
                    $s_limit .= " LIMIT " . intval( $i_display_start ) . ", " . intval( $i_display_length );
                }

                $actualing_data_sql  = "SELECT
                    p.id AS pid, a.id AS aid, a.resource_id, a.pic_id, " . implode(", ", $a_columns) . "
                FROM
                    project_planning_detail_mfg p
                JOIN
                    actualing_mfg a ON a.planning_id=p.id
                JOIN
                    master_process pc ON pc.id=p.process_id
                JOIN
                    project_bom m ON m.id=p.material_id";
                // JOIN
                //     master_resource r ON r.id=a.resource_id
                // JOIN
                //     master_pic AS pic ON pic.id=a.pic_id";
                $all_data = DB::select(DB::raw($actualing_data_sql));
                $total_all_data = count($all_data);

                $s_where = "";
                if ( isset($s_search) && $s_search != "" )
                {
                    $s_where .= " WHERE (";
                    for ( $i=0 ; $i < 6 ; $i++ )
                    {
                        $s_where .= $a_columns[$i]." LIKE '%".( $s_search )."%' OR ";
                    }
                    $s_where = substr_replace( $s_where, "", -3 );
                    $s_where .= ')';
                }

                $s_order = "";
                if ( isset( $i_sort_col_0 ) )
                {
                    $s_order = " ORDER BY  ";
                    for ( $i=0 ; $i<intval( $i_sorting_cols ) ; $i++ )
                    {
                        if ( ${"b_sortable_".intval(${"i_sort_col_{$i}"})} == "true" )
                        {
                            $s_order .= $a_columns[ intval( ${"i_sort_col_{$i}"} ) ]." ".
                                (${"i_sort_dir_{$i}"}==='asc' ? 'asc' : 'desc') .", ";
                        }
                    }

                    $s_order = rtrim(trim( $s_order ), ',');
                    if ( $s_order == "ORDER BY" )
                    {
                        $s_order = "";
                    }
                }

                $s_filter .= " " . $s_where . " " . $s_order;

                $actualing_data_sql .= $s_filter;
                $filtered_data = DB::select(DB::raw($actualing_data_sql));
                $total_filtered_data = count($filtered_data);

                $actualing_data_sql .= $s_limit;
                $actualing_data = DB::select(DB::raw($actualing_data_sql));

                if(count($actualing_data) > 0)
                {
                    foreach ($actualing_data as $actualing) {
                        $pic = Pic::find($actualing->pic_id);
                        $resource = Resource::find($actualing->resource_id);
                        $total_qty = BomQuantity::where('planning_id','=',$actualing->pid)->count();
                        $total_position = ActualingMFGPosition::where('actualing_id','=',$actualing->aid)->count();
                        $total_complete_position = ActualingMFGPosition::where('actualing_id','=',$actualing->aid)->where('status','=','COMPLETE')->count();
                        $output[] = array(
                            $actualing->barcode,
                            $actualing->material_name,
                            $actualing->code,
                            $actualing->process,
                            isset($resource->name) ? $resource->name : '',
                            isset($pic->first_name) ? $pic->first_name." ".$pic->last_name : '',
                            '<input type="checkbox" name="aid[]" class="aid" value="'.$actualing->aid.'" />'
                            // '<a href="'.route('actualing.do_reset_data', array($actualing->aid, 'material')).'" onclick="return confirm(\'Are you sure to reset this actualing data?\')?true:false;" class="btn btn-primary">RESET DATA</a>'
                        );
                    }
                }
                break;

            default:
                $a_columns = array(
                    'pj.nop',
                    'pj.project_name',
                    'pc.code',
                    'pc.process',
                    // 'r.name',
                    // 'pic.first_name',
                    'p.estimation_hour',
                    'a.start_date'
                );

                $s_index_column = "id";

                $s_limit = $s_filter = "";
                if (isset($i_display_start) && $i_display_length != '-1' )
                {
                    $s_limit .= " LIMIT " . intval( $i_display_start ) . ", " . intval( $i_display_length );
                }

                $actualing_data_sql  = "SELECT
                    p.id AS pid, a.id AS aid, a.resource_id, a.pic_id, " . implode(", ", $a_columns) . "
                FROM
                    project_planning_detail_non_mfg p
                JOIN
                    actualing_non_mfg a ON a.planning_id=p.id
                JOIN
                    master_process pc ON pc.id=p.process_id
                JOIN
                    projects pj ON pj.id=p.project_id";
                // JOIN
                //     master_resource r ON r.id=a.resource_id
                // JOIN
                //     master_pic AS pic ON pic.id=a.pic_id";
                $all_data = DB::select(DB::raw($actualing_data_sql));
                $total_all_data = count($all_data);

                $s_where = "";
                if ( isset($s_search) && $s_search != "" )
                {
                    $s_where .= " WHERE (";
                    for ( $i=0 ; $i < 6 ; $i++ )
                    {
                        $s_where .= $a_columns[$i]." LIKE '%".( $s_search )."%' OR ";
                    }
                    $s_where = substr_replace( $s_where, "", -3 );
                    $s_where .= ')';
                }

                $s_order = "";
                if ( isset( $i_sort_col_0 ) )
                {
                    $s_order = " ORDER BY  ";
                    for ( $i=0 ; $i<intval( $i_sorting_cols ) ; $i++ )
                    {
                        if ( ${"b_sortable_".intval(${"i_sort_col_{$i}"})} == "true" )
                        {
                            $s_order .= $a_columns[ intval( ${"i_sort_col_{$i}"} ) ]." ".
                                (${"i_sort_dir_{$i}"}==='asc' ? 'asc' : 'desc') .", ";
                        }
                    }

                    $s_order = rtrim(trim( $s_order ), ',');
                    if ( $s_order == "ORDER BY" )
                    {
                        $s_order = "";
                    }
                }

                $s_filter .= " " . $s_where . " " . $s_order;

                $actualing_data_sql .= $s_filter;
                $filtered_data = DB::select(DB::raw($actualing_data_sql));
                $total_filtered_data = count($filtered_data);

                $actualing_data_sql .= $s_limit;
                $actualing_data = DB::select(DB::raw($actualing_data_sql));

                if(count($actualing_data) > 0)
                {
                    foreach ($actualing_data as $actualing) {
                        $pic = Pic::find($actualing->pic_id);
                        $resource = Resource::find($actualing->resource_id);
                        $total_qty = MasterplanQuantity::where('planning_id','=',$actualing->pid)->count();
                        $total_position = ActualingNonMFGPosition::where('actualing_id','=',$actualing->aid)->count();
                        $total_complete_position = ActualingNonMFGPosition::where('actualing_id','=',$actualing->aid)->where('status','=','COMPLETE')->count();
                        $output[] = array(
                            $actualing->nop,
                            $actualing->project_name,
                            $actualing->code,
                            $actualing->process,
                            isset($resource->name) ? $resource->name : '',
                            isset($pic->first_name) ? $pic->first_name." ".$pic->last_name : '',
                            '<input type="checkbox" name="aid[]" class="aid" value="'.$actualing->aid.'" />'
                            // '<a href="'.route('actualing.do_reset_data', array($actualing->aid, 'material')).'" onclick="return confirm(\'Are you sure to reset this actualing data?\')?true:false;" class="btn btn-primary">RESET DATA</a>'
                        );
                    }
                }
                break;
        }

        $data = array(
            "sEcho" => intval($s_echo),
            "iTotalRecords" => $total_all_data,
            "iTotalDisplayRecords" => $total_filtered_data,
            "aaData" => $output
        );

        $response = Response::make($data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_actualing_data_editing()
    {
        $type = Input::get('type');
        $s_echo = Input::get('sEcho');
        $i_columns = Input::get('iColumns');
        $s_columns = Input::get('sColumns');
        $i_display_start = Input::get('iDisplayStart');
        $i_display_length = Input::get('iDisplayLength');
        $s_search = Input::get('sSearch');
        $b_regex = Input::get('bRegex');
        $i_sort_col_0 = Input::get('iSortCol_0');
        $i_sort_dir_0 = Input::get('sSortDir_0');
        $i_sorting_cols = Input::get('iSortingCols');
        $b_sortable_0 = Input::get('bSortable_0');
        $b_sortable_1 = Input::get('bSortable_1');
        $b_sortable_2 = Input::get('bSortable_2');
        $b_sortable_3 = Input::get('bSortable_3');
        $b_sortable_4 = Input::get('bSortable_4');
        $b_sortable_5 = Input::get('bSortable_5');
        $b_sortable_6 = Input::get('bSortable_6');
        $b_sortable_7 = Input::get('bSortable_7');
        $b_sortable_8 = Input::get('bSortable_8');
        $b_sortable_9 = Input::get('bSortable_9');
        $b_sortable_10 = Input::get('bSortable_10');
        $b_sortable_11 = Input::get('bSortable_11');
        $output = array();

        switch($type)
        {
            case 'material':
                $a_columns = array(
                    'm.barcode',
                    'm.material_name',
                    'pc.code',
                    'pc.process',
                    'r.name',
                    'pic.first_name',
                    'p.estimation_hour',
                    'a.start_date'
                );

                $s_index_column = "id";

                $s_limit = $s_filter = "";
                if (isset($i_display_start) && $i_display_length != '-1' )
                {
                    $s_limit .= " LIMIT " . intval( $i_display_start ) . ", " . intval( $i_display_length );
                }

                $actualing_data_sql  = "SELECT
                    p.id AS pid, a.id AS aid," . implode(", ", $a_columns) . ", pic.last_name
                FROM
                    project_planning_detail_mfg p
                JOIN
                    actualing_mfg a ON a.planning_id=p.id
                JOIN
                    master_process pc ON pc.id=p.process_id
                JOIN
                    project_bom m ON m.id=p.material_id
                JOIN
                    master_resource r ON r.id=a.resource_id
                JOIN
                    master_pic AS pic ON pic.id=a.pic_id
                WHERE
                    (a.status = 'ON PROCESS' OR a.subfinish_type = 'ONPROCESS')";
                $all_data = DB::select(DB::raw($actualing_data_sql));
                $total_all_data = count($all_data);

                $s_where = "";
                if ( isset($s_search) && $s_search != "" )
                {
                    $s_where .= " AND (";
                    for ( $i=0 ; $i < 6 ; $i++ )
                    {
                        $s_where .= $a_columns[$i]." LIKE '%".( $s_search )."%' OR ";
                    }
                    $s_where = substr_replace( $s_where, "", -3 );
                    $s_where .= ')';
                }

                $s_order = "";
                if ( isset( $i_sort_col_0 ) )
                {
                    $s_order = " ORDER BY  ";
                    for ( $i=0 ; $i<intval( $i_sorting_cols ) ; $i++ )
                    {
                        if ( ${"b_sortable_".intval(${"i_sort_col_{$i}"})} == "true" )
                        {
                            $s_order .= $a_columns[ intval( ${"i_sort_col_{$i}"} ) ]." ".
                                (${"i_sort_dir_{$i}"}==='asc' ? 'asc' : 'desc') .", ";
                        }
                    }

                    $s_order = rtrim(trim( $s_order ), ',');
                    if ( $s_order == "ORDER BY" )
                    {
                        $s_order = "";
                    }
                }

                $s_filter .= " " . $s_where . " " . $s_order;

                $actualing_data_sql .= $s_filter;
                $filtered_data = DB::select(DB::raw($actualing_data_sql));
                $total_filtered_data = count($filtered_data);

                $actualing_data_sql .= $s_limit;
                $actualing_data = DB::select(DB::raw($actualing_data_sql));

                if(count($actualing_data) > 0)
                {
                    foreach ($actualing_data as $actualing) {
                        $total_qty = BomQuantity::where('planning_id','=',$actualing->pid)->count();
                        $total_position = ActualingMFGPosition::where('actualing_id','=',$actualing->aid)->count();
                        $total_complete_position = ActualingMFGPosition::where('actualing_id','=',$actualing->aid)->where('status','=','COMPLETE')->count();
                        $output[] = array(
                            $actualing->barcode,
                            $actualing->material_name,
                            $actualing->code,
                            $actualing->process,
                            $actualing->name,
                            isset($actualing->first_name) ? $actualing->first_name." ".$actualing->last_name : '',
                            $actualing->estimation_hour,
                            $actualing->start_date,
                            "0/".($total_qty > 0 ? $total_qty : 1),
                            $total_complete_position."/".($total_position > 0 ? $total_position : 1),
                            '<a href="'.route('actualing.edit_data.form', array($actualing->aid, 'material')).'" onclick="openForm(this);return false;" class="btn btn-primary">Edit</a>'
                        );
                    }
                }
                break;

            default:
                $a_columns = array(
                    'pj.nop',
                    'pj.project_name',
                    'pc.code',
                    'pc.process',
                    'r.name',
                    'pic.first_name',
                    'p.estimation_hour',
                    'a.start_date'
                );

                $s_index_column = "id";

                $s_limit = $s_filter = "";
                if (isset($i_display_start) && $i_display_length != '-1' )
                {
                    $s_limit .= " LIMIT " . intval( $i_display_start ) . ", " . intval( $i_display_length );
                }

                $actualing_data_sql  = "SELECT
                    p.id AS pid, a.id AS aid," . implode(", ", $a_columns) . ", pic.last_name
                FROM
                    project_planning_detail_non_mfg p
                JOIN
                    actualing_non_mfg a ON a.planning_id=p.id
                JOIN
                    master_process pc ON pc.id=p.process_id
                JOIN
                    projects pj ON pj.id=p.project_id
                JOIN
                    master_resource r ON r.id=a.resource_id
                JOIN
                    master_pic AS pic ON pic.id=a.pic_id
                WHERE
                    (a.status = 'ON PROCESS' OR a.subfinish_type = 'ONPROCESS')";
                $all_data = DB::select(DB::raw($actualing_data_sql));
                $total_all_data = count($all_data);

                $s_where = "";
                if ( isset($s_search) && $s_search != "" )
                {
                    $s_where .= " AND (";
                    for ( $i=0 ; $i < 6 ; $i++ )
                    {
                        $s_where .= $a_columns[$i]." LIKE '%".( $s_search )."%' OR ";
                    }
                    $s_where = substr_replace( $s_where, "", -3 );
                    $s_where .= ')';
                }

                $s_order = "";
                if ( isset( $i_sort_col_0 ) )
                {
                    $s_order = " ORDER BY  ";
                    for ( $i=0 ; $i<intval( $i_sorting_cols ) ; $i++ )
                    {
                        if ( ${"b_sortable_".intval(${"i_sort_col_{$i}"})} == "true" )
                        {
                            $s_order .= $a_columns[ intval( ${"i_sort_col_{$i}"} ) ]." ".
                                (${"i_sort_dir_{$i}"}==='asc' ? 'asc' : 'desc') .", ";
                        }
                    }

                    $s_order = rtrim(trim( $s_order ), ',');
                    if ( $s_order == "ORDER BY" )
                    {
                        $s_order = "";
                    }
                }

                $s_filter .= " " . $s_where . " " . $s_order;

                $actualing_data_sql .= $s_filter;
                $filtered_data = DB::select(DB::raw($actualing_data_sql));
                $total_filtered_data = count($filtered_data);

                $actualing_data_sql .= $s_limit;
                $actualing_data = DB::select(DB::raw($actualing_data_sql));

                if(count($actualing_data) > 0)
                {
                    foreach ($actualing_data as $actualing) {
                        $total_qty = MasterplanQuantity::where('planning_id','=',$actualing->pid)->count();
                        $total_position = ActualingNonMFGPosition::where('actualing_id','=',$actualing->aid)->count();
                        $total_complete_position = ActualingNonMFGPosition::where('actualing_id','=',$actualing->aid)->where('status','=','COMPLETE')->count();
                        $output[] = array(
                            $actualing->nop,
                            $actualing->project_name,
                            $actualing->code,
                            $actualing->process,
                            $actualing->name,
                            isset($actualing->first_name) ? $actualing->first_name." ".$actualing->last_name : '',
                            $actualing->estimation_hour,
                            $actualing->start_date,
                            "0/".($total_qty > 0 ? $total_qty : 1),
                            $total_complete_position."/".($total_position > 0 ? $total_position : 1),
                            '<a href="'.route('actualing.edit_data.form', array($actualing->aid, 'nop')).'" onclick="openForm(this);return false;" class="btn btn-primary">Edit</a>'
                        );
                    }
                }
                break;
        }

        $data = array(
            "sEcho" => intval($s_echo),
            "iTotalRecords" => $total_all_data,
            "iTotalDisplayRecords" => $total_filtered_data,
            "aaData" => $output
        );

        $response = Response::make($data, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }
}
