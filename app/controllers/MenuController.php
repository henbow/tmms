<?php
use Menu;
// // use Auth, BaseController, Form, Input, Redirect, Sentry, Str, View, Notification;

class MenuController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'menu_management');
            $view->with('child_active', 'menu');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.menu.list');
        $view->with('page_title', 'Menu Management');
        $view->with('sub_title', 'Manage application menus');
        $view->with('menu_lists', Menu::orderBy('order')->get());
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.menu.create');
        $view->with('page_title', 'Menu Management');
        $view->with('sub_title', 'Create new menu');
        $view->with('theme', Config::get('app.theme'));
        $view->with('parents', Menu::where('menu_name', '<>', '[LINE]')->orderBy('order')->get());

        return $view;
    }

    public function store()
    {
        $menu = new Menu;
        $menu->order = Input::get('order');
        $menu->parent_id = Input::get('parent_id');
        $menu->menu_name = Input::get('menu');
        $menu->alias = str_replace('-', '_', Str::slug(Input::get('menu')));
        $menu->route = Input::get('route');
        $menu->param = Input::get('param');
        $menu->class_attr = Input::get('class');

        if($menu->save())
        {
            $all_menus = Menu::orderBy('order')->orderBy('id');
            if($all_menus->count() > 0)
            {
                $order = 0;
                foreach($all_menus->get(array('id')) as $m)
                {
                    $the_menu = Menu::find($m->id);
                    $the_menu->order = ++$order;
                    $the_menu->save();
                }
            }

            Notification::success('Successfully add new menu.');
            return Redirect::route('menu.index');
        }
    }

    public function edit($id)
    {
        $view = View::make(Config::get('app.theme').'.menu.edit');
        $view->with('page_title', 'Menu Management');
        $view->with('sub_title', 'Edit menu');
        $view->with('theme', Config::get('app.theme'));
        $view->with('menu', Menu::find($id));
        $view->with('parents', Menu::where('menu_name', '<>', '[LINE]')->orderBy('order')->get());

        return $view;
    }

    public function update($id)
    {
        $menu = Menu::find($id);
        $menu->order = Input::get('order');
        $menu->parent_id = Input::get('parent_id');
        $menu->menu_name = Input::get('menu');
        $menu->alias = str_replace('-', '_', Str::slug(Input::get('menu')));
        $menu->route = Input::get('route');
        $menu->param = Input::get('param');
        $menu->class_attr = Input::get('class');

        if($menu->save())
        {
            $all_menus = Menu::orderBy('order')->orderBy('id');
            if($all_menus->count() > 0)
            {
                $order = 0;
                foreach($all_menus->get(array('id')) as $m)
                {
                    $the_menu = Menu::find($m->id);
                    $the_menu->order = ++$order;
                    $the_menu->save();
                }
            }

            Notification::success('Menu data changes was saved.');
            return Redirect::route('menu.index');
        }
    }

    public function destroy($id)
    {
        $menu = Menu::find($id);
        $menu->delete();

        return Redirect::route('menu.index');
    }
}
