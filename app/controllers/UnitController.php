<?php
use Unit;
// // use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification;

class UnitController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'user_management');
            $view->with('child_active', 'unit');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.unit.list');
        $view->with('page_title', 'Unit Management');
        $view->with('sub_title', 'Manage company unit');
        $view->with('theme', Config::get('app.theme'));
        $view->with('units', Unit::getAll());

        return $view;
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.unit.create');
        $view->with('page_title', 'Unit Management');
        $view->with('sub_title', 'Create new unit');
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function store()
    {
        $unit = new Unit;
        $unit->code = Input::get('code');
        $unit->name = Input::get('unitname');
        $unit->desc = Input::get('desc');

        if($unit->save())
            Notification::success('New unit data was saved.');

        return Redirect::route('unit.index');
    }

    public function edit($id)
    {
        $view = View::make(Config::get('app.theme').'.unit.edit');
        $view->with('page_title', 'Unit Management');
        $view->with('sub_title', 'Edit unit data');
        $view->with('theme', Config::get('app.theme'));
        $view->with('unit', Unit::find($id));

        return $view;
    }

    public function update($id)
    {
        $unit = Unit::find($id);
        $unit->code = Input::get('code');
        $unit->name = Input::get('unitname');
        $unit->desc = Input::get('desc');

        if($unit->save())
            Notification::success('Unit data changes was saved.');

        return Redirect::route('unit.index');
    }

    public function destroy($id)
    {
        $unit = Unit::find($id);
        $unit->delete();

        return Redirect::route('unit.index');
    }
}
