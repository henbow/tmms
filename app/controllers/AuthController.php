<?php
// // // use Auth as Auth, BaseController as BaseController, Form as Form, Input as Input, Redirect as Redirect, Sentry as Sentry, View as View, Notification as Notification;

class AuthController extends BaseController
{

    /**
     * Display the login page
     *
     * @return View
     */
    public function getLogin()
    {
        return View::make(Config::get('app.theme').'.auth.login');
    }

    /**
     * Login action
     *
     * @return Redirect
     */
    public function postLogin()
    {
        $credentials = array(
            'username' => Input::get('login'),
            'password' => Input::get('password')
        );
        
        try {
            $user = Sentry::authenticate($credentials, true);
            if ($user) {
                Activity::log([
                    'contentId'   => $user->id,
                    'contentType' => 'User',
                    'action'      => 'Login',
                    'description' => 'Logging in a User',
                    'details'     => 'Username: '.$user->username,
                    'updated'     => isset($user->id) ? true : false,
                ]);
                return Redirect::route('dashboard.index');
            }
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            Notification::error("User \"{$credentials['username']}\" is not found!");

            return Redirect::back()->withInput();
        }
    }

    /**
     * Logout action
     *
     * @return Redirect
     */
    public function getLogout()
    {
        Activity::log([
            'contentId'   => Sentry::getUser()->id,
            'contentType' => 'User',
            'action'      => 'Logout',
            'description' => 'Logging out a User',
            'details'     => 'Username: '.Sentry::getUser()->username,
            'updated'     => isset(Sentry::getUser()->id) ? true : false,
        ]);
        Sentry::logout();
        
        return Redirect::route('auth.login');
    }
}