<?php
use Menu;
use Groups;
use App\Services\Validators\UserLevelValidator;
// // use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification;

/**
 *
 * @todo assign user level to menu permissions
 */
class UserLevelController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'user_management');
            $view->with('child_active', 'user_level');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.userlevel.list');
        $view->with('page_title', 'User Level Management');
        $view->with('sub_title', 'Manage user levels');
        $view->with('theme', Config::get('app.theme'));
        $view->with('level_lists', Sentry::findAllGroups());

        return $view;
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.userlevel.create');
        $view->with('page_title', 'User Level Management');
        $view->with('sub_title', 'Create new user level');
        $view->with('theme', Config::get('app.theme'));
        $view->with('permissions', Menu::orderBy('order')->get());

        return $view;
    }

    public function store()
    {
        $validation = new UserLevelValidator();

        if ($validation->passes())
        {
            $level_data = array();
            $level_data['name'] = Input::get('levelname');
            $permissions = Input::get('permissions');
            if (count($permissions) > 0)
            {
                foreach ($permissions as $permission)
                {
                    $level_data['permissions'][$permission] = 1;
                }
            }

            $level = Sentry::createGroup($level_data);

            Notification::success('The new user level was saved.');

            return Redirect::route('userlevel.index');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    public function edit($id)
    {
        $level = Sentry::findGroupById($id);
        $view = View::make(Config::get('app.theme').'.userlevel.edit');
        $view->with('page_title', 'Level Management');
        $view->with('sub_title', 'Edit level ' . $level->name);
        $view->with('theme', Config::get('app.theme'));
        $view->with('level', $level);
        $view->with('permissions', Menu::orderBy('order')->get());

        return $view;
    }

    public function update($id)
    {
        $validation = new UserLevelValidator();

        if ($validation->passes()) {
            $level = Groups::find($id);

            $permissions = Input::get('permissions');
            $level_permissions = array();

            if (count($permissions) > 0)
            {
                foreach ($permissions as $permission)
                {
                    $level_permissions[$permission] = 1;
                }
            }

            $level->name = Input::get('levelname');
            $level->permissions = json_encode($level_permissions);
            $level->save();

            Notification::success('The user level changes was saved.');

            return Redirect::route('userlevel.index');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    public function destroy($id)
    {
        $level = Sentry::findGroupById($id);
        $level->delete();

        return Redirect::route('userlevel.index');
    }
}
