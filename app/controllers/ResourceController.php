<?php
use Resource;
use ResourceGroup;
use ResourceSpec;
use ResourceWorkTime;
use ResourcePIC;
use Pic;
// use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification;

class ResourceController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'resource_center');
            $view->with('child_active', 'resource_management');
        });
    }

    public function index()
    {
        $view = View::make(Config::get('app.theme').'.resource.list');
        $view->with('page_title', 'Resource Management');
        $view->with('sub_title', 'Manage All Resources / Work Center');
        $view->with('theme', Config::get('app.theme'));
        $view->with('in_plan_resources', Resource::where('plan_type', '=', 'in plan')->get());
        $view->with('out_plan_resources', Resource::where('plan_type', '=', 'out plan')->get());
        $view->with('breadcrumb', array(
            'Dashboard' => array(route('dashboard.index')),
            'Resource Management' => array(route('resource.index'), 'last')
        ));

        return $view;
    }

    public function show($id)
    {
        $view = View::make(Config::get('app.theme').'.resource.show');
        $view->with('page_title', 'Resource Management');
        $view->with('sub_title', 'Show resource detail data');
        $view->with('theme', Config::get('app.theme'));
        $view->with('resource', Resource::find($id));
        $view->with('specification', ResourceSpec::getByResource($id));
        $view->with('work_time', ResourceWorkTime::getByResource($id));

        return $view;
    }

    public function create()
    {
        $view = View::make(Config::get('app.theme').'.resource.create');
        $view->with('page_title', 'Resource Management');
        $view->with('sub_title', 'Add new resource');
        $view->with('theme', Config::get('app.theme'));
        $view->with('pics', Pic::getPICs());
        $view->with('groups', ResourceGroup::all());

        return $view;
    }

    public function store()
    {
        $resource = new Resource();
        $resource->code = Input::get('code');
        $resource->name = Input::get('resname');
        $resource->group_id = Input::get('groupid');
        $resource->plan_type = Input::get('plan_type');
        $resource->is_human = Input::get('human') ? '1' : '0';
        $resource->is_offline = Input::get('offline') ? '1' : '0';

        if (Input::get('offline'))
            $resource->offline_reason = Input::get('offlinereason');

        if ($resource->save())
        {
            $pic_ids = Input::get('picid');
            if(count($pic_ids) > 0)
            {
                foreach ($pic_ids as $pic_id)
                {
                    $existing_pic_resource = ResourcePIC::where('resource_id','=',$resource->id)->where('pic_id','=',$pic_id)->count() > 0 ? true : false;
                    if(!$existing_pic_resource)
                    {
                        $res_pic = new ResourcePIC;
                        $res_pic->resource_id = $resource->id;
                        $res_pic->pic_id = $pic_id;
                        $res_pic->save();
                    }
                }
            }

            Notification::success('New resource data was saved.');
        }

        return Redirect::route('resource.index');
    }

    public function edit($id)
    {
        $selected_pic_data = ResourcePIC::where('resource_id','=',$id)->get();
        $selected_pics = array();
        if(count($selected_pic_data) > 0)
        {
            foreach ($selected_pic_data as $res_pic) {
                $selected_pics[] = $res_pic->pic_id;
            }
        }

        $view = View::make(Config::get('app.theme').'.resource.edit');
        $view->with('page_title', 'Resource Management');
        $view->with('sub_title', 'Edit resource data');
        $view->with('theme', Config::get('app.theme'));
        $view->with('resource', Resource::find($id));
        $view->with('pics', Pic::getPICs());
        $view->with('selected_pics', $selected_pics);
        $view->with('groups', ResourceGroup::all());

        return $view;
    }

    public function update($id)
    {
        $resource = Resource::find($id);
        $resource->code = Input::get('code');
        $resource->name = Input::get('resname');
        $resource->group_id = Input::get('groupid');
        $resource->plan_type = Input::get('plan_type');
        $resource->is_human = Input::get('human') ? '1' : '0';
        $resource->is_offline = Input::get('offline') ? '1' : '0';

        if (Input::get('offline'))
            $resource->offline_reason = Input::get('offlinereason');

        if ($resource->save())
        {
            $pic_ids = Input::get('picid');
            if(count($pic_ids) > 0)
            {
                DB::table('resource_pic')->where('resource_id', '=', $resource->id)->delete();
                foreach ($pic_ids as $pic_id)
                {
                    $res_pic = new ResourcePIC;
                    $res_pic->resource_id = $resource->id;
                    $res_pic->pic_id = $pic_id;
                    $res_pic->save();
                }
            }

            Notification::success('Resource data changes was saved.');
        }

        return Redirect::route('resource.index');
    }

    public function destroy($id)
    {
        $resource = Resource::find($id);
        $resource->delete();

        Notification::success('Resource has been deleted.');

        return Redirect::route('resource.index');
    }

    public function export()
    {

    }
}
