<?php
use ResourceWorkTime;
use ResourceWorkTimeTemplate;
use ResourcePlanner;
use Resource;
// use Auth, BaseController, Form, Input, Redirect, Sentry, View, DB, Notification;

class ResourceWorkTimeController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'resource_center');
            $view->with('child_active', 'resource_management');
        });
    }

    public function template($resource_id)
    {
        $view = View::make(Config::get('app.theme') . '.resource.template');
        $view->with('templates', ResourceWorkTimeTemplate::all());
        $view->with('resource_id', $resource_id);
        return $view;
    }

    public function from_template($resource_id)
    {
        $template_id = Input::get('template-id');
        $work_time_template = ResourceWorkTimeTemplate::find($template_id);

        DB::table('master_resource_work_time')->where('resource_id', '=', $resource_id)->delete();
        $resource_work_time = new ResourceWorkTime;
        $resource_work_time->resource_id = $resource_id;
        $resource_work_time->mon = $work_time_template->mon;
        $resource_work_time->tue = $work_time_template->tue;
        $resource_work_time->wed = $work_time_template->wed;
        $resource_work_time->thu = $work_time_template->thu;
        $resource_work_time->fri = $work_time_template->fri;
        $resource_work_time->sat = $work_time_template->sat;
        $resource_work_time->sun = $work_time_template->sun;
        $resource_work_time->per_week = intval($work_time_template->mon) +
                                        intval($work_time_template->tue) +
                                        intval($work_time_template->wed) +
                                        intval($work_time_template->thu) +
                                        intval($work_time_template->fri) +
                                        intval($work_time_template->sat) +
                                        intval($work_time_template->sun);

        if ($resource_work_time->save())
        {
            //DB::table('resource_work_plan')->where('resource_id', '=', $resource_id)->delete();
            //update_work_plan($resource_id);

            Notification::success('Resource work time for ' . Resource::getName($resource_id) . ' was saved.');
        }

        return Redirect::route('resource.show', $resource_id);
    }

    public function create($resource_id)
    {
        $view = View::make(Config::get('app.theme').'.resource_work_time.create');
        $view->with('page_title', 'Resource Work Time Management');
        $view->with('sub_title', 'Specify resource work time for ' . strtoupper(Resource::getName($resource_id)));
        $view->with('theme', Config::get('app.theme'));
        $view->with('resource_id', $resource_id);

        return $view;
    }

    public function store($resource_id)
    {
        $resource_work_time = new ResourceWorkTime;
        $resource_work_time->resource_id = $resource_id;
        $resource_work_time->mon = Input::get('mon');
        $resource_work_time->tue = Input::get('tue');
        $resource_work_time->wed = Input::get('wed');
        $resource_work_time->thu = Input::get('thu');
        $resource_work_time->fri = Input::get('fri');
        $resource_work_time->sat = Input::get('sat');
        $resource_work_time->sun = Input::get('sun');
        $resource_work_time->per_week = Input::get('per_week');

        if ($resource_work_time->save())
        {
            // update_work_plan($resource_id);

            Notification::success('Resource work time for ' . Resource::getName($resource_id) . ' was saved.');
        }

        return Redirect::route('resource.show', $resource_id);
    }

    public function edit($resource_id, $id)
    {
        $view = View::make(Config::get('app.theme').'.resource_work_time.edit');
        $view->with('page_title', 'Resource Work Time Management');
        $view->with('sub_title', 'Edit resource work time for ' . strtoupper(Resource::getName($resource_id)));
        $view->with('theme', Config::get('app.theme'));
        $view->with('resource_work_time', ResourceWorkTime::find($id));
        $view->with('resource_id', $resource_id);

        return $view;
    }

    public function update($resource_id, $id)
    {
        $resource_work_time = ResourceWorkTime::find($id);
        $resource_work_time->resource_id = $resource_id;
        $resource_work_time->mon = Input::get('mon');
        $resource_work_time->tue = Input::get('tue');
        $resource_work_time->wed = Input::get('wed');
        $resource_work_time->thu = Input::get('thu');
        $resource_work_time->fri = Input::get('fri');
        $resource_work_time->sat = Input::get('sat');
        $resource_work_time->sun = Input::get('sun');
        $resource_work_time->per_week = Input::get('per_week');

        if ($resource_work_time->save())
        {
            // DB::table('resource_work_plan')->where('resource_id', '=', $resource_id)->delete();
            // update_work_plan($resource_id);

            Notification::success('Resource work time changes for ' . Resource::getName($resource_id) . ' was saved.');
        }

        return Redirect::route('resource.show', $resource_id);
    }

    public function destroy($resource_id, $id)
    {
        $resource = ResourceWorkTime::find($id);
        $resource->delete();

        Notification::success('Resource work time for ' . Resource::getName($resource_id) . ' was deleted.');

        return Redirect::route('resource_work_time.index', $resource_id);
    }
}