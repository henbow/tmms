<?php

use Process;
use ProcessGroup;
use ProcessResourceGroup;
use PlanningMFG;
use PlanningNonMFG;
use ProjectMasterPlan;
use SchedulingMFG;
use SchedulingNonMFG;
use SchedulingPBBOM;
use Resource;
use ResourceGroup;
use ResourceActualUsed;
use ResourcePlanner;
use GeneralOptions;

class SchedulingController extends BaseController {

    private $total_delivery_unix = 0;
    private $total_priority = 0;
    private $resource_group_id = 0;

    public function __construct() {
        View::composer(Config::get('app.theme') . '._layouts.master', function ($view) {
            $view->with('parent_active', 'planning');
            $view->with('child_active', 'scheduling');
        });
    }

    public function index() {
        $view = View::make(Config::get('app.theme') . '.scheduling.index');

        $plan = 'IN PLAN';
        $process_group_id = 1;
        $resource_group = 1;

        if (Input::get('_token')) {
            $plan = Input::get('plan');
            $process_group_id = Input::get('step');
            $resource_group = Input::get('resource_group');
        }

        $process_id = Input::get('process');
        $start_date_filter = Input::get('start');

        $view->with('plan', $plan);
        $view->with('step', $process_group_id);
        $view->with('selected_resource_group', $resource_group);
        $view->with('selected_resources', Resource::where('group_id','=',$resource_group)->get());

        if($process_id) {
            $view->with('process_id', $process_id);
        }

        if (empty($start_date_filter)) {
            $start_date = date('Y-m-d H:i');
        } else {
            list($date_part, $time_part) = explode(' ', $start_date_filter);
            list($month, $date, $year) = explode('/', $date_part);
            $start_date = "{$year}-{$month}-{$date} {$time_part}";
        }

        $process_group_id = $process_group_id ? $process_group_id : '1';
        $process_group_id_data = ProcessGroup::find($process_group_id);

        if (intval($process_group_id_data->parent_id) > 0) {
            $process_group_id = $process_group_id_data->parent_id;
        }

        $key = 1000000;
        switch ($process_group_id_data->type) {
            case 'material':
                $planning_data = DB::table('project_planning_detail_mfg AS p')
                ->join('project_bom AS m', 'm.id', '=', 'p.material_id')
                ->join('project_master_plan AS mp', 'mp.id', '=', 'p.master_planning_id')
                ->join('projects AS pr', 'pr.id', '=', 'p.project_id')
                ->join('master_process AS mpr', 'mpr.id', '=', 'p.process_id')
                ->where(function($query) {
                    $resource_group = Input::get('resource_group');
                    $process_group_id = Input::get('step');
                    $process_id = Input::get('process');
                    $resource_group_ids = array();

                    if ($resource_group) {
                        $query->where('p.resource_group_id', '=', $resource_group);
                    }

                    if ($process_id) {
                        $query->where('p.process_id', '=', $process_id);
                    }

                    if ($process_group_id) {
                        $query_group = ProcessGroup::where('parent_id', '=', $process_group_id);
                        $has_child = $query_group->count() > 0 ? true : false;
                        if ($has_child) {
                            $params = array();
                            foreach ($query_group->get() as $pg) {
                                $params[] = $pg->id;
                            }

                            $query->whereIn('p.process_group_id', $params);
                        } else {
                            $query->where('p.process_group_id', '=', $process_group_id);
                        }
                    }

                    $query->where('p.is_actualing', '<>', '1');
                })
                ->orderBy('p.priority')
                ->select(
                    'p.id', 'p.project_id', 'pr.project_name AS project', 'pr.nop',
                    'm.barcode', 'p.master_planning_id', 'p.material_id', 'm.material_name', 'mpr.process',
                    'p.process_group_id', 'p.process_id', 'p.resource_group_id', 'p.estimation_hour',
                    'mp.start_date', 'mp.finish_date', 'p.priority', 'm.length', 'm.width', 'm.height','m.qty'
                )->get();

                $schedules = array();
                if(count($planning_data))
                {
                    foreach($planning_data as $planning)
                    {
                        $resource_group = ResourceGroup::find($planning->resource_group_id);
                        if(isset($resource_group->name))
                        {
                            $key++;

                            $data = array();
                            $data['planning_id'] = $planning->id;
                            $data['master_planning_id'] = $planning->master_planning_id;
                            $data['barcode'] = $planning->barcode;
                            $data['material'] = $planning->material_name;
                            $data['qty'] = $planning->qty;
                            $data['process'] = $planning->process;
                            $data['estimation'] = $planning->estimation_hour;
                            $data['resource_group'] = $resource_group->name;
                            $data['schedule_id'] = '';
                            $data['resource'] = '';
                            $data['priority'] = '';
                            $data['start_date'] = '';
                            $data['finish_date'] = '';

                            $scheduling = SchedulingMFG::where('planning_id', '=', $planning->id)->get();
                            if(count($scheduling) > 0)
                            {
                                $scheduling = $scheduling[0];
                                $resource = Resource::find($scheduling->resource_id);

                                $thekey = $scheduling->priority;
                                $data['schedule_id'] = $scheduling->id;
                                $data['resource'] = $resource ? $resource->name : '';
                                $data['priority'] = $scheduling->priority;
                                $data['start_date'] = $scheduling->start_date;
                                $data['finish_date'] = $scheduling->finish_date;
                            }
                            else
                            {
                                $thekey = $key;
                            }

                            $schedules[$thekey] = (Object) $data;
                            $data = null;
                        }
                    }
                }

                ksort($schedules);

                $view->with('schedules', $schedules);
                $view->with('template_step', 'mfg');
                break;

            default:
                $planning_data = DB::table('project_planning_detail_non_mfg AS p')
                ->join('master_process AS m', 'm.id', '=', 'p.process_id')
                ->join('project_master_plan AS mp', 'mp.id', '=', 'p.master_planning_id')
                ->join('projects AS pr', 'pr.id', '=', 'p.project_id')
                ->join('master_process AS mpr', 'mpr.id', '=', 'p.process_id')
                ->where(function($query) {
                    $process_group_id = Input::get('step');
                    $resource_group = Input::get('resource_group');
                    $process_id = Input::get('process');
                    $resource_group_ids = array();

                    if ($resource_group) {
                        $query->where('p.resource_group_id', '=', $resource_group);
                    }

                    if ($process_id) {
                        $query->where('p.process_id', '=', $process_id);
                    }

                    if ($process_group_id)
                    {
                        $query->where('p.process_group_id', '=', $process_group_id);
                    }

                    $query->where('p.is_actualing', '<>', '1');
                })
                ->orderBy('priority')
                ->select(
                    'p.id', 'p.project_id', 'pr.project_name AS project', 'pr.nop',
                    'p.master_planning_id', 'mpr.process',
                    'p.process_id', 'p.resource_group_id', 'p.estimation_hour',
                    'mp.start_date', 'mp.finish_date', 'p.priority'
                )->get();

                $schedules = array();
                if(count($planning_data) > 0)
                {

                    foreach($planning_data as $planning)
                    {
                        $resource_group = ResourceGroup::find($planning->resource_group_id);
                        if(isset($resource_group->name))
                        {
                            $key++;

                            $data = array();
                            $data['planning_id'] = $planning->id;
                            $data['master_planning_id'] = $planning->master_planning_id;
                            $data['nop'] = $planning->nop;
                            $data['project'] = $planning->project;
                            $data['process'] = $planning->process;
                            $data['estimation'] = $planning->estimation_hour;
                            $data['resource_group'] = @$resource_group->name;
                            $data['schedule_id'] = '';
                            $data['resource'] = '';
                            $data['priority'] = '';
                            $data['start_date'] = '';
                            $data['finish_date'] = '';

                            $scheduling = SchedulingNonMFG::where('planning_id', '=', $planning->id)->get();
                            if(count($scheduling) > 0)
                            {
                                $scheduling = $scheduling[0];
                                $resource = Resource::find($scheduling->resource_id);
                                // debug($scheduling->resource_id,false);

                                $thekey = $scheduling->priority;
                                $data['schedule_id'] = $scheduling->id;
                                $data['resource'] = $resource ? $resource->name : '';
                                $data['priority'] = $scheduling->priority;
                                $data['start_date'] = $scheduling->start_date;
                                $data['finish_date'] = $scheduling->finish_date;
                            }
                            else
                            {
                                $thekey = $key;
                            }

                            $schedules[$thekey] = (Object) $data;
                            $data = null;
                        }
                    }
                }

                ksort($schedules);


                $view->with('schedules', $schedules);
                $view->with('template_step', 'non-mfg');
                break;
        }

        $processes = Process::where(function($query) {
                        $group_id = Input::get('step');
                        $query_process = ProcessGroup::where('parent_id', '=', $group_id);

                        if ($query_process->count() > 0) {
                            $params = array();
                            foreach ($query_process->get() as $pg) {
                                $params[] = $pg->id;
                            }
                            $query->whereIn('group_id', $params);
                        } else {
                            $query->where('group_id', '=', $group_id ? $group_id : '1');
                        }
                    })->get();

        $resource_groups = ResourceGroup::orderBy('name')->get();
        $view->with('process_groups', ProcessGroup::where('parent_id', '=', '0')->where('id' , '<>' , '2')->get());
        $view->with('process_group_id', $process_group_id);
        $view->with('start_date', $start_date);
        $view->with('page_title', 'Project Scheduling');
        $view->with('theme', Config::get('app.theme'));
        $view->with('active_resource_groups', $resource_groups);
        $view->with('processes', $processes);
        $view->with('is_scheduling', true);

        return $view;
    }

    public function process()
    {
        $view_data = (Object) $this->parallel_process();
        $view = View::make(Config::get('app.theme') . '.scheduling.redirect');
        $view->with('data', $view_data);
        return $view;
    }

    public function parallel_process()
    {
        // debug($_POST);

        $plan_ids = Input::get('plan_id');
        $process_group_id = Input::get('step');
        $resource_group_id = Input::get('resource_group_id');
        $use_priority_order = Input::get('use_priority_order');

        if (count($plan_ids) == 0) {
            Notification::error('No plans selected for scheduling process.');
            return Redirect::route('scheduling.index');
        }

        self::_clear_previous_schedule($process_group_id, $resource_group_id);
        self::_save_parallel_scheduling($process_group_id, $resource_group_id, $plan_ids, $use_priority_order ? true : false);

        return array(
            'plan' => 'IN PLAN',
            'step' => $process_group_id,
            'resource_group' => $resource_group_id,
            'start' => date('m/d/Y H:i')
        );
    }

    private static function _save_parallel_scheduling($process_group_id, $resource_group_id, $plan_ids, $use_priority_order = false) {
        list($date, $time) = explode(' ', Input::get('start_date'));
        list($Y, $m, $d) = explode('-', $date);
        $start_date = "$Y-$m-$d $time";
        $process_group_data = ProcessGroup::find($process_group_id);
        $next_process_delay = 0;

        if(count($plan_ids) > 0) {
            // Find priority number
            $raw_datas = $final_data = $selected_resource = array();
            foreach($plan_ids as $plan_id) {
                $planning = $process_group_data->type == 'material' ? PlanningMFG::find($plan_id) : PlanningNonMFG::find($plan_id);
                $raw_datas[$planning->id] = $planning->estimation_hour;
            }

            if(!$use_priority_order)
            {
                arsort($raw_datas);
            }

            // debug($resource_group_id);

            $priority = 0;
            foreach ($raw_datas as $planning_id => $estimation) {
                $resource_data = Resource::where('group_id','=',$resource_group_id);
                $total_resource = $resource_data->count();

                if(count($selected_resource) > 0) $resource_data->whereNotIn('id', $selected_resource);

                // $schedule_data = $process_group_data->type == 'material' ? SchedulingMFG::where('planning_id','=',$plan_id)->get() : SchedulingNonMFG::where('planning_id','=',$plan_id)->get();
                $resource_id = @$resource_data->get()[0]->id;
                $selected_resource[] = $resource_id;
                // $priority_number = count($schedule_data) > 0 ? $schedule_data[0]->priority : ++$priority;
                $priority_number = ++$priority;
                $final_data[] = (Object) array(
                    'resource_id' => $resource_id,
                    'planning_id' => $planning_id,
                    'estimation'  => $estimation,
                    'priority'    => $priority_number
                );

                if(count($selected_resource) >= $total_resource) {
                    $selected_resource = array();
                }
            }

            // debug($final_data);

            $total_items = count($final_data);
            $total_pages = ceil($total_items / $total_resource);
            $prev_start_date = $prev_finish_date = $earliest_finish_date = null;
            $tmp_finish_date = $predecessors = array();
            // debug($total_items,false);
            // debug($total_resource,false);
            // debug($start_date);

            for($i = 0; $i < $total_pages; $i++) {
                for($j = $total_resource * $i; $j < ($total_resource * $i) + $total_resource; $j++ ) {
                    // var_dump(isset($final_data[$j]));debug($j,false);continue;
                    if(isset($final_data[$j]))
                    {
                        if($i == 0) {
                            // $planning = $process_group_data->type == 'material' ? PlanningMFG::find($final_data[$j]->planning_id) : PlanningNonMFG::find($final_data[$j]->planning_id);
                            // $master_planning = ProjectMasterPlan::find($planning->master_planning_id);
                            $start_date = $start_date;
                            $start_date_unix = strtotime($start_date);
                        } else {
                            $start_date_unix = $tmp_finish_date[$j - $total_resource];
                            $start_date = date('Y-m-d H:i:s', $start_date_unix);
                        }

                        $finish_date_unix = $start_date_unix + ($final_data[$j]->estimation * 3600);
                        $finish_date = date('Y-m-d H:i:s', $finish_date_unix);

                        $prev_index = $j - $total_resource;

                        $scheduling = $process_group_data->type == 'material' ? new SchedulingMFG : new SchedulingNonMFG;
                        $scheduling->planning_id = $final_data[$j]->planning_id;
                        $scheduling->resource_id = $final_data[$j]->resource_id;
                        $scheduling->resource_group_id = $resource_group_id;
                        $scheduling->estimation_hour = $final_data[$j]->estimation;
                        $scheduling->estimation_day = round($final_data[$j]->estimation / 24, 2);
                        $scheduling->priority = intval($final_data[$j]->priority);
                        $scheduling->start_date = $start_date;
                        $scheduling->finish_date = $finish_date;
                        $scheduling->predecessor = $prev_index < 0 ? 0 : $predecessors[$prev_index];
                        $scheduling->save();

                        // debug($scheduling->toArray(), false);

                        $predecessors[$j] = $scheduling->id;
                        $tmp_finish_date[$j] = $finish_date_unix;
                    }
                }
                // debug('====================================================',false);
            }
            $tmp_finish_date = null;
        }
        // die;
    }

    private static function _clear_previous_schedule($process_group_id, $resource_group_id) {
        $process_group_data = ProcessGroup::find($process_group_id);
        switch ($process_group_data->type) {
            case 'material':
                $table = "scheduling_mfg";
                break;

            default:
                $table = "scheduling_non_mfg";
                break;
        }

        DB::table($table)->where('resource_group_id', '=', '0')->delete();
        DB::table($table)->where('resource_group_id', '=', $resource_group_id)->delete();
    }

    public function open_gantt($resource_group_id, $process_group_id) {
        $view = View::make(Config::get('app.theme') . '.scheduling.result.gantt');
        $view->with('theme', Config::get('app.theme'));
        $view->with('resource_group_id', $resource_group_id);
        $view->with('step', $process_group_id);

        return $view;
    }

    public function results_json($resource_group_id, $process_group_id) {
        $schedules = array();
        $group_data = ProcessGroup::find($process_group_id);
        switch ($group_data->type) {
            case 'material':
                $schedule_data = DB::table('scheduling_mfg AS p')
                        ->join('project_planning_detail_mfg AS mp', 'mp.id', '=', 'p.planning_id')
                        ->join('master_process AS m', 'm.id', '=', 'mp.process_id')
                        ->join('projects AS pr', 'pr.id', '=', 'mp.project_id')
                        ->join('project_bom AS pb', 'pb.id', '=', 'mp.material_id')
                        ->where('p.resource_group_id', '=', $resource_group_id)
                        ->orderBy('p.priority');
                // ->orderBy('p.finish_date');
                $schedule_result = $schedule_data->get(array('p.*', 'pr.nop', 'pr.project_name AS project', 'm.process', 'pb.material_name'));
                break;

            default:
                $schedule_data = DB::table('scheduling_non_mfg AS p')
                        ->join('project_planning_detail_non_mfg AS mp', 'mp.id', '=', 'p.planning_id')
                        ->join('master_process AS m', 'm.id', '=', 'mp.process_id')
                        ->join('projects AS pr', 'pr.id', '=', 'mp.project_id')
                        ->where('mp.process_group_id', '=', $process_group_id)
                        ->where('p.resource_group_id', '=', $resource_group_id)
                        ->orderBy('p.priority');
                $schedule_result = $schedule_data->get(array('p.*', 'pr.nop', 'pr.project_name AS project', 'm.process'));
                break;
        }

        if ($schedule_data->count() > 0) {
            foreach ($schedule_result as $schedule) {
                $resource = Resource::find($schedule->resource_id);
                switch ($group_data->type) {
                    case 'material':
                        $text = $schedule->process ." - " . $schedule->material_name . ' :: ' . $resource->name;
                        break;

                    default:
                        $text = $schedule->nop . " - " . $schedule->process . ' :: ' . $resource->name;
                        break;
                }
                $schedules['data'][] = array(
                    'id' => $schedule->id,
                    'start_date' => $schedule->start_date,
                    'duration' => $schedule->estimation_hour,
                    'text' => $text
                );
                $schedules['collections']['links'][] = array(
                    'id' => $schedule->id,
                    'source' => $schedule->predecessor,
                    'target' => $schedule->id,
                    'type' => 0
                );
            }
        }
        $response = Response::make($schedules, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function edit_priority() {
        $process_type = Input::get('name');

        if ($process_type) {
            $schedule_id = Input::get('pk');
            $new_priority = Input::get('value');
            $old_priority = explode(',', Input::get('old_priority_order'));
            $priority_in_array = convert_to_array($old_priority);

            if($new_priority > $priority_in_array[$schedule_id])
            {
                $target = array_search($new_priority, $priority_in_array);
                $target_priority = $priority_in_array[$target];
                $priority_in_array[$schedule_id] = $target_priority;
                foreach ($priority_in_array as $key => $value) {
                    if($key != $schedule_id && $value <= $new_priority)
                    {
                        $priority_in_array[$key] = $value - 1;
                    }
                }
            }
            else
            {
                $priority_in_array[$schedule_id] = $new_priority;
                foreach ($priority_in_array as $key => $value)
                {
                    if($key != $schedule_id && $value >= $new_priority)
                    {
                        $priority_in_array[$key] = $value + 1;
                    }
                }
            }

            asort($priority_in_array);

            $plan_ids = array();
            switch ($process_type)
            {
                case 'material':
                    foreach ($priority_in_array as $id => $priority_num)
                    {
                        $scheduling = SchedulingMFG::find($id);
                        $scheduling->priority = $priority_num;
                        $scheduling->save();

                        $process_group_id = @PlanningMFG::find($scheduling->planning_id)->process_group_id;
                        $resource_group_id = $scheduling->resource_group_id;
                        $plan_ids[] = $scheduling->planning_id;
                    }

                    break;

                default:
                    foreach ($priority_in_array as $id => $priority_num)
                    {
                        $scheduling = SchedulingNonMFG::find($id);
                        $scheduling->priority = $priority_num;
                        $scheduling->save();

                        $process_group_id = @PlanningNonMFG::find($scheduling->planning_id)->process_group_id;
                        $resource_group_id = $scheduling->resource_group_id;
                        $plan_ids[] = $scheduling->planning_id;
                    }
                    break;
            }
            // list($date, $time) = explode(' ', Input::get('start_date'));
            // list($m, $d, $Y) = explode('/', $date);
            // Input::merge(array('start_date' => "$Y-$m-$d $time"));
            // self::_save_parallel_scheduling($process_group_id, $resource_group_id, $plan_ids, true);

        }
    }

    public function get_process() {
        $processes = Process::where(function($query) {
            $group_id = Input::get('group_id');

            $has_child = ProcessGroup::where('parent_id', '=', $group_id);
            if ($has_child->count() > 0) {
                $group_ids = array();
                foreach ($has_child->get() as $group) {
                    $group_ids[] = $group->id;
                }
                $query->whereIn('group_id', $group_ids);
            } else {
                $query->where('group_id', '=', $group_id);
            }
        })->get();
        $response = Response::make($processes, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }

    public function get_resource_group() {
        $resource_groups = Resource::where(function($query) {
                    $process_id = Input::get('process_id');
                    $plan_type = Input::get('plan_type');

                    $process_resource_group = ProcessResourceGroup::where('process_id', '=', $process_id)->get();
                    $resource_group_ids = array();
                    foreach ($process_resource_group as $pr) {
                        $resource_group_ids[] = $pr->resource_group_id;
                    }

                    $query->whereIn('id', $resource_group_ids);
                    $query->where('is_offline', '=', '0');
                    $query->where('plan_type', '=', strtolower($plan_type));
                })->get();
        $response = Response::make($resource_groups, 200);
        $response->header('Content-Type', 'Application/JSON');
        return $response;
    }



}
