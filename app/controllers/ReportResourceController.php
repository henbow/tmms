<?php

class ReportResourceController extends BaseController {

    public function __construct()
    {
        View::composer(Config::get('app.theme') . '._layouts.master', function ($view) {
            $view->with('parent_active', 'report');
            $view->with('child_active', 'report_resource_availability');
        });
    }

    public function index()
    {
        return $this->availability();
    }

    public function availability()
    {
        $keywords = Input::get('keywords');
        $year = Input::get('year') ? Input::get('year') : date('Y');
        $view = View::make(Config::get('app.theme').'.reports.resource.availability2');
        $view->with('page_title', 'Resource Availability Report');
        $view->with('sub_title', 'Resource Work Plans in ' . date('Y'));
        $view->with('theme', Config::get('app.theme'));
        $view->with('resources', Resource::where(function($query) use ($keywords) {
            $query->where('plan_type','=','in plan');
            if($keywords)
            {
                $query->where('name', 'like', '%'.$keywords.'%');
            }
        })->paginate(10));
        $view->with('months', $this->_generate_month());
        $view->with('selected_year', intval($year));
        $view->with('keywords', $keywords);
        $view->with('start_year', date('Y'));
        $view->with('last_year', intval(date('Y')) + 5);
        $view->with('child_active', 'report_resource_availability');
        $view->with('breadcrumb', array(
            'Dashboard' => array(route('dashboard.index')),
            'Resource Availability Report' => array(route('report.resource_availability'), 'last')
        ));

        return $view;
    }

    public function availability_detail()
    {
        $resource_id = Input::get('resource_id');
        $month = Input::get('month');
        $year = Input::get('year');
        $type = Input::get('type');
        $description = array('cp' => 'Capacity', 'wp' => 'Work Plan', 'av' => 'Avalaibility (H)', 'avp'=> 'Avalaibility (%)');
        $resource = Resource::find($resource_id);

        $view = View::make(Config::get('app.theme').'.reports.resource.availability_detail');
        $view->with('resource_name', $resource->name);
        $view->with('month_year', date('M Y', strtotime("{$year}-{$month}-01")));
        $view->with('resource', Resource::find($resource_id));
        $view->with('weeks', $this->_generate_weeks($month));
        $view->with('description', $description[$type]);
        $view->with('month', $month);
        $view->with('year', $year);
        $view->with('type', $type);

        return $view;
    }

    private function _generate_month()
    {
        $months = array();

        foreach (range(1, 12) as $month) {
            $months[] = date('F', mktime(0, 0, 0, $month, 1, date('Y')));
        }

        return $months;
    }

    private function _generate_weeks($month)
    {
        $month = intval(date('m', strtotime(date('Y') . '-' . $month . '-1')));
        $weekStart = ($month * 4) - 3;
        $weeks = array();
        $months = range(1, 12);

        $month = intval($month);
        for($i = $weekStart; $i <= $weekStart + 3; $i++){
            $weeks[] = $i;
        }

        return $weeks;
    }
}
