<?php

use Iqi;
use EmployeeCompensation;
use Pic;
use Project;
use ProjectBOM;
use Department;
use Unit;

class EmployeeCompensationsController extends \BaseController {

	public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'actualing');
            $view->with('child_active', 'iqi');
        });
    }

	/**
	 * Display a listing of the resource.
	 * GET /employeecompensations
	 *
	 * @return Response
	 */
	public function index()
	{
		return Redirect::route('iqi.index');
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /employeecompensations/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$view = View::make(Config::get('app.theme').'.lpk.create');
        $view->with('page_title', 'Employee Compensation Reports (ECR) / Laporan Kompensasi Karyawan (LPK)');
        $view->with('sub_title', 'Create new LPK');
        $view->with('theme', Config::get('app.theme'));
        $view->with('hrd_staffs', Pic::where('dept_id','=','8')->get());
        $view->with('random', mt_rand(1,999));

        return $view;
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /employeecompensations
	 *
	 * @return Response
	 */
	public function store()
	{
		$lpk = new EmployeeCompensation;
		$lpk->lpk_code = Input::get('lpk_code');
		if(Input::get('iqi_id')) $lpk->iqi_id = Input::get('iqi_id');
		$lpk->sanctions = Input::get('sanction');
		$lpk->penalty = Input::get('penalty');
		$lpk->hour_losts = Input::get('lost_hours');
		$lpk->remarks = Input::get('remark');
		$lpk->hr_dev = Input::get('hr_dev');
		$lpk->save();

		return Redirect::to('iqi/index?mode=lpk');
	}

	/**
	 * Display the specified resource.
	 * GET /employeecompensations/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /employeecompensations/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$view = View::make(Config::get('app.theme').'.lpk.edit');
        $view->with('page_title', 'Employee Compensation Reports (ECR) / Laporan Kompensasi Karyawan (LPK)');
        $view->with('sub_title', 'Edit LPK');
        $view->with('theme', Config::get('app.theme'));
        $view->with('hrd_staffs', Pic::where('dept_id','=','8')->get());
        $view->with('lpk', EmployeeCompensation::find($id));
        $view->with('random', mt_rand(1,999));

        return $view;
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /employeecompensations/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$lpk = EmployeeCompensation::find($id);
		$lpk->lpk_code = Input::get('lpk_code');
		$lpk->iqi_id = Input::get('iqi_id');
		$lpk->sanctions = Input::get('sanction');
		$lpk->penalty = Input::get('penalty');
		$lpk->hour_losts = Input::get('lost_hours');
		$lpk->remarks = Input::get('remark');
		$lpk->hr_dev = Input::get('hr_dev');
		$lpk->save();

		return Redirect::to('iqi/index?mode=lpk');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /employeecompensations/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}