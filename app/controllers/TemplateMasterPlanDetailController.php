<?php
use ProcessGroup;
use ProjectMasterPlan;
use TemplateMasterPlanHeader;
use TemplateMasterPlanDetail;

// use BaseController, Form, Input, Redirect, Sentry, View, Notification;

class TemplateMasterPlanDetailController extends BaseController
{

    public function __construct()
    {
        View::composer(Config::get('app.theme').'._layouts.master', function ($view)
        {
            $view->with('parent_active', 'planning');
            $view->with('child_active', 'master_plan_template');
        });
    }

    public function index($planning_id)
    {
       $details = DB::table('template_master_plan_detail AS tpl')
            ->join('master_process_group AS g', 'g.id', '=', 'tpl.process_group_id')
            ->where('tpl.header_id', '=', $planning_id)
            ->orderBy('order')->get(array('tpl.*', 'g.group'));
        $view = View::make(Config::get('app.theme').'.template.master_plan.detail.index');
        $view->with('page_title', 'Template Master Plan Detail');
        $view->with('sub_title', 'Manage Template Master Plan');
        $view->with('theme', Config::get('app.theme'));
        $view->with('header_id', $planning_id);
        $view->with('details', $details);

        return $view;
    }

    public function create($planning_id)
    {
        $plannings = TemplateMasterPlanDetail::where('header_id', '=', $planning_id)->orderBy('order')->get();
        $view = View::make(Config::get('app.theme').'.template.master_plan.detail.add');
        $view->with('page_title', 'Add Planning');
        $view->with('sub_title', 'Add Planning');
        $view->with('header_id', $planning_id);
        $view->with('process_group', ProcessGroup::all());
        $view->with('random', mt_rand(100,999));
        $view->with('plannings', $plannings);
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function create_child($planning_id, $detail_id)
    {
    $plannings = TemplateMasterPlanDetail::where('header_id', '=', $planning_id)->orderBy('order')->get();
        $view = View::make(Config::get('app.theme').'.template.master_plan.detail.add');
        $view->with('page_title', 'Add Planning');
        $view->with('sub_title', 'Add Planning');
        $view->with('header_id', $planning_id);
        $view->with('detail_id', $detail_id);
    $view->with('random', mt_rand(100,999));
    $view->with('plannings', $plannings);
    $view->with('process_group', ProcessGroup::all());
        $view->with('theme', Config::get('app.theme'));

        return $view;
    }

    public function store($header_id)
    {
    $parent_id = Input::get('parent_id');
    $position_type = Input::get('position_type');
    $planning_id = Input::get('planning_id');

    $m_planning = TemplateMasterPlanDetail::find($planning_id);
    $m_order = intval(@$m_planning->order);
    $planning_order = $position_type == 'BEFORE' ? $m_order - .1 : $m_order + .1;

        $planning = new TemplateMasterPlanDetail;
        $planning->header_id = $header_id;
    $planning->parent_id = $parent_id;
    $planning->order = $planning_order;
        $planning->process_group_id = Input::get('group_id');
    $planning->process_type = Input::get('process_type');
        $planning->percent = Input::get('percent');

    if($planning->save())
    {
        self::_resort_order($header_id);
            Notification::success('New planning was saved.');
        }

        return Redirect::back();
    }

    public function edit($id)
    {
    $detail = DB::table('template_master_plan_detail AS tpl')
            ->join('master_process_group AS g', 'g.id', '=', 'tpl.process_group_id')
            ->where('tpl.id', '=', $id)
            ->orderBy('order')->get(array('tpl.*', 'g.group'));
    $next_detail = TemplateMasterPlanDetail::where('header_id', '=', @$detail[0]->header_id)
            ->where('order', '>', @$detail[0]->order)
            ->orderBy('order')->get();
    $plannings = TemplateMasterPlanDetail::where('header_id', '=', @$detail[0]->header_id)->get();
    $groups = ProcessGroup::all();

        $view = View::make(Config::get('app.theme').'.template.master_plan.detail.edit');
        $view->with('page_title', 'Template Master Plan');
        $view->with('sub_title', 'Edit detail template');
        $view->with('theme', Config::get('app.theme'));
        $view->with('detail', @$detail[0]);
        $view->with('next_detail', @$next_detail[0]);
    $view->with('random', mt_rand(100,999));
    $view->with('process_group', $groups);
    $view->with('plannings', $plannings);

        return $view;
    }

    public function update($id)
    {
    $parent_id = Input::get('parent_id');
    $position_type = Input::get('position_type');
    $planning_id = Input::get('planning_id');

    $m_planning = TemplateMasterPlanDetail::find($planning_id);
    $m_order = intval($m_planning->order);
    $planning_order = $position_type == 'BEFORE' ? $m_order - .1 : $m_order + .1;

        $planning = TemplateMasterPlanDetail::find($id);
        $planning->parent_id = $parent_id;
    $planning->order = $planning_order;
        $planning->process_group_id = Input::get('group_id');
    $planning->process_type = Input::get('process_type');
        $planning->percent = Input::get('percent');

        if($planning->save())
    {
        self::_resort_order($planning->header_id);
            Notification::success('Planning data changes was saved.');
    }

        return Redirect::back();
    }

    public function destroy($id)
    {
        $planning = TemplateMasterPlanDetail::find($id);
        $header_id = $planning->header_id;
        $planning->delete();

    self::_resort_order($header_id);
        return Redirect::back();
    }

    private static function _resort_order($header_id)
    {
    $detail_items = TemplateMasterPlanDetail::where('header_id', '=', $header_id)->orderBy('order');
    $item_count = $detail_items->count();
    if($item_count > 0)
    {
        $items = $detail_items->get();
        for($i = 1; $i <= $item_count; $i++)
        {
        $detail = TemplateMasterPlanDetail::find($items[$i - 1]->id);
        $detail->order = $i;
        $detail->save();
        }
    }
    }
}
