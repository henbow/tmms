<?php

use Process;
use ProcessGroup;
use ProjectMasterPlan;
use TemplateMfgProcessHeader;
use TemplateMfgProcessDetail;

class TemplateMfgProcessDetailController extends BaseController {

	public function __construct() {
		View::composer(Config::get('app.theme') . '._layouts.master', function ($view) {
					$view->with('parent_active', 'planning');
					$view->with('child_active', 'mfg_process_template');
				});
	}

	public function index($header_id) {
		$details = DB::table('template_mfg_process_detail AS tpl')
				->join('master_process_group AS g', 'g.id', '=', 'tpl.process_group_id')
				->join('master_process AS p', 'p.id', '=', 'tpl.process_id')
				->where('tpl.header_id', '=', $header_id)
				->get(array('tpl.*', 'p.process', 'g.group'));
		$view = View::make(Config::get('app.theme') . '.template.mfg_process.detail.index');
		$view->with('page_title', 'Template Manufacturing Process');
		$view->with('sub_title', TemplateMfgProcessHeader::find($header_id)->name);
		$view->with('theme', Config::get('app.theme'));
		$view->with('header_id', $header_id);
		$view->with('details', $details);

		return $view;
	}

	public function create($planning_id) {
		// $processes = Process::whereNotIn('group_id', array('1', '2', '9', '10', '11', '12', '15', '18', '19', '20', '22'))->get();
		$processes = Process::whereNotIn('group_id', array('15'))->get();

		$process_data = array();
		foreach ($processes as $process) {
			$process_data[] = array(
				'id' => $process->id,
				'group_id' => $process->group_id,
				'process_name' => $process->process,
				'code' => $process->code
			);
		}

		$view = View::make(Config::get('app.theme') . '.template.mfg_process.detail.add');
		$view->with('theme', Config::get('app.theme'));
		$view->with('page_title', 'Add Planning');
		$view->with('sub_title', 'Add Planning');
		$view->with('header_id', $planning_id);
		$view->with('process_data', $process_data);
		$view->with('random', mt_rand(100, 999));


		return $view;
	}

	public function store($header_id) {
		// debug($_POST);
		$process_id = Input::get('process_id');
		$group_id = Input::get('group_id');
		$estimation = Input::get('estimation');
		$estimation_hour = Input::get('estimation_hour');
		$position = Input::get('position');
		$specification = Input::get('specification');

		$planning = new TemplateMfgProcessDetail;
		$planning->header_id = $header_id;
		$planning->process_group_id = $group_id;
		$planning->process_id = $process_id;
		$planning->estimation = $estimation;
		$planning->estimation_hour = $estimation_hour;
		$planning->position = $position;
		$planning->specification = $specification;

		if ($planning->save()) {
			Notification::success('New process was saved.');
		}

		return Redirect::back();
	}

	public function edit($id) {
		$processes = Process::whereNotIn('group_id', array('1', '2', '9', '10', '11', '12', '15', '18', '19', '20', '22'))->get();

		$process_data = array();
		foreach ($processes as $process) {
			$process_data[] = array(
				'id' => $process->id,
				'group_id' => $process->group_id,
				'process_name' => $process->process,
				'code' => $process->code
			);
		}

		$detail_data = TemplateMfgProcessDetail::find($id);

		$view = View::make(Config::get('app.theme') . '.template.mfg_process.detail.edit');
		$view->with('theme', Config::get('app.theme'));
		$view->with('page_title', 'Edit Planning');
		$view->with('sub_title', 'Edit Planning');
		$view->with('header_id', $detail_data->header_id);
		$view->with('template', $detail_data);
		$view->with('process_data', $process_data);
		$view->with('random', mt_rand(100, 999));

		return $view;
	}

	public function update($id) {
		$header_id = Input::get('header_id');
		$process_id = Input::get('process_id');
		$group_id = Input::get('group_id');
		$estimation = Input::get('estimation');
		$estimation_hour = Input::get('estimation_hour');
		$position = Input::get('position');
		$specification = Input::get('specification');

		$planning = TemplateMfgProcessDetail::find($id);
		$planning->header_id = $header_id;
		$planning->process_group_id = $group_id;
		$planning->process_id = $process_id;
		$planning->estimation = $estimation;
		$planning->estimation_hour = $estimation_hour;
		$planning->position = $position;
		$planning->specification = $specification;

		if ($planning->save()) {
			Notification::success('Process data changes was saved.');
		}

		return Redirect::back();
	}

	public function destroy($id) {
		$planning = TemplateMfgProcessDetail::find($id);
		$planning->delete();

		Notification::success('Process was deleted.');

		return Redirect::back();
	}

}
