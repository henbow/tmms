<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

Artisan::add(new ERPDataSyncCommand);
Artisan::add(new SmsSenderCommand);
Artisan::add(new TSROutPlanCommand);
Artisan::add(new PlanningResourceGroupCommand);
Artisan::add(new GenerateQuantityPositionCommand);
Artisan::add(new PBBOMSyncCommand);
Artisan::add(new GenerateReportCommand);
Artisan::add(new SetProjectIDPBBOM);
Artisan::add(new UpdateDateProjectCommand);
