<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SetProjectIDPBBOM extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'generate:projectid-pbbom';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate Project ID for PBBOM Planning table.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$pbbom_plannings = PlanningPBBOM::with('masterplan')->get();

		foreach ($pbbom_plannings as $planning) {
			$update_planning = PlanningPBBOM::find($planning->id);
			$update_planning->project_id = $planning->masterplan->project_id;
			$update_planning->save();
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			// array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
