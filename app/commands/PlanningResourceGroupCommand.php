<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PlanningResourceGroupCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'planning:update-resource-group';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Refresh planning data.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$planning_mfg_data = DB::table('project_planning_detail_mfg AS p')->join('process_resource_group AS pg', 'pg.process_id', '=', 'p.process_id')->select('p.id', 'pg.*')->get();
		foreach($planning_mfg_data as $planning_mfg)
		{
			$planning = PlanningMFG::find($planning_mfg->id);

			if(isset($planning->id) && isset($planning_mfg->process_id))
			{
				$planning->resource_group_id = select_resource_randomly($planning_mfg->process_id);
				$planning->save();
			}
			else
			{
				$this->error("Planning material {$planning_mfg->id} not found.");
			}
		}

		$planning_non_mfg_data = DB::table('project_planning_detail_non_mfg AS p')->join('process_resource_group AS pg', 'pg.process_id', '=', 'p.process_id')->select('p.id', 'pg.*')->get();
		foreach($planning_non_mfg_data as $planning_non_mfg)
		{
			$planning = PlanningNonMFG::find($planning_non_mfg->id);

			if(isset($planning->id) && isset($planning_non_mfg->process_id))
			{
				$planning->resource_group_id = select_resource_randomly($planning_non_mfg->process_id);
				$planning->save();
			}
			else
			{
				$this->error("Planning NOP {$planning_non_mfg->id} not found.");
			}
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			// array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
