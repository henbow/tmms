<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class GenerateQuantityPositionCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'generate:qty-pos';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate planning quantities and positions.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$planning_mfg = PlanningMFG::all();
		if(count($planning_mfg) > 0)
		{
			$no = 1;
			$this->info("Start generating quantities and positions for planning with material type...");
			foreach ($planning_mfg as $mfg)
			{
				if(isset($mfg->id))
				{
					$this->info($no . " - Generate quantities...");
					save_mfg_qty($mfg->id);
					$this->info($no . " - Generate positions...");
	            	save_mfg_position($mfg->id);
	            	$no++;
				}
			}
			$this->info("Finish generating quantities and positions for planning with material type...");
		}

		$planning_non_mfg = PlanningNonMFG::all();
		if(count($planning_non_mfg) > 0)
		{
			$no = 1;
			$this->info("Start generating positions for planning with NOP type...");
			foreach ($planning_non_mfg as $non_mfg)
			{
				if(isset($non_mfg))
				{
					$this->info($no . " - Generate quantities...");
					save_non_mfg_qty($non_mfg->id);
					$this->info($no . " - Generate positions...");
	            	save_non_mfg_position($non_mfg->id);
	            	$no++;
				}
			}
			$this->info("Finish generating positions for planning with NOP type...");
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('type', null, InputOption::VALUE_OPTIONAL, 'Planning type (Material / NOP).', null),
		);
	}

}
