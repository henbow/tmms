<?php
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ERPDataSyncCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'erpdatasync:syncronize';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Syncronize data from ERP 1 to TMMS application.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$process_start = time();
        $nop = $this->option('nop');
        $limit = $this->option('limit');
        $limit_type = $this->option('limit-type');

        $tmms_projects = array();
		$tmms_projects = Project::limit($limit)->orderBy('id', 'desc')->get();
		foreach ($tmms_projects as $tmms_project) {
			$tmms_projects[md5($tmms_project->nop . $tmms_project->order_num)] = (Object) array(
              'id' => $tmms_project->id,
              'is_planning' => $tmms_project->is_planning,
              'is_complete' => $tmms_project->is_complete,
              'is_filtered' => $tmms_project->is_filtered,
              'is_actualing' => $tmms_project->is_actualing,
              'start_date' => $tmms_project->start_date,
              'first_date' => $tmms_project->trial_date,
              'secondary_date' => $tmms_project->delivery_date
			);
		}

		if(count($tmms_projects))
		{
			$ids = array();
			foreach ($tmms_projects as $key => $tmms_project)
			{
				$ids[] = $tmms_project->id;
			}
			Project::whereIn('id', $ids)->delete();
		}

		if($nop)
		{
			$erp_projects = NOPPImporter::getProjectByNOP($nop);
		}
		else
		{
			$erp_projects = NOPPImporter::getProjects($limit, $limit_type);
		}

		$this->info("Preparing to process ".count($erp_projects)." data...");

        foreach ($erp_projects as $erp_project)
        {
			$this->info("");
			$this->info("Get project data from: ".$erp_project->project."[".$erp_project->nop."]");

			$query = Project::where('nop', '=', $erp_project->nop)->where('order_num','=',$erp_project->order_num);
			$new_project = new Project;

			$erp_customer = NOPPImporter::getCustomerByKey($erp_project->customer_id);
			if(count($erp_customer))
			{
				$customer = $erp_customer[0];
				$existing_customer = Customer::where('erp_code','=',$erp_project->customer_id)->first();
				if(count($existing_customer))
				{
					$new_customer = Customer::find($existing_customer->id);
				}
				else
				{
					$new_customer = new Customer;
				}

				$new_customer->erp_code = $erp_project->customer_id;
				$new_customer->code = $customer->Initial;
				$new_customer->name = $customer->Nama;
				$new_customer->address = $customer->Alamat;
				$new_customer->city = $customer->Kota;
				$new_customer->mailing_address = $customer->AlamatKirim;
				$new_customer->tax_address = $customer->AlamatFakPjk;
				$new_customer->contact_person = $customer->Person;
				$new_customer->phone = $customer->Telp;
				$new_customer->fax = $customer->Fax;
				$new_customer->mobile = $customer->Hp;
				$new_customer->email = $customer->Email;
				$new_customer->save();
			}

			if(isset($tmms_projects[md5($erp_project->nop . $erp_project->order_num)]))
			{
				$new_project->id = $tmms_projects[md5($erp_project->nop . $erp_project->order_num)]->id;
				$new_project->is_planning = $tmms_projects[md5($erp_project->nop . $erp_project->order_num)]->is_planning;
				$new_project->is_complete = $tmms_projects[md5($erp_project->nop . $erp_project->order_num)]->is_complete;
				$new_project->is_filtered = $tmms_projects[md5($erp_project->nop . $erp_project->order_num)]->is_filtered;
				$new_project->is_actualing = $tmms_projects[md5($erp_project->nop . $erp_project->order_num)]->is_actualing;
				$new_project->start_date = $tmms_projects[md5($erp_project->nop . $erp_project->order_num)]->start_date;
				$new_project->trial_date = $tmms_projects[md5($erp_project->nop . $erp_project->order_num)]->first_date;
				$new_project->delivery_date = $tmms_projects[md5($erp_project->nop . $erp_project->order_num)]->secondary_date;

				unset($tmms_projects[md5($erp_project->nop . $erp_project->order_num)]);
			}

			$new_project->nop = $erp_project->nop;
			$new_project->order_num = $erp_project->order_num;
			$new_project->priority = $erp_project->priority;
			$new_project->project_name = $erp_project->project;
			$new_project->qty = $erp_project->qty;
			$new_project->price_per_unit = $erp_project->price_per_unit;
			$new_project->price_total = $erp_project->price_total;
			$new_project->customer_id = isset($new_customer->id) ? $new_customer->id : 0;
			$new_project->po_num = $erp_project->po_num;
			$new_project->is_planning = 'yes';

			if($new_project->save())
			{
				$this->info("{$erp_project->nop} => success update project data");
			}
			else
			{
				$this->info("{$erp_project->nop} => failed update project data.");
			}
        }

        // if(count($tmms_projects))
        // {
        // 	foreach ($tmms_projects as $key => $unused_tmms_project)
        // 	{
        // 		$unused_project = Project::find($unused_tmms_project->id);
        // 		$unused_project->delete();
        // 	}
        // }

		$process_duration = time() - $process_start;
        $this->info("Process time: ".round($process_duration / 60, 2)." minutes");
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('nop', null, InputOption::VALUE_OPTIONAL, 'Project NOP.', null),
			array('limit', null, InputOption::VALUE_OPTIONAL, 'Limit fetched data.', null),
			array('limit-type', null, InputOption::VALUE_OPTIONAL, 'Limit type.', null),
		);
	}

}
