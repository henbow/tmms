<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class GenerateReportCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'report:actualing';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate actualing report.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// $this->pbbom();
		// $this->by_nop();
		// $this->by_material();
		$this->general();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('type', null, InputOption::VALUE_OPTIONAL, 'Report type. General or Detail report. Default: detail.', null),
		);
	}

	private function general()
	{
		$projects = Project::where('is_complete', '<>', 'yes')->get();
		if(count($projects))
		{
			foreach ($projects as $project)
			{
				$data = array();
				$masterplans = ProjectMasterPlan::with('process_group')->where('project_id','=',$project->id)->get();
				if(count($masterplans))
				{
					foreach ($masterplans as $masterplan)
					{
						$key = $project->id.':'.$masterplan->id.':'.$masterplan->process_group->id;
						switch($masterplan->process_group->type)
						{
							case 'nop':
								$total_non_mfg = PlanningNonMFG::where('project_id', '=', $project->id)->count();
								$progress_non_mfg = ReportActualingNonMFG::where('project_id', '=', $project->id)->where('status','=','COMPLETE')->get();
								foreach ($progress_non_mfg as $non_mfg)
								{
									$data[$key][$non_mfg->process->group->group][] = $non_mfg->process->process;
								}
								break;

							case 'material':
								$total_mfg = PlanningMFG::where('project_id', '=', $project->id)->count();
								$progress_mfg = ReportActualingMFG::with(array('process' => function ($query){
									$query->with('group');
								}))->where('project_id', '=', $project->id)->where('status','=','COMPLETE')->get();

								foreach($progress_mfg as $mfg)
								{
									$has_parent = $mfg->process->group->parent_id > 0 ? TRUE : FALSE;
									if($has_parent)
									{
										$data[$key]['MFG'][$mfg->process->group->group][] = @$mfg->process->process." --> ".@$mfg->material->material_name;
									}
									else
									{
										$data[$key][$mfg->process->group->group][] = @$mfg->process->process." --> ".@$mfg->material->material_name;
									}
								}
								break;

							default:
								$total_pb_bom = PlanningPBBOM::where('project_id', '=', $project->id)->count();
								$progress_pb_bom = ReportActualingPBBOM::where('project_id', '=', $project->id)->where('status','=','COMPLETE')->get();
								foreach ($progress_pb_bom as $pb_bom) {
									$data[$key][$pb_bom->process->group->group][] = $pb_bom->process->process." --> ".@$pb_bom->material->material_name;
								}
								break;
						}
					}

					print_r($data);

					if(count($data))
					{
						foreach ($data as $key => $actualing_processes) {
							list($project_id, $masterplan_id, $process_group_id) = explode(':', $key);
							$existing_report = ReportActualingGeneral::where('project_id','=',$project_id)->where('planning_id','=',$masterplan_id)->first();
							if($existing_report)
							{
								$report = $existing_report;
							}
							else
							{
								$report = new ReportActualingGeneral;
							}

							foreach ($actualing_processes as $key => $process)
							{
								if(array_depth($process) > 1 && array_depth($process) == 2)
								{

								}
								else
								{

								}
							}

							// if(array_depth($actualing_processes))

							$report->project_id = $project_id;
							$report->planning_id = $masterplan_id;
							$report->process_group_id = $process_group_id;
							// $report->progress = $total_pb_bom > 0 ? round(($progress_pb_bom / $total_pb_bom) * 100, 2) : 0;
							// $report->save();

							// print_r($report);
						}
					}
				}
				// die;
				$sum_progress = ReportActualingGeneral::with('process_group')->where('project_id','=',$project->id);//->sum('progress');
				// print_r(array(
				// 	$project->id => $sum_progress->get()->toArray(),
				// ));
				// if($sum_progress >= 100)
				// {
				// 	$current_project = Project::find($project->id);
				// 	$current_project->is_complete = 'yes';
				// 	// $current_project->save();
				// }
			}
		}
	}

	private function pbbom()
	{
		$actualing_pbbom = ActualingPBBOM::with('planning')->get();
		foreach ($actualing_pbbom as $pbbom)
		{
			$masterplan = $pbbom->planning->masterplan()->first();
			$estimation_plan = $masterplan->estimation_hour;

			$existing_report_pbbom = ReportActualingPBBOM::where('project_id','=',$pbbom->planning->project_id)
										->where('actualing_id','=',$pbbom->id)
										->where('material_id','=',$pbbom->material_id)
										->first();

			if($existing_report_pbbom)
			{
				$existing_report_pbbom->delete();
			}

			$report_pbbom = new ReportActualingPBBOM;
			$report_pbbom->project_id = $masterplan->project_id;
			$report_pbbom->planning_id = $pbbom->planning->id;
			$report_pbbom->actualing_id = $pbbom->id;
			$report_pbbom->material_id = $pbbom->material_id;
			$report_pbbom->process_id = $pbbom->planning->process_id;
			$report_pbbom->resource_id = $pbbom->planning->resource_id;
			$report_pbbom->estimation_hour = $masterplan->estimation_hour;
			$report_pbbom->actualing_hour = $pbbom->po_ttb_time;
			$report_pbbom->status = 'COMPLETE';
			$report_pbbom->save();
		}
	}

	private function by_nop()
	{
		$actualing_non_mfg = ActualingNonMFG::with(array('planning', 'logs'))->get();
		foreach($actualing_non_mfg as $non_mfg)
		{
			$logs = $non_mfg->logs()->where('state','=','COMPLETE');

			$total_duration = $logs->sum('last_duration');
			if($total_duration > 0)
			{
				$existing_report_non_mfg = ReportActualingNonMFG::where('project_id','=',$non_mfg->planning->project_id)->where('actualing_id','=',$non_mfg->id)->first();

				if($existing_report_non_mfg)
				{
					$existing_report_non_mfg->delete();
				}

				$report_non_mfg = new ReportActualingNonMFG;
				$report_non_mfg->project_id = $non_mfg->planning->project_id;
				$report_non_mfg->planning_id = $non_mfg->planning->id;
				$report_non_mfg->actualing_id = $non_mfg->id;
				$report_non_mfg->process_id = $non_mfg->process_id;
				$report_non_mfg->resource_id = $non_mfg->resource_id;
				$report_non_mfg->estimation_hour = $non_mfg->planning->estimation_hour;
				$report_non_mfg->actualing_hour = round($total_duration / 3600, 2);
				$report_non_mfg->status = $non_mfg->status == 'COMPLETE' ? str_replace(' ', '', $non_mfg->status) : "WAITING";
				$report_non_mfg->save();
			}
		}
	}

	private function by_material()
	{
		$actualing_mfg = ActualingMFG::with(array('planning', 'logs'))->get();
		foreach($actualing_mfg as $mfg)
		{
			$logs = $mfg->logs()->where('state','=','COMPLETE');
			$total_duration = $logs->sum('last_duration');

			if($total_duration > 0 && $mfg->process_id)
			{
				$existing_report_mfg = ReportActualingMFG::where('project_id','=',$mfg->planning->project_id)
										->where('actualing_id','=',$mfg->id)
										->where('material_id','=',$mfg->material_id)
										->first();

				if($existing_report_mfg)
				{
					$existing_report_mfg->delete();
				}

				$report_mfg = new ReportActualingMFG;
				$report_mfg->project_id = $mfg->planning->project_id;
				$report_mfg->planning_id = $mfg->planning->id;
				$report_mfg->actualing_id = $mfg->id;
				$report_mfg->material_id = $mfg->material_id;
				$report_mfg->process_id = $mfg->process_id;
				$report_mfg->resource_id = $mfg->resource_id;
				$report_mfg->estimation_hour = $mfg->planning->estimation_hour;
				$report_mfg->actualing_hour = round($total_duration / 3600, 2);
				$report_mfg->status = $mfg->status == 'COMPLETE' ? str_replace(' ', '', $mfg->status) : "WAITING";
				$report_mfg->save();
			}
		}
	}

}
