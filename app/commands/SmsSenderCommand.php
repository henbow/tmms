<?php
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SmsSenderCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'sender:sms';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send sms alert to listed mobile numbers.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$message_queues = SmsQueue::where('is_sent', '=', '0')->orderBy('created_at')->get();
		if(count($message_queues) > 0)
		{
			foreach ($message_queues as $queue) {
				$send_sms = sms_sender($queue->send_to, $queue->message);
				$sms_queue = SmsQueue::find($queue->id);
				$sms_queue->is_sent = '1';
				$sms_queue->save();
			}
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			// array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
