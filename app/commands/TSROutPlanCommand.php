<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TSROutPlanCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'erpdatasync:tsr';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Retrieve and save TSR and TTB data from ERP 1.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$projects = DB::table('projects')->take(20)->orderBy('created_at', 'desc')->get();
		foreach($projects as $project)
		{
			$sqlTsr = "SELECT
							t2.BOMNOPKdFak AS nop,
							t2.NamaBrgBOM AS material_name,
							t2.BarcodeNo AS material_barcode,
							t2.Qty AS qty,
							t1.Catatan AS process,
							t1.KdFak AS tsr_no,
							t1.Tgl AS tsr_date,
							s1.Nama AS resource_name
			           FROM TSR1 t1
			           INNER JOIN TSR2N t2 ON t2.KdFak=t1.KdFak
			           INNER JOIN Supl s1 ON s1.SuplKey=t1.SuplKey
					   WHERE t2.BOMNOPKdFak = '{$project->nop}'
			           ORDER BY t1.Tgl";
			$tsrData = $this->_runQuery($sqlTsr);
			// print_r($tsrData);
			if(count($tsrData) > 0)
			{
				foreach($tsrData as $tsr)
				{
					$ttb_data = $this->_runQuery("SELECT KdFak AS ttb_no, Tgl AS ttb_date FROM TSRTerimaDT WHERE TSRKdFak = '{$tsr->tsr_no}' ORDER BY Tgl DESC");
					$ttb = count($ttb_data) > 0 ? $ttb_data[0] : array();
					$existing = DB::table('actualing_out_material')->where('material_barcode', '=', $tsr->material_barcode)->where('resource_name', '=', $tsr->resource_name)->get();;
					$data = array(
						'nop' => $tsr->nop,
						'material_name' => $tsr->material_name,
						'material_barcode' => $tsr->material_barcode,
						'material_qty' => $tsr->qty,
						'process_name' => $tsr->process,
						'resource_name' => $tsr->resource_name,
						'tsr_no' => $tsr->tsr_no,
						'tsr_date' => $tsr->tsr_date,
					);

					if(count($ttb) > 0)
					{
						$data['ttb_no'] = $ttb->ttb_no;
						$data['ttb_date'] = $ttb->ttb_date;
						$data['total_time'] = round((strtotime($ttb->ttb_date) - strtotime($tsr->tsr_date)) / (24 * 3600));
					}

					if(count($existing) == 0)
					{
						$data['created_at'] = date('Y-m-d H:i:s');
						$data['updated_at'] = date('Y-m-d H:i:s');
						$insert_id = DB::table('actualing_out_material')->insertGetId($data);
						if($insert_id)
							$this->info("{$tsr->material_barcode} => success insert data");
						else
							$this->error("{$tsr->material_barcode} => failed insert data.");
					}
					else
					{
						$data['updated_at'] = date('Y-m-d H:i:s');
						$update = DB::table('actualing_out_material')->where('id','=',$existing[0]->id)->update($data);
						if($update)
							$this->info("{$tsr->material_barcode} => success update data");
						else
							$this->error("{$tsr->material_barcode} => failed update data.");
					}
				}
			}
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

	private function _runQuery($sql)
	{
		return DB::connection('sqlsrv')->select($sql);
	}

}
