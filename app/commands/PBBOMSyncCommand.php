<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PBBOMSyncCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'erpdatasync:pbbom';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'PB-BOM actualing data syncronizer.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$nop = $this->option('nop');
        $limit = $this->option('limit');
        $limit_type = $this->option('limit-type');

		if($nop)
		{
			$projects = NOPPImporter::getProjectByNOP($nop);
		}
		else
		{
			$projects = NOPPImporter::getProjects($limit, $limit_type);
		}

		foreach ($projects as $project)
		{
			$plannings = Project::where('nop','=',$project->nop)->where('order_num','=',$project->order_num)->first()->masterplans()->with('planning_pb_bom')->get();

			foreach($plannings as $planning)
			{
				if(count($planning->planning_pb_bom) > 0)
				{
					foreach ($planning->planning_pb_bom as $pbbom)
					{
						// print_r($pbbom);continue;
						if($pbbom->id && $pbbom->material_id)
						{
							$po_ttb_data = NOPPImporter::getPOTTB($project->nop, $project->order_num);

							if(count($po_ttb_data))
							{
								foreach($po_ttb_data as $po_ttb)
								{
									$time = round((strtotime($po_ttb->ttb_date) - strtotime($po_ttb->po_date)) / 3600, 2);

									$existing = ActualingPBBOM::where('planning_id','=',$pbbom->id)->where('material_id','=',$pbbom->material_id)->get();

									if(count($existing) == 0 && (strtotime($po_ttb->ttb_date) > strtotime($po_ttb->po_date)))
									{
										$actualing = new ActualingPBBOM;
										$actualing->planning_id = $pbbom->id;
										$actualing->material_id = $pbbom->material_id;
										$actualing->po_number = $po_ttb->po_no;
										$actualing->po_order = $po_ttb->po_order;
										$actualing->po_date = $po_ttb->po_date;
										$actualing->po_qty = $po_ttb->po_qty;
										$actualing->ttb_number = $po_ttb->ttb_no;
										$actualing->ttb_order = $po_ttb->ttb_order;
										$actualing->ttb_date = $po_ttb->ttb_date;
										$actualing->ttb_qty = $po_ttb->ttb_qty;
										$actualing->po_ttb_time = $time > 0 ? $time : 0;

										if($actualing->save())
										{
											$this->info("Success.");
										}
										else
										{
											$this->error("Failed.");
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('nop', null, InputOption::VALUE_OPTIONAL, 'Project NOP.', null),
			array('limit', null, InputOption::VALUE_OPTIONAL, 'Limit fetched data.', null),
			array('limit-type', null, InputOption::VALUE_OPTIONAL, 'Limit type.', null),
		);
	}

}
