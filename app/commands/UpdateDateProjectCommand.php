<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UpdateDateProjectCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'update-project-date';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update projects date.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$project2 = DB::table('projects_2')->orderBy('id','desc')->get();
		foreach ($project2 as $p2)
		{
			$project = Project::find($p2->id);
			if($project)
			{
				$project->start_date = strtotime($p2->start_date) ? $p2->start_date : '';
				$project->trial_date = strtotime($p2->trial_date) ? $p2->trial_date : '';
				$project->delivery_date = strtotime($p2->delivery_date) ? $p2->delivery_date : '';
				if($project->save())
				{
					$this->info("SUCCESS");
				}
				else
				{
					$this->error("ERROR");
				}
			}
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			// array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
