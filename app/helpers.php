<?php

/*
  | Helpers
  |
 */

if (!function_exists('generate_helpdesk_ticket')) {

    function generate_helpdesk_ticket() {
        $helpdesk = HelpDesk::where('ticket_id', 'LIKE', date('ymd') . '%')->orderBy('ticket_id', 'desc')->take(1)->get();

        if (isset($helpdesk[0])) {
            if ($helpdesk[0]->ticket_id) {
                $new_ticket_num = intval(substr($helpdesk[0]->ticket_id, 6)) + 1;
                return date('ymd') . str_pad($new_ticket_num, 3, '0', STR_PAD_LEFT);
            }
        }

        return date('ymd') . '001';
    }

}

if (!function_exists('generate_iqi_code')) {

    function generate_iqi_code() {
        $iqi = Iqi::where('iqi_code', 'LIKE', 'IQI-' . date('ymd') . '%')->orderBy('iqi_code', 'desc')->take(1)->get();

        if (isset($iqi[0])) {
            if ($iqi[0]->iqi_code) {
                $new_code = intval(substr($iqi[0]->iqi_code, 10)) + 1;
                return 'IQI-' . date('ymd') . str_pad($new_code, 3, '0', STR_PAD_LEFT);
            }
        }

        return 'IQI-' . date('ymd') . '001';
    }

}

if (!function_exists('generate_lpk_code')) {

    function generate_lpk_code() {
        $lpk = EmployeeCompensation::where('lpk_code', 'LIKE', 'LPK-' . date('ymd') . '%')->orderBy('lpk_code', 'desc')->take(1)->get();

        if (isset($lpk[0])) {
            if ($lpk[0]->lpk_code) {
                $new_code = intval(substr($lpk[0]->lpk_code, 10)) + 1;
                return 'LPK-' . date('ymd') . str_pad($new_code, 3, '0', STR_PAD_LEFT);
            }
        }

        return 'LPK-' . date('ymd') . '001';
    }

}

if (!function_exists('generate_issue_code')) {

    function generate_issue_code() {
        $prefix = "ISS";
        $issue = IssueManagement::where('issue_code', 'LIKE', $prefix.'-' . date('ymd') . '%')->orderBy('issue_code', 'desc')->take(1)->get();

        if (isset($issue[0])) {
            if ($issue[0]->issue_code) {
                $new_code = intval(substr($issue[0]->issue_code, 10)) + 1;
                return $prefix.'-' . date('ymd') . str_pad($new_code, 3, '0', STR_PAD_LEFT);
            }
        }

        return $prefix.'-' . date('ymd') . '001';
    }

}

if (!function_exists('material_type')) {

    function material_type($code) {
        $code_dict = array('M' => 'Material', 'S' => 'Standard Parts', 'H' => 'H');
        return $code_dict[$code];
    }

}

if (!function_exists('is_bom_imported')) {

    function is_bom_imported($id) {
        $bom = ProjectBOM::where('project_id', '=', $id);
        return $bom->count() > 0 ? true : false;
    }

}

if (!function_exists('save_erp_material')) {

    function save_erp_material($projectid, $bom_data) {
        $bom = new ProjectBOM;
        $bom->project_id = $projectid;
        $bom->nop = $bom_data->nop;
        $bom->order_num = $bom_data->order_num;
        $bom->priority = $bom_data->priority;
        $bom->part_code = $bom_data->part_code;
        $bom->barcode = trim($bom_data->barcode);
        $bom->material_name = $bom_data->material_name;
        $bom->type = $bom_data->type;
        $bom->standard = $bom_data->standard;
        $bom->length = $bom_data->length;
        $bom->width = $bom_data->width;
        $bom->height = $bom_data->height;
        $bom->diameter_start = $bom_data->diameter_start;
        $bom->diameter_finish = $bom_data->diameter_finish;
        $bom->dimension_desc = $bom_data->dimension_desc;
        $bom->qty = $bom_data->qty;
        $bom->unit = $bom_data->unit;
        $bom->description = $bom_data->description;
        $bom->po_number = $bom_data->po_number;
        $bom->insert_date = $bom_data->insert_date;
        return $bom->save() ? $bom->id : false;
    }

}

if (!function_exists('save_mfg_qty')) {

    function save_mfg_qty($planning_id) {
        $planning = PlanningMFG::find($planning_id);
        $bom = ProjectBOM::find($planning->material_id);
        $qty = isset($bom->qty) ? $bom->qty : 1;;
        if($qty > 0) {
            $current_qty = BomQuantity::where('planning_id','=',$planning_id)->count();
            if($current_qty <= $qty) {
                for($i = 1; $i <= $qty; $i++) {
                    $check = BomQuantity::where('planning_id','=',$planning_id)->where('qty_num','=',$i)->count();
                    if(!$check) {
                        $bom_qty = new BomQuantity;
                        $bom_qty->planning_id = $planning_id;
                        $bom_qty->qty_num = $i;
                        $bom_qty->save();
                    }
                }
            }
            else {
                for($i = $qty + 1; $i <= $current_qty; $i++) {
                    $delete = DB::table('bom_quantities')->where('planning_id','=',$planning_id)->where('qty_num','=',$i)->delete();
                }
            }
            return true;
        }

        return false;
    }

}

if (!function_exists('save_mfg_position')) {

    function save_mfg_position($planning_id) {
        $planning = PlanningMFG::find($planning_id);
        $positions = $planning->position > 0 ? $planning->position : 1;

        $qty_data = BomQuantity::where('planning_id','=',$planning_id)->get();
        if(count($qty_data) > 0){
            foreach ($qty_data as $qty) {
                $current_positions = BomPosition::where('qty_id','=',$qty->id)->count();
                if($current_positions <= $positions) {
                    for($i = 1; $i <= $positions; $i++) {
                        $check = BomPosition::where('qty_id','=',$qty->id)->where('position_num','=',$i)->count();
                        if(!$check) {
                            $position_obj = new BomPosition;
                            $position_obj->qty_id = $qty->id;
                            $position_obj->position_num = $i;
                            $position_obj->save();
                        }
                    }
                } else {
                    for($i = $positions + 1; $i <= $current_positions; $i++) {
                        $delete = DB::table('bom_positions')->where('qty_id','=',$qty->id)->where('position_num','=',$i)->delete();
                    }
                }
            }
            return true;
        }

        return false;
    }

}

if (!function_exists('save_non_mfg_qty')) {

    function save_non_mfg_qty($planning_id) {
        $planning = PlanningNonMFG::find($planning_id);
        if(isset($planning->project_id)) {
            $project = Project::find($planning->project_id);
            $qty = isset($project->qty) ? $project->qty : 1;;
            if($qty > 0) {
                $current_qty = MasterplanQuantity::where('planning_id','=',$planning_id)->count();
                if($current_qty <= $qty) {
                    for($i = 1; $i <= $qty; $i++) {
                        $check = MasterplanQuantity::where('planning_id','=',$planning_id)->where('qty_num','=',$i)->count();
                        if(!$check) {
                            $bom_qty = new MasterplanQuantity;
                            $bom_qty->planning_id = $planning_id;
                            $bom_qty->qty_num = $i;
                            $bom_qty->save();
                        }
                    }
                }
                else {
                    for($i = $qty + 1; $i <= $current_qty; $i++) {
                        $delete = DB::table('masterplan_quantities')->where('planning_id','=',$planning_id)->where('qty_num','=',$i)->delete();
                    }
                }
                return true;
            }
        }

        return false;
    }

}

if (!function_exists('save_non_mfg_position')) {

    function save_non_mfg_position($planning_id) {
        $planning = PlanningNonMFG::find($planning_id);
        if(isset($planning->id)) {
            $positions = $planning->position > 0 ? $planning->position : 1;

            $qty_data = MasterplanQuantity::where('planning_id','=',$planning_id)->get();
            if(count($qty_data) > 0){
                foreach ($qty_data as $qty) {
                    $current_positions = MasterplanPosition::where('qty_id','=',$qty->id)->count();
                    if($current_positions <= $positions) {
                        for($i = 1; $i <= $positions; $i++) {
                            $check = MasterplanPosition::where('qty_id','=',$qty->id)->where('position_num','=',$i)->count();
                            if(!$check) {
                                $position_obj = new MasterplanPosition;
                                $position_obj->qty_id = $qty->id;
                                $position_obj->position_num = $i;
                                $position_obj->save();
                            }
                        }
                    } else {
                        for($i = $positions + 1; $i <= $current_positions; $i++) {
                            $delete = DB::table('masterplan_positions')->where('qty_id','=',$qty->id)->where('position_num','=',$i)->delete();
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

}

if (!function_exists('save_actualing_mfg_position')) {

    function save_actualing_mfg_position($actualing_id) {
        $actualing = ActualingMFG::find($actualing_id);
        $planning = PlanningMFG::find($actualing->planning_id);
        $quantities = BomQuantity::where('planning_id','=',$planning->id)->get();

        if(count($quantities)) {
            foreach ($quantities as $qty) {
                $positions = BomPosition::where('qty_id','=',$qty->id)->get();
                if(count($positions) > 0) {
                    foreach ($positions as $pos) {
                        $check = ActualingMFGPosition::where('position_id','=',$pos->id)->count();
                        if(!$check) {
                            $actualing_position = new ActualingMFGPosition;
                            $actualing_position->qty_id = $qty->id;
                            $actualing_position->position_id = $pos->id;
                            $actualing_position->actualing_id = $actualing_id;
                            $actualing_position->status = 'ONPROCESS';
                            $actualing_position->save();
                        }
                    }
                }
            }

            return true;
        }

        return false;
    }

}

if (!function_exists('save_actualing_non_mfg_position')) {

    function save_actualing_non_mfg_position($actualing_id) {
        $actualing = ActualingNonMFG::find($actualing_id);
        $planning = PlanningNonMFG::find($actualing->planning_id);
        $quantities = MasterplanQuantity::where('planning_id','=',$planning->id)->get();

        if(count($quantities) > 0) {
            foreach ($quantities as $qty) {
                $positions = MasterplanPosition::where('qty_id','=',$qty->id)->get();
                if(count($positions) > 0) {
                    foreach ($positions as $pos) {
                        $check = ActualingNonMFGPosition::where('position_id','=',$pos->id)->count();
                        if(!$check) {
                            $actualing_position = new ActualingNonMFGPosition;
                            $actualing_position->qty_id = $qty->id;
                            $actualing_position->position_id = $pos->id;
                            $actualing_position->actualing_id = $actualing_id;
                            $actualing_position->status = 'ONPROCESS';
                            $actualing_position->save();
                        }
                    }
                }
            }
        }

        return false;
    }

}

if(!function_exists('is_mfg_qty_complete'))
{
    function is_mfg_qty_complete($qty_id)
    {
        $pos_total = BomPosition::where('qty_id', '=', $qty_id)->count();
        $pos_complete = ActualingMFGPosition::where('qty_id', '=', $qty_id)->where('status','=','COMPLETE')->count();
        if($pos_complete==$pos_total)
        {
            return true;
        }
        return false;
    }
}

if(!function_exists('is_non_mfg_qty_complete'))
{
    function is_non_mfg_qty_complete($qty_id)
    {
        $pos_total = MasterplanPosition::where('qty_id', '=', $qty_id)->count();
        $pos_complete = ActualingNonMFGPosition::where('qty_id', '=', $qty_id)->where('status','=','COMPLETE')->count();
        if($pos_complete==$pos_total)
        {
            return true;
        }
        return false;
    }
}

if (!function_exists('update_actualing_mfg_position_status')) {

    function update_actualing_mfg_position_status($actualing_id, $position_id, $status) {
        return DB::table('actualing_mfg_positions')
            ->where('actualing_id','=',$actualing_id)
            ->where('position_id','=',$position_id)
            ->update(array('status' => $status, 'updated_at' => date('Y-m-d H:i:s')));
    }

}

if (!function_exists('update_actualing_non_mfg_position_status')) {

    function update_actualing_non_mfg_position_status($actualing_id, $position_id, $status) {
        return DB::table('actualing_non_mfg_positions')
            ->where('actualing_id','=',$actualing_id)
            ->where('position_id','=',$position_id)
            ->update(array('status' => $status, 'updated_at' => date('Y-m-d H:i:s')));
    }

}

if (!function_exists('delete_planning_qty')) {

    function delete_planning_qty($planning_id) {
        $delete = BomQuantity::where('planning_id','=',$planning_id)->delete();
        return $delete;
    }

}

if (!function_exists('delete_mfg_position')) {

    function delete_mfg_position($planning_id) {
        $planning = PlanningMFG::find($planning_id);
        $positions = $planning->position > 0 ? $planning->position : 1;

        $qty_data = BomQuantity::where('planning_id','=',$planning_id)->get();
        if(count($qty_data) > 0){
            foreach ($qty_data as $qty) {
                $delete = BomPosition::where('qty_id','=',$qty->id)->delete();
            }
            return true;
        }

        return false;
    }

}

if (!function_exists('delete_non_mfg_position')) {

    function delete_non_mfg_position($planning_id) {
        $delete = MasterplanPosition::where('masterplan_id','=',$planning_id)->delete();
        return $delete;
    }

}

if (!function_exists('update_erp_material')) {

    function update_erp_material($id, $projectid, $bom_data) {
        $bom = ProjectBOM::find($id);
        $bom->project_id = $projectid;
        $bom->nop = $bom_data->nop;
        $bom->order_num = $bom_data->order_num;
        $bom->priority = $bom_data->priority;
        $bom->part_code = $bom_data->part_code;
        $bom->barcode = trim($bom_data->barcode);
        $bom->material_name = $bom_data->material_name;
        $bom->type = $bom_data->type;
        $bom->standard = $bom_data->standard;
        $bom->length = $bom_data->length;
        $bom->width = $bom_data->width;
        $bom->height = $bom_data->height;
        $bom->diameter_start = $bom_data->diameter_start;
        $bom->diameter_finish = $bom_data->diameter_finish;
        $bom->dimension_desc = $bom_data->dimension_desc;
        $bom->qty = $bom_data->qty;
        $bom->unit = $bom_data->unit;
        $bom->description = $bom_data->description;
        $bom->po_number = $bom_data->po_number;
        $bom->insert_date = $bom_data->insert_date;
        return $bom->save();
    }

}

if (!function_exists('import_process_csv')) {

    function import_process_csv() {
        $fpath = '/var/www/tmms-theme/process.csv';
        $file = fopen($fpath, 'rb');
        $contents = explode("\n", fread($file, filesize($fpath)));

        foreach ($contents as $line) {
            $field_item = explode("\t", $line);
            if (isset($field_item[0]) && isset($field_item[1])) {
                $code = $field_item[0];
                $group = ProcessGroup::where('group', '=', $field_item[1])->get();
                $group_id = isset($group[0]) ? $group[0]->id : '0';
                $process_name = ucwords($field_item[2]);
                $resources = str_replace(',  ', ', ', str_replace(',', ', ', $field_item[3]));
                $type = $field_item[4] == '' ? 'in plan' : 'out plan';

                $process = new Process;
                $process->code = $code;
                $process->group_id = $group_id;
                $process->process = $process_name;
                $process->resources = ucwords($resources);
                $process->type = $type;
                $process->save();
            }
        }
    }

}

if (!function_exists('last_date_resource_used')) {

    function last_date_resource_used($process_id, $type, $resource_id, &$estimation_hour) {
        $resource_used = ResourceActualUsed::select('start_used')
            ->where('resource_id', '=', $resource_id)
            ->where('is_used','=','1')
            ->orderBy('id', 'DESC')
            ->take(1)->get();


        if (count($resource_used) > 0)
        {
            if($type == 'material')
            {
                $actualing_data = ActualingMFG::where('process_id','=',$process_id)->where('resource_id','=',$resource_id)->select('planning_id')->take(1)->orderBy('id','desc')->get();
            }
            else
            {
                $actualing_data = ActualingNonMFG::where('process_id','=',$process_id)->where('resource_id','=',$resource_id)->select('planning_id')->take(1)->orderBy('id','desc')->get();
            }

            if(count($actualing_data) > 0)
            {
                if($type == 'material')
                {
                    $planning_data = PlanningMFG::find($actualing_data->planning_id);
                }
                else
                {
                    $planning_data = PlanningNonMFG::find($actualing_data->planning_id);
                }

                if($planning_data)
                {
                    $estimation = $planning_data->estimation_hour;
                    $estimation_hour = $estimation;
                    $finish_time = strtotime($resource_used->start_used) + ($estimation * 3600);
                    $finish_date = date('Y-m-d H:i:s', $finish_time);

                    return $finish_date;
                }
            }
        }

        $estimation_hour = 0;

        return date('Y-m-d H:i:s');
    }

}

if (!function_exists('select_resource'))
{

    function select_resource($process_id, $resource_group_id, $type = '', $except_resource_id = array())
    {
        $resources = Resource::where('group_id','=',$resource_group_id)->get();
        $total_resource = count($resources);
        $available_resource = array();
        if(count($resources) > 0)
        {
            foreach ($resources as $resource)
            {
                $available_resource[$resource->id] = strtotime(last_date_resource_used($process_id, $type, $resource->id, $estimation));
            }
        }

        asort($available_resource);
        // return

        // if(count($available_resource) > 0)
        // {
        //     return $available_resource[mt_rand(0, count($available_resource))];
        // }
        // else
        // {
        //     $resources = Resource::where('group_id','=',$resource_group_id)->get();
        //     $resource_data = array();
        //     $estimation_data = array();
        //     if(count($resources) > 0)
        //     {
        //         foreach ($resources as $resource)
        //         {
        //             if(!in_array($resource->id, $except_resource_id))
        //             {
        //                 $resource_data[$resource->id] = strtotime(last_date_resource_used($process_id, $type, $resource->id, $estimation));
        //                 $estimation_data[$resource->id] = $estimation;
        //             }
        //         }
        //     }

        //     asort($resource_data);
        //     $finish_time = reset($resource_data);
        //     $finish_date = $finish_time ? date('Y-m-d H:i:s', $finish_time) : '';
        //     $selected_resource = key($resource_data);
        //     // $estimation_hour = $estimation_data[$selected_resource];

        //     // debug($resource_data, false);
        //     // debug($estimation_data, false);

        //     return $selected_resource;
        // }
    }

}

if (!function_exists('select_resource_group_randomly')) {

    function select_resource_group_randomly($process_id) {
        $resources = ProcessResourceGroup::select('resource_group_id AS rid')->where('process_id', '=', $process_id)->get();
        return count($resources) > 0 ? $resources[mt_rand(0, count($resources) - 1)]->rid : '';
    }

}

if (!function_exists('get_resource_ids_per_group')) {

    function get_resource_ids_per_group($group_id) {
        $resources = Resource::where('group_id', '=', $group_id)->get();
        $resource_ids = array();
        if(count($resources)>0) {
            foreach ($resources as $resource) {
                $resource_ids[] = $resource->id;
            }
        }
        return $resource_ids;
    }

}

if (!function_exists('select_resource_sequencially')) {

    function select_resource_sequencially($group_id) {
        $resources = get_resource_ids_per_group($group_id);
        $selected_resource = array_shift($resources);
        return $selected_resource;
    }

}

if(!function_exists('total_resource_per_group')) {
    function total_resource_per_group($group_id) {
        return Resource::where('group_id','=',$group_id)->count();
    }
}

if (!function_exists('select_resource_randomly')) {

    function select_resource_randomly($group_id, $type = '', $except_resource_id = array()) {
        $total_resource = total_resource_per_group($group_id);
        $planning_table = $type == 'material' ? 'project_planning_detail_mfg' : 'project_planning_detail_non_mfg';
        $query = DB::table('master_resource AS r')
                ->join('master_resource_group AS g', 'g.id', '=', 'r.group_id')
                ->join($planning_table . ' AS a', 'a.resource_group_id', '=', 'g.id')
                ->where('r.group_id', '=', $group_id)
                ->where('r.is_offline','<>','1');

        if(count($except_resource_id) > 0 && count($except_resource_id) < $total_resource) {
            $query->whereNotIn('r.id', count($except_resource_id) > 0 ? $except_resource_id : array(''));
        }

        $resources = $query->groupBy('r.id')->select('r.id AS id')->get();
        $available_resource = array();
        if(count($resources) > 0)
        {
            foreach ($resources as $resource)
            {
                $available_resource[] = $resource->id;
            }
        }

        return @$available_resource[0];
    }

}

if (!function_exists('get_available_resource_by_process')) {

    function get_available_resource_by_process($process_id, $start_date, $finish_date) {
        $used_resources = ResourceActualUsed::select('resource_id')
                ->where('process_id', '=', $process_id)
                ->whereBetween('start_used', array($start_date, $finish_date))
                ->whereBetween('finish_used', array($start_date, $finish_date));

        $id_not_in = array();
        if ($used_resources->count() > 0) {
            foreach ($used_resources->get() as $used_resource) {
                $id_not_in[] = $used_resource->resource_id;
            }
        }

        $available_resource = DB::table('process_resource AS pr')
                ->join('master_process AS mp', 'pr.process_id', '=', 'mp.id')
                ->join('master_resource AS mr', 'mr.id', '=', 'pr.resource_id')
                ->where('mp.group_id', '=', $process_group_id)
                ->groupBy('mr.name');

        if (count($id_not_in) > 0) {
            $available_resource->whereNotIn('mr.id', $id_not_in);
        }

        $resource = json_decode(json_encode($available_resource->get(array('mr.id', 'mr.code', 'mr.name', 'mr.pic_id'))), true);
        return $resource;
    }

}

if (!function_exists('get_available_resource_by_process_group')) {

    function get_available_resource_by_process_group($process_group_id, $start_date, $finish_date) {
        $used_resources = ResourceActualUsed::select('resource_id')
                ->where('process_group_id', '=', $process_group_id)
                ->whereBetween('start_used', array($start_date, $finish_date))
                ->whereBetween('finish_used', array($start_date, $finish_date));

        $id_not_in = array();
        if ($used_resources->count() > 0) {
            foreach ($used_resources->get() as $used_resource) {
                $id_not_in[] = $used_resource->resource_id;
            }
        }

        $available_resource = DB::table('process_resource AS pr')
                ->join('master_process AS mp', 'pr.process_id', '=', 'mp.id')
                ->join('master_resource AS mr', 'mr.id', '=', 'pr.resource_id')
                ->where('mp.group_id', '=', $process_group_id)
                ->groupBy('mr.name');

        if (count($id_not_in) > 0) {
            $available_resource->whereNotIn('mr.id', $id_not_in);
        }

        $resource = json_decode(json_encode($available_resource->get(array('mr.id', 'mr.code', 'mr.name', 'mr.pic_id'))), true);
        return $resource;
    }

}

if (!function_exists('is_resource_available')) {

    function is_resource_available($resource_id) {
        $resource_used = ResourceActualUsed::select('resource_id')->where('resource_id', '=', $resource_id)->take(1)->orderBy('id', 'desc')->get();

        if (count($resource_used) > 0) {
            return $resource_used[0]->is_used == 1 ? false : true;
        }

        return false;
    }

}

if (!function_exists('is_bom_planned')) {

    function is_bom_planned($material_id) {
        $is_planned = PlanningPBBOM::where('material_id', '=', $material_id)->count() > 0 ? true : false;
        return $is_planned;
    }

}

if (!function_exists('calculate_lags')) {

    function calculate_lags($start, $finish, $type) {
        $start_unix = strtotime($start);
        $finish_unix = strtotime($finish);
        $one_day = 24 * 60 * 60;
        $lags = 0;

        switch ($type) {
            case 'start-finish':
                $lags_in_seconds = ($finish_unix + $one_day) - $start_unix;
                $lags = intval($lags_in_seconds / $one_day);
                break;

            default:
            case 'finish-start':
                $lags_in_seconds = $start_unix - ($finish_unix + $one_day);
                $lags = $lags_in_seconds / $one_day;
                break;
        }

        return $lags <= 0 ? 0 : abs($lags);
    }

}

if (!function_exists('convert_to_array')) {

    function convert_to_array(array $priority_order) {
        $priority_array = array();

        if (count($priority_order) > 0) {
            foreach ($priority_order as $priority) {
                list($id, $priority_number) = explode(':', $priority);
                $priority_array[$id] = $priority_number;
            }
        }

        return $priority_array;
    }

}

if (!function_exists('is_incremental')) {

    function is_incremental(array $priority_order, $id, $new_priority) {
        foreach ($priority_order as $priority) {
            if ($priority_order[$id] > $new_priority) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

}

if (!function_exists('sort_priority_number')) {

    function sort_priority_number(array $priority_order, $new_priority, $is_incremental = TRUE) {
        $reorder_priority = array();
        $tmp_priority = $new_priority;
        if(count($priority_order) == 2) {
            $keys = array_keys($priority_order);
            $first = current($keys);
            $second = next($keys);
            $tmp = $priority_order[$first];
            $priority_order[$first]=$priority_order[$second];
            $priority_order[$second]=$tmp;
            $reorder_priority = $priority_order;
        } else {
            foreach ($priority_order as $planning_id => $priority_num) {
                if ($priority_num >= $new_priority) {
                    $tmp_priority++;
                    $reorder_priority[$planning_id] = $tmp_priority;
                } else {
                    $reorder_priority[$planning_id] = $priority_num;
                }
            }
        }

        return $reorder_priority;
    }

}

if (!function_exists('fix_priority_numbers')) {

    function fix_priority_numbers(array $priority_order) {
        $priority_number = 1;
        if (count($priority_order) > 0) {
            $new_priority_order = array();
            foreach ($priority_order as $id => $priority) {
                $new_priority_order[$id] = $priority_number;
                $priority_number++;
            }

            return $new_priority_order;
        }

        return $priority_order;
    }

}

if (!function_exists('schedule_score')) {

    function schedule_score($delivery_date_unix, $total_delivery) {
        $delivery_score = $total_delivery / (intval($delivery_date_unix) - time());
        return $delivery_score;
    }

}

if (!function_exists('is_material_planned')) {

    function is_material_planned($master_plan_id, $material_id) {
        $master_plan_data = ProjectMasterPlan::find($master_plan_id);
        $process_data = PlanningMFG::where('material_id', '=', $material_id);

        if($master_plan_data->parent_id > 0)
        {
            $process_data->where('process_group_id', '=', $master_plan_data->process_group_id);
        }
        else
        {
            $process_data->where('master_planning_id', '=', $master_plan_data->id);
        }
        // debug($material_id, false);
        // debug($process_data->get());
        return $process_data->count() > 0 ? true : false;
    }

}

if (!function_exists('update_work_plan')) {

    function update_work_plan($resource_id, $selected_year = false) {
        $current_year = intval(date('Y'));
        $current_month = intval(date('m'));
        $current_day = intval(date('d'));
        $resource_work_time = ResourceWorkTime::where('resource_id', '=', $resource_id)->get();
        $exception_days_data = ExceptionDays::where('year', '>=', $current_year)
                ->where('month', '>=', $current_month)->get();

        $exception_days = array();
        if (count($exception_days_data) > 0) {
            foreach ($exception_days_data as $exception_day) {
                $exception_days['year'][] = $exception_day->year;
                $exception_days['month'][] = $exception_day->month;
                $exception_days['day'][] = $exception_day->day;
            }
        }

        if($selected_year === false) {
            $active_year = GeneralOptions::getSingleOption('active_years');
            $last_year = $current_year + intval($active_year);
            for ($year = $current_year; $year <= $last_year; $year++) {
                _save_work_plan($year, $resource_work_time, $exception_days);
            }
        } else {
            _save_work_plan($selected_year, $resource_work_time, $exception_days);
        }

        return true;
    }

}

if (!function_exists('_save_work_plan')) {
    function _save_work_plan($year, $resource_work_time, $exception_days) {
        $months = generate_month();
        $no = 1;
        foreach ($months as $month) {
            $month_num = intval(date('m', strtotime($year . '-' . $month . '-1')));
            foreach (generate_days($month_num, $year) as $days_num) {
                $is_exception = in_array($year, $exception_days['year']) && in_array($month_num, $exception_days['month']) && in_array($days_num, $exception_days['day']);

                if (!$is_exception) {
                    $day_name = strtolower(date('D', strtotime("{$year}-{$month_num}-{$days_num}")));
                    $work_time = isset($resource_work_time[0]) ? $resource_work_time[0]->{$day_name} : 0;
                } else {
                    $work_time = 0;
                }

                $ddate = "$year-$month_num-$days_num";
                $date = new DateTime($ddate);

                $resource_plan = new ResourcePlanner;
                $resource_plan->resource_id = @$resource_work_time[0]->resource_id;
                $resource_plan->year = $year;
                $resource_plan->month = $month_num;
                $resource_plan->week_num = $date->format("W");
                $resource_plan->day_num = $days_num;
                $resource_plan->work_time = $work_time;
                $resource_plan->save();
            }
        }
    }
}

if (!function_exists('generate_month')) {

    function generate_month() {
        $months = array();

        foreach (range(1, 12) as $month) {
            $months[] = date('F', mktime(0, 0, 0, $month, 1, date('Y')));
        }

        return $months;
    }

}

if (!function_exists('generate_days')) {

    function generate_days($month, $year) {
        $day_num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $days = array();
        for ($i = 1; $i <= $day_num; $i++) {
            $days[] = $i;
        }

        return $days;
    }

}

if (!function_exists('search_key')) {

    function search_key($array, $sub_key, $sub_value) {
        foreach ($array as $key => $sub_array) {
            if ($sub_array[$sub_key] === $sub_value)
                return $key;
        }

        return false;
    }

}

if (!function_exists('sms_sender')) {

    function sms_sender($mobile_number, $message = '') {
        exec("expect -f ".__DIR__."/../tools/expect/send-sms.tcl ".$mobile_number." \"".$message."\"", $output);
        return count($output) > 0 ? $output : false;
    }

}

if (!function_exists('submit_email_alert_queues')) {

    function submit_email_alert_queues($from, $message) {
        Mail::queue(Config::get('app.theme').'.mails.main', array('content' => $message->content), function($msg) use ($from, $message)
        {
            $msg->from('alert@tmms.tridaya-as.com', 'TMMS Alert');
            $msg->to($from->email, $from->fullname)->subject($message->subject);
            Log::debug(json_encode($from));
        });
    }

}

if (!function_exists('submit_sms_alert_queues')) {

    function submit_sms_alert_queues($phone_number, $message) {
        $data = (Object) array('phone_number' => $phone_number, 'message'=> $message);
        Queue::push('SmsSender', $data);
    }

}

if (!function_exists('debug')) {

    function debug($input, $exit = TRUE) {
        print '<pre>';
        print_r($input);
        print '</pre>';

        if ($exit) {
            die();
        }
    }

}

if(!function_exists('array_depth')) {

    function array_depth(array $array) {
        $max_depth = 1;

        foreach ($array as $value) {
            if (is_array($value)) {
                $depth = array_depth($value) + 1;

                if ($depth > $max_depth) {
                    $max_depth = $depth;
                }
            }
        }

        return $max_depth;
    }
}
