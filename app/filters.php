<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

Route::filter('auth.apps', function()
{
	if ( ! Sentry::check())
	{
		return Redirect::route('auth.login');
	}
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/* - See more at: http://laravelsnippets.com/snippets/sentry-route-filters#sthash.TWR6aPJH.dpuf */
/*Route::filter('hasAccess', function($route, $request, $value)
{
	try
	{
		$user = Sentry::getUser();

		print_r($user);die;

		if( ! $user->hasAccess($value))
		{
			return Redirect::route('auth.login');
		}
	}
	catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
	{
		return Redirect::route('auth.login');
	}

});*/


/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function ()
{
	if (Session::token() != Input::get('_token')) {
		throw new Illuminate\Session\TokenMismatchException();
	}
});

/*
| -------------------------------------------------------------------------
| View Composer for an Application
| -------------------------------------------------------------------------
|
*/

$theme = Config::get('app.theme');

View::composer($theme.'._layouts.master', function ($view)
{
	$view->with('app_name', GeneralOptions::getSingleOption('app_name')['value']);
	$view->with('company_name', GeneralOptions::getSingleOption('company_name')['value']);
	$view->with('user_data', UserInformation::getFullname(Sentry::getUser()->id));
	$view->with('theme', Config::get('app.theme'));
});

View::composer($theme.'._layouts.master-popup', function ($view)
{
	$view->with('app_name', GeneralOptions::getSingleOption('app_name')['value']);
	$view->with('company_name', GeneralOptions::getSingleOption('company_name')['value']);
	$view->with('user_data', UserInformation::getFullname(Sentry::getUser()->id));
	$view->with('base_url', Request::root());
	$view->with('theme', Config::get('app.theme'));
});

View::composer($theme.'.auth.login', function ($view)
{
	$view->with('app_name', GeneralOptions::getSingleOption('app_name')['value']);
	$view->with('company_name', GeneralOptions::getSingleOption('company_name')['value']);
	$view->with('theme', Config::get('app.theme'));
});

/*
| End of file
|
*/